#ifndef UTILS_CLASS_HPP
#define UTILS_CLASS_HPP

namespace meta {

/**
 * @brief Inheritable class with disabled copy constructors.
 */
class INonCopyable {
   public:
    INonCopyable& operator=(const INonCopyable&) = delete;
    INonCopyable(const INonCopyable&) = delete;

    INonCopyable& operator=(INonCopyable&&) = default;
    INonCopyable(INonCopyable&&) = default;

    INonCopyable() = default;
    ~INonCopyable() = default;
};

}  // namespace meta

#endif /*UTILS_CLASS_HPP*/