#ifndef UTILS_HAMMING_WEIGHT
#define UTILS_HAMMING_WEIGHT

#include <stdint.h>

int hamming_weight_u8(uint8_t a);

int hamming_weight_u16(uint16_t a);

int hamming_weight_u32(uint32_t a);

int hamming_weight_u64(uint64_t a);

#endif /*UTILS_HAMMING_WEIGHT*/
