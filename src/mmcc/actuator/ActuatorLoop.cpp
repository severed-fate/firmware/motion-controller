#include "Actuator.hpp"
#include "TCodeImplActuator.hpp"
#include "UBJsonBuilder.hpp"
#include "io/GPIO.hpp"
#include "utils/PidController.hpp"

#include <Objects.hpp>
#include <TCodeConfig.hpp>

#include <cstdlib>

#include <boot.h>
#include <libopencm3/cm3/nvic.h>

namespace mmcc {

void Actuator::_configure_pid_ticker() {
    ACT_PID_TIMER::set_mode(stm32::timer::CkIntDiv::DIV1, stm32::timer::Alignment::Edge,
                            stm32::timer::Direction::Up);
    ACT_PID_TIMER::set_prescaler(96 - 1); /* 1Mhz */
    ACT_PID_TIMER::set_period(4000 - 1);  /* 250Hz */
    ACT_PID_TIMER::set_counter(0x0);
    ACT_PID_TIMER::set_interrupt(TIM_DIER_UIE, true);
    runtime_vector_table.irq[NVIC_TIM1_BRK_TIM9_IRQ] = __on_pid_loop_tick;
    // nvic_set_priority(NVIC_TIM1_BRK_TIM9_IRQ, 0x40);
    nvic_set_priority(NVIC_TIM1_BRK_TIM9_IRQ, 0x70);
    nvic_enable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
}

void Actuator::_set_pid_loop_enabled(bool enable) {
    if (enable) {
        auto cfg = mmcc::config.get_current_state();
        // Reset all state before enabling.
        _pid_loop_tick_counter = 0;
        _act0_engaged = false;
        _act1_engaged = false;
        _act2_engaged = false;
        _aux_engaged = false;
        _pid_loop_enable_plots = cfg->actuator.plot_enable;
        _act0_ctl = {};
        _act1_ctl = {};
        _act2_ctl = {};
        set_actuators_enabled(!cfg->actuator.act0_disengage_break,
                              !cfg->actuator.act1_disengage_break,
                              !cfg->actuator.act2_disengage_break);
        ACT_PID_TIMER::enable();
    }
    else {
        ACT_PID_TIMER::disable();
        // Disengage axes.
        _act0_engaged = false;
        _act1_engaged = false;
        _act2_engaged = false;
        _aux_engaged = false;
        // TODO: Stall flag testing.
        {
            InterruptGuard<true> sup;
            PINSET_RESET_ALL_CTX(HOME_ESTOP_LED, sup);
        }
    }
    // Send tcode events if host is connected.
    if (trfctl_task.is_connected()) {
        tcode::ParserDispatcher::EventQueueEntryT<void> ev0 = {
             &tcode::config::event_meta_pid_loop_status};
        tcode::ParserDispatcher::EventQueueEntryT<void> ev1 = {
             &tcode::config::event_meta_act0_engaged};
        tcode::ParserDispatcher::EventQueueEntryT<void> ev2 = {
             &tcode::config::event_meta_act1_engaged};
        tcode::ParserDispatcher::EventQueueEntryT<void> ev3 = {
             &tcode::config::event_meta_act2_engaged};
        tcode::ParserDispatcher::EventQueueEntryT<void> ev4 = {
             &tcode::config::event_meta_aux_engaged};
        trfctl_task.pend_event(ev0);
        trfctl_task.pend_event(ev1);
        trfctl_task.pend_event(ev2);
        trfctl_task.pend_event(ev3);
        trfctl_task.pend_event(ev4);
    }
}
bool Actuator::is_pid_loop_enabled() const {
    return ACT_PID_TIMER::is_enabled();
}

void Actuator::set_act0_engage(bool en) {
    if (is_pid_loop_enabled()) {
        if (en) {
            // Load pid parameters from config.
            auto cfg = mmcc::config.get_current_state();
            if (cfg->actuator.act0_disengage_break) {
                PINSET_SET_ALL(ACT0_CTL_EN_PINS);
            }
            if (cfg->actuator.home_enable) {
                _act0_ctl = algo::pid::Control(
                     cfg->actuator.act0.pid_kp, cfg->actuator.act0.pid_ki,
                     cfg->actuator.act0.pid_kd, 1.f / 250.f, -1.0f, 1.0f,
                     algo::pid::Mode::Automatic, algo::pid::Direction::Direct);
                _act0_ctl_bias = cfg->actuator.act0.pid_bias;
                _act0_ctl_deadband = cfg->actuator.act0.pwm_deadband;
                auto current_pos = get_actuator0_position_scaled();
                _act0_ctl.set_first_setpoint(current_pos);
                _act0_interp.setup(current_pos);
                _act0_interp.update(current_pos);
                /**
                 * Ensure that the above stores are not reordered
                 * by the compiler after the flag is set.
                 */
                asm volatile("" : : : "memory");
                _act0_engaged = true;
            }
        }
        else {
            _act0_engaged = false;
            auto cfg = mmcc::config.get_current_state();
            if (cfg->actuator.act0_disengage_break) {
                PINSET_RESET_ALL(ACT0_CTL_EN_PINS);
            }
        }
        if (trfctl_task.is_connected()) {
            tcode::ParserDispatcher::EventQueueEntryT<void> ev = {
                 &tcode::config::event_meta_act0_engaged};
            trfctl_task.pend_event(ev);
        }
    }
}
void Actuator::set_act1_engage(bool en) {
    if (is_pid_loop_enabled()) {
        if (en) {
            // Load pid parameters from config.
            auto cfg = mmcc::config.get_current_state();
            if (cfg->actuator.act1_disengage_break) {
                PINSET_SET_ALL(ACT1_CTL_EN_PINS);
            }
            if (cfg->actuator.home_enable) {
                _act1_ctl = algo::pid::Control(
                     cfg->actuator.act1.pid_kp, cfg->actuator.act1.pid_ki,
                     cfg->actuator.act1.pid_kd, 1.f / 250.f, -1.0f, 1.0f,
                     algo::pid::Mode::Automatic, algo::pid::Direction::Direct);
                _act1_ctl_bias = cfg->actuator.act1.pid_bias;
                _act1_ctl_deadband = cfg->actuator.act1.pwm_deadband;
                auto current_pos = get_actuator1_position_scaled();
                _act1_ctl.set_first_setpoint(current_pos);
                _act1_interp.update(current_pos);
                /**
                 * Ensure that the above stores are not reordered
                 * by the compiler after the flag is set.
                 */
                asm volatile("" : : : "memory");
                _act1_engaged = true;
            }
        }
        else {
            _act1_engaged = false;
            auto cfg = mmcc::config.get_current_state();
            if (cfg->actuator.act1_disengage_break) {
                PINSET_RESET_ALL(ACT1_CTL_EN_PINS);
            }
        }
        if (trfctl_task.is_connected()) {
            tcode::ParserDispatcher::EventQueueEntryT<void> ev = {
                 &tcode::config::event_meta_act1_engaged};
            trfctl_task.pend_event(ev);
        }
    }
}
void Actuator::set_act2_engage(bool en) {
    if (is_pid_loop_enabled()) {
        if (en) {
            // Load pid parameters from config.
            auto cfg = mmcc::config.get_current_state();
            if (cfg->actuator.act2_disengage_break) {
                PINSET_SET_ALL(ACT2_CTL_EN_PINS);
            }
            if (cfg->actuator.home_enable) {
                _act2_ctl = algo::pid::Control(
                     cfg->actuator.act2.pid_kp, cfg->actuator.act2.pid_ki,
                     cfg->actuator.act2.pid_kd, 1.f / 250.f, -1.0f, 1.0f,
                     algo::pid::Mode::Automatic, algo::pid::Direction::Direct);
                _act2_ctl_bias = cfg->actuator.act2.pid_bias;
                _act2_ctl_deadband = cfg->actuator.act2.pwm_deadband;
                auto current_pos = get_actuator2_position_scaled();
                _act2_ctl.set_first_setpoint(current_pos);
                _act2_interp.update(current_pos);
                /**
                 * Ensure that the above stores are not reordered
                 * by the compiler after the flag is set.
                 */
                asm volatile("" : : : "memory");
                _act2_engaged = true;
            }
        }
        else {
            _act2_engaged = false;
            auto cfg = mmcc::config.get_current_state();
            if (cfg->actuator.act2_disengage_break) {
                PINSET_RESET_ALL(ACT2_CTL_EN_PINS);
            }
        }
        if (trfctl_task.is_connected()) {
            tcode::ParserDispatcher::EventQueueEntryT<void> ev = {
                 &tcode::config::event_meta_act2_engaged};

            trfctl_task.pend_event(ev);
        }
    }
}
void Actuator::set_aux_engage(bool en) {
    if (is_pid_loop_enabled()) {
        if (en) {
            // Load tuning parameters from config.
            auto cfg = mmcc::config.get_current_state();
            /**
             * Ensure that the above stores are not reordered
             * by the compiler after the flag is set.
             */
            asm volatile("" : : : "memory");
            _aux_engaged = true;
        }
        else {
            _aux_engaged = false;
        }
        if (trfctl_task.is_connected()) {
            tcode::ParserDispatcher::EventQueueEntryT<void> ev = {
                 &tcode::config::event_meta_aux_engaged};

            trfctl_task.pend_event(ev);
        }
    }
}

void Actuator::actuate0(float stp) {
    if (_act0_engaged) {
        nvic_disable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
        _act0_interp.update(stp);
        nvic_enable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
    }
}
void Actuator::actuate0(float trgt, size_t duration_ms) {
    if (_act0_engaged) {
        nvic_disable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
        size_t duration_ticks = duration_ms / PID_LOOP_PERIOD_MS;
        _act0_interp.update(trgt, duration_ticks);
        nvic_enable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
    }
}
void Actuator::actuate1(float stp) {
    if (_act1_engaged) {
        nvic_disable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
        _act1_interp.update(stp);
        nvic_enable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
    }
}
void Actuator::actuate1(float trgt, size_t duration_ms) {
    if (_act1_engaged) {
        nvic_disable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
        size_t duration_ticks = duration_ms / PID_LOOP_PERIOD_MS;
        _act1_interp.update(trgt, duration_ticks);
        nvic_enable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
    }
}
void Actuator::actuate2(float stp) {
    if (_act2_engaged) {
        nvic_disable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
        _act2_interp.update(stp);
        nvic_enable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
    }
}
void Actuator::actuate2(float trgt, size_t duration_ms) {
    if (_act2_engaged) {
        nvic_disable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
        size_t duration_ticks = duration_ms / PID_LOOP_PERIOD_MS;
        _act2_interp.update(trgt, duration_ticks);
        nvic_enable_irq(NVIC_TIM1_BRK_TIM9_IRQ);
    }
}

constexpr float fract(float v) {
    return std::abs(v - (int32_t) v);
}

void Actuator::_on_pid_loop_tick() {
    _pid_loop_tick_counter++;
    const float time = (float) (_pid_loop_tick_counter) * (1.0f / 250.0f);
    bool global_stall_flag = false;
    if (_act0_engaged) {
        float current_pos = get_actuator0_position_scaled();
        float target_pos = _act0_interp.step();
        _act0_ctl.set_input(current_pos);
        _act0_ctl.set_setpoint(target_pos);
        float err = _act0_ctl.compute();
        float pwm = _act0_ctl.get_output();
        if (pwm < 0) {
            pwm -= _act0_ctl_bias;
        }
        else if (pwm > 0) {
            pwm += _act0_ctl_bias;
        }
        if (std::abs(pwm) <= _act0_ctl_deadband) {
            set_actuator0_pwm(0.0f);
        }
        else {
            set_actuator0_pwm(pwm);
        }
        if (_act0_ctl.get_stall_counter() > 0) {
            global_stall_flag = true;
        }
        if (_pid_loop_enable_plots && trfctl_task.is_connected()) {
            tcode::callbacks::ActPlotData plot_data = {
                 time, current_pos, _act0_ctl.get_setpoint(),
                 pwm,  err,         _act0_ctl.get_integral_term()};
            tcode::ParserDispatcher::EventQueueEntryT<tcode::callbacks::ActPlotData> ev = {
                 &tcode::config::event_meta_act0_plot, plot_data};
            trfctl_task.pend_event(ev);
        }
    }
    else {
        set_actuator0_pwm(0.0f);
    }
    if (_act1_engaged) {
        float current_pos = get_actuator1_position_scaled();
        float target_pos = _act1_interp.step();
        _act1_ctl.set_input(current_pos);
        _act1_ctl.set_setpoint(target_pos);
        float err = _act1_ctl.compute();
        float pwm = _act1_ctl.get_output();
        if (pwm < 0) {
            pwm -= _act1_ctl_bias;
        }
        else if (pwm > 0) {
            pwm += _act1_ctl_bias;
        }
        if (std::abs(pwm) <= _act1_ctl_deadband) {
            set_actuator1_pwm(0.0f);
        }
        else {
            set_actuator1_pwm(pwm);
        }
        if (_act1_ctl.get_stall_counter() > 0) {
            global_stall_flag = true;
        }
        if (_pid_loop_enable_plots && trfctl_task.is_connected()) {
            tcode::callbacks::ActPlotData plot_data = {
                 time, current_pos, _act1_ctl.get_setpoint(),
                 pwm,  err,         _act1_ctl.get_integral_term()};
            tcode::ParserDispatcher::EventQueueEntryT<tcode::callbacks::ActPlotData> ev = {
                 &tcode::config::event_meta_act1_plot, plot_data};
            trfctl_task.pend_event(ev);
        }
    }
    else {
        set_actuator1_pwm(0.0f);
    }
    if (_act2_engaged) {
        float current_pos = get_actuator2_position_scaled();
        float target_pos = _act2_interp.step();
        _act2_ctl.set_input(current_pos);
        _act2_ctl.set_setpoint(target_pos);
        float err = _act2_ctl.compute();
        float pwm = _act2_ctl.get_output();
        if (pwm < 0) {
            pwm -= _act2_ctl_bias;
        }
        else if (pwm > 0) {
            pwm += _act2_ctl_bias;
        }
        if (std::abs(pwm) <= _act2_ctl_deadband) {
            set_actuator2_pwm(0.0f);
        }
        else {
            set_actuator2_pwm(pwm);
        }
        if (_act2_ctl.get_stall_counter() > 0) {
            global_stall_flag = true;
        }
        if (_pid_loop_enable_plots && trfctl_task.is_connected()) {
            tcode::callbacks::ActPlotData plot_data = {
                 time, current_pos, _act2_ctl.get_setpoint(),
                 pwm,  err,         _act2_ctl.get_integral_term()};
            tcode::ParserDispatcher::EventQueueEntryT<tcode::callbacks::ActPlotData> ev = {
                 &tcode::config::event_meta_act2_plot, plot_data};
            trfctl_task.pend_event(ev);
        }
    }
    else {
        set_actuator2_pwm(0.0);
    }
    // Send global stall event as required.
    {
        InterruptGuard<true> sup;
        PINSET_WRITE_DYN_CTX(HOME_ESTOP_LED, sup, global_stall_flag ? 0x1 : 0x0);
    }
    // TODO: Aux pwm interpolation.
}

void Actuator::__on_pid_loop_tick() {
    if (ACT_PID_TIMER::get_interrupt_flags(TIM_SR_UIF) && !actuator_task._emergency_stopped)
         [[likely]] {
        actuator_task._on_pid_loop_tick();
    }
}

}  // namespace mmcc
