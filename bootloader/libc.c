#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

__attribute__((noreturn)) void __stack_chk_fail(void) {
    abort();
}
