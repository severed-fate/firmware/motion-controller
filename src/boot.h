#ifndef MMCC_BOOT
#define MMCC_BOOT

#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/vector.h>
#include <stddef.h>
#include <stdint.h>
#include <utils/macros.h>

#define nop __asm__("nop")
#define nop2                                                                                   \
    nop;                                                                                       \
    nop
#define nop3                                                                                   \
    nop2;                                                                                      \
    nop
#define nop4                                                                                   \
    nop3;                                                                                      \
    nop

C_DECLS_START

#include <bootloader.h>

// Forward declarations.
void mmcc_reset_handler(void);
void null_handler(void);

// Initialization functions.
void clock_setup(void);
void gpio_setup(void);
int main(void);

// Linker variables.
typedef void (*funcp_t)(void);
extern size_t _stack;
extern void* __sramtext_start;
extern void* __sramtext_end;
extern void* __sramtext_loadaddr;
extern void* __data_start;
extern void* __data_end;
extern void* __data_loadaddr;
extern void* __bss_start;
extern void* __bss_end;
#define __sramtext_size ((size_t) (((uintptr_t) &__sramtext_end) - ((uintptr_t) &__sramtext_start)))
#define __data_size ((size_t) (((uintptr_t) &__data_end) - ((uintptr_t) &__data_start)))
#define __bss_size ((size_t) (((uintptr_t) &__bss_end) - ((uintptr_t) &__bss_start)))
extern funcp_t __preinit_array_start, __preinit_array_end;
extern funcp_t __init_array_start, __init_array_end;
extern funcp_t __fini_array_start, __fini_array_end;

// Vector tables.
__attribute__((section(".boot_header"))) extern mmcc_bin_header mmcc_boot_header;

__attribute__((section(".vectors_dynamic"))) extern vector_table_t runtime_vector_table;

C_DECLS_END

#endif /*MMCC_BOOT*/
