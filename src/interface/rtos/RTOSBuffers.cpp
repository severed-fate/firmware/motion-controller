#include "RTOSBuffers.hpp"

namespace rtos {

bool StreamBuffer::init(size_t size, size_t trigger_level) {
    _handle = xStreamBufferCreate(size, trigger_level);
    return _handle != nullptr;
}

StreamBuffer::~StreamBuffer() {
    if (_handle != nullptr) {
        vStreamBufferDelete(_handle);
        _handle = nullptr;
    }
}

size_t StreamBuffer::send_ticks(const void* d, size_t l, TickType_t timeout_ticks) const {
    return xStreamBufferSend(_handle, d, l, timeout_ticks);
}

size_t StreamBuffer::send_from_isr(const void* d, size_t l) const {
    BaseType_t task_switch = 0;
    size_t read = xStreamBufferSendFromISR(_handle, d, l, &task_switch);
    if (task_switch) {
        taskYIELD();
    }
    return read;
}

size_t StreamBuffer::receive_ticks(void* d, size_t l, TickType_t timeout_ticks) const {
    return xStreamBufferReceive(_handle, d, l, timeout_ticks);
}

size_t StreamBuffer::receive_from_isr(void* d, size_t l) const {
    BaseType_t task_switch = 0;
    size_t read = xStreamBufferReceiveFromISR(_handle, d, l, &task_switch);
    if (task_switch) {
        taskYIELD();
    }
    return read;
}

size_t StreamBuffer::bytes_available() const {
    return xStreamBufferBytesAvailable(_handle);
}

size_t StreamBuffer::spaces_available() const {
    return xStreamBufferSpacesAvailable(_handle);
}

bool StreamBuffer::set_trigger_level(size_t trigger_level) const {
    return xStreamBufferSetTriggerLevel(_handle, trigger_level) == pdTRUE;
}

bool StreamBuffer::reset() const {
    return xStreamBufferReset(_handle) == pdPASS;
}

bool StreamBuffer::is_empty() const {
    return xStreamBufferIsEmpty(_handle) == pdTRUE;
}

bool StreamBuffer::is_full() const {
    return xStreamBufferIsFull(_handle) == pdTRUE;
}

bool MessageBuffer::init(size_t size) {
    _handle = xMessageBufferCreate(size);
    return _handle != nullptr;
}

MessageBuffer::~MessageBuffer() {
    if (_handle != nullptr) {
        vMessageBufferDelete(_handle);
        _handle = nullptr;
    }
}

size_t MessageBuffer::send_ticks(const void* d, size_t l, TickType_t timeout_ticks) const {
    return xMessageBufferSend(_handle, d, l, timeout_ticks);
}

size_t MessageBuffer::send_from_isr(const void* d, size_t l) const {
    BaseType_t task_switch = 0;
    size_t read = xMessageBufferSendFromISR(_handle, d, l, &task_switch);
    if (task_switch) {
        taskYIELD();
    }
    return read;
}

size_t MessageBuffer::receive_ticks(void* d, size_t l, TickType_t timeout_ticks) const {
    return xMessageBufferReceive(_handle, d, l, timeout_ticks);
}

size_t MessageBuffer::receive_from_isr(void* d, size_t l) const {
    BaseType_t task_switch = 0;
    size_t read = xMessageBufferReceiveFromISR(_handle, d, l, &task_switch);
    if (task_switch) {
        taskYIELD();
    }
    return read;
}

size_t MessageBuffer::spaces_available() const {
    return xMessageBufferSpacesAvailable(_handle);
}

bool MessageBuffer::reset() const {
    return xMessageBufferReset(_handle) == pdPASS;
}

bool MessageBuffer::is_empty() const {
    return xMessageBufferIsEmpty(_handle) == pdTRUE;
}

bool MessageBuffer::is_full() const {
    return xMessageBufferIsFull(_handle) == pdTRUE;
}

}  // namespace rtos
