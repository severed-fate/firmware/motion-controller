//*********************************************************************************
// Arduino PID Library Version 1.0.1 Modified Version for C++
// Platform Independent
//
// Revision: 1.1
//
// Description: The PID Controller module originally meant for Arduino made
// platform independent. Some small bugs present in the original Arduino source
// have been rectified as well.
//
// For a detailed explanation of the theory behind this library, go to:
// http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/
//
// Revisions can be found here:
// https://github.com/tcleg
//
// Modified by: Trent Cleghorn , <trentoncleghorn@gmail.com>
//
// Copyright (C) Brett Beauregard , <br3ttb@gmail.com>
//
//                                 GPLv3 License
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
//*********************************************************************************

//
// Header Guard
//
#ifndef UTILS_PID_CONTROLLER_HPP
#define UTILS_PID_CONTROLLER_HPP

//*********************************************************************************
// Headers
//*********************************************************************************
#include <stdbool.h>
#include <stdint.h>

namespace algo::pid {

//*********************************************************************************
// Macros and Globals
//*********************************************************************************

enum class Mode { Manual, Automatic };

enum class Direction { Direct, Reverse };

//*********************************************************************************
// Class
//*********************************************************************************

class Control {
   public:
    //
    // Constructor
    // Description:
    //      Initializes the PIDControl instantiation. This should be called at
    //      least once before any other PID functions are called on the
    //      instantiation.
    // Parameters:
    //      kp - Positive P gain constant value.
    //      ki - Positive I gain constant value.
    //      kd - Positive D gain constant value.
    //      sampleTimeSeconds - Interval in seconds on which PIDCompute will be
    //          called.
    //      minOutput - Constrain PID output to this minimum value.
    //      maxOutput - Constrain PID output to this maximum value.
    //      mode - Tells how the controller should respond if the user has
    //          taken over manual control or not.
    //          MANUAL:    PID controller is off. User can manually control the
    //                     output.
    //          AUTOMATIC: PID controller is on. PID controller controls the
    //                     output.
    //      controllerDirection - The sense of direction of the controller
    //          DIRECT:  A positive setpoint gives a positive output.
    //          REVERSE: A positive setpoint gives a negative output.
    // Returns:
    //      Nothing.
    //
    constexpr Control() = default;
    Control(float kp, float ki, float kd, float sampleTimeSeconds, float minOutput,
            float maxOutput, Mode mode, Direction controllerDirection);

    //
    // PID Compute
    // Description:
    //      Should be called on a regular interval defined by sampleTimeSeconds.
    //      Typically, PIDSetpointSet and PIDInputSet should be called
    //      immediately before PIDCompute.
    // Parameters:
    //      None.
    // Returns:
    //      The error variable.
    //
    float compute();

    //
    // PID Mode Set
    // Description:
    //      Sets the PID controller to a new mode. Tells how the controller
    //      should respond if the user has taken over manual control or not.
    // Parameters:
    //      mode -
    //          MANUAL:    PID controller is off. User can manually control the
    //                     output.
    //          AUTOMATIC: PID controller is on. PID controller controls the
    //                     output.
    // Returns:
    //      Nothing.
    //
    void set_mode(Mode mode);

    //
    // PID Output Limits Set
    // Description:
    //      Sets the new output limits. The new limits are applied to the PID
    //      immediately.
    // Parameters:
    //      min - Constrain PID output to this minimum value.
    //      max - Constrain PID output to this maximum value.
    // Returns:
    //      Nothing.
    //
    void set_output_limits(float min, float max);

    //
    // PID Tunings Set
    // Description:
    //      Sets the new gain constant values.
    // Parameters:
    //      kp - Positive P gain constant value.
    //      ki - Positive I gain constant value.
    //      kd - Positive D gain constant value.
    // Returns:
    //      Nothing.
    //
    void set_tunings(float kp, float ki, float kd);

    //
    // PID Tuning Gain Constant P Set
    // Description:
    //      Sets the proportional gain constant value.
    // Parameters:
    //      kp - Positive P gain constant value.
    // Returns:
    //      Nothing.
    //
    void set_tuning_kp(float kp);

    //
    // PID Tuning Gain Constant I Set
    // Description:
    //      Sets the proportional gain constant value.
    // Parameters:
    //      ki - Positive I gain constant value.
    // Returns:
    //      Nothing.
    //
    void set_tuning_ki(float ki);

    //
    // PID Tuning Gain Constant D Set
    // Description:
    //      Sets the proportional gain constant value.
    // Parameters:
    //      kd - Positive D gain constant value.
    // Returns:
    //      Nothing.
    //
    void set_tuning_kd(float kd);

    //
    // PID Controller Direction Set
    // Description:
    //      Sets the new controller direction.
    // Parameters:
    //      controllerDirection - The sense of direction of the controller
    //          DIRECT:  A positive setpoint gives a positive output
    //          REVERSE: A positive setpoint gives a negative output
    // Returns:
    //      Nothing.
    //
    void set_controller_direction(Direction controllerDirection);

    //
    // PID Sample Time Set
    // Description:
    //      Sets the new sampling time (in seconds).
    // Parameters:
    //      sampleTimeSeconds - Interval in seconds on which PIDCompute will be
    //          called.
    // Returns:
    //      Nothing.
    //
    void set_sample_time(float sampleTimeSeconds);

    //
    // PID Setpoint Set
    // Description:
    //      Alters the setpoint the PID controller will try to achieve.
    // Parameters:
    //      setpoint - The desired setpoint the PID controller will try to
    //          obtain.
    // Returns:
    //      Nothing.
    //
    void set_setpoint(float setpoint);
    void set_first_setpoint(float setpoint);
    inline float get_setpoint() const {
        return _setpoint;
    }

    //
    // PID Input Set
    // Description:
    //      Should be called before calling PIDCompute so the PID controller
    //      will have an updated input value to work with.
    // Parameters:
    //      input - The value the controller will work with.
    // Returns:
    //      Nothing.
    //
    inline void set_input(float input) {
        _input = input;
    }

    //
    // PID Output Get
    // Description:
    //      Typically, this function is called after PIDCompute in order to
    //      retrieve the output of the controller.
    // Parameters:
    //      None.
    // Returns:
    //      The output of the specific PID controller.
    //
    inline float get_output() {
        return _output;
    }

    //
    // PID Proportional Gain Constant Get
    // Description:
    //      Returns the proportional gain constant value the particular
    //      controller is set to.
    // Parameters:
    //      None.
    // Returns:
    //      The proportional gain constant.
    //
    inline float get_kp() {
        return _disp_kp;
    }

    //
    // PID Integral Gain Constant Get
    // Description:
    //      Returns the integral gain constant value the particular
    //      controller is set to.
    // Parameters:
    //      None.
    // Returns:
    //      The integral gain constant.
    //
    inline float get_ki() {
        return _disp_ki;
    }

    //
    // PID Derivative Gain Constant Get
    // Description:
    //      Returns the derivative gain constant value the particular
    //      controller is set to.
    // Parameters:
    //      None.
    // Returns:
    //      The derivative gain constant.
    //
    inline float get_kd() {
        return _disp_kd;
    }

    inline float get_integral_term() const {
        return _i_term;
    }
    inline uint32_t get_stall_counter() const {
        return _stall_counter;
    }

    //
    // PID Mode Get
    // Description:
    //      Returns the mode the particular controller is set to.
    // Parameters:
    //      None.
    // Returns:
    //      MANUAL or AUTOMATIC depending on what the user set the
    //      controller to.
    //
    inline Mode get_mode() {
        return _mode;
    }

    //
    // PID Direction Get
    // Description:
    //      Returns the direction the particular controller is set to.
    // Parameters:
    //      None.
    // Returns:
    //      DIRECT or REVERSE depending on what the user set the
    //      controller to.
    //
    inline Direction get_direction() {
        return _controller_direction;
    }

   private:
    //
    // Input to the PID Controller
    //
    float _input = 0.0f;

    //
    // Previous input to the PID Controller
    //
    float _last_input = 0.0f;
    static constexpr float _DINPUT_SMOOTHING = 0.68;
    float _last_dinput = 0.0f;

    //
    // Output of the PID Controller
    //
    float _output = 0.0f;

    //
    // Gain constant values that were passed by the user
    // These are for display purposes
    //
    float _disp_kp = 0.0f;
    float _disp_ki = 0.0f;
    float _disp_kd = 0.0f;

    //
    // Gain constant values that the controller alters for
    // its own use
    //
    float _altered_kp = 0.0f;
    float _altered_ki = 0.0f;
    float _altered_kd = 0.0f;

    //
    // The Integral Term
    //
    float _i_term = 0.0f;
    static constexpr float I_TERM_FAST_UNWIND_FACTOR = 4.f;
    uint32_t _stall_counter = 0;

    //
    // The interval (in seconds) on which the PID controller
    // will be called
    //
    float _sample_time = 0.0f;

    //
    // The values that the output will be constrained to
    //
    float _out_min = 0.0f;
    float _out_max = 0.0f;

    //
    // The user chosen operating point
    //
    float _setpoint = 0.0f;

    //
    // The sense of direction of the controller
    // DIRECT:  A positive setpoint gives a positive output
    // REVERSE: A positive setpoint gives a negative output
    //
    Direction _controller_direction = Direction::Direct;

    //
    // Tells how the controller should respond if the user has
    // taken over manual control or not
    // MANUAL:    PID controller is off.
    // AUTOMATIC: PID controller is on.
    //
    Mode _mode = Mode::Manual;
};

}  // namespace algo::pid

#endif  // UTILS_PID_CONTROLLER_HPP
