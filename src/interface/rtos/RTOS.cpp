#include "RTOS.hpp"

#include <interface/io/Timer.hpp>

#include <cstdlib>

void _rtos_bindings_get_idle_task_memory(StaticTask_t** idle_task_tcb_buffer,
                                         StackType_t** idle_task_stack_buffer,
                                         uint32_t* idle_task_stack_size) {
    rtos::Kernel::get_idle_task_memory(idle_task_tcb_buffer, idle_task_stack_buffer,
                                       idle_task_stack_size);
}
void _rtos_bindings_pre_sleep_processing(void* arg) {
    auto& expected_idle_time = *((TickType_t*) arg);
    rtos::Kernel::on_pre_sleep_processing(expected_idle_time);
}
void _rtos_bindings_post_sleep_processing(void* arg) {
    auto& expected_idle_time = *((TickType_t*) arg);
    rtos::Kernel::on_post_sleep_processing(expected_idle_time);
}
void _rtos_bindings_on_idle(void) {
    rtos::Kernel::on_idle();
}

namespace rtos {

void Kernel::start() {
    on_start();
    vTaskStartScheduler();
    on_end();
}
void Kernel::end() {
    vTaskEndScheduler();
}

void Kernel::suspend_all() {
    vTaskSuspendAll();
}
bool Kernel::resume_all() {
    return xTaskResumeAll() != pdFALSE;
}
void Kernel::step_tick(TickType_t ticks) {
    vTaskStepTick(ticks);
}
bool Kernel::catch_up_ticks(TickType_t ticks) {
    return xTaskCatchUpTicks(ticks) != pdFALSE;
}

TickType_t Kernel::get_tickcount() {
    if (is_inside_interrupt()) {
        return xTaskGetTickCountFromISR();
    }
    else {
        return xTaskGetTickCount();
    }
}
Kernel::State Kernel::get_scheduler_state() {
    return (Kernel::State) xTaskGetSchedulerState();
}
UBaseType_t Kernel::get_number_of_tasks() {
    return uxTaskGetNumberOfTasks();
}

void Kernel::yield() {
    taskYIELD();
}
void Kernel::enter_critical() {
    taskENTER_CRITICAL();
}
void Kernel::exit_critical() {
    taskEXIT_CRITICAL();
}
uint32_t Kernel::enter_critical_from_isr() {
    return taskENTER_CRITICAL_FROM_ISR();
}
void Kernel::exit_critical_from_isr(uint32_t mask) {
    taskEXIT_CRITICAL_FROM_ISR(mask);
}
void Kernel::disable_interrupts() {
    taskDISABLE_INTERRUPTS();
}
void Kernel::enable_interrupts() {
    taskENABLE_INTERRUPTS();
}

}  // namespace rtos
