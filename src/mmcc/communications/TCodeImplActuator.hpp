#ifndef MMCC_TCODEIMPLACTUATOR_HPP
#define MMCC_TCODEIMPLACTUATOR_HPP

#include <TCodeConfig.hpp>

namespace tcode::callbacks {

struct ActPlotData {
    float time;
    float pos;
    float target;
    float power;
    float error;
    float integral_term;
};

void meta_pwm_prescaler(codec::UBJsonObject&);
void meta_homing_speed(codec::UBJsonObject&);
void meta_tune_pid_kp(codec::UBJsonObject&);
void meta_tune_pid_ki(codec::UBJsonObject&);
void meta_tune_pid_kd(codec::UBJsonObject&);
void meta_tune_pid_bias(codec::UBJsonObject&);
void meta_tune_pwm_bias(codec::UBJsonObject&);
void meta_tune_pwm_deadband(codec::UBJsonObject&);
void meta_axis_limit_min(codec::UBJsonObject&);
void meta_axis_limit_max(codec::UBJsonObject&);
void meta_act_plot(codec::UBJsonObject&);

response::Error get_pid_loop_status(const void*, size_t);
response::Error set_pid_loop_status(void* dat, size_t n);
response::Error get_homing_enable(const void*, size_t);
response::Error set_homing_enable(void* dat, size_t n);
response::Error get_pid_tick_count(const void*, size_t);
response::Error get_pwm_prescaler(const void*, size_t);
response::Error set_pwm_prescaler(void* dat, size_t n);

response::Error get_plot_enable(const void*, size_t);
response::Error set_plot_enable(void* dat, size_t n);
response::Error get_act0_plot(const void*, size_t);
response::Error get_act1_plot(const void*, size_t);
response::Error get_act2_plot(const void*, size_t);

response::Error act0_normal_update(fractional<uint32_t> value);
response::Error act0_interval_update(fractional<uint32_t> value,
                                     request::IntervalData interval);
// response::Error act0_speed_update(fractional<uint32_t> value, request::SpeedData speed);
response::Error act0_stop();
response::Error act0_get_engaged(const void*, size_t);
response::Error act0_set_engaged(void* dat, size_t n);
response::Error act0_get_engaged_break(const void*, size_t);
response::Error act0_set_engaged_break(void* dat, size_t n);
response::Error act0_get_raw_pos(const void*, size_t);
response::Error act0_get_scaled_pos(const void*, size_t);
response::Error act0_get_homing_speed(const void*, size_t);
response::Error act0_set_homing_speed(void* dat, size_t n);
response::Error act0_get_tune_pid_kp(const void*, size_t);
response::Error act0_set_tune_pid_kp(void* dat, size_t n);
response::Error act0_get_tune_pid_ki(const void*, size_t);
response::Error act0_set_tune_pid_ki(void* dat, size_t n);
response::Error act0_get_tune_pid_kd(const void*, size_t);
response::Error act0_set_tune_pid_kd(void* dat, size_t n);
response::Error act0_get_tune_pid_bias(const void*, size_t);
response::Error act0_set_tune_pid_bias(void* dat, size_t n);
response::Error act0_get_tune_pwm_deadband(const void*, size_t);
response::Error act0_set_tune_pwm_deadband(void* dat, size_t n);
response::Error act0_get_axis_limit_min(const void*, size_t);
response::Error act0_set_axis_limit_min(void* dat, size_t n);
response::Error act0_get_axis_limit_max(const void*, size_t);
response::Error act0_set_axis_limit_max(void* dat, size_t n);

response::Error act1_normal_update(fractional<uint32_t> value);
response::Error act1_interval_update(fractional<uint32_t> value,
                                     request::IntervalData interval);
// response::Error act1_speed_update(fractional<uint32_t> value, request::SpeedData speed);
response::Error act1_stop();
response::Error act1_get_engaged(const void*, size_t);
response::Error act1_set_engaged(void* dat, size_t n);
response::Error act1_get_engaged_break(const void*, size_t);  //
response::Error act1_set_engaged_break(void* dat, size_t n);  //
response::Error act1_get_raw_pos(const void*, size_t);
response::Error act1_get_scaled_pos(const void*, size_t);
response::Error act1_get_homing_speed(const void*, size_t);
response::Error act1_set_homing_speed(void* dat, size_t n);
response::Error act1_get_tune_pid_kp(const void*, size_t);
response::Error act1_set_tune_pid_kp(void* dat, size_t n);
response::Error act1_get_tune_pid_ki(const void*, size_t);
response::Error act1_set_tune_pid_ki(void* dat, size_t n);
response::Error act1_get_tune_pid_kd(const void*, size_t);
response::Error act1_set_tune_pid_kd(void* dat, size_t n);
response::Error act1_get_tune_pid_bias(const void*, size_t);
response::Error act1_set_tune_pid_bias(void* dat, size_t n);
response::Error act1_get_tune_pwm_deadband(const void*, size_t);
response::Error act1_set_tune_pwm_deadband(void* dat, size_t n);
response::Error act1_get_axis_limit_min(const void*, size_t);
response::Error act1_set_axis_limit_min(void* dat, size_t n);
response::Error act1_get_axis_limit_max(const void*, size_t);
response::Error act1_set_axis_limit_max(void* dat, size_t n);

response::Error act2_normal_update(fractional<uint32_t> value);
response::Error act2_interval_update(fractional<uint32_t> value,
                                     request::IntervalData interval);
// response::Error act2_speed_update(fractional<uint32_t> value, request::SpeedData speed);
response::Error act2_stop();
response::Error act2_get_engaged(const void*, size_t);
response::Error act2_set_engaged(void* dat, size_t n);
response::Error act2_get_engaged_break(const void*, size_t);  //
response::Error act2_set_engaged_break(void* dat, size_t n);  //
response::Error act2_get_raw_pos(const void*, size_t);
response::Error act2_get_scaled_pos(const void*, size_t);
response::Error act2_get_homing_speed(const void*, size_t);
response::Error act2_set_homing_speed(void* dat, size_t n);
response::Error act2_get_tune_pid_kp(const void*, size_t);
response::Error act2_set_tune_pid_kp(void* dat, size_t n);
response::Error act2_get_tune_pid_ki(const void*, size_t);
response::Error act2_set_tune_pid_ki(void* dat, size_t n);
response::Error act2_get_tune_pid_kd(const void*, size_t);
response::Error act2_set_tune_pid_kd(void* dat, size_t n);
response::Error act2_get_tune_pid_bias(const void*, size_t);
response::Error act2_set_tune_pid_bias(void* dat, size_t n);
response::Error act2_get_tune_pwm_deadband(const void*, size_t);
response::Error act2_set_tune_pwm_deadband(void* dat, size_t n);
response::Error act2_get_axis_limit_min(const void*, size_t);
response::Error act2_set_axis_limit_min(void* dat, size_t n);
response::Error act2_get_axis_limit_max(const void*, size_t);
response::Error act2_set_axis_limit_max(void* dat, size_t n);

response::Error aux_get_engaged(const void*, size_t);
response::Error aux_set_engaged(void* dat, size_t n);

response::Error aux1_normal_update(fractional<uint32_t> value);
// response::Error aux1_interval_update(fractional<uint32_t> value,
//                                      request::IntervalData interval);
// response::Error aux1_speed_update(fractional<uint32_t> value, request::SpeedData speed);
response::Error aux1_stop();
response::Error aux1_get_tune_pwm_bias(const void*, size_t);
response::Error aux1_set_tune_pwm_bias(void* dat, size_t n);
response::Error aux1_get_tune_pwm_deadband(const void*, size_t);
response::Error aux1_set_tune_pwm_deadband(void* dat, size_t n);
response::Error aux1_get_axis_limit_min(const void*, size_t);
response::Error aux1_set_axis_limit_min(void* dat, size_t n);
response::Error aux1_get_axis_limit_max(const void*, size_t);
response::Error aux1_set_axis_limit_max(void* dat, size_t n);

response::Error aux2_normal_update(fractional<uint32_t> value);
// response::Error aux2_interval_update(fractional<uint32_t> value,
//                                      request::IntervalData interval);
// response::Error aux2_speed_update(fractional<uint32_t> value, request::SpeedData speed);
response::Error aux2_stop();
response::Error aux2_get_tune_pwm_bias(const void*, size_t);
response::Error aux2_set_tune_pwm_bias(void* dat, size_t n);
response::Error aux2_get_tune_pwm_deadband(const void*, size_t);
response::Error aux2_set_tune_pwm_deadband(void* dat, size_t n);
response::Error aux2_get_axis_limit_min(const void*, size_t);
response::Error aux2_set_axis_limit_min(void* dat, size_t n);
response::Error aux2_get_axis_limit_max(const void*, size_t);
response::Error aux2_set_axis_limit_max(void* dat, size_t n);

}  // namespace tcode::callbacks

#endif /*MMCC_TCODEIMPLACTUATOR_HPP*/
