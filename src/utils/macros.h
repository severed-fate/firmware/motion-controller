/**
 * C/C++ syntax shortcuts.
 */
#ifndef C_DECLS_START
  #ifdef __cplusplus
    /**
     * Should be used before the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_START extern "C" {
  #else
    /**
     * Should be used before the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_START
  #endif
#endif
#ifndef C_DECLS_END
  #ifdef __cplusplus
    /**
     * Should be used after the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_END }
  #else
    /**
     * Should be used after the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_END
  #endif
#endif

#ifndef cold_func_attr
  #define cold_func_attr __attribute__((cold, section(".cold")))
#endif

#ifndef abort_func_attr
  #define abort_func_attr __attribute__((noreturn, cold, section(".cold")))
#endif

#ifndef sram_func_attr
    #define sram_func_attr __attribute__((minsize, section(".sramtext")))
#endif
