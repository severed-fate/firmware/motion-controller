#ifndef MMCC_GPIO_HPP
#define MMCC_GPIO_HPP

#include <interface/io/Core.hpp>
#include <utils/Array.hpp>

#include <array>
#include <cstddef>
#include <cstdint>
#include <limits>

#include <etl/bitset.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>

namespace stm32 {

namespace __PinSet_common {

enum class Direction : uint8_t {
    Input = GPIO_MODE_INPUT,
    Output = GPIO_MODE_OUTPUT,
    AlternateFunction = GPIO_MODE_AF,
    Analog = GPIO_MODE_ANALOG
};
enum class Speed : uint8_t {
    Low = GPIO_OSPEED_2MHZ,
    Medium = GPIO_OSPEED_25MHZ,
    High = GPIO_OSPEED_50MHZ,
    VeryHigh = GPIO_OSPEED_100MHZ
};

/** Mask-Value pair with special constexpr-ness. */
template<typename T> requires(std::is_unsigned_v<T>)
struct mask_value_t {
    typedef T type;
    typedef T first_type;
    typedef T second_type;

    T mask;
    T value;

    constexpr mask_value_t() = default;
    constexpr mask_value_t(const T& x, const T& y) : mask(x), value(y) {}
    template<class U, class V> requires(std::is_integral_v<U> && std::is_integral_v<V>)
    constexpr mask_value_t(U&& x, V&& y) : mask(std::forward(x)), value(std::forward(y)) {}

    template<class U> requires(std::is_unsigned_v<U>)
    constexpr mask_value_t(const mask_value_t<U>& mv) : mask(mv.mask), value(mv.value) {}
    template<class U> requires(std::is_unsigned_v<U>)
    constexpr mask_value_t(mask_value_t<U>&& mv) :
        mask(std::move(mv.mask)), value(std::move(mv.value)) {}

    template<class U, class V> requires(std::is_integral_v<U> && std::is_integral_v<V>)
    constexpr mask_value_t(const std::pair<U, V>& p) :
        mask(std::forward(p.first)), value(std::forward(p.second)) {}
    template<class U, class V> requires(std::is_integral_v<U> && std::is_integral_v<V>)
    constexpr mask_value_t(std::pair<U, V>&& p) :
        mask(std::move(p.first)), value(std::move(p.second)) {}

    // template<class... Args1, class... Args2>
    // constexpr mask_value_t(std::piecewise_construct_t, std::tuple<Args1...> first_args,
    //                        std::tuple<Args2...> second_args);

    constexpr mask_value_t(const mask_value_t&) = default;
    constexpr mask_value_t(mask_value_t&&) = default;

    constexpr mask_value_t& operator=(const mask_value_t& p) = default;
    constexpr mask_value_t& operator=(mask_value_t&& p) noexcept(
         std::is_nothrow_move_assignable_v<T>) = default;
    template<class U> requires(std::is_unsigned_v<T>)
    constexpr mask_value_t& operator=(const mask_value_t<U>& mv) {
        mask = mv.mask;
        value = mv.value;
        return *this;
    }
    template<class U> requires(std::is_unsigned_v<T>)
    constexpr mask_value_t& operator=(mask_value_t<U>&& mv) {
        mask = std::move(mv.mask);
        value = std::move(mv.value);
        return *this;
    }
};

}  // namespace __PinSet_common

struct Pin {
   public:
    uint8_t port : 4;
    uint8_t pin : 4;

   private:
    constexpr Pin(const char port_, const int pin_) noexcept : port(port_ - 'A'), pin(pin_) {}

   public:
    /* Default constructor */
    constexpr Pin() : port(0xf), pin(0) {}

    template<char port_, int pin_> static constexpr Pin make() {
        // Verify gpio port:pin existance.
        static_assert(port_ >= 'A' && port_ <= 'K', "Invalid range for parameter 'gpio port'!");
        static_assert(pin_ >= 0 && pin_ < 16, "Invalid range for parameter 'gpio pin'!");
        // TODO: Part specific bounds.
        return Pin(port_, pin_);
    }

    constexpr uint32_t get_port_address() const {
        switch (port) {
            case 0x0: return GPIOA;
            case 0x1: return GPIOB;
            case 0x2: return GPIOC;
            case 0x3: return GPIOD;
            case 0x4: return GPIOE;
            case 0x5: return GPIOF;
            case 0x6: return GPIOG;
            case 0x7: return GPIOH;
            case 0x8: return GPIOI;
            case 0x9: return GPIOJ;
            case 0xa: return GPIOK;
            default: return std::numeric_limits<uint32_t>::max();
        }
    }

    constexpr rcc_periph_clken get_port_clen() const {
        switch (port) {
            case 0x00: return RCC_GPIOA;
            case 0x01: return RCC_GPIOB;
            case 0x02: return RCC_GPIOC;
            case 0x03: return RCC_GPIOD;
            case 0x04: return RCC_GPIOE;
            case 0x05: return RCC_GPIOF;
            case 0x06: return RCC_GPIOG;
            case 0x07: return RCC_GPIOH;
            case 0x08: return RCC_GPIOI;
            case 0x09: return RCC_GPIOJ;
            case 0x10: return RCC_GPIOK;
            default: return rcc_periph_clken(0);
        }
    }

    constexpr rcc_periph_rst get_port_rst() const {
        switch (port) {
            case 0x00: return RST_GPIOA;
            case 0x01: return RST_GPIOB;
            case 0x02: return RST_GPIOC;
            case 0x03: return RST_GPIOD;
            case 0x04: return RST_GPIOE;
            case 0x05: return RST_GPIOF;
            case 0x06: return RST_GPIOG;
            case 0x07: return RST_GPIOH;
            case 0x08: return RST_GPIOI;
            case 0x09: return RST_GPIOJ;
            case 0x10: return RST_GPIOK;
            default: return rcc_periph_rst(0);
        }
    }

    template<int stride> requires(stride == 1)
    constexpr uint16_t get_pin_mask() const {
        return static_cast<uint16_t>(0b1) << pin;
    }
    template<int stride> requires(stride == 2)
    constexpr uint32_t get_pin_mask() const {
        return static_cast<uint32_t>(0b11) << (2 * pin);
    }
    template<int stride> requires(stride == 4)
    constexpr uint64_t get_pin_mask() const {
        return static_cast<uint64_t>(0b1111) << (4 * pin);
    }

    template<int stride> requires(stride == 1)
    constexpr uint16_t get_pin_mask(uint16_t bit_value) const {
        return static_cast<uint16_t>(bit_value & 0b1) << pin;
    }
    template<int stride> requires(stride == 2)
    constexpr uint32_t get_pin_mask(uint16_t bit_value) const {
        return static_cast<uint32_t>(bit_value & 0b11) << (2 * pin);
    }
    template<int stride> requires(stride == 4)
    constexpr uint64_t get_pin_mask(uint16_t bit_value) const {
        return static_cast<uint64_t>(bit_value & 0b1111) << (4 * pin);
    }

    constexpr bool operator==(Pin rhs) const {
        return port == rhs.port && pin == rhs.pin;
    }
    constexpr bool operator!=(Pin rhs) const {
        return !(*this == rhs);
    }
};

template<size_t C>
inline consteval bool __make_test_for_duplicates(const std::array<Pin, C> pins) {
    for (size_t i = 0; i < C; i++) {
        for (size_t j = 0; j < C; j++) {
            if (i != j && pins[i] == pins[j]) {
                return true;
            }
        }
    }
    return false;
}

template<size_t N = 0> class PinSet {
    static_assert(N >= 0);

   public:
    using Direction = __PinSet_common::Direction;
    using Speed = __PinSet_common::Speed;
    template<typename T> using mask_value_t = __PinSet_common::mask_value_t<T>;
    using structural_pin_mask_t = std::array<bool, N>;
    using packed_pin_mask_t = etl::bitset<N>;

    /* Pin mask utilities. */
    static consteval structural_pin_mask_t make_structural_pin_mask(bool init_value = false) {
        structural_pin_mask_t spm;
        for (size_t i = 0; i < N; i++) {
            spm[i] = init_value;
        }
        return spm;
    }
    static consteval packed_pin_mask_t make_packed_pin_mask(bool init_value = false) {
        packed_pin_mask_t ppm;
        if (init_value) {
            ppm.set();
        }
        else {
            ppm.reset();
        }
        return ppm;
    }

   protected:
    const std::array<Pin, N> _pins;

   protected:
    template<size_t M> friend class PinSet;

    constexpr PinSet(std::array<Pin, N> pins) : _pins(pins) {}

    template<size_t M = N - 1>
    constexpr PinSet(typename std::enable_if_t<(M == N - 1) && M != 0, PinSet<M>> o, Pin p) :
        _pins(meta::concat(o._pins, std::array<Pin, 1>{p})) {}

    template<size_t M = N - 1>
    constexpr PinSet(typename std::enable_if_t<(M == N - 1) && M == 0, Pin> p) : _pins{p} {}

   public:
    template<Pin... _pins, size_t C = sizeof...(_pins)> static constexpr PinSet<C> make() {
        constexpr std::array<Pin, C> pins = {_pins...};
        // Confirm that there aren't any duplicate pins.
        static_assert(!__make_test_for_duplicates(pins),
                      "Cannot make PinSet with duplicate pins!");

        // Construct final PinSet object.
        return PinSet<C>(pins);
    }

    template<size_t M1, std::array<Pin, M1> a_pins, size_t M2, std::array<Pin, M2> b_pins>
    requires((M1 + M2) == N)
    static consteval PinSet<M1 + M2> concat() {
        constexpr auto pins = meta::concat(a_pins, b_pins);
        // Confirm that there aren't any duplicate pins.
        static_assert(!__make_test_for_duplicates(pins),
                      "Cannot make PinSet with duplicate pins!");
        return PinSet(pins);
    }

   public:
    constexpr bool has_pin(Pin p) const {
        for (size_t i = 0; i < N; i++) {
            if (_pins[i] == p) {
                return true;
            }
        }
        return false;
    }
    constexpr Pin operator[](size_t i) const {
        return _pins[i];
    }
    consteval uint32_t get_pin_count() const {
        return _pins.size();
    }
    constexpr auto get_pins() const {
        return _pins;
    }
    consteval uint8_t get_port_count() const {
        // Count of unique ports.
        bool mask[N];
        for (size_t i = 0; i < N; i++) {
            mask[i] = false;
        }
        uint8_t c = 0;
        for (size_t i = 0; i < N; i++) {
            if (!mask[i]) {
                // New unique port found.
                c += 1;
                for (size_t j = i; j < N; j++) {
                    if (_pins[i].port == _pins[j].port) {
                        mask[j] = true;
                    }
                }
            }
        }
        return c;
    }

    template<size_t port_count>
    consteval std::array<uint32_t, port_count> get_port_addresses() const {
        // For unique port, record its address.
        std::array<uint32_t, port_count> port_addrs;

        bool mask[N];
        for (size_t i = 0; i < N; i++) {
            mask[i] = false;
        }

        uint8_t c = 0;
        for (size_t i = 0; i < N; i++) {
            if (!mask[i]) {
                // New unique port found.
                port_addrs.at(c) = _pins[i].get_port_address();
                c += 1;
                for (size_t j = i; j < N; j++) {
                    if (_pins[i].port == _pins[j].port) {
                        mask[j] = true;
                    }
                }
            }
        }

        return port_addrs;
    }

    // TODO: Pin filtering/spliting (PinSetMask parameter).
    // TODO: enable/disable ports.

    template<size_t port_count, std::array<uint32_t, port_count> ports, Direction mode>
    consteval auto make_direction_mask_values() const {
        return make_2bit_mask_values<port_count, ports>(static_cast<uint8_t>(mode));
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint32_t>, port_count> direction_mask_values, bool _igs>
    void apply_direction([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = direction_mask_values[p].mask;
            uint32_t value = direction_mask_values[p].value;
            uint32_t mode_reg = GPIO_MODER(port);
            mode_reg &= ~mask;
            mode_reg |= value;
            GPIO_MODER(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint32_t>, port_count> direction_mask_values>
    void apply_direction() const {
        mmcc::InterruptGuard ctx;
        apply_direction<port_count, ports, direction_mask_values>(ctx);
    }

    template<size_t port_count, std::array<uint32_t, port_count> ports, bool open_drain>
    consteval auto make_output_type_mask_values() const {
        return make_1bit_mask_values<port_count, ports>(open_drain);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint16_t>, port_count> otype_mask_values, bool _igs>
    void apply_output_type([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = otype_mask_values[p].mask;
            uint32_t value = otype_mask_values[p].value;
            uint32_t mode_reg = GPIO_OTYPER(port);
            mode_reg &= ~mask;
            mode_reg |= value;
            GPIO_OTYPER(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint16_t>, port_count> otype_mask_values>
    void apply_output_type() const {
        mmcc::InterruptGuard ctx;
        apply_output_type<port_count, ports, otype_mask_values>(ctx);
    }

    template<size_t port_count, std::array<uint32_t, port_count> ports, uint8_t alt_func_idx>
    requires(alt_func_idx >= 0 && alt_func_idx < 16)
    consteval auto make_alternate_function_mask_values() const {
        return make_4bit_mask_values<port_count, ports>(alt_func_idx);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint64_t>, port_count> alt_func_mask_values, bool _igs>
    void apply_alternate_function(
         [[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask_low = alt_func_mask_values[p].mask & 0xffffffff;
            uint32_t mask_high = (alt_func_mask_values[p].mask >> 32) & 0xffffffff;
            uint32_t value_low = alt_func_mask_values[p].value & 0xffffffff;
            uint32_t value_high = (alt_func_mask_values[p].value >> 32) & 0xffffffff;

            if (mask_low != 0) {
                uint32_t mode_reg_low = GPIO_AFRL(port);
                mode_reg_low &= ~mask_low;
                mode_reg_low |= value_low;
                GPIO_AFRL(port) = mode_reg_low;
            }

            if (mask_high != 0) {
                uint32_t mode_reg_high = GPIO_AFRH(port);
                mode_reg_high &= ~mask_high;
                mode_reg_high |= value_high;
                GPIO_AFRH(port) = mode_reg_high;
            }
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint64_t>, port_count> alt_func_mask_values>
    void apply_alternate_function() const {
        mmcc::InterruptGuard ctx;
        apply_alternate_function<port_count, ports, alt_func_mask_values>(ctx);
    }

    template<size_t port_count, std::array<uint32_t, port_count> ports, Speed mode>
    consteval std::array<mask_value_t<uint32_t>, port_count> make_speed_mask_values() const {
        return make_2bit_mask_values<port_count, ports>(static_cast<uint8_t>(mode));
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint32_t>, port_count> speed_mask_values, bool _igs>
    void apply_speed([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = speed_mask_values[p].mask;
            uint32_t value = speed_mask_values[p].value;
            // Load.
            uint32_t mode_reg = GPIO_OSPEEDR(port);
            // Clear bits.
            mode_reg &= ~mask;
            // Apply new values.
            mode_reg |= value;
            // Store.
            GPIO_OSPEEDR(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint32_t>, port_count> mask_values>
    void apply_speed() const {
        mmcc::InterruptGuard ctx;
        apply_speed<port_count, ports, mask_values>(ctx);
    }

    /* Pull up/down resistors */
    template<size_t port_count, std::array<uint32_t, port_count> ports, bool up, bool down>
    consteval std::array<mask_value_t<uint32_t>, port_count> make_pullres_mask_values() const {
        uint8_t mode_bits = 0;
        if (up) {
            mode_bits |= GPIO_PUPD_PULLUP;
        }
        if (down) {
            mode_bits |= GPIO_PUPD_PULLDOWN;
        }
        return make_2bit_mask_values<port_count, ports>(mode_bits);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint32_t>, port_count> pullres_mask_values, bool _igs>
    void apply_pullres([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = pullres_mask_values[p].mask;
            uint32_t value = pullres_mask_values[p].value;
            uint32_t mode_reg = GPIO_PUPDR(port);
            mode_reg &= ~mask;
            mode_reg |= value;
            GPIO_PUPDR(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint32_t>, port_count> pullres_mask_values>
    void apply_pullres() const {
        mmcc::InterruptGuard ctx;
        apply_pullres<port_count, ports, pullres_mask_values>(ctx);
    }

    template<size_t port_count, std::array<uint32_t, port_count> ports>
    static constexpr size_t get_port_index_for_pin(Pin pin) {
        for (size_t p = 0; p < port_count; p++) {
            // If port matches,
            if (pin.get_port_address() == ports[p]) {
                return p;
            }
        }
        __builtin_unreachable();
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    consteval auto get_port_index_for_pins() const {
        std::array<size_t, N> indexes;
        for (size_t i = 0; i < N; i++) {
            indexes[i] = get_port_index_for_pin<port_count, ports>(_pins[i]);
        }
        return indexes;
    }

    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<size_t, N> port_indexes, bool _igs>
    bool test_all([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        auto pin_state = read<port_count, ports, port_indexes, _igs>(_ctx);
        return pin_state.all();
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<size_t, N> port_indexes>
    bool test_all() const {
        mmcc::InterruptGuard ctx;
        return test_all<port_count, ports, port_indexes>(ctx);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<size_t, N> port_indexes, bool _igs>
    bool test_any([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        auto pin_state = read<port_count, ports, port_indexes, _igs>(_ctx);
        return pin_state.any();
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<size_t, N> port_indexes>
    bool test_any() const {
        mmcc::InterruptGuard ctx;
        return test_any<port_count, ports, port_indexes>(ctx);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<size_t, N> port_indexes, bool _igs>
    bool test_none([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        auto pin_state = read<port_count, ports, port_indexes, _igs>(_ctx);
        return pin_state.none();
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<size_t, N> port_indexes>
    bool test_none() const {
        mmcc::InterruptGuard ctx;
        return test_none<port_count, ports, port_indexes>(ctx);
    }

    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<size_t, N> port_indexes, bool _igs>
    packed_pin_mask_t read([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        std::array<uint16_t, port_count> input_data_regs;
        for (size_t p = 0; p < port_count; p++) {
            input_data_regs[p] = GPIO_IDR(ports[p]);
        }
        packed_pin_mask_t m;
        for (size_t i = 0; i < N; i++) {
            uint16_t input_data_reg = input_data_regs[port_indexes[i]];
            uint16_t pin_mask = _pins[i].template get_pin_mask<1>();
            bool state = !!(input_data_reg & pin_mask);
            m[i] = state;
        }
        return m;
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<size_t, N> port_indexes>
    packed_pin_mask_t read() const {
        mmcc::InterruptGuard ctx;
        return read<port_count, ports, port_indexes>(ctx);
    }

    /* Static writes */
    template<size_t port_count, std::array<uint32_t, port_count> ports, const bool state>
    consteval auto make_write_mask_values() const {
        return make_1bit_mask_values<port_count, ports>(state);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             const structural_pin_mask_t states>
    consteval auto make_write_mask_values() const {
        std::array<mask_value_t<uint16_t>, port_count> mvs;
        for (size_t p = 0; p < port_count; p++) {
            auto port = ports[p];
            uint16_t m = 0;
            uint16_t v = 0;
            for (size_t i = 0; i < N; i++) {
                // If port matches,
                if (_pins[i].get_port_address() == port) {
                    auto curr_pin_mask = _pins[i].template get_pin_mask<1>();
                    // append to bitmask.
                    m |= curr_pin_mask;
                    // Copy state for current pin.
                    if (states[i]) {
                        v |= curr_pin_mask;
                    }
                }
            }
            mvs[p] = {m, v};
        }
        return mvs;
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint16_t>, port_count> write_mask_values, bool _igs>
    void write([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = write_mask_values[p].mask;
            uint32_t value = write_mask_values[p].value;
            uint32_t mode_reg = GPIO_ODR(port);
            mode_reg &= ~mask;
            mode_reg |= value;
            GPIO_ODR(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<mask_value_t<uint16_t>, port_count> write_mask_values>
    void write() const {
        mmcc::InterruptGuard ctx;
        write<port_count, ports, write_mask_values>(ctx);
    }
    /* Dynamic writes */
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    consteval auto make_write_masks() const {
        std::array<uint16_t, port_count> masks;
        for (size_t p = 0; p < port_count; p++) {
            masks[p] = make_1bit_mask_value(ports[p], false).mask;
        }
        return masks;
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<uint16_t, port_count> write_masks, bool _igs>
    void write([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx,
               packed_pin_mask_t states) const {
        std::array<uint16_t, port_count> write_values;
        // For each port x pin:
        for (size_t p = 0; p < port_count; p++) {
            uint16_t v = 0;
            for (size_t i = 0; i < N; i++) {
                // If pin is to be set and port matches:
                if (states.test(i) && _pins[i].get_port_address() == ports[p]) {
                    v |= _pins[i].template get_pin_mask<1>();
                }
            }
            write_values[p] = v;
        }

        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = write_masks[p];
            uint32_t value = write_values[p];
            uint32_t mode_reg = GPIO_ODR(port);
            mode_reg &= ~mask;
            mode_reg |= value;
            GPIO_ODR(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<uint16_t, port_count> write_masks>
    void write(packed_pin_mask_t states) const {
        mmcc::InterruptGuard ctx;
        write<port_count, ports, write_masks>(ctx, states);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<uint16_t, port_count> write_masks, bool _igs>
    void write([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx,
               structural_pin_mask_t states) const {
        std::array<uint16_t, port_count> write_values;
        // For each port x pin:
        for (size_t p = 0; p < port_count; p++) {
            uint16_t v = 0;
            for (size_t i = 0; i < N; i++) {
                // If pin is to be set and port matches:
                if (states[i] && _pins[i].get_port_address() == ports[p]) {
                    v |= _pins[i].template get_pin_mask<1>();
                }
            }
            write_values[p] = v;
        }

        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = write_masks[p];
            uint32_t value = write_values[p];
            uint32_t mode_reg = GPIO_ODR(port);
            mode_reg &= ~mask;
            mode_reg |= value;
            GPIO_ODR(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<uint16_t, port_count> write_masks>
    void write(structural_pin_mask_t states) const {
        mmcc::InterruptGuard ctx;
        write<port_count, ports, write_masks>(ctx, states);
    }

    /* Static toggles */
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    consteval auto make_toggle_masks() const {
        // Reuse mask maker, as it is the same target register.
        return make_write_masks<port_count, ports>();
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             const structural_pin_mask_t pin_mask>
    consteval auto make_toggle_masks() const {
        std::array<uint16_t, port_count> masks;
        // For each port x pin:
        for (size_t p = 0; p < port_count; p++) {
            uint16_t m = 0;
            for (size_t i = 0; i < N; i++) {
                // If pin is to be set and port matches:
                if (pin_mask[i] && _pins[i].get_port_address() == ports[p]) {
                    m |= _pins[i].template get_pin_mask<1>();
                }
            }
            masks[p] = m;
        }
        return masks;
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<uint16_t, port_count> toggle_masks, bool _igs>
    void toggle([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = toggle_masks[p];
            if (mask) {
                uint32_t mode_reg = GPIO_ODR(port);
                mode_reg ^= mask;
                GPIO_ODR(port) = mode_reg;
            }
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<uint16_t, port_count> toggle_masks>
    void toggle() const {
        mmcc::InterruptGuard ctx;
        toggle<port_count, ports, toggle_masks>(ctx);
    }

    /* Dynamic toggles */
    template<size_t port_count, std::array<uint32_t, port_count> ports, bool _igs>
    void toggle([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx,
                packed_pin_mask_t pin_mask) const {
        std::array<uint16_t, port_count> toggle_masks;
        // For each port x pin:
        for (size_t p = 0; p < port_count; p++) {
            uint16_t m = 0;
            for (size_t i = 0; i < N; i++) {
                // If pin is to be set and port matches:
                if (pin_mask.test(i) && _pins[i].get_port_address() == ports[p]) {
                    m |= _pins[i].template get_pin_mask<1>();
                }
            }
            toggle_masks[p] = m;
        }
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = toggle_masks[p];
            uint32_t mode_reg = GPIO_ODR(port);
            mode_reg ^= mask;
            GPIO_ODR(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    void toggle(packed_pin_mask_t pin_mask) const {
        mmcc::InterruptGuard ctx;
        toggle<port_count, ports>(ctx, pin_mask);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports, bool _igs>
    void toggle([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx,
                structural_pin_mask_t pin_mask) const {
        std::array<uint16_t, port_count> toggle_masks;
        // For each port x pin:
        for (size_t p = 0; p < port_count; p++) {
            uint16_t m = 0;
            for (size_t i = 0; i < N; i++) {
                // If pin is to be set and port matches:
                if (pin_mask[i] && _pins[i].get_port_address() == ports[p]) {
                    m |= _pins[i].template get_pin_mask<1>();
                }
            }
            toggle_masks[p] = m;
        }
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = toggle_masks[p];
            uint32_t mode_reg = GPIO_ODR(port);
            mode_reg ^= mask;
            GPIO_ODR(port) = mode_reg;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    void toggle(structural_pin_mask_t pin_mask) const {
        mmcc::InterruptGuard ctx;
        toggle<port_count, ports>(ctx, pin_mask);
    }

    /* Static set/reset */
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             structural_pin_mask_t set_mask, structural_pin_mask_t reset_mask>
    consteval auto make_set_reset_masks() const {
        std::array<uint32_t, port_count> masks;
        // For each port x pin:
        for (size_t p = 0; p < port_count; p++) {
            uint16_t sm = 0;
            uint16_t rm = 0;
            for (size_t i = 0; i < N; i++) {
                // If port matches:
                if (_pins[i].get_port_address() == ports[p]) {
                    // and if pin is to be set:
                    if (set_mask[i]) {
                        sm |= _pins[i].template get_pin_mask<1>();
                    }
                    // or if pin is to be reset/cleared:
                    if (reset_mask[i]) {
                        rm |= _pins[i].template get_pin_mask<1>();
                    }
                }
            }
            masks[p] = ((uint32_t) sm) | (((uint32_t) rm) << 16);
        }
        return masks;
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<uint32_t, port_count> sr_masks, bool _igs>
    void set_reset([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx) const {
        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = sr_masks[p];
            if (mask) {
                GPIO_BSRR(port) = mask;
            }
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports,
             std::array<uint32_t, port_count> sr_masks>
    void set_reset() const {
        mmcc::InterruptGuard ctx;
        set_reset<port_count, ports, sr_masks>(ctx);
    }

    /* Dynamic set/reset */
    template<size_t port_count, std::array<uint32_t, port_count> ports, bool _igs>
    void set_reset([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx,
                   packed_pin_mask_t set_mask, packed_pin_mask_t reset_mask) const {
        std::array<uint32_t, port_count> sr_masks;
        // For each port x pin:
        for (size_t p = 0; p < port_count; p++) {
            uint16_t sm = 0;
            uint16_t rm = 0;
            for (size_t i = 0; i < N; i++) {
                // If port matches:
                if (_pins[i].get_port_address() == ports[p]) {
                    // and if pin is to be set:
                    if (set_mask.test(i)) {
                        sm |= _pins[i].template get_pin_mask<1>();
                    }
                    // or if pin is to be reset/cleared:
                    if (reset_mask.test(i)) {
                        rm |= _pins[i].template get_pin_mask<1>();
                    }
                }
            }
            sr_masks[p] = ((uint32_t) sm) | (((uint32_t) rm) << 16);
        }

        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = sr_masks[p];
            GPIO_BSRR(port) = mask;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    void set_reset(packed_pin_mask_t set_mask, packed_pin_mask_t reset_mask) const {
        mmcc::InterruptGuard ctx;
        set_reset<port_count, ports>(ctx, set_mask, reset_mask);
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports, bool _igs>
    void set_reset([[maybe_unused]] const mmcc::InterruptGuard<_igs>& _ctx,
                   structural_pin_mask_t set_mask, structural_pin_mask_t reset_mask) const {
        std::array<uint32_t, port_count> sr_masks;
        // For each port x pin:
        for (size_t p = 0; p < port_count; p++) {
            uint16_t sm = 0;
            uint16_t rm = 0;
            for (size_t i = 0; i < N; i++) {
                // If port matches:
                if (_pins[i].get_port_address() == ports[p]) {
                    // and if pin is to be set:
                    if (set_mask[i]) {
                        sm |= _pins[i].template get_pin_mask<1>();
                    }
                    // or if pin is to be reset/cleared:
                    if (reset_mask[i]) {
                        rm |= _pins[i].template get_pin_mask<1>();
                    }
                }
            }
            sr_masks[p] = ((uint32_t) sm) | (((uint32_t) rm) << 16);
        }

        // For each port:
        for (size_t p = 0; p < port_count; p++) {
            uint32_t port = ports[p];
            uint32_t mask = sr_masks[p];
            GPIO_BSRR(port) = mask;
        }
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    void set_reset(structural_pin_mask_t set_mask, structural_pin_mask_t reset_mask) const {
        mmcc::InterruptGuard ctx;
        set_reset<port_count, ports>(ctx, set_mask, reset_mask);
    }

   protected:
    /**
     * \p ebv stands for elemental bit value.
     * \returns a [mask, value] pair.
     */
    constexpr mask_value_t<uint16_t> make_1bit_mask_value(uint32_t port, bool ebv) const {
        uint16_t m = 0;
        for (size_t i = 0; i < N; i++) {
            // If port matches,
            if (_pins[i].get_port_address() == port) {
                // append to bitmask.
                m |= _pins[i].template get_pin_mask<1>();
            }
        }
        return {m, ebv ? m : 0};
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    consteval auto make_1bit_mask_values(bool ebv) const {
        std::array<mask_value_t<uint16_t>, port_count> mvs;
        for (size_t p = 0; p < port_count; p++) {
            mvs[p] = make_1bit_mask_value(ports[p], ebv);
        }
        return mvs;
    }

    /**
     * \returns a [mask, value] pair.
     */
    constexpr mask_value_t<uint32_t> make_2bit_mask_value(uint32_t port, uint8_t bits) const {
        uint32_t m = 0;
        uint32_t v = 0;
        for (size_t i = 0; i < N; i++) {
            // If port matches,
            if (_pins[i].get_port_address() == port) {
                // append to bitmask
                m |= _pins[i].template get_pin_mask<2>();
                // and value.
                v |= _pins[i].template get_pin_mask<2>(bits);
            }
        }
        return {m, v};
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    consteval auto make_2bit_mask_values(uint8_t bits) const {
        std::array<mask_value_t<uint32_t>, port_count> mvs;
        for (size_t p = 0; p < port_count; p++) {
            mvs[p] = make_2bit_mask_value(ports[p], bits);
        }
        return mvs;
    }

    /**
     * \returns a tuple with [mask, value].
     */
    constexpr mask_value_t<uint64_t> make_4bit_mask_value(uint32_t port, uint8_t bits) const {
        uint64_t m = 0;
        uint64_t v = 0;
        for (size_t i = 0; i < N; i++) {
            // If port matches,
            if (_pins[i].get_port_address() == port) {
                // append to bitmask
                m |= _pins[i].template get_pin_mask<4>();
                // and value.
                v |= _pins[i].template get_pin_mask<4>(bits);
            }
        }
        return {m, v};
    }
    template<size_t port_count, std::array<uint32_t, port_count> ports>
    consteval auto make_4bit_mask_values(uint8_t bits) const {
        std::array<mask_value_t<uint64_t>, port_count> mvs;
        for (size_t p = 0; p < port_count; p++) {
            mvs[p] = make_4bit_mask_value(ports[p], bits);
        }
        return mvs;
    }
};

}  // namespace stm32

#define PINSET_CONCAT(A, B)                                                                    \
    stm32::PinSet<A.get_pin_count() + B.get_pin_count()>::concat<                              \
         A.get_pin_count(), A.get_pins(), B.get_pin_count(), B.get_pins()>()

#define PINSET_CONFIGURE_CTX(pinset, ctx, direction)                                           \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }
#define PINSET_CONFIGURE(pinset, direction)                                                    \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        pinset.apply_direction<port_count, ports, direction_mask_values>();                    \
    }

#define PINSET_CONFIGURE_OUTPUT_TYPE_CTX(pinset, ctx, open_drain)                              \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
    }
#define PINSET_CONFIGURE_OUTPUT_TYPE(pinset, open_drain)                                       \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
        pinset.apply_output_type<port_count, ports, outtype_masks>();                          \
    }

#define PINSET_CONFIGURE_ALTERNATE_FUNCTION_INDEX_CTX(pinset, ctx, index)                      \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto alt_func_mask_values =                                           \
             pinset.make_alternate_function_mask_values<port_count, ports, index>();           \
        pinset.apply_alternate_function<port_count, ports, alt_func_mask_values>(ctx);         \
    }
#define PINSET_CONFIGURE_ALTERNATE_FUNCTION_INDEX(pinset, index)                               \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto alt_func_mask_values =                                           \
             pinset.make_alternate_function_mask_values<port_count, ports, index>();           \
        pinset.apply_alternate_function<port_count, ports, alt_func_mask_values>();            \
    }

#define PINSET_CONFIGURE_SPEED_CTX(pinset, ctx, speed)                                         \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
    }
#define PINSET_CONFIGURE_SPEED(pinset, speed)                                                  \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        pinset.apply_speed<port_count, ports, speed_mask_values>();                            \
    }

#define PINSET_CONFIGURE_PUD_CTX(pinset, ctx, pull_up, pull_down)                              \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
    }
#define PINSET_CONFIGURE_PUD(pinset, pull_up, pull_down)                                       \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>();                        \
    }

#define PINSET_CONFIGURE_INPUT_CTX(pinset, ctx, pull_up, pull_down)                            \
    {                                                                                          \
        static constexpr auto direction = stm32::PinSet<>::Direction::Input;                   \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
                                                                                               \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }
#define PINSET_CONFIGURE_INPUT(pinset, pull_up, pull_down)                                     \
    {                                                                                          \
        mmcc::InterruptGuard ctx;                                                              \
        static constexpr auto direction = stm32::PinSet<>::Direction::Input;                   \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
                                                                                               \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }

#define PINSET_CONFIGURE_OUTPUT_CTX(pinset, ctx, speed, open_drain)                            \
    {                                                                                          \
        static constexpr auto direction = stm32::PinSet<>::Direction::Output;                  \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
                                                                                               \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }
#define PINSET_CONFIGURE_OUTPUT(pinset, speed, open_drain)                                     \
    {                                                                                          \
        mmcc::InterruptGuard ctx;                                                              \
        static constexpr auto direction = stm32::PinSet<>::Direction::Output;                  \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
                                                                                               \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }

#define PINSET_CONFIGURE_OUTPUT_CTX_PUD(pinset, ctx, speed, open_drain, pull_up, pull_down)    \
    {                                                                                          \
        static constexpr auto direction = stm32::PinSet<>::Direction::Output;                  \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
                                                                                               \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }
#define PINSET_CONFIGURE_OUTPUT_PUD(pinset, speed, open_drain, pull_up, pull_down)             \
    {                                                                                          \
        mmcc::InterruptGuard ctx;                                                              \
        static constexpr auto direction = stm32::PinSet<>::Direction::Output;                  \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
                                                                                               \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }

#define PINSET_CONFIGURE_ALTERNATE_INPUT_CTX_PUD(pinset, ctx, index, pull_up, pull_down)       \
    {                                                                                          \
        static constexpr auto direction = stm32::PinSet<>::Direction::AlternateFunction;       \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto alt_func_mask_values =                                           \
             pinset.make_alternate_function_mask_values<port_count, ports, index>();           \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
                                                                                               \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
        pinset.apply_alternate_function<port_count, ports, alt_func_mask_values>(ctx);         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }
#define PINSET_CONFIGURE_ALTERNATE_INPUT_PUD(pinset, index, pull_up, pull_down)                \
    {                                                                                          \
        mmcc::InterruptGuard ctx;                                                              \
        static constexpr auto direction = stm32::PinSet<>::Direction::AlternateFunction;       \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto alt_func_mask_values =                                           \
             pinset.make_alternate_function_mask_values<port_count, ports, index>();           \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
                                                                                               \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
        pinset.apply_alternate_function<port_count, ports, alt_func_mask_values>(ctx);         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }

#define PINSET_CONFIGURE_ALTERNATE_OUTPUT_CTX(pinset, ctx, speed, open_drain, index)           \
    {                                                                                          \
        static constexpr auto direction = stm32::PinSet<>::Direction::AlternateFunction;       \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
        static constexpr auto alt_func_mask_values =                                           \
             pinset.make_alternate_function_mask_values<port_count, ports, index>();           \
                                                                                               \
        pinset.apply_alternate_function<port_count, ports, alt_func_mask_values>(ctx);         \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }
#define PINSET_CONFIGURE_ALTERNATE_OUTPUT(pinset, speed, open_drain, index)                    \
    {                                                                                          \
        mmcc::InterruptGuard ctx;                                                              \
        static constexpr auto direction = stm32::PinSet<>::Direction::AlternateFunction;       \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
        static constexpr auto alt_func_mask_values =                                           \
             pinset.make_alternate_function_mask_values<port_count, ports, index>();           \
                                                                                               \
        pinset.apply_alternate_function<port_count, ports, alt_func_mask_values>(ctx);         \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }

#define PINSET_CONFIGURE_ALTERNATE_OUTPUT_CTX_PUD(pinset, ctx, speed, open_drain, index,       \
                                                  pull_up, pull_down)                          \
    {                                                                                          \
        static constexpr auto direction = stm32::PinSet<>::Direction::AlternateFunction;       \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
        static constexpr auto alt_func_mask_values =                                           \
             pinset.make_alternate_function_mask_values<port_count, ports, index>();           \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
                                                                                               \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
        pinset.apply_alternate_function<port_count, ports, alt_func_mask_values>(ctx);         \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }
#define PINSET_CONFIGURE_ALTERNATE_OUTPUT_PUD(pinset, speed, open_drain, index, pull_up,       \
                                              pull_down)                                       \
    {                                                                                          \
        mmcc::InterruptGuard ctx;                                                              \
        static constexpr auto direction = stm32::PinSet<>::Direction::AlternateFunction;       \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto direction_mask_values =                                          \
             pinset.make_direction_mask_values<port_count, ports, direction>();                \
        static constexpr auto speed_mask_values =                                              \
             pinset.make_speed_mask_values<port_count, ports, speed>();                        \
        static constexpr auto outtype_masks =                                                  \
             pinset.make_output_type_mask_values<port_count, ports, open_drain>();             \
        static constexpr auto alt_func_mask_values =                                           \
             pinset.make_alternate_function_mask_values<port_count, ports, index>();           \
        static constexpr auto pullres_mask_values =                                            \
             pinset.make_pullres_mask_values<port_count, ports, pull_up, pull_down>();         \
                                                                                               \
        pinset.apply_pullres<port_count, ports, pullres_mask_values>(ctx);                     \
        pinset.apply_alternate_function<port_count, ports, alt_func_mask_values>(ctx);         \
        pinset.apply_output_type<port_count, ports, outtype_masks>(ctx);                       \
        pinset.apply_speed<port_count, ports, speed_mask_values>(ctx);                         \
        pinset.apply_direction<port_count, ports, direction_mask_values>(ctx);                 \
    }

#define PINSET_TEST_ALL_CTX(pinset, ctx)                                                       \
    ({                                                                                         \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto port_indexes =                                                   \
             pinset.get_port_index_for_pins<port_count, ports>();                              \
        pinset.test_all<port_count, ports, port_indexes>(ctx);                                 \
    })
#define PINSET_TEST_ALL(pinset)                                                                \
    ({                                                                                         \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto port_indexes =                                                   \
             pinset.get_port_index_for_pins<port_count, ports>();                              \
        pinset.test_all<port_count, ports, port_indexes>();                                    \
    })

#define PINSET_TEST_ANY_CTX(pinset, ctx)                                                       \
    ({                                                                                         \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto port_indexes =                                                   \
             pinset.get_port_index_for_pins<port_count, ports>();                              \
        pinset.test_any<port_count, ports, port_indexes>(ctx);                                 \
    })
#define PINSET_TEST_ANY(pinset)                                                                \
    ({                                                                                         \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto port_indexes =                                                   \
             pinset.get_port_index_for_pins<port_count, ports>();                              \
        pinset.test_any<port_count, ports, port_indexes>();                                    \
    })

#define PINSET_TEST_NONE_CTX(pinset, ctx)                                                      \
    ({                                                                                         \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto port_indexes =                                                   \
             pinset.get_port_index_for_pins<port_count, ports>();                              \
        pinset.test_none<port_count, ports, port_indexes>(ctx);                                \
    })
#define PINSET_TEST_NONE(pinset)                                                               \
    ({                                                                                         \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto port_indexes =                                                   \
             pinset.get_port_index_for_pins<port_count, ports>();                              \
        pinset.test_none<port_count, ports, port_indexes>();                                   \
    })

#define PINSET_READ_CTX(pinset, ctx)                                                           \
    ({                                                                                         \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto port_indexes =                                                   \
             pinset.get_port_index_for_pins<port_count, ports>();                              \
        pinset.read<port_count, ports, port_indexes>(ctx);                                     \
    })
#define PINSET_READ(pinset)                                                                    \
    ({                                                                                         \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto port_indexes =                                                   \
             pinset.get_port_index_for_pins<port_count, ports>();                              \
        pinset.read<port_count, ports, port_indexes>();                                        \
    })

#define PINSET_WRITE_CTX(pinset, ctx, states)                                                  \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto write_mask_values =                                              \
             pinset.make_write_mask_values<port_count, ports, states>();                       \
        pinset.write<port_count, ports, write_mask_values>(ctx);                               \
    }
#define PINSET_WRITE(pinset, states)                                                           \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto write_mask_values =                                              \
             pinset.make_write_mask_values<port_count, ports, states>();                       \
        pinset.write<port_count, ports, write_mask_values>();                                  \
    }

#define PINSET_WRITE_DYN_CTX(pinset, ctx, states)                                              \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto write_masks = pinset.make_write_masks<port_count, ports>();      \
        pinset.write<port_count, ports, write_masks>(ctx, states);                             \
    }
#define PINSET_WRITE_DYN(pinset, states)                                                       \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto write_masks = pinset.make_write_masks<port_count, ports>();      \
        pinset.write<port_count, ports, write_masks>(states);                                  \
    }

#define PINSET_TOGGLE_ALL_CTX(pinset, ctx)                                                     \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto toggle_masks = pinset.make_toggle_masks<port_count, ports>();    \
        pinset.toggle<port_count, ports, toggle_masks>(ctx);                                   \
    }
#define PINSET_TOGGLE_ALL(pinset)                                                              \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto toggle_masks = pinset.make_toggle_masks<port_count, ports>();    \
        pinset.toggle<port_count, ports, toggle_masks>();                                      \
    }

#define PINSET_TOGGLE_CTX(pinset, ctx, pinmask)                                                \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto toggle_masks =                                                   \
             pinset.make_toggle_masks<port_count, ports, pinmask>();                           \
        pinset.toggle<port_count, ports, toggle_masks>(ctx);                                   \
    }
#define PINSET_TOGGLE(pinset, pinmask)                                                         \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto toggle_masks =                                                   \
             pinset.make_toggle_masks<port_count, ports, pinmask>();                           \
        pinset.toggle<port_count, ports, toggle_masks>();                                      \
    }

#define PINSET_TOGGLE_DYN_CTX(pinset, ctx, pinmask)                                            \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        pinset.toggle<port_count, ports, toggle_masks>(ctx, pinmask);                          \
    }
#define PINSET_TOGGLE_DYN(pinset, pinmask)                                                     \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        pinset.toggle<port_count, ports>(pinmask);                                             \
    }

#define PINSET_SET_RESET_CTX(pinset, ctx, set_mask, reset_mask)                                \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>(ctx);                                    \
    }
#define PINSET_SET_RESET(pinset, set_mask, reset_mask)                                         \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>();                                       \
    }

#define PINSET_SET_CTX(pinset, ctx, set_mask)                                                  \
    {                                                                                          \
        static constexpr auto reset_mask = pinset.make_structural_pin_mask();                  \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>(ctx);                                    \
    }
#define PINSET_SET(pinset, set_mask)                                                           \
    {                                                                                          \
        static constexpr auto reset_mask = pinset.make_structural_pin_mask();                  \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>();                                       \
    }

#define PINSET_SET_ALL_CTX(pinset, ctx)                                                        \
    {                                                                                          \
        static constexpr auto set_mask = pinset.make_structural_pin_mask(true);                \
        static constexpr auto reset_mask = pinset.make_structural_pin_mask(false);             \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>(ctx);                                    \
    }
#define PINSET_SET_ALL(pinset)                                                                 \
    {                                                                                          \
        static constexpr auto set_mask = pinset.make_structural_pin_mask(true);                \
        static constexpr auto reset_mask = pinset.make_structural_pin_mask(false);             \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>();                                       \
    }

#define PINSET_RESET_CTX(pinset, ctx, reset_mask)                                              \
    {                                                                                          \
        static constexpr auto set_mask = pinset.make_structural_pin_mask();                    \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>(ctx);                                    \
    }
#define PINSET_RESET(pinset, reset_mask)                                                       \
    {                                                                                          \
        static constexpr auto set_mask = pinset.make_structural_pin_mask();                    \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>();                                       \
    }

#define PINSET_RESET_ALL_CTX(pinset, ctx)                                                      \
    {                                                                                          \
        static constexpr auto set_mask = pinset.make_structural_pin_mask(false);               \
        static constexpr auto reset_mask = pinset.make_structural_pin_mask(true);              \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>(ctx);                                    \
    }
#define PINSET_RESET_ALL(pinset)                                                               \
    {                                                                                          \
        static constexpr auto set_mask = pinset.make_structural_pin_mask(false);               \
        static constexpr auto reset_mask = pinset.make_structural_pin_mask(true);              \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        static constexpr auto sr_masks =                                                       \
             pinset.make_set_reset_masks<port_count, ports, set_mask, reset_mask>();           \
        pinset.set_reset<port_count, ports, sr_masks>();                                       \
    }

#define PINSET_SET_RESET_DYN_CTX(pinset, ctx, set_mask, reset_mask)                            \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        pinset.set_reset<port_count, ports>(ctx, set_mask, reset_mask);                        \
    }
#define PINSET_SET_RESET_DYN(pinset, set_mask, reset_mask)                                     \
    {                                                                                          \
        static constexpr auto port_count = pinset.get_port_count();                            \
        static constexpr auto ports = pinset.get_port_addresses<port_count>();                 \
        pinset.set_reset<port_count, ports>(set_mask, reset_mask);                             \
    }

#endif /*MMCC_GPIO_HPP*/
