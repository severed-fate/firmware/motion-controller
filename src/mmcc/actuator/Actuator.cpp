#include "Actuator.hpp"

#include <Objects.hpp>
#include <interface/rtos/RTOS.hpp>
#include <mmcc/board/Config.hpp>

#include <cstdint>
#include <cstdlib>

#include <boot.h>

namespace mmcc {

const char* Actuator::state2str(States state) {
    switch (state) {
        case States::Invalid: return "States::Invalid";
        case States::Pending: return "States::Pending";
        case States::Homing: return "States::Homing";
        case States::Idle: return "States::Idle";
        case States::Running: return "States::Running";
        case States::EmergencyStopped: return "States::EmergencyStopped";
    }
}

const char* Actuator::event2str(Events event) {
    switch (event) {
        case Events::Invalid: return "Events::Invalid";
        case Events::HomeStart: return "Events::HomeStart";
        case Events::HomeStop: return "Events::HomeStop";
        case Events::HomeEnd: return "Events::HomeEnd";
        case Events::LoopStart: return "Events::LoopStart";
        case Events::LoopUpdate: return "Events::LoopUpdate";
        case Events::LoopStop: return "Events::LoopStop";
        case Events::EmergencyStopped: return "Events::EmergencyStopped";
        case Events::EmergencyStopRelease: return "Events::EmergencyStopRelease";
        case Events::EmergencyStopReset: return "Events::EmergencyStopReset";
    }
}

void Actuator::init() {
    _evque.init("act_event_queue");
    ACT_PWM_TIMER::activate();
    ACT0_POS_ENC_TIMER::activate();
    ACT1_POS_TIMER::activate();
    ACT2_POS_TIMER::activate();
    ACT_PID_TIMER::activate();
    HOME_ESTOP_TIMER::activate();
    _homing_task.init();
}
bool Actuator::start() {
    return rtos::StaticTask<256>::start<Actuator, &Actuator::run>("actuator", 5) &&
           _homing_task.start();
}

void Actuator::_configure() {
    _configure_home_estop_button();
    _configure_act0_encoder();
    _configure_act1_position();
    _configure_act2_position();
    _configure_pwm();
    _configure_aux1();
    _configure_aux2();
    _configure_pid_ticker();

    _state = States::Pending;
}

Actuator::States Actuator::get_state() const {
    return _state;
}
bool Actuator::push_event_ticks(Events ev, TickType_t timeout_ticks) {
    return _evque.send_to_back_ticks(ev, timeout_ticks);
}
bool Actuator::push_event_irq(Events ev) {
    return _evque.send_to_back_from_isr(ev);
}

static void __on_invalid_state_transition() {
    static const char MSG[] = "Actuator::run -> invalid state transition!\n";
    dbg.print(MSG);
    std::abort();
}

void Actuator::run() {
    _configure();
    while (true) {
        auto may_be_ev = _evque.receive_ticks(rtos::ms2ticks(3000));
        if (may_be_ev.has_value()) {
            Events ev = may_be_ev.value();
            switch (ev) {
                case Events::HomeStart: {
                    if (_state != States::Pending) {
                        __on_invalid_state_transition();
                    }
                    _reset_enable_actuators_pwm();
                    _set_full_bridge_enable(true);
                    _state = States::Homing;
                    _homing_task.start_homing();
                } break;
                case Events::HomeStop: {
                    if (_state != States::Homing) {
                        __on_invalid_state_transition();
                    }
                    _state = States::Pending;
                    _set_actuator_pwm_brake();
                    _set_full_bridge_enable(false);
                    // Homing task will figure it out and stop by itself.
                } break;
                case Events::HomeEnd: {
                    if (_state != States::Homing) {
                        __on_invalid_state_transition();
                    }
                    _state = States::Idle;
                    on_idle_state_transition();
                } break;
                case Events::LoopStart: {
                    if (_state != States::Idle) {
                        __on_invalid_state_transition();
                    }
                    _state = States::Running;
                    // Engage PID loop.
                    _set_pid_loop_enabled(true);
                } break;
                case Events::LoopStop: {
                    if (_state != States::Running) {
                        __on_invalid_state_transition();
                    }
                    _state = States::Idle;
                    // Stop PID and reset PWM.
                    _set_pid_loop_enabled(false);
                    on_idle_state_transition();
                } break;
                case Events::EmergencyStopped: {
                    // We can transition to here from any other state.
                    _state = States::EmergencyStopped;
                    // Reset PWM and PID if running.
                    /* MOE has already been cleared */
                    _set_pid_loop_enabled(false);
                } break;
                case Events::EmergencyStopRelease: {
                    if (_state != States::EmergencyStopped) {
                        __on_invalid_state_transition();
                    }
                    // Release breaks.
                    on_idle_state_transition();
                    _state = States::Idle;
                } break;
                case Events::EmergencyStopReset: {
                    if (_state != States::EmergencyStopped) {
                        __on_invalid_state_transition();
                    }
                    _state = States::Pending;
                } break;

                case Events::Invalid:
                default: {
                    dbg.print("Actuator::run -> invalid event value!\n");
                    std::abort();
                }
            }
        }
    }
}

void Actuator::on_idle_state_transition() {
    _reset_enable_actuators_pwm();
    _set_full_bridge_enable(true);
}

void Actuator::apply_emergency_stop() {
    _apply_emergency_stop();
    while (!push_event_ticks(Events::EmergencyStopped, rtos::ms2ticks(10))) {
    }
}

void Actuator::set_actuators_enabled(bool act0_en, bool act1_en, bool act2_en) {
    _set_full_bridge_enable(act0_en, act1_en, act2_en);
}
void Actuator::set_actuator0_pwm(float pwm0) {
    bool dir0 = pwm0 < 0;
    if (dir0) {
        pwm0 = -pwm0;
    }
    uint16_t upwm0 = (uint16_t) ((pwm0 + 1e-3f) * PWM_PERIOD_RESOLUTION);
    _set_actuator0_pwm(dir0, upwm0);
}
void Actuator::set_actuator1_pwm(float pwm1) {
    bool dir1 = pwm1 < 0;
    if (dir1) {
        pwm1 = -pwm1;
    }
    uint16_t upwm1 = (uint16_t) ((pwm1 + 1e-3f) * PWM_PERIOD_RESOLUTION);
    _set_actuator1_pwm(dir1, upwm1);
}
void Actuator::set_actuator2_pwm(float pwm2) {
    bool dir2 = pwm2 < 0;
    if (dir2) {
        pwm2 = -pwm2;
    }
    uint16_t upwm2 = (uint16_t) ((pwm2 + 1e-3f) * PWM_PERIOD_RESOLUTION);
    _set_actuator2_pwm(dir2, upwm2);
}
void Actuator::set_actuator12_pwm(float pwm1, float pwm2) {
    bool dir1 = pwm1 < 0;
    if (dir1) {
        pwm1 = -pwm1;
    }
    uint16_t upwm1 = (uint16_t) ((pwm1 + 1e-3f) * PWM_PERIOD_RESOLUTION);
    bool dir2 = pwm2 < 0;
    if (dir2) {
        pwm2 = -pwm2;
    }
    uint16_t upwm2 = (uint16_t) ((pwm2 + 1e-3f) * PWM_PERIOD_RESOLUTION);
    _set_actuator12_pwm(dir1, upwm1, dir2, upwm2);
}
void Actuator::set_actuators_pwm(float pwm0, float pwm1, float pwm2) {
    bool dir0 = pwm0 < 0;
    if (dir0) {
        pwm0 = -pwm0;
    }
    uint16_t upwm0 = (uint16_t) ((pwm0 + 1e-3f) * PWM_PERIOD_RESOLUTION);
    bool dir1 = pwm1 < 0;
    if (dir1) {
        pwm1 = -pwm1;
    }
    uint16_t upwm1 = (uint16_t) ((pwm1 + 1e-3f) * PWM_PERIOD_RESOLUTION);
    bool dir2 = pwm2 < 0;
    if (dir2) {
        pwm2 = -pwm2;
    }
    uint16_t upwm2 = (uint16_t) ((pwm2 + 1e-3f) * PWM_PERIOD_RESOLUTION);
    _set_actuators_pwm(dir0, upwm0, dir1, upwm1, dir2, upwm2);
}

}  // namespace mmcc
