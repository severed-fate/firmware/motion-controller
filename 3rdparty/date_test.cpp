#include <chrono>
#include <cstdlib>
#include <iostream>

#include <bits/chrono.h>
#include <date/date.h>

int main() {
    using namespace std::chrono;
    // constexpr milliseconds input(1702598094397);
    constexpr milliseconds input(1709170499000 + 1200);

    // constexpr date::days input_days2 = duration_cast<date::days>(input);
    constexpr date::days input_days = duration_cast<date::days>(input);
    constexpr date::sys_days sys_input_days(duration_cast<date::days>(input));
    constexpr milliseconds input_time(input - duration_cast<milliseconds>(input_days));

    std::cout << "days split: " << input_days.count() << std::endl;
    std::cout << "millis split: " << input_time.count() << std::endl;

    date::year_month_day date(input_days);
    std::cout << "date: " << date << std::endl;

    date::weekday wdate(sys_input_days);
    std::cout << "week day: " << wdate << " (" << wdate.c_encoding() << ")" << std::endl;

    date::hh_mm_ss<milliseconds> time(input_time);
    std::cout << "time: " << time << std::endl;

    for (int64_t midnight_counter = 1702598094397; midnight_counter <= 1702598523000;
         midnight_counter += 333) {
        milliseconds input(midnight_counter);
        date::days input_days = duration_cast<date::days>(input);
        milliseconds input_time(input - duration_cast<milliseconds>(input_days));
        date::year_month_day date(input_days);
        date::hh_mm_ss<milliseconds> time(input_time);
        std::cout << date << "\t" << time << std::endl;
    }

    return EXIT_SUCCESS;
}
