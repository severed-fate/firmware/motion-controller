#ifndef MMCC_COM_HPP
#define MMCC_COM_HPP

#include <interface/io/GPIO.hpp>
#include <interface/io/USART.hpp>
#include <interface/io/USB.hpp>
#include <interface/rtos/RTOSBuffers.hpp>
#include <interface/rtos/RTOSTask.hpp>

#include <cstdint>

#include <utils/macros.h>

namespace mmcc {

class USBTask final : public rtos::StaticTask<384> {
   public:
    static constexpr uint32_t NOTIFICATION_CONNECT_STATE_CHANGE_BIT = 1 << 0;
    static constexpr uint32_t NOTIFICATION_DATA_SEND_PENDING_BIT = 1 << 2;

    enum class ConnectionState : uint8_t {
        PhyDisconnected = 0b00,
        PhyConnected = 0b01,
        UserConnected = 0b11,
    };

    static constexpr uint8_t SOF_WATCHDOG_RELOAD = 3;
    static constexpr auto SOF_WATCHDOG_PERIOD = rtos::ms2ticks(100);

    static constexpr size_t BULK_IN_INTERMEDIATE_BUFFER_SIZE = 64;
    /** Can't seem to always send out full size packets ¯\_(ツ)_/¯ */
    static constexpr size_t BULK_OUT_INTERMEDIATE_BUFFER_SIZE = 52;

   protected:
    uint8_t usb_control_buffer[128];
    const stm32::USB::DeviceDescriptor _dev;
    const stm32::USB::EndpointDescriptor _comm_endp[1];
    const stm32::USB::EndpointDescriptor _data_endp[2];
    const stm32::USB::CDCACMFunctionalDescriptors _comm_desc;
    const stm32::USB::InterfaceDescriptor _comm_iface[1];
    const stm32::USB::InterfaceDescriptor _data_iface[1];
    const stm32::USB::ConfigDescriptor::Interface _iface[2];
    const stm32::USB::ConfigDescriptor _config;
    const char* _strings[3];

    rtos::StaticStreamBuffer<512> _receive_stream;
    rtos::StaticStreamBuffer<512> _send_stream;

    /* State block: access may need to be guarded */
    rtos::iTask* volatile _receive_task;
    volatile bool _not_receiving;
    volatile bool _usb_transmitting;
    volatile ConnectionState _connection_state;
    volatile uint8_t _sof_watchdog_counter;
    TickType_t _sof_watchdog_last_tick;

   public:
    constexpr USBTask();
    cold_func_attr ~USBTask() = default;

    void init();
    bool start();
    void run();

   private:
    void run_watchdog();
    void run_send();
    void configure();

   public:
    ConnectionState get_connection_state() const;
    size_t receive_bytes_available();
    /**
     * NOTE: As it waits until the send buffer in empty,
     * it must be called only from tasks with lower priority than usb_task.
     */
    void disconnect(bool dc);
    /**
     * @returns the actual read byte count.
     * This can be 0 either on timeout, or when usb has disconnected,
     * or on error, such as when requesting a trigger level above the capacity.
     */
    size_t receive_ticks(rtos::iTask& task, void* data, size_t size, size_t trigger_level = 1,
                         TickType_t timeout_ticks = 0);
    size_t send_ticks(const void* data, size_t size, TickType_t timeout_ticks = 0);
    size_t send_from_isr(const void* data, size_t size);

   protected:
    void on_set_config(stm32::USB& usb, uint16_t v);
    usbd_request_return_codes cdcacm_control_request(stm32::USB&, usb_setup_data* req,
                                                     uint8_t**, uint16_t* len,
                                                     usbd_control_complete_callback*);

    void on_sof(stm32::USB& usb);
    void on_data_received(stm32::USB&, uint8_t ep);
    void on_data_sent(stm32::USB&, uint8_t ep);

   private:
    static void on_set_config(usbd_device*, uint16_t v);
    static usbd_request_return_codes cdcacm_control_request(
         usbd_device*, usb_setup_data* req, uint8_t** buf, uint16_t* len,
         usbd_control_complete_callback* complete);
    static void on_sof();
    static void on_data_received(usbd_device*, uint8_t ep);
    static void on_data_sent(usbd_device*, uint8_t ep);
};

constexpr USBTask::USBTask() :
    rtos::StaticTask<384>(), _dev(stm32::USB::Class::CDC, 0x0483, 0x5740, 0x0200),
    _comm_endp{
         stm32::USB::EndpointDescriptor(0x81, stm32::USB::TransferType::Interrupt, 16, 255)},
    _data_endp{stm32::USB::EndpointDescriptor(0x02, stm32::USB::TransferType::Bulk, 64, 1),
               stm32::USB::EndpointDescriptor(0x82, stm32::USB::TransferType::Bulk, 64, 1)},
    _comm_desc(0, 1),
    _comm_iface{stm32::USB::InterfaceDescriptor(0, 0, _comm_endp, stm32::USB::Class::CDC,
                                                stm32::USB::CDCSubClass::ACM,
                                                stm32::USB::CDCProtocol::AT, _comm_desc)},
    _data_iface{stm32::USB::InterfaceDescriptor(1, 0, _data_endp, stm32::USB::Class::DATA)},
    _iface{stm32::USB::ConfigDescriptor::Interface(_comm_iface),
           stm32::USB::ConfigDescriptor::Interface(_data_iface)},
    _config(_iface), _strings{"LewdSima", "CDC-ACM TCode Device", "STM32F411"},
    _receive_stream(), _send_stream(), _receive_task(nullptr), _not_receiving(false),
    _usb_transmitting(false), _connection_state(ConnectionState::PhyDisconnected),
    _sof_watchdog_counter(0) {}

}  // namespace mmcc

#endif /*MMCC_COM_HPP*/