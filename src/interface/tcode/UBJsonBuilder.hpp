#ifndef MMCC_UBJSONBUILDER_HPP
#define MMCC_UBJSONBUILDER_HPP

#include "etl/string_view.h"

#include <utils/Class.hpp>

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <type_traits>
#include <utility>

#define ASSERT_FILE_ID (0xF6A1)

namespace tcode {

/* Fwd decls */
class ParserDispatcher;

namespace codec {

class UBJsonInstance;
class UBJsonObject;
class UBJsonArray;

enum class UBJsonType : char {
    Null = 'Z',
    NoOp = 'N',
    BoolTrue = 'T',
    BoolFalse = 'F',
    Int8 = 'i',
    UInt8 = 'U',
    Int16 = 'I',
    Int32 = 'l',
    Int64 = 'L',
    Float = 'd',
    Double = 'D',
    Char = 'C',
    /* HighPrecision = Not supported */
    String = 'S',
    Object = '{',
    Array = '['
};

template<typename T> requires(std::is_integral_v<T>)
constexpr UBJsonType get_optimal_ubjson_type(T v) {
    if ((int8_t) v >= std::numeric_limits<int8_t>::min() &&
        v <= std::numeric_limits<int8_t>::max()) {
        return UBJsonType::Int8;
    }
    else if ((uintmax_t) std::numeric_limits<T>::max() > std::numeric_limits<int8_t>::max() &&
             v >= 0 && v <= std::numeric_limits<uint8_t>::max()) {
        return UBJsonType::UInt8;
    }
    else if ((uintmax_t) std::numeric_limits<T>::max() > std::numeric_limits<uint8_t>::max() &&
             (int16_t) v >= std::numeric_limits<int16_t>::min() &&
             (uint16_t) v <= std::numeric_limits<int16_t>::max()) {
        return UBJsonType::Int16;
    }
    else if ((uintmax_t) std::numeric_limits<T>::max() > std::numeric_limits<int16_t>::max() &&
             (int32_t) v >= std::numeric_limits<int32_t>::min() &&
             (uint32_t) v <= std::numeric_limits<int32_t>::max()) {
        return UBJsonType::Int32;
    }
    else if ((uintmax_t) std::numeric_limits<T>::max() > std::numeric_limits<int32_t>::max() &&
             (int64_t) v >= std::numeric_limits<int64_t>::min() &&
             (uint64_t) v <= std::numeric_limits<int64_t>::max()) {
        return UBJsonType::Int64;
    }
    else {
        /** Value is not representable in ubjson! */
        assert(0, __COUNTER__);
        std::abort();
    }
}

class UBJsonContainer : public meta::INonCopyable {
   protected:
    UBJsonInstance* _instance = nullptr;
    char _end_marker = '\0';
    UBJsonType _type = UBJsonType::NoOp;
    size_t _size = SIZE_MAX;
    size_t _count = 0;

   public:
    constexpr UBJsonContainer() = default;
    UBJsonContainer(UBJsonContainer&& o) :
        _instance(std::exchange(o._instance, nullptr)), _end_marker(o._end_marker),
        _type(o._type), _size(o._size), _count(o._count) {}
    UBJsonContainer& operator=(UBJsonContainer&& o) {
        _instance = std::exchange(o._instance, nullptr);
        _end_marker = o._end_marker;
        _type = o._type;
        _size = o._size;
        _count = o._count;
        return *this;
    }
    /**
     * Create a new, optionally nested container,
     * without the size known ahead of time.
     */
    explicit UBJsonContainer(UBJsonInstance& instance, char start_marker);
    /**
     * Create a new, optionally nested container,
     * with the size known ahead of time.
     */
    explicit UBJsonContainer(UBJsonInstance& instance, char start_marker, size_t size);
    /**
     * Create a new, optionally nested container,
     * with both the element/value type and count known ahead of time.
     */
    explicit UBJsonContainer(UBJsonInstance& instance, char start_marker, UBJsonType type,
                             size_t size);
    ~UBJsonContainer();

    operator bool() const {
        return _instance != nullptr;
    }

    bool is_typed() const;
    bool is_sized() const;

    UBJsonType get_type() const;
    size_t get_max_size() const;
    size_t get_size() const;

    void on_new_element(UBJsonType type);

    [[nodiscard]] UBJsonObject begin_object();
    [[nodiscard]] UBJsonObject begin_object(size_t size);
    [[nodiscard]] UBJsonObject begin_object(UBJsonType type, size_t size);
    [[nodiscard]] UBJsonArray begin_array();
    [[nodiscard]] UBJsonArray begin_array(size_t size);
    [[nodiscard]] UBJsonArray begin_array(UBJsonType type, size_t size);

    void write_null();
    void write(bool v);
    void write(char v);
    void write(int8_t v);
    void write(uint8_t v);
    void write(int16_t v);
    void write(int32_t v);
    void write(int64_t v);
    void write(float v);
    void write(double v);
    void write(const char* str, size_t n);

    template<typename T> requires(std::is_integral_v<T>)
    void write_smart(T v);

    __attribute__((always_inline)) void write(const char* str_begin, const char* str_end) {
        write(str_begin, static_cast<uintptr_t>(str_end - str_begin));
    }
    template<std::size_t N> __attribute__((always_inline)) void write(const char (&str)[N]) {
        write(&str[0], N - 1);
    }
    __attribute__((always_inline)) void write(etl::string_view str) {
        write(str.data(), str.size());
    }
};

class UBJsonObject : public UBJsonContainer {
   protected:
   public:
    constexpr UBJsonObject() = default;
    UBJsonObject(UBJsonObject&&) = default;
    UBJsonObject& operator=(UBJsonObject&&) = default;
    UBJsonObject(UBJsonInstance& instance);
    UBJsonObject(UBJsonInstance& instance, size_t size);
    UBJsonObject(UBJsonInstance& instance, UBJsonType type, size_t size);
    ~UBJsonObject() = default;

    void write_key(const char* str, size_t n);

    __attribute__((always_inline)) void write_key(const char* str_begin, const char* str_end) {
        write_key(str_begin, static_cast<uintptr_t>(str_end - str_begin));
    }
    template<std::size_t N>
    __attribute__((always_inline)) void write_key(const char (&str)[N]) {
        write_key(&str[0], N - 1);
    }
    template<std::size_t N>
    __attribute__((always_inline)) void write_key(const etl::array<char, N> str) {
        write_key(str.begin(), N);
    }
    __attribute__((always_inline)) void write_key(etl::string_view str) {
        write_key(str.data(), str.size());
    }
    template<std::size_t N>
    __attribute__((always_inline)) UBJsonObject& operator[](const char (&str)[N]) {
        write_key(&str[0], N - 1);
        return *this;
    }
};

class UBJsonArray : public UBJsonContainer {
   protected:
   public:
    constexpr UBJsonArray() = default;
    UBJsonArray(UBJsonArray&&) = default;
    UBJsonArray& operator=(UBJsonArray&&) = default;
    UBJsonArray(UBJsonInstance& instance);
    UBJsonArray(UBJsonInstance& instance, size_t size);
    UBJsonArray(UBJsonInstance& instance, UBJsonType type, size_t size);
    ~UBJsonArray() = default;

    void write_bytes(const uint8_t* data, size_t n);
};

class UBJsonInstance : public meta::INonCopyable {
   protected:
    ParserDispatcher* _owner;
    size_t _buffer_usage;
    uint8_t _buffer[16];

    static_assert(sizeof(_buffer) % 4 == 0,
                  "_buffer must be multiples of z85's encode input token size.");

   public:
    constexpr UBJsonInstance() : _owner(nullptr), _buffer_usage(0) {}
    UBJsonInstance(ParserDispatcher& owner);
    UBJsonInstance(UBJsonInstance&& o) :
        _owner(std::exchange(o._owner, nullptr)),
        _buffer_usage(std::exchange(o._buffer_usage, 0)) {
        std::memcpy(_buffer, o._buffer, sizeof(_buffer));
    }
    UBJsonInstance& operator=(UBJsonInstance&& o) {
        _owner = std::exchange(o._owner, nullptr);
        _buffer_usage = std::exchange(o._buffer_usage, 0);
        std::memcpy(_buffer, o._buffer, sizeof(_buffer));
        return *this;
    }
    ~UBJsonInstance();

    [[nodiscard]] UBJsonObject begin_object();
    [[nodiscard]] UBJsonObject begin_object(size_t size);
    [[nodiscard]] UBJsonObject begin_object(UBJsonType type, size_t size);
    [[nodiscard]] UBJsonArray begin_array();
    [[nodiscard]] UBJsonArray begin_array(size_t size);
    [[nodiscard]] UBJsonArray begin_array(UBJsonType type, size_t size);

    void write_noop();
    void write_null();
    void write(bool v);
    void write(char v, bool skip_header = false);
    void write(int8_t v, bool skip_header = false);
    void write(uint8_t v, bool skip_header = false);
    void write(int16_t v, bool skip_header = false);
    void write(int32_t v, bool skip_header = false);
    void write(int64_t v, bool skip_header = false);
    void write(float v, bool skip_header = false);
    void write(double v, bool skip_header = false);
    void write(const char* str, size_t n, bool skip_header = false);

    __attribute__((always_inline)) void write(const char* str_begin, const char* str_end,
                                              bool skip_header = false) {
        write(str_begin, static_cast<uintptr_t>(str_end - str_begin), skip_header);
    }
    template<std::size_t N>
    __attribute__((always_inline)) void write(const char (&str)[N], bool skip_header = false) {
        write(&str[0], N - 1, skip_header);
    }
    __attribute__((always_inline)) void write(etl::string_view str, bool skip_header = false) {
        write(str.data(), str.size(), skip_header);
    }

   protected:
    size_t get_buffer_space() const;
    void append(uint8_t ch);
    void append(const void* dat, size_t n);
    /**
     * Flushes current buffer data, but only up to the last full z85 token.
     * Moves the remaining data to the start of the buffer.
     * @returns new available buffer space.
     */
    size_t flush();
    void finalize();

    void write_size_smart(size_t size);

    friend class UBJsonContainer;
    friend class UBJsonObject;
    friend class UBJsonArray;
};

template<typename T> requires(std::is_integral_v<T>)
void UBJsonContainer::write_smart(T v) {
    UBJsonType type = get_optimal_ubjson_type(v);
    on_new_element(type);
    switch (type) {
        case UBJsonType::Int8: {
            _instance->write((int8_t) v, is_typed());
        } break;
        case UBJsonType::UInt8: {
            _instance->write((uint8_t) v, is_typed());
        } break;
        case UBJsonType::Int16: {
            _instance->write((int16_t) v, is_typed());
        } break;
        case UBJsonType::Int32: {
            _instance->write((int32_t) v, is_typed());
        } break;
        case UBJsonType::Int64: {
            _instance->write((int64_t) v, is_typed());
        } break;
        default: {
            /** unreachable */
            assert(0, __COUNTER__);
            std::abort();
        } break;
    }
}

}  // namespace codec

}  // namespace tcode

#undef ASSERT_FILE_ID
#endif /*MMCC_UBJSONBUILDER_HPP*/
