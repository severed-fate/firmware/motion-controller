#ifndef MMCC_RTOS_HPP
#define MMCC_RTOS_HPP

#include <utils/Class.hpp>

#include <limits>

#include <FreeRTOS.h>
#include <task.h>

namespace rtos {

using uint = UBaseType_t;

inline bool is_inside_interrupt() {
    return xPortIsInsideInterrupt() != pdFALSE;
}

constexpr float MAX_DELAY = std::numeric_limits<float>::infinity();

constexpr TickType_t ms2ticks(float ms) {
    if (ms == MAX_DELAY) {
        return portMAX_DELAY;
    }
    return ((TickType_t) (ms * configTICK_RATE_HZ)) / 1000;
}

constexpr float ticks2ms(TickType_t ticks) {
    if (ticks == portMAX_DELAY) {
        return MAX_DELAY;
    }
    return (ticks * 1000) / ((float) configTICK_RATE_HZ);
}
constexpr float ticks2sec(TickType_t ticks) {
    if (ticks == portMAX_DELAY) {
        return MAX_DELAY;
    }
    return ticks / ((float) configTICK_RATE_HZ);
}

class Kernel {
   public:
    enum State {
        SchedulerNotStarted = taskSCHEDULER_NOT_STARTED,
        SchedulerRunning = taskSCHEDULER_RUNNING,
        SchedulerSuspended = taskSCHEDULER_SUSPENDED
    };

   public:
    Kernel() = delete;
    Kernel& operator=(Kernel&) = delete;
    Kernel(Kernel&) = delete;
    Kernel& operator=(Kernel&&) = delete;
    Kernel(Kernel&&) = delete;

    static void start();
    static void end();
    static void suspend_all();
    static bool resume_all();
    static void step_tick(TickType_t);
    static bool catch_up_ticks(TickType_t);

    static TickType_t get_tickcount();
    static State get_scheduler_state();
    static UBaseType_t get_number_of_tasks();

    static void yield();
    static void enter_critical();
    static void exit_critical();
    static uint32_t enter_critical_from_isr();
    static void exit_critical_from_isr(uint32_t);
    static void disable_interrupts();
    static void enable_interrupts();

    static void get_idle_task_memory(StaticTask_t** idle_task_tcb_buffer,
                                     StackType_t** idle_task_stack_buffer,
                                     uint32_t* idle_task_stack_size);

    static void on_pre_sleep_processing(TickType_t& expected_idle_time);
    static void on_post_sleep_processing(TickType_t& expected_idle_time);

    static void on_idle();
    static void on_start();
    static void on_end();
};

}  // namespace rtos

#endif /*MMCC_RTOS_HPP*/