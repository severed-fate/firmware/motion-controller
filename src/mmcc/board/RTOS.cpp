#include "Objects.hpp"

#include <mmcc/board/Config.hpp>

#include <cstdlib>

// void rtos_print(const char* str) {
//     mmcc::cli_task.print(str);
// }

namespace rtos {

void Kernel::get_idle_task_memory(StaticTask_t** idle_task_tcb_buffer,
                                  StackType_t** idle_task_stack_buffer,
                                  uint32_t* idle_task_stack_size) {
    static StaticTask_t _idle_task_tcb;
    static StackType_t _idle_task_stack[configMINIMAL_STACK_SIZE];
    *idle_task_tcb_buffer = &_idle_task_tcb;
    *idle_task_stack_buffer = _idle_task_stack;
    *idle_task_stack_size = sizeof(_idle_task_stack) / sizeof(decltype(_idle_task_stack[0]));
}

void Kernel::on_pre_sleep_processing([[maybe_unused]] TickType_t& expected_idle_time) {
    mmcc::InterruptGuard<true> suppressed;
    PINSET_SET_ALL_CTX(mmcc::STATUS_LED, suppressed);
}
void Kernel::on_post_sleep_processing([[maybe_unused]] TickType_t& expected_idle_time) {
    mmcc::InterruptGuard<true> suppressed;
    PINSET_RESET_ALL_CTX(mmcc::STATUS_LED, suppressed);
}

void Kernel::on_idle() {}
void Kernel::on_start() {
    mmcc::InterruptGuard<true> suppressed;
    PINSET_RESET_ALL_CTX(mmcc::STATUS_LED, suppressed);
}
void Kernel::on_end() {
    mmcc::InterruptGuard<true> suppressed;
    PINSET_SET_ALL_CTX(mmcc::STATUS_LED, suppressed);
}

}  // namespace rtos
