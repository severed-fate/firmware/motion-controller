#ifndef MMCC_FLASH_HPP
#define MMCC_FLASH_HPP

#include <interface/io/flash_prg.h>

namespace stm32::flash {

enum class PrgError {
    Success = FLASH_PRG_ERROR_SUCCESS,
    RuntimeError = FLASH_PRG_ERROR_RUNTIME,
    BusyError = FLASH_PRG_ERROR_BUSY,
    PartialError = FLASH_PRG_ERROR_PARTIAL,
    WireProtectionError = FLASH_PRG_ERROR_WPROT,
    AlignmentError = FLASH_PRG_ERROR_ALIGNMENT,
    ParallelismError = FLASH_PRG_ERROR_PARALLELISM,
    SequenceError = FLASH_PRG_ERROR_SEQUENCE,
    LowVoltage = FLASH_PRG_ERROR_LOW_VOLTAGE,
};

PrgError erase_sector(sector_index_t sector_index);
PrgError erase_sector(const void* flash_addr);

PrgError write(void* dst, const void* src, size_t n);

}  // namespace stm32::flash

#endif /*MMCC_FLASH_HPP*/