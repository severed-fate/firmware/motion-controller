#include "Actuator.hpp"
#include "Config.hpp"
#include "io/Timer.hpp"

namespace mmcc {

void Actuator::_configure_aux1() {
    AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                  stm32::timer::OutputChannelMode::ForceHigh);
    AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                  stm32::timer::OutputChannelMode::ForceHigh);
    AUX1_MOTOR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC3, 0x0);
    AUX1_MOTOR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC4, 0x0);
    AUX1_MOTOR_TIMER::set_oc_output(stm32::timer::OutputChannel::OC3, true);
    AUX1_MOTOR_TIMER::set_oc_output(stm32::timer::OutputChannel::OC4, true);
}
void Actuator::_configure_aux2() {
    AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                  stm32::timer::OutputChannelMode::ForceHigh);
    AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                  stm32::timer::OutputChannelMode::ForceHigh);
    AUX2_MOTOR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC3, 0x0);
    AUX2_MOTOR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC4, 0x0);
    AUX2_MOTOR_TIMER::set_oc_output(stm32::timer::OutputChannel::OC3, true);
    AUX2_MOTOR_TIMER::set_oc_output(stm32::timer::OutputChannel::OC4, true);
}

constexpr uint16_t reference_period = 11200;

void Actuator::_set_aux_pwm_brake() {
    AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                  stm32::timer::OutputChannelMode::ForceHigh);
    AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                  stm32::timer::OutputChannelMode::ForceHigh);
    AUX1_MOTOR_TIMER::generate_event(stm32::timer::Event::UG);

    AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                  stm32::timer::OutputChannelMode::ForceHigh);
    AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                  stm32::timer::OutputChannelMode::ForceHigh);
    AUX2_MOTOR_TIMER::generate_event(stm32::timer::Event::UG);
}

void Actuator::_set_aux1_pwm(bool dir, uint16_t pwm) {
    if (_emergency_stopped) [[unlikely]] {
        AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                      stm32::timer::OutputChannelMode::ForceHigh);
        AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                      stm32::timer::OutputChannelMode::ForceHigh);
    }
    else if (dir) {
        AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                      stm32::timer::OutputChannelMode::ForceLow);
        AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                      stm32::timer::OutputChannelMode::Pwm1);
    }
    else {
        AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                      stm32::timer::OutputChannelMode::Pwm1);
        AUX1_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                      stm32::timer::OutputChannelMode::ForceLow);
    }

    AUX1_MOTOR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC3, pwm);
    AUX1_MOTOR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC4, pwm);
}
void Actuator::_set_aux2_pwm(bool dir, uint16_t pwm) {
    if (_emergency_stopped) [[unlikely]] {
        AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                      stm32::timer::OutputChannelMode::ForceHigh);
        AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                      stm32::timer::OutputChannelMode::ForceHigh);
    }
    else if (dir) {
        AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                      stm32::timer::OutputChannelMode::ForceLow);
        AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                      stm32::timer::OutputChannelMode::Pwm1);
    }
    else {
        AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                                      stm32::timer::OutputChannelMode::Pwm1);
        AUX2_MOTOR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC4,
                                      stm32::timer::OutputChannelMode::ForceLow);
    }

    AUX2_MOTOR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC3, pwm);
    AUX2_MOTOR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC4, pwm);
}

void Actuator::set_aux1_pwm(float pwm1) {
    bool dir = pwm1 < 0;
    if (dir) {
        pwm1 = -pwm1;
    }
    uint16_t pwm = (uint16_t) ((pwm1 + 1e-3f) * reference_period);
    _set_aux1_pwm(dir, pwm);
}
void Actuator::set_aux2_pwm(float pwm2) {
    bool dir = pwm2 < 0;
    if (dir) {
        pwm2 = -pwm2;
    }
    uint16_t pwm = (uint16_t) ((pwm2 + 1e-3f) * reference_period);
    _set_aux2_pwm(dir, pwm);
}

}  // namespace mmcc
