#include "TCode.hpp"

#include <Objects.hpp>

namespace tcode {

bool TCodeDevice::tokenize(const char* pos, size_t len) {
    const char* end_pos = pos + len;
    const char* marked_pos;

    const char *t1, *t2, *t3, *t4, *t5, *t6;
    /*!stags:re2c format = 'const char *@@;\n'; */

    for (;;) {
        /*!re2c
            re2c:define:YYCTYPE = char;
            re2c:define:YYCURSOR = pos;
            re2c:define:YYMARKER = marked_pos;
            re2c:define:YYLIMIT = end_pos;
            re2c:tags:prefix = tmp_tag_;
            re2c:indent:top = 3;
            re2c:indent:string = "    ";
            re2c:yyfill:enable = 0;
            re2c:tags = 1;
            re2c:eof = 0;

            number = [0-9]+;
            padding = [ \t\r\n]+;
            z85 = ([\]\-0-9a-zA-Z/[*.:+=^!?&<>(){}@%$#]{5})+;

            axis_idx = [LlRrVvAa][0-9];
            all_cmd_idx = [LlRrVvAaDd][0-9];
            property_name = [0-9a-z_]+;

            *      { return false; }
            $      { return true; }
            padding { continue; }

            '?' @t1 axis_idx @t2 padding {
                mmcc::dbg.print("QUERY_CMD_AXIS");
                continue;
            }
            @t1 axis_idx @t2 number @t3 padding {
                mmcc::dbg.print("CMD_AXIS");
                continue;
            }
            @t1 axis_idx @t2 number @t3 'I' @t4 number @t5 padding {
                mmcc::dbg.print("CMD_AXIS_INTERVAL");
                continue;
            }
            @t1 axis_idx @t2 number @t3 'S' @t4 number @t5 padding {
                mmcc::dbg.print("CMD_AXIS_SPEED");
                continue;
            }

            @t1 'd'[0-9] @t2 padding {
                mmcc::dbg.print("CMD_DEV");
                continue;
            }
            '?' @t1 all_cmd_idx @t2 'P' @t3 property_name @t4 padding {
                mmcc::dbg.print("QUERY_CMD_PROP");
                continue;
            }
            '$' @t1 all_cmd_idx @t2 'P' @t3 property_name @t4 'Z' @t5 z85 @t6 padding {
                mmcc::dbg.print("SET_CMD_PROP");
                continue;
            }
            @t1 all_cmd_idx @t2 'P' @t3 property_name @t4 'I' @t5 number @t6 padding {
                mmcc::dbg.print("PROP_SET_INTERVAL");
                continue;
            }

            @t1 axis_idx @t2 'stop' padding {
                mmcc::dbg.print("CMD_AXIS_STOP");
                continue;
            }
            'dstop' {
                mmcc::dbg.print("ALL_STOP");
                continue;
            }
        */
    }
}

}  // namespace tcode
