#ifndef UTILS_ARRAY_HPP
#define UTILS_ARRAY_HPP

#include <array>
#include <cstddef>

namespace meta {

template<typename T, size_t N, size_t M>
constexpr std::array<T, N + M> concat(const std::array<T, N> a, const std::array<T, M> b) {
    std::array<T, N + M> r;

    for (size_t i = 0; i < N; i++) {
        r[i] = a[i];
    }
    for (size_t i = 0; i < M; i++) {
        r[N + i] = b[i];
    }

    return r;
}

}  // namespace meta

#endif /*UTILS_ARRAY_HPP*/