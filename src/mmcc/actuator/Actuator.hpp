#ifndef MMCC_ACTUATOR_HPP
#define MMCC_ACTUATOR_HPP

#include <interface/io/Timer.hpp>
#include <interface/rtos/RTOS.hpp>
#include <interface/rtos/RTOSQueue.hpp>
#include <interface/rtos/RTOSTask.hpp>
#include <mmcc/actuator/Interpolator.hpp>
#include <mmcc/board/Config.hpp>
#include <utils/PidController.hpp>

#include <optional>

#include <utils/macros.h>

namespace mmcc {

class ActuatorHomingTask final : public rtos::StaticTask<256> {
   protected:
    static constexpr float HOMING_ACT12_DEADBAND = 0.033;

   public:
    constexpr ActuatorHomingTask() = default;
    cold_func_attr ~ActuatorHomingTask() = default;

    void init();
    bool start();
    void run();

    void start_homing() const;

   private:
    /**
     * Returns true if start-up routine completed successfully.
     */
    bool _home();
};

class Actuator final : public rtos::StaticTask<256> {
   public:
    enum class States : uint8_t {
        Invalid,
        Pending, /* PWM break idle, full-bridge disabled */
        Homing,  /* PWM enabled, full-bridge enabled, controlled by homing task */
        Idle,    /* PWM enabled, full-bridge enabled, idle */
        Running /* PWM enabled, full-bridge enabled, controlled by PID loop */,
        EmergencyStopped /* PWM break idle, full-bridge disabled */
    };

    enum class Events : uint8_t {
        Invalid,
        /* Homing */
        HomeStart,
        HomeStop,
        HomeEnd,
        /* PID loop control */
        LoopStart,
        LoopUpdate,
        LoopStop,
        /* Error state reports */
        EmergencyStopped,
        EmergencyStopRelease, /* Return to idle */
        EmergencyStopReset,   /* Return to pending */
    };

    static const char* state2str(States state);
    static const char* event2str(Events event);

   protected:
    enum class HomeEstopButtonState : uint16_t {
        WaitingRise,
        Rise2Steady,
        WaitingFall,
        Fall2Steady
    };

   protected:
    /* Task state */
    States _state = States::Invalid;
    rtos::StaticQueue<Events, 8> _evque{};

    /* Home/Estop module state */
    HomeEstopButtonState _actuator_home_estop_button_state = HomeEstopButtonState::WaitingRise;
    uint16_t _actuator_home_estop_button_counter = 0;
    TickType_t _home_estop_button_last_press = 0;
    bool _emergency_stopped = false;

    /* Homing state */
    ActuatorHomingTask _homing_task{};
    uint32_t _act0_pos_start = 0;
    uint32_t _act0_pos_end = 0xffff;
    float _act0_pos_fmag = 4.0f;

    /* PWM & PID state */
    size_t _pid_loop_tick_counter = 0;
    bool _act0_engaged = false;
    bool _act1_engaged = false;
    bool _act2_engaged = false;
    bool _aux_engaged = false;
    bool _pid_loop_enable_plots = false;
    // TODO: Wrap in speed limiter.
    SpeedLimiter _act0_interp{3.f * PID_LOOP_PERIOD};
    algo::pid::Control _act0_ctl{};
    float _act0_ctl_bias = 0;
    float _act0_ctl_deadband = 0;
    AxisInterpolator _act1_interp{};
    algo::pid::Control _act1_ctl{};
    float _act1_ctl_bias = 0;
    float _act1_ctl_deadband = 0;
    AxisInterpolator _act2_interp{};
    algo::pid::Control _act2_ctl{};
    float _act2_ctl_bias = 0;
    float _act2_ctl_deadband = 0;

   public:
    /* Configuration constants */
    static constexpr uint16_t PID_LOOP_FREQ = 250;
    static constexpr float PID_LOOP_PERIOD = 1.f / PID_LOOP_FREQ;
    static constexpr size_t PID_LOOP_PERIOD_MS = 1000 / PID_LOOP_FREQ;
    static constexpr TickType_t HOME_ESTOP_BUTTON_LONG_PRESS = rtos::ms2ticks(800);
    static constexpr uint16_t PWM_PERIOD_RESOLUTION = 720;

    /**
     * Notes about motion.
     *  - Carriage down -> act 0 position increases
     *  - Act 0 forward -> carriage down
     *  - Act 1 forward -> angular position increases
     *  - Act 2 forward -> angular position increases
     *
     * Calibration values.
     *  - Act 1 angular position range: 0.115 -> 0.875
     *  - Act 2 angular position range: 0.120 -> 0.875
     */
    static constexpr std::pair<float, float> ACT1_POS_DEADBAND = {0.15, 0.85};
    static constexpr std::pair<float, float> ACT2_POS_DEADBAND = {0.15, 0.85};

   public:
    constexpr Actuator() :
        rtos::StaticTask<256>(), _state(States::Invalid), _evque(std::nullopt),
        _home_estop_button_last_press(0), _emergency_stopped(false), _homing_task() {}
    cold_func_attr ~Actuator() = default;

    void init();
    bool start();
    void run();

   protected:
    cold_func_attr void _configure();
    void on_idle_state_transition();

   public:
    States get_state() const;
    bool push_event_ticks(Events ev, TickType_t timeout_ticks = 0);
    bool push_event_irq(Events ev);

    uint32_t get_actuator0_position_raw();
    void set_actuator0_position_limits(uint32_t start, uint32_t end);
    float get_actuator0_position_scaled();

    std::pair<uint16_t, uint16_t> get_actuator1_position_raw();
    float get_actuator1_position_scaled();

    std::pair<uint16_t, uint16_t> get_actuator2_position_raw();
    float get_actuator2_position_scaled();

    /* Public interface version. Brakes PWM outputs and sends events for state transition. */
    void apply_emergency_stop();

    bool is_pid_loop_enabled() const;
    size_t get_pid_loop_tick_count() const {
        return _pid_loop_tick_counter;
    }
    bool is_act0_engaged() const {
        return _act0_engaged;
    }
    bool is_act1_engaged() const {
        return _act1_engaged;
    }
    bool is_act2_engaged() const {
        return _act2_engaged;
    }
    bool is_aux_engaged() const {
        return _aux_engaged;
    }
    void set_act0_engage(bool en);
    void set_act1_engage(bool en);
    void set_act2_engage(bool en);
    void set_aux_engage(bool en);

    void actuate0(float stp);
    void actuate0(float trgt, size_t duration_ms);
    void actuate1(float stp);
    void actuate1(float trgt, size_t duration_ms);
    void actuate2(float stp);
    void actuate2(float trgt, size_t duration_ms);

    void set_actuators_pwm_prescaler(uint16_t psc);
    void set_actuators_enabled(bool act0_en, bool act1_en, bool act2_en);
    void set_actuator0_pwm(float pwm0);
    void set_actuator1_pwm(float pwm1);
    void set_actuator2_pwm(float pwm2);
    void set_actuator12_pwm(float pwm1, float pwm2);
    void set_actuators_pwm(float pwm0, float pwm1, float pwm2);

    void set_aux1_pwm(float pwm1);
    void set_aux2_pwm(float pwm2);

    void on_abort();

   protected:
    void _configure_home_estop_button();
    void _configure_act0_encoder();
    void _configure_act1_position();
    void _configure_act2_position();
    void _configure_aux1();
    void _configure_aux2();
    void _configure_pwm();
    void _configure_pid_ticker();

    void _apply_emergency_stop();

    /* Force-disables PWM outputs */
    void _set_actuator_pwm_brake();
    void _set_aux_pwm_brake();

    /**
     * Sets PWM values to 0 and enables PWM outputs.
     */
    void _reset_enable_actuators_pwm();

    /**
     * Sets the state of the full bridge board-level enable.
     *  - true -> normal operation
     *  - false -> output brake mode.
     */
    void _set_full_bridge_enable(bool act_en);
    void _set_full_bridge_enable(bool act0_en, bool act1_en, bool act2_en);

    void _set_actuator0_pwm(bool dir0, uint16_t pwm0);
    void _set_actuator1_pwm(bool dir1, uint16_t pwm1);
    void _set_actuator2_pwm(bool dir2, uint16_t pwm2);
    void _set_actuator12_pwm(bool dir1, uint16_t pwm1, bool dir2, uint16_t pwm2);
    void _set_actuators_pwm(bool dir0, uint16_t pwm0, bool dir1, uint16_t pwm1, bool dir2,
                            uint16_t pwm2);
    void _set_aux1_pwm(bool dir, uint16_t pwm);
    void _set_aux2_pwm(bool dir, uint16_t pwm);

    /**
     * Starts or stops PID loop.
     * If starting, modifies set value to current
     * measurements, to avoid sudden control changes.
     */
    void _set_pid_loop_enabled(bool enable);

    void _on_pid_loop_tick();

    void on_home_estop_button_event();
    void on_home_estop_button_trigger();

   private:
    static void __on_pid_loop_tick();

    static void __on_home_estop_button_event();
    static void __on_home_estop_button_trigger();
};

}  // namespace mmcc

#endif /*MMCC_ACTUATOR_HPP*/
