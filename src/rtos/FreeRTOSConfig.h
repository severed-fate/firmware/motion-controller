#ifndef MMCC_FREERTOSCONFIG_HPP
#define MMCC_FREERTOSCONFIG_HPP

#define configUSE_PREEMPTION 1
#define configUSE_16_BIT_TICKS 0
#define configIDLE_SHOULD_YIELD 1

#define configMAX_PRIORITIES 8
#define configMAX_TASK_NAME_LEN 12

#define configUSE_IDLE_HOOK 1
#define configMINIMAL_STACK_SIZE 96

#define configUSE_TICK_HOOK 0

#define configUSE_MALLOC_FAILED_HOOK 0

#define configUSE_TASK_NOTIFICATIONS 1
#define configTASK_NOTIFICATION_ARRAY_ENTRIES 3
#define configUSE_MUTEXES 1
#define configUSE_RECURSIVE_MUTEXES 1
#define configUSE_COUNTING_SEMAPHORES 1

#define configUSE_TIMERS 0
// #define configTIMER_TASK_PRIORITY (2)
// #define configTIMER_QUEUE_LENGTH 10
// #define configTIMER_TASK_STACK_DEPTH (configMINIMAL_STACK_SIZE * 2)

#define configCHECK_FOR_STACK_OVERFLOW 1
#define configSUPPORT_STATIC_ALLOCATION 1
#define configSUPPORT_DYNAMIC_ALLOCATION 1

#define configTICK_RATE_HZ 1000
#define configAPPLICATION_ALLOCATED_HEAP 1
#define configTOTAL_HEAP_SIZE (32 * 1024)

#define configMESSAGE_BUFFER_LENGTH_TYPE uint16_t

/** Stats */
#define configUSE_TRACE_FACILITY 0
#define configGENERATE_RUN_TIME_STATS 0

/** API options */
#define INCLUDE_vTaskPrioritySet 1
#define INCLUDE_uxTaskPriorityGet 1
#define INCLUDE_vTaskDelete 1
#define INCLUDE_vTaskCleanUpResources 1
#define INCLUDE_vTaskSuspend 1
#define INCLUDE_vTaskDelayUntil 1
#define INCLUDE_vTaskDelay 1
#define INCLUDE_xTaskAbortDelay 1
#define INCLUDE_xTaskGetSchedulerState 1
#define INCLUDE_eTaskGetState 1

/** Platform specific */
#define configUSE_PORT_OPTIMISED_TASK_SELECTION 1
#define configUSE_TICKLESS_IDLE 1
#define configCPU_CLOCK_HZ CORE_CLOCK_HZ
/**
 * Number of bits in priority levels.
 */
#define configPRIO_BITS 4
/**
 * The lowest interrupt priority that can be used in
 * a call to a "set priority" function.
 */
#define configLIBRARY_LOWEST_INTERRUPT_PRIORITY 0xf
/**
 * The highest interrupt priority that can be used by any interrupt service
 * routine that makes calls to interrupt safe FreeRTOS API functions.
 * DO NOT CALL INTERRUPT SAFE FREERTOS API FUNCTIONS FROM
 * ANY INTERRUPT THAT HAS A HIGHER PRIORITY THAN THIS!
 * (higher priorities are lower numeric values.
 */
#define configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY 5
#define configKERNEL_INTERRUPT_PRIORITY                                                        \
    (configLIBRARY_LOWEST_INTERRUPT_PRIORITY << (8 - configPRIO_BITS))
#define configMAX_SYSCALL_INTERRUPT_PRIORITY                                                   \
    (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY << (8 - configPRIO_BITS))

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Exception handlers.
 */
void xPortPendSVHandler(void) __attribute__((naked));
void xPortSysTickHandler(void);
void vPortSVCHandler(void) __attribute__((naked));

/**
 * Extra.
 */
typedef enum {
    RtosSourceFileIdentifierUnknown,
    RtosSourceFileIdentifierHeap,
    RtosSourceFileIdentifierPort,
    RtosSourceFileIdentifierTasks,
    RtosSourceFileIdentifierList,
    RtosSourceFileIdentifierQueue,
    RtosSourceFileIdentifierEventGroups,
    RtosSourceFileIdentifierStreamBuffer,
    RtosSourceFileIdentifierTimers,
    RtosSourceFileIdentifierCoRoutine,
} RtosSourceFileIdentifier;

void _rtos_bindings_on_assert(RtosSourceFileIdentifier file_id, int line);
#define _rtos_bindings_on_stack_overflow vApplicationStackOverflowHook
#define _rtos_bindings_get_idle_task_memory vApplicationGetIdleTaskMemory
#define _rtos_bindings_on_idle vApplicationIdleHook
void _rtos_bindings_pre_sleep_processing(void* expected_idle_time);
void _rtos_bindings_post_sleep_processing(void* expected_idle_time);
void _rtos_bindings_on_idle(void);

#define configPRE_SLEEP_PROCESSING(x) _rtos_bindings_pre_sleep_processing(&x)
#define configPOST_SLEEP_PROCESSING(x) _rtos_bindings_post_sleep_processing(&x)
#define configASSERT(x)                                                                        \
    {                                                                                          \
        if (__builtin_expect(((x) == 0), 0)) {                                                 \
            _rtos_bindings_on_assert(RTOS_FILE_ID, __LINE__);                                  \
        }                                                                                      \
    }

// void rtos_print(const char* str);

// #define portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() _rtos_bindings_stats_init()
// #define portGET_RUN_TIME_COUNTER_VALUE() _rtos_bindings_stats_get_time()
// void _rtos_bindings_stats_init();
// uint32_t _rtos_bindings_stats_get_time();

#ifdef __cplusplus
}
#endif

#endif /*MMCC_FREERTOSCONFIG_HPP*/
