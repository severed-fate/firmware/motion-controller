#ifndef MMCC_TIMER_HPP
#define MMCC_TIMER_HPP

#include <cstdint>

#include <libopencm3/stm32/timer.h>

namespace stm32::timer {

enum CkIntDiv {
    DIV1 = TIM_CR1_CKD_CK_INT,
    DIV2 = TIM_CR1_CKD_CK_INT_MUL_2,
    DIV4 = TIM_CR1_CKD_CK_INT_MUL_4
};

enum Alignment {
    Edge = TIM_CR1_CMS_EDGE,
    Center1 = TIM_CR1_CMS_CENTER_1,
    Center2 = TIM_CR1_CMS_CENTER_2,
    Center3 = TIM_CR1_CMS_CENTER_3
};

enum Direction { Up = TIM_CR1_DIR_UP, Down = TIM_CR1_DIR_DOWN };

enum class RunMode { OneShotMode, ContinuousMode };

enum MasterMode {
    Reset = TIM_CR2_MMS_RESET,
    Enable = TIM_CR2_MMS_ENABLE,
    Update = TIM_CR2_MMS_UPDATE,
    ComparePulse = TIM_CR2_MMS_COMPARE_PULSE,
    CompareOc1ref = TIM_CR2_MMS_COMPARE_OC1REF,
    CompareOc2ref = TIM_CR2_MMS_COMPARE_OC2REF,
    CompareOc3ref = TIM_CR2_MMS_COMPARE_OC3REF,
    CompareOc4ref = TIM_CR2_MMS_COMPARE_OC4REF
};

enum class DMATrigger { Compare, Update };

enum OutputChannel {
    OC1 = TIM_OC1,
    OC1N = TIM_OC1N,
    OC2 = TIM_OC2,
    OC2N = TIM_OC2N,
    OC3 = TIM_OC3,
    OC3N = TIM_OC3N,
    OC4 = TIM_OC4
};

enum OutputChannelMode {
    Frozen = TIM_OCM_FROZEN,
    Active = TIM_OCM_ACTIVE,
    Inactive = TIM_OCM_INACTIVE,
    Toggle = TIM_OCM_TOGGLE,
    ForceLow = TIM_OCM_FORCE_LOW,
    ForceHigh = TIM_OCM_FORCE_HIGH,
    Pwm1 = TIM_OCM_PWM1,
    Pwm2 = TIM_OCM_PWM2,
};

enum class OutputChannelPolarity { Low, High, Buffer = High, Invert = Low };

enum Event {
    BG = TIM_EGR_BG,
    TG = TIM_EGR_TG,
    COMG = TIM_EGR_COMG,
    CC4G = TIM_EGR_CC4G,
    CC3G = TIM_EGR_CC3G,
    CC2G = TIM_EGR_CC2G,
    CC1G = TIM_EGR_CC1G,
    UG = TIM_EGR_UG,
};

enum InputChannelFilter {
    OFF = TIM_IC_OFF,
    CK_INT_N_2 = TIM_IC_CK_INT_N_2,
    CK_INT_N_4 = TIM_IC_CK_INT_N_4,
    CK_INT_N_8 = TIM_IC_CK_INT_N_8,
    DTF_DIV_2_N_6 = TIM_IC_DTF_DIV_2_N_6,
    DTF_DIV_2_N_8 = TIM_IC_DTF_DIV_2_N_8,
    DTF_DIV_4_N_6 = TIM_IC_DTF_DIV_4_N_6,
    DTF_DIV_4_N_8 = TIM_IC_DTF_DIV_4_N_8,
    DTF_DIV_8_N_6 = TIM_IC_DTF_DIV_8_N_6,
    DTF_DIV_8_N_8 = TIM_IC_DTF_DIV_8_N_8,
    DTF_DIV_16_N_5 = TIM_IC_DTF_DIV_16_N_5,
    DTF_DIV_16_N_6 = TIM_IC_DTF_DIV_16_N_6,
    DTF_DIV_16_N_8 = TIM_IC_DTF_DIV_16_N_8,
    DTF_DIV_32_N_5 = TIM_IC_DTF_DIV_32_N_5,
    DTF_DIV_32_N_6 = TIM_IC_DTF_DIV_32_N_6,
    DTF_DIV_32_N_8 = TIM_IC_DTF_DIV_32_N_8
};

enum InputChannelPolarity {
    Rising = TIM_ET_RISING,
    Falling = TIM_ET_FALLING,
};

enum SlaveMode {
    OffMode = TIM_SMCR_SMS_OFF,
    EncoderMode1 = TIM_SMCR_SMS_EM1,
    EncoderMode2 = TIM_SMCR_SMS_EM2,
    EncoderMode3 = TIM_SMCR_SMS_EM3,
    ResetMode = TIM_SMCR_SMS_RM,
    GatedMode = TIM_SMCR_SMS_GM,
    TriggerMode = TIM_SMCR_SMS_TM,
    ExternalClockMode1 = TIM_SMCR_SMS_ECM1
};

enum SlaveTrigger {
    InternalTrigger0 = TIM_SMCR_TS_ITR0,
    InternalTrigger1 = TIM_SMCR_TS_ITR1,
    InternalTrigger2 = TIM_SMCR_TS_ITR2,
    InternalTrigger3 = TIM_SMCR_TS_ITR3,
    TI1EdgeDetector = TIM_SMCR_TS_TI1F_ED,
    FilteredTI1 = TIM_SMCR_TS_TI1FP1,
    FilteredTI2 = TIM_SMCR_TS_TI2FP2,
    ExternalTriggerInput = TIM_SMCR_TS_ETRF
};

enum InputChannel { IC1 = TIM_IC1, IC2 = TIM_IC2, IC3 = TIM_IC3, IC4 = TIM_IC4 };

enum InputChannelConf {
    OUT = TIM_IC_OUT,
    IN_TI1 = TIM_IC_IN_TI1,
    IN_TI2 = TIM_IC_IN_TI2,
    IN_TRC = TIM_IC_IN_TRC,
    IN_TI3 = TIM_IC_IN_TI3,
    IN_TI4 = TIM_IC_IN_TI4,
};

template<uint32_t prh> class Timer {
    // TODO: IRQ utils.
   public:
    Timer() = delete;

    static void activate();
    static void deactivate();
    static void generate_event(Event event);
    static bool is_enabled();
    static void enable();
    static void disable();
    static void reset();

    static void set_mode(CkIntDiv div, Alignment alignment, Direction direction);
    static void set_mode(CkIntDiv div);
    static void set_mode(Alignment alignment);
    static void set_mode(Direction direction);
    static void set_mode(RunMode mode);

    static void set_master_mode(MasterMode mode);
    static void set_slave_filter(InputChannelFilter flt);
    static void set_slave_prescaler(uint8_t psc);
    static void set_slave_polarity(InputChannelPolarity pol);
    static void set_slave_mode(SlaveMode mode);
    static void set_slave_trigger(SlaveTrigger trg);

    static void set_interrupt(uint32_t irq_mask, bool enable = true);
    static bool get_interrupt_flags(uint32_t flag, bool clear = true);
    static void set_update_on(bool overflow_only);
    static void set_update_enable(bool enable);
    static void set_dma_trigger(DMATrigger trg);

    static void set_prescaler(uint32_t value);
    static uint32_t get_prescaler();
    static void set_period(uint32_t period);
    static uint32_t get_counter();
    static void set_counter(uint32_t count);
    static void set_preload(bool enable);

    static void set_oc_clear(OutputChannel id, bool enable_clear);
    static void set_oc_speed(OutputChannel id, bool fast_mode_enable);
    static void set_oc_mode(OutputChannel id, OutputChannelMode mode);
    static void set_oc_preload(OutputChannel id, bool enable);
    static void set_oc_polarity(OutputChannel id, OutputChannelPolarity polarity);
    static void set_oc_output(OutputChannel id, bool enable);
    static void set_oc_value(OutputChannel id, uint32_t value);

    static void set_ti1_ch123_xor(bool enable);
    static void set_ic_filter(InputChannel ic, InputChannelFilter flt);
    static void set_ic_prescaler(InputChannel ic, uint8_t psc);
    static void set_ic_input(InputChannel ic, InputChannelConf in);
    static void set_ic_polarity(InputChannel ic, InputChannelPolarity pol);
    static void set_ic(InputChannel ic, bool enable);
    static uint32_t get_ic_value(InputChannel id);

    // void remap(uint32_t option);
};

template<uint32_t prh> class AdvancedTimer : public Timer<prh> {
   public:
    AdvancedTimer() = delete;

    static void set_repetition_counter(uint32_t value);

    static void compare_control_update_on_trigger(bool enable);
    static void set_preload_complementry_enable_bits(bool enable);

    static void set_oc_idle_state(OutputChannel oc_id, bool v);
    static void set_idle_state_in_run_mode(bool enable);
    static void set_idle_state_in_idle_mode(bool enable);

    static void set_break_main_output(bool enable);
    static void break_automatic_output(bool enable);
    static void set_break_polarity(OutputChannelPolarity pol);
    static void set_break(bool enable);
    static void set_break_lock(uint8_t lock_level);
    static void set_deadtime(uint32_t deadtime);
};

}  // namespace stm32::timer

#endif /*MMCC_TIMER_HPP*/