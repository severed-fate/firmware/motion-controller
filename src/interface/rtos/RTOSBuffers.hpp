#ifndef MMCC_RTOSBUFFERS_HPP
#define MMCC_RTOSBUFFERS_HPP

#include <interface/rtos/RTOS.hpp>
#include <utils/Class.hpp>

#include <cstddef>
#include <utility>

#include <FreeRTOS.h>
#include <message_buffer.h>
#include <stream_buffer.h>
#include <utils/macros.h>

namespace rtos {

class StreamBuffer : public meta::INonCopyable {
   protected:
    StreamBufferHandle_t _handle = nullptr;

   public:
    constexpr StreamBuffer() = default;
    StreamBuffer(size_t size, size_t trigger_level) {
        init(size, trigger_level);
    }
    ~StreamBuffer();
    StreamBuffer(StreamBuffer&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    StreamBuffer& operator=(StreamBuffer&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    cold_func_attr bool init(size_t size, size_t trigger_level);
    operator bool() const {
        return _handle != nullptr;
    }

    size_t send_ticks(const void* data, size_t len, TickType_t timeout_ticks = 0) const;
    size_t send_from_isr(const void* data, size_t len) const;
    size_t receive_ticks(void* data, size_t len, TickType_t timeout_ticks = 0) const;
    size_t receive_from_isr(void* data, size_t len) const;

    size_t bytes_available() const;
    size_t spaces_available() const;
    bool set_trigger_level(size_t trigger_level) const;
    bool reset() const;
    bool is_empty() const;
    bool is_full() const;
};

template<size_t N> class StaticStreamBuffer : public StreamBuffer {
   protected:
    StaticStreamBuffer_t _handle_storage;
    uint8_t _data[N];

   public:
    constexpr StaticStreamBuffer() = default;
    StaticStreamBuffer(size_t trigger_level) : StreamBuffer() {
        init(trigger_level);
    }
    cold_func_attr bool init(size_t trigger_level) {
        _handle = xStreamBufferCreateStatic(N, trigger_level, _data, &_handle_storage);
        return _handle != nullptr;
    }

    constexpr size_t max_size() const {
        return N;
    }
};

class MessageBuffer : public meta::INonCopyable {
   protected:
    MessageBufferHandle_t _handle = nullptr;

   public:
    constexpr MessageBuffer() = default;
    MessageBuffer(size_t size) {
        init(size);
    }
    ~MessageBuffer();
    MessageBuffer(MessageBuffer&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    MessageBuffer& operator=(MessageBuffer&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    cold_func_attr bool init(size_t size);
    operator bool() const {
        return _handle != nullptr;
    }

    size_t send_ticks(const void* data, size_t len, TickType_t timeout_ticks = 0) const;
    size_t send_from_isr(const void* data, size_t len) const;
    size_t receive_ticks(void* data, size_t len, TickType_t timeout_ticks = 0) const;
    size_t receive_from_isr(void* data, size_t len) const;
    size_t spaces_available() const;
    bool reset() const;
    bool is_empty() const;
    bool is_full() const;
};

template<size_t N> class StaticMessageBuffer : public MessageBuffer {
   protected:
    StaticMessageBuffer_t _handle_storage;
    uint8_t _data[N];

   public:
    constexpr StaticMessageBuffer() = default;

    cold_func_attr bool init() {
        _handle = xMessageBufferCreateStatic(N, _data, &_handle_storage);
        return _handle != nullptr;
    }

    constexpr size_t max_size() const {
        return N;
    }
};

}  // namespace rtos

#endif /*MMCC_RTOSBUFFERS_HPP*/
