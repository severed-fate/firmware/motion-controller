#include <interface/tcode/Messages.hpp>
#include <interface/tcode/ParserDispatcher.hpp>
#include <utils/Z85.hpp>

#include <cassert>
#include <cstddef>
#include <cstdint>

#include <etl/string_view.h>
#include <etl/to_arithmetic.h>

#define ASSERT_FILE_ID (0x4D37)

namespace tcode {

namespace request {

bool CommandIndex::parse(const char* begin, const char* const sequence_start) {
    cmd = common::char2cmdtyp(begin[0]);
    idx = begin[1] - '0';
    if (cmd == CommandType::Unknown) [[unlikely]] {
        ParserDispatcher::_finalize_response({response::ErrorCode::Tokenization,
                                              static_cast<uint16_t>(begin - sequence_start + 1),
                                              "invalid cmd type"});
        return false;
    }
    if (idx > 9) [[unlikely]] {
        ParserDispatcher::_finalize_response({response::ErrorCode::Tokenization,
                                              static_cast<uint16_t>(begin - sequence_start + 2),
                                              "cmd idx out of bounds"});
        return false;
    }
    return true;
}

bool AxisUpdateData::parse(const char* cmd_begin, const char* value_begin, const char* end,
                           const char* const sequence_start) {
    if (!cmd.parse(cmd_begin, sequence_start)) {
        return false;
    }
    if (!value.parse(value_begin, end)) [[unlikely]] {
        ParserDispatcher::_finalize_response({response::ErrorCode::Tokenization,
                                              static_cast<uint16_t>(end - sequence_start),
                                              "fractional parsing"});
        return false;
    }
    return true;
}

bool IntervalData::parse(const char* begin, const char* end, const char* const sequence_start) {
    auto conv_res = etl::to_arithmetic<uint32_t>(etl::string_view(begin, end));
    if (conv_res.has_value()) {
        interval = conv_res.value();
    }
    else [[unlikely]] {
        ParserDispatcher::_finalize_response({response::ErrorCode::Tokenization,
                                              static_cast<uint16_t>(end - sequence_start),
                                              "number parsing"});
        return false;
    }
    return true;
}

bool SpeedData::parse(const char* begin, const char* end, const char* const sequence_start) {
    auto conv_res = etl::to_arithmetic<uint32_t>(etl::string_view(begin, end));
    if (conv_res.has_value()) {
        speed = conv_res.value();
    }
    else [[unlikely]] {
        ParserDispatcher::_finalize_response({response::ErrorCode::Tokenization,
                                              static_cast<uint16_t>(end - sequence_start),
                                              "number parsing"});
        return false;
    }
    return true;
}

bool Z85Data::parse(const char* begin, const char* end, utils::BufferPoolAllocator& tmp_alloc,
                    const char* const sequence_start) {
    size_t str_len = static_cast<uintptr_t>(end - begin);
    size_t code_count = str_len / 5;
    n = code_count * 4;
    // TODO: alignof(max_align_t) == 8, which might be too wasteful.
    data = (uint8_t*) tmp_alloc.allocate(n, alignof(max_align_t));
    if (data != nullptr) {
        assert(::codec::decode(reinterpret_cast<uint32_t*>(data), code_count,
                               reinterpret_cast<const ::codec::z85_pack_t*>(begin),
                               code_count) == n,
               3);
    }
    else [[unlikely]] {
        ParserDispatcher::_finalize_response({response::ErrorCode::Tokenization,
                                              static_cast<uint16_t>(end - sequence_start),
                                              "z85 decode dest alloc"});
        return false;
    }
    return true;
}

}  // namespace request

namespace parser {

#define parser_checked_parse(parser, token, data)                                              \
    {                                                                                          \
        response::Error err;                                                                   \
        yyParser::State status = parser.parse(token, data, err);                               \
        if (status == yyParser::State::Continue) {                                             \
            continue;                                                                          \
        }                                                                                      \
        else [[unlikely]] {                                                                    \
            err.stream_idx = static_cast<uintptr_t>(pos - sequence_start);                     \
            ParserDispatcher::_finalize_response(err);                                         \
            parser.reset();                                                                    \
            return false;                                                                      \
        }                                                                                      \
    }

bool tokenize(yyParser& parser, const char* const sequence_start, const size_t sequence_len,
              utils::BufferPoolAllocator& tmp_alloc) {
    const char* pos = sequence_start;
    const char* const end_pos = sequence_start + sequence_len;
    assert(*end_pos == '\0', 0);
    assert(*(end_pos - 1) == '\n', 1);
    const char* marked_pos;

    const char *t1, *t2, *t3;
    /*!stags:re2c format = 'const char *@@;\n'; */

    while (true) {
        /*!re2c
            re2c:define:YYCTYPE = char;
            re2c:define:YYCURSOR = pos;
            re2c:define:YYMARKER = marked_pos;
            re2c:define:YYLIMIT = end_pos;
            re2c:tags:prefix = tmp_tag_;
            re2c:yyfill:enable = 0;
            re2c:eof = 0;

            number = [0-9]+;
            z85 = ([\]\-0-9a-zA-Z/[*.:+=^!?&<>(){}@%$#]{5})+;

            command_axis = [LlRrVvAa][0-9];
            command = [LlRrVvAaDd][0-9];
            property_name = [0-9a-z_]+;

            *      {
                size_t idx = static_cast<uintptr_t>(pos - sequence_start);
                ParserDispatcher::_finalize_response({
                    response::ErrorCode::Tokenization,
                    static_cast<uint16_t>(idx), "invalid token"
                });
                parser.reset();
                return false;
            }
            $      {
                response::Error err;
                yyParser::State status = parser.parse(Token::Finalize, err);
                if (status == yyParser::State::Ok) {
                    ParserDispatcher::_finalize_response();
                    return true;
                } else [[unlikely]] {
                    err.stream_idx = static_cast<uintptr_t>(pos - sequence_start);
                    ParserDispatcher::_finalize_response(err);
                    parser.reset();
                    return false;
                }
            }
            '\n'   {
                parser_checked_parse(parser, Token::Exec, {});
                continue;
            }
            [ \t]+ {
                parser_checked_parse(parser, Token::Pad, {});
                continue;
            }
            @t1 command {
                request::CommandIndex cmd_idx;
                if (!cmd_idx.parse(t1, sequence_start)) {
                    parser.reset();
                    return false;
                }
                parser_checked_parse(parser, Token::CmdIdx, cmd_idx);
                continue;
            }
            @t1 command_axis @t2 number @t3 {
                request::AxisUpdateData axis;
                if (!axis.parse(t1, t2, t3, sequence_start)) {
                    parser.reset();
                    return false;
                }
                parser_checked_parse(parser, Token::AxisUpdate, axis);
                continue;
            }
            'I' @t1 number @t2 {
                request::IntervalData itv;
                if (!itv.parse(t1, t2, sequence_start)) {
                    parser.reset();
                    return false;
                }
                parser_checked_parse(parser, Token::Interval, itv);
                continue;
            }
            'S' @t1 number @t2 {
                request::SpeedData sp;
                if (!sp.parse(t1, t2, sequence_start)) {
                    parser.reset();
                    return false;
                }
                parser_checked_parse(parser, Token::Speed, sp); continue;
            }
            'P' @t1 property_name @t2 {
                parser_checked_parse(parser, Token::Property, request::PropertyData(t1, t2));
                continue;
            }
            'Z' @t1 z85 @t2 {
                request::Z85Data dat;
                if (!dat.parse(t1, t2, tmp_alloc, sequence_start)) {
                    parser.reset();
                    return false;
                }
                parser_checked_parse(parser, Token::Z85Data, dat); continue;
            }

            'stop' {
                parser_checked_parse(parser, Token::AxisStop, {});
                continue;
            }
            'dstop' {
                parser_checked_parse(parser, Token::DeviceStop, {});
                continue;
            }
        */
    }
}

}  // namespace parser

}  // namespace tcode
