#ifndef MMCC_RTOSTASK_HPP
#define MMCC_RTOSTASK_HPP

#include <interface/rtos/RTOS.hpp>
#include <utils/Class.hpp>

#include <cstdlib>
#include <utility>

#include <FreeRTOS.h>
#include <etl/delegate.h>
#include <task.h>
#include <utils/macros.h>

namespace rtos {

class iTask : public meta::INonCopyable {
   public:
    using State = eTaskState;
    using NotificationAction = eNotifyAction;

   protected:
    TaskHandle_t _handle;

   public:
    constexpr iTask() : _handle(nullptr) {}
    constexpr iTask(TaskHandle_t handle) : _handle(handle) {}
    iTask(iTask&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    iTask& operator=(iTask&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }
    cold_func_attr ~iTask();

    operator bool() const {
        return _handle != nullptr;
    }

    static void delay_ticks(TickType_t ticks);
    static void delay_ticks_until(TickType_t& previous_wake_time, TickType_t time_increment);
    void abort_delay() const;

    void set_priority(uint prio) const;
    uint get_priority() const;

    void suspend() const;
    void resume() const;
    void stop();

    bool notify(uint index, NotificationAction action, uint32_t value) const;
    bool notify_give(uint index) const {
        return notify(index, NotificationAction::eIncrement, 0);
    }
    static uint32_t notify_take_ticks(uint index, bool clear_count_on_exit,
                                      TickType_t timeout_ticks = portMAX_DELAY);
    std::pair<bool, uint32_t> notify_and_query(uint index, NotificationAction action,
                                               uint32_t value) const;
    std::pair<bool, uint32_t> notify_wait_ticks(uint index, uint32_t clear_mask_on_entry,
                                                uint32_t clear_mask_on_exit,
                                                TickType_t timeout_ticks = portMAX_DELAY) const;
    bool notify_state_clear(uint index) const;
    uint32_t notify_value_clear(uint index, uint32_t clear_mask) const;

    State get_state();
    const char* task_get_name();
};

template<typename T, void (T::*run_cb)()>
cold_func_attr static void _run_dispatcher(void* ptr) {
    // Combine compile-time with runtime information.
    T* obj = reinterpret_cast<T*>(ptr);
    // auto d = etl::delegate<void(void)>::create<T, run_cb>(*obj);
    // Call task's main/run method.
    (obj->*run_cb)();
    // Reaching this point is a programming error, as it results in memory leakage.
    abort();
}

class HeapTask : public iTask {
   public:
    constexpr HeapTask() = default;

   protected:
    template<typename T, void (T::*run_cb)()>
    HeapTask(const char* const name, const uint32_t stack_size, uint priority) : HeapTask() {
        start<T, run_cb>(name, stack_size, priority);
    }

    template<typename T, void (T::*run_cb)()>
    cold_func_attr bool start(const char* const name, const uint32_t stack_size,
                              uint priority) {
        BaseType_t status = xTaskCreate(_run_dispatcher<T, run_cb>, name, stack_size, this,
                                        priority, &_handle);
        if (status != pdPASS) [[unlikely]] {
            _handle = nullptr;
            return false;
        }
        return true;
    }
};

template<size_t N = configMINIMAL_STACK_SIZE> class StaticTask : public iTask {
   protected:
    StaticTask_t _state;
    StackType_t _stack[N];

   public:
    constexpr StaticTask() = default;

   protected:
    template<typename T, void (T::*run_cb)()>
    StaticTask(const char* const name, uint priority) : StaticTask() {
        start<T, run_cb>(name, priority);
    }

    template<typename T, void (T::*run_cb)()>
    cold_func_attr bool start(const char* const name, uint priority) {
        _handle = xTaskCreateStatic(_run_dispatcher<T, run_cb>, name, N, this, priority, _stack,
                                    &_state);
        return _handle != nullptr;
    }
};

}  // namespace rtos

#endif /*MMCC_RTOSTASK_HPP*/