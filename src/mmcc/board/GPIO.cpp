#include "Config.hpp"

#include <interface/io/Core.hpp>
#include <interface/io/GPIO.hpp>

#include <boot.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/syscfg.h>

/* Compensation cell power-down */
#define SYSCFG_CMPCR_CMP_PD (1 << 0)
#define SYSCFG_CMPCR_READY (1 << 8)

void gpio_setup(void) {
    mmcc::InterruptGuard<true> suppressed;

    // Enable peripheral clocks.
    rcc_periph_clock_enable(RCC_SYSCFG);
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);

    // Disable JTAG as early as possible to avoid interference?
    PINSET_CONFIGURE_INPUT_CTX(mmcc::JTAG_PINS, suppressed, false, false);

    // Enable compensation cell and wait until it is active.
    // SYSCFG_CMPCR |= SYSCFG_CMPCR_CMP_PD;
    // while (!(SYSCFG_CMPCR & SYSCFG_CMPCR_READY)) {
    //     nop;
    // }

    // Setup GPIOs.
    /* Leave SWD_PINS to default state */
    /* Leave BOOT1 alone. TODO: Maybe add external pull-down? */
    PINSET_CONFIGURE_OUTPUT_CTX(mmcc::STATUS_LED, suppressed, stm32::PinSet<>::Speed::Medium,
                                false);
    PINSET_SET_ALL_CTX(mmcc::STATUS_LED, suppressed);

    PINSET_CONFIGURE_INPUT_CTX(mmcc::HOME_ESTOP_BUTTON, suppressed, true, false);
    PINSET_CONFIGURE_OUTPUT_CTX(mmcc::HOME_ESTOP_LED, suppressed, stm32::PinSet<>::Speed::Low,
                                false);
    PINSET_RESET_ALL_CTX(mmcc::HOME_ESTOP_LED, suppressed);

    PINSET_CONFIGURE_ALTERNATE_OUTPUT_CTX_PUD(
         mmcc::B2B_PINS, suppressed, stm32::PinSet<>::Speed::High, true, 7, true, false);

    PINSET_CONFIGURE_ALTERNATE_OUTPUT_CTX_PUD(
         mmcc::ACT_PWM_PINS, suppressed, stm32::PinSet<>::Speed::High, false, 1, false, true);
    PINSET_CONFIGURE_OUTPUT_CTX_PUD(mmcc::ACT_CTL_PINS, suppressed,
                                    stm32::PinSet<>::Speed::High, false, false, true);

    PINSET_CONFIGURE_ALTERNATE_INPUT_CTX_PUD(mmcc::ACT0_POS_ENC_PINS, suppressed, 1, true,
                                             false);
    PINSET_CONFIGURE_ALTERNATE_INPUT_CTX_PUD(mmcc::ACT12_POS_PINS, suppressed, 2, true, false);

    PINSET_CONFIGURE_ALTERNATE_OUTPUT_CTX_PUD(
         mmcc::AUX_MOTOR_PINS, suppressed, stm32::PinSet<>::Speed::High, false, 2, false, true);

    PINSET_CONFIGURE_INPUT_CTX(mmcc::SONAR_ECHO_PIN, suppressed, false, true);
    PINSET_CONFIGURE_OUTPUT_CTX_PUD(mmcc::SONAR_TRIG_PIN, suppressed,
                                    stm32::PinSet<>::Speed::Medium, false, false, true);

    PINSET_CONFIGURE_INPUT_CTX(mmcc::UNUSED_PINS, suppressed, false, true);
}
