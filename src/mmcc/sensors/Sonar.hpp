#ifndef MMCC_SONAR_HPP
#define MMCC_SONAR_HPP

#include <interface/rtos/RTOSTask.hpp>

#include <limits>

namespace mmcc {

class SonarResolverTask final : public rtos::StaticTask<384> {
   protected:
    static constexpr uint16_t TIMING_PRESCALER = 145;
    static constexpr float CLOCK_PERIOD = (float) (TIMING_PRESCALER + 1) / AHB_CLOCK_HZ;
    // TODO: Dynamic temperature and pressure compensation.
    static constexpr float SPEED_OF_SOUND = 343;
    static constexpr float MILLIMETER_SCALE_FACTOR = CLOCK_PERIOD * (SPEED_OF_SOUND / 2) * 1000;
    static constexpr uint16_t TRIGGER_INTERVAL = 7;
    static constexpr uint16_t MEASUREMENT_COMPLETE_INTERVAL = 10;

    static constexpr uint16_t MAX_VALID_RAW_MEASUREMENT = 12778;

   protected:
    bool _scanning = false;
    bool _pending_filter_reset = false;
    uint16_t _latest_valid_measurement = std::numeric_limits<uint16_t>::max();
    float _latest_valid_measurement_scaled = 0;
    float _latest_valid_measurement_filtered = 0;
    float _latest_valid_measurement_normalized = 0;
    size_t _invalid_measurement_counter = 0;

    static bool _measurement_mode;
    static std::pair<bool, bool> _measurement_noise;
    static std::pair<uint16_t, uint16_t> _measurement;

   public:
    constexpr SonarResolverTask() = default;
    cold_func_attr ~SonarResolverTask();

    cold_func_attr void init();
    cold_func_attr bool start();
    void run();

    bool is_scanning() const {
        return _scanning;
    }
    void start_scanning();
    void stop_scanning();

    uint16_t get_measurement_raw() const {
        return _latest_valid_measurement;
    }
    float get_measurement_scaled() const {
        return _latest_valid_measurement_scaled;
    }
    float get_measurement_filtered() const {
        return _latest_valid_measurement_filtered;
    }
    float get_measurement_normalized() const {
        return _latest_valid_measurement_normalized;
    }
    size_t get_invalid_measurement_count() const {
        return _invalid_measurement_counter;
    }

    void pend_filter_reset() {
        _pending_filter_reset = true;
    }

   private:
    static void __timer_handler();
    static void __echo_signal_handler();

    static bool _is_measurement_valid();
    void _update_measurement();
};

}  // namespace mmcc

#endif /*MMCC_SONAR_HPP*/