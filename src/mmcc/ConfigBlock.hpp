#ifndef MMCC_CONFIGBLOCK_HPP
#define MMCC_CONFIGBLOCK_HPP
/**
 * @file
 * @brief A virtual eeprom implementation of fixed-size blocks.
 */

#include <interface/io/Flash.hpp>
#include <interface/rtos/RTOSMutex.hpp>
#include <mmcc/board/Config.hpp>
#include <utils/Class.hpp>

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <utility>

#include <etl/bitset.h>

namespace mmcc {

static constexpr size_t CONFIG_BLOCK_SIZE = 1024;

struct ActuatorConfigSubBlock {
    float pwm_deadband;
    float homing_speed;
    float axis_limits_min;
    float axis_limits_max;
    float pid_kp;
    float pid_ki;
    float pid_kd;
    float pid_bias;

    constexpr ActuatorConfigSubBlock(float pwm_deadband_, float homing_speed_,
                                     float axis_limits_min_, float axis_limits_max_,
                                     float pid_kp_, float pid_ki_, float pid_kd_,
                                     float pid_bias_) noexcept :
        pwm_deadband(pwm_deadband_),
        homing_speed(homing_speed_), axis_limits_min(axis_limits_min_),
        axis_limits_max(axis_limits_max_), pid_kp(pid_kp_), pid_ki(pid_ki_), pid_kd(pid_kd_),
        pid_bias(pid_bias_) {}

    constexpr ActuatorConfigSubBlock(float pwm_deadband_, float homing_speed_, float pid_kp_,
                                     float pid_ki_, float pid_kd_, float pid_bias_) noexcept :
        pwm_deadband(pwm_deadband_),
        homing_speed(homing_speed_), axis_limits_min(0.f), axis_limits_max(1.f),
        pid_kp(pid_kp_), pid_ki(pid_ki_), pid_kd(pid_kd_), pid_bias(pid_bias_) {}

    constexpr ActuatorConfigSubBlock(float pwm_deadband_ = 0, float homing_speed_ = 0.8f,
                                     float axis_limits_min_ = 0.f,
                                     float axis_limits_max_ = 1.f) noexcept :
        pwm_deadband(pwm_deadband_),
        homing_speed(homing_speed_), axis_limits_min(axis_limits_min_),
        axis_limits_max(axis_limits_max_), pid_kp(1.f), pid_ki(0.f), pid_kd(0.f),
        pid_bias(0.f) {}
};

struct ActuatorConfigAuxBlock {
    float pwm_bias;
    float pwm_deadband;
    float axis_limits_min;
    float axis_limits_max;

    constexpr ActuatorConfigAuxBlock(float pwm_bias_ = 0, float pwm_deadband_ = 0,
                                     float axis_limits_min_ = 0.f,
                                     float axis_limits_max_ = 1.f) noexcept :
        pwm_bias(pwm_bias_),
        pwm_deadband(pwm_deadband_), axis_limits_min(axis_limits_min_),
        axis_limits_max(axis_limits_max_) {}
};

struct ActuatorConfigBlock {
    uint16_t pwm_prescaler = 132;
    bool plot_enable : 1 = false;
    bool home_enable : 1 = true;
    bool __reserved0 : 1 = true;
    bool act0_disengage_break : 1 = false;
    bool act1_disengage_break : 1 = true;
    bool act2_disengage_break : 1 = true;
    uint8_t __reserved1 : 2 = 0x3;
    uint8_t __reserved2 = 0xff;

    ActuatorConfigSubBlock act0 = {0.15f, 0.44f, 0.38f, 0.98f, 9.f, 8.8f, 0.24f, 0.f};
    ActuatorConfigSubBlock act1 = {0.08f, 0.25f, 0.35f, 0.65f, 6.2f, 4.8f, 0.144f, 0.f};
    ActuatorConfigSubBlock act2 = {0.08f, 0.25f, 0.3f, 0.7f, 6.2f, 4.8f, 0.144f, 0.f};

    ActuatorConfigAuxBlock aux1 = {0.11f, 0.f, 0.f, 1.f};
    ActuatorConfigAuxBlock aux2 = {0.11f, 0.f, 0.f, 1.f};

    uint32_t __reserved3[7] = {UINT32_MAX, UINT32_MAX, UINT32_MAX, UINT32_MAX,
                               UINT32_MAX, UINT32_MAX, UINT32_MAX};

    constexpr ActuatorConfigBlock() noexcept = default;
};

struct SonarConfigBlock {
    float near = 35.f;
    float far = 250.f;
    // Thresholds are relative to abs(far - near).
    float threshold1 = 0.66f;
    float threshold2 = 1.0f;
    /**
     * @brief Determines the weighting of the threshold.
     *
     * Values close to 1 prioritize the difference between current and previous scaled inputs.
     * Values close to 0 prioritize the difference between current scaled input and previous
     * filtered output.
     */
    float threshold_selector = 1.0f;
    // Factors for adaptive ema filter.
    float alpha1 = 0.95f;
    float alpha2 = 0.05f;
    bool invert : 1 = false;
    uint8_t __reserved1 : 7 = 0x7f;
    uint8_t __reserved2[3] = {UINT8_MAX, UINT8_MAX, UINT8_MAX};

    constexpr SonarConfigBlock() noexcept = default;
};

struct ConfigBlockHeader {
    uint16_t crc = 0;
    uint16_t ncrc = 0xffff;

    constexpr ConfigBlockHeader() noexcept = default;

    constexpr ConfigBlockHeader(uint16_t crc_) noexcept : crc(crc_), ncrc(~crc_) {}

    constexpr bool verify() const {
        return crc == static_cast<uint16_t>(~ncrc);
    }
};

struct ConfigBlockData {
    // Block write count tracker.
    size_t counter = 0;
    uint32_t __reserved0[6] = {UINT32_MAX, UINT32_MAX, UINT32_MAX,
                               UINT32_MAX, UINT32_MAX, UINT32_MAX};
    ActuatorConfigBlock actuator;
    SonarConfigBlock sonar;

    constexpr ConfigBlockData() noexcept = default;

    uint16_t calc_crc() const;
};

struct ConfigBlock {
    ConfigBlockHeader header;
    ConfigBlockData data;

    static constexpr size_t PAYLOAD_SIZE = sizeof(ConfigBlockHeader) + sizeof(ConfigBlockData);
    uint8_t _padding[CONFIG_BLOCK_SIZE - PAYLOAD_SIZE];

    constexpr ConfigBlock() noexcept : header(), data() {
        std::fill_n(_padding, sizeof(_padding), 0xff);
    }

    bool is_valid() const;
    bool is_free() const;
};

static_assert(sizeof(ConfigBlock) == CONFIG_BLOCK_SIZE, "Incorrect mmcc::ConfigBlock size! "
                                                        "Please check the padding.");

class Config : public meta::INonCopyable {
   public:
    class ConfigBlockRef : public meta::INonCopyable {
       protected:
        ConfigBlockData* _dat = nullptr;
        rtos::StaticMutex<false>* _mtx = nullptr;

       protected:
        ConfigBlockRef(ConfigBlockData& dat, rtos::StaticMutex<false>& mtx) noexcept;

        friend class Config;

       public:
        constexpr ConfigBlockRef() noexcept = default;

        constexpr ConfigBlockRef(ConfigBlockRef&& o) noexcept :
            _dat(std::exchange(o._dat, nullptr)), _mtx(std::exchange(o._mtx, nullptr)) {}
        constexpr ConfigBlockRef& operator=(ConfigBlockRef&& o) noexcept {
            _dat = std::exchange(o._dat, nullptr);
            _mtx = std::exchange(o._mtx, nullptr);
            return *this;
        }

        ~ConfigBlockRef();

        constexpr ConfigBlockData& operator*() const noexcept {
            return *_dat;
        }
        constexpr ConfigBlockData* operator->() const noexcept {
            return _dat;
        }
    };

   protected:
    static constexpr std::array<sector_index_t, 3> AVAILABLE_FLASH_SECTORS = {1, 2, 3};
    static constexpr size_t SECTOR_COMMON_SIZE =
         FLASH_MODULE_MAP[*std::min_element(AVAILABLE_FLASH_SECTORS.begin(),
                                            AVAILABLE_FLASH_SECTORS.end(),
                                            [](int si_a, int si_b) {
                                                return FLASH_MODULE_MAP[si_a].second <
                                                       FLASH_MODULE_MAP[si_b].second;
                                            })]
              .second;
    static constexpr size_t SECTOR_BLOCK_COUNT = SECTOR_COMMON_SIZE / CONFIG_BLOCK_SIZE;
    static_assert(SECTOR_BLOCK_COUNT == 16, "Config's virtual eeprom is implemented "
                                            "only for 16kB sectors.");

    std::array<etl::bitset<16, int16_t>, AVAILABLE_FLASH_SECTORS.size()> _free_blocks;
    std::array<etl::bitset<16, int16_t>, AVAILABLE_FLASH_SECTORS.size()> _valid_blocks;

    size_t _state_available_flash_sector_index = SIZE_MAX;
    size_t _state_block_index = SIZE_MAX;
    ConfigBlockData _state;
    rtos::StaticMutex<false> _state_mutex;

   public:
    constexpr Config() noexcept = default;

    void init();

    ConfigBlockRef get_current_state();
    bool commit_current_state();

   private:
    bool is_block_free(size_t avail_sect_idx, size_t bi) const;
    bool is_block_valid(size_t avail_sect_idx, size_t bi) const;
    bool is_block_invalid(size_t avail_sect_idx, size_t bi) const;

    bool write_state();
};

}  // namespace mmcc

#endif /*MMCC_CONFIGBLOCK_HPP*/