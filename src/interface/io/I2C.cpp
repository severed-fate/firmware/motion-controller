#include "I2C.hpp"

#include <functional>

#include <libopencm3/stm32/rcc.h>

namespace stm32 {

template<uintptr_t port> constexpr rcc_periph_clken _port2rcc();
template<> constexpr rcc_periph_clken _port2rcc<I2C1>() {
    return RCC_I2C1;
}
template<> constexpr rcc_periph_clken _port2rcc<I2C2>() {
    return RCC_I2C2;
}
template<> constexpr rcc_periph_clken _port2rcc<I2C3>() {
    return RCC_I2C3;
}
template<uintptr_t port> constexpr rcc_periph_rst _port2rst();
template<> constexpr rcc_periph_rst _port2rst<I2C1>() {
    return RST_I2C1;
}
template<> constexpr rcc_periph_rst _port2rst<I2C2>() {
    return RST_I2C2;
}
template<> constexpr rcc_periph_rst _port2rst<I2C3>() {
    return RST_I2C3;
}

template<uint32_t prh>
I2C<prh>::Transaction::Transaction(I2C<prh>& unit) :
    _unit(std::ref(unit)), _receiving(false), _valid(true) {}
template<uint32_t prh> I2C<prh>::Transaction::~Transaction() {
    if (_valid) {
        end();
    }
}
template<uint32_t prh> bool I2C<prh>::Transaction::start() {
    if (!_valid) {
        return false;
    }
    i2c_send_start(prh);
    while (true) {
        const auto I2C_SR2_MSL_BUSY = I2C_SR2_MSL | I2C_SR2_BUSY;
        if (I2C_SR1(prh) & I2C_SR1_SB &&
            (I2C_SR2(prh) & I2C_SR2_MSL_BUSY) == I2C_SR2_MSL_BUSY) {
            // If start bit send and we are the master while the line is busy:
            return true;
        }
    }
}
template<uint32_t prh>
bool I2C<prh>::Transaction::address(uint8_t addr, bool write, float timeout_ms) {
    if (!_valid) {
        return false;
    }
    _receiving = !write;
    auto timeout_ticks = rtos::ms2ticks(timeout_ms);
    auto start_ticks = rtos::Kernel::get_tickcount();
    i2c_send_7bit_address(prh, addr, write ? I2C_WRITE : I2C_READ);
    while (true) {
        if (I2C_SR1(prh) & I2C_SR1_ADDR) {
            // If address accepted by a device:
            return true;
            // ^- NOTE: ADDR is not cleared here, we hold the bus until
            // all the registers are configured (important when receiving).
        }
        auto current_ticks = rtos::Kernel::get_tickcount();
        auto elapsed_ticks = current_ticks - start_ticks;
        if (elapsed_ticks >= timeout_ticks) {
            return false;
        }
    }
}
template<uint32_t prh> size_t I2C<prh>::Transaction::send(const uint8_t* data, const size_t n) {
    if (!_valid || _receiving || n == 0) {
        return 0;
    }
    // Clear ADDR and resume communication.
    I2C_SR2(prh);
    size_t i = 0;
    while (i < n) {
        if (I2C_SR1(prh) & I2C_SR1_TxE) {
            // DR ready.
            i2c_send_data(prh, data[i]);
            i++;
        }
        if (I2C_SR1(prh) & I2C_SR1_AF) {
            // NACK received.
            return i;
        }
    }
    while (!(I2C_SR1(prh) & (I2C_SR1_BTF | I2C_SR1_AF))) {
        // Wait for transfer completion or NACK on last byte.
    }
    return n;
}
template<uint32_t prh> size_t I2C<prh>::Transaction::receive(uint8_t* data, const size_t n) {
    if (!_valid || !_receiving) {
        return 0;
    }
    if (n == 0) {
        i2c_disable_ack(prh);
        I2C_SR2(prh);
        i2c_send_stop(prh);
        return 0;
    }
    else if (n == 1) {
        // Configure for one byte.
        i2c_disable_ack(prh);
        i2c_nack_current(prh);
        // Clear ADDR and resume communication.
        I2C_SR2(prh);
        while (true) {
            if (I2C_SR1(prh) & I2C_SR1_RxNE) {
                i2c_send_stop(prh);
                data[0] = i2c_get_data(prh);
                return n;
            }
        }
    }
    else if (n == 2) {
        i2c_disable_ack(prh);
        i2c_nack_next(prh);
        // Clear ADDR and resume communication.
        I2C_SR2(prh);
        while (true) {
            if (I2C_SR1(prh) & I2C_SR1_BTF) {
                i2c_send_stop(prh);
                data[0] = i2c_get_data(prh);
                data[1] = i2c_get_data(prh);
                return n;
            }
        }
    }
    else {
        i2c_enable_ack(prh);
        i2c_nack_current(prh);
        // Clear ADDR and resume communication.
        I2C_SR2(prh);
        for (size_t i = 0; i < n - 3; i++) {
            while (!(I2C_SR1(prh) & I2C_SR1_RxNE)) {
                // Wait until a byte is received.
            }
            data[i] = i2c_get_data(prh);
        }
        // Special sequence for receiving last 3 bytes.
        while (!(I2C_SR1(prh) & I2C_SR1_BTF)) {
            // Wait until two bytes are received.
        }
        i2c_disable_ack(prh);
        data[n - 3] = i2c_get_data(prh);
        i2c_send_stop(prh);
        data[n - 2] = i2c_get_data(prh);
        while (!(I2C_SR1(prh) & I2C_SR1_RxNE)) {
            // Wait until the last byte is received.
        }
        data[n - 1] = i2c_get_data(prh);
        return n;
    }
}
template<uint32_t prh> bool I2C<prh>::Transaction::stop() {
    if (!_valid) {
        return false;
    }
    if (!_receiving) {
        // Stop condition not sent by send().
        i2c_send_stop(prh);
    }
    while (I2C_CR1(prh) & I2C_CR1_STOP) {
        // Wait until the hardware 'executes' the stop.
    }
    return true;
}
template<uint32_t prh> void I2C<prh>::Transaction::end() {
    _unit.get().end_transaction(*this);
    _valid = false;
}

template<uint32_t prh> I2C<prh>::I2C() {
    rcc_periph_reset_release(_port2rst<prh>());
    rcc_periph_clock_enable(_port2rcc<prh>());
}
template<uint32_t prh> I2C<prh>::~I2C() {
    rcc_periph_reset_hold(_port2rst<prh>());
    rcc_periph_clock_disable(_port2rcc<prh>());
}
template<uint32_t prh> I2C<prh>& I2C<prh>::get_instance() {
    static I2C<prh> _instance;
    return _instance;
}

template<uint32_t prh> void I2C<prh>::set_speed(Mode speed, uint32_t apb_mhz) {
    while (!_transaction_lock.take_ticks()) {
        /* NOP */
    }
    i2c_set_speed(prh, speed, apb_mhz);
    while (!_transaction_lock.give()) {
        /* NOP */
    }
}
template<uint32_t prh> typename I2C<prh>::Transaction I2C<prh>::start_transaction() {
    // Acquire.
    while (!_transaction_lock.take_ticks()) {
        /* NOP */
    }
    i2c_peripheral_enable(prh);
    Transaction tr(*this);
    return tr;
}
template<uint32_t prh> void I2C<prh>::end_transaction(Transaction&) {
    // Also resets any status bits left uncleared.
    i2c_peripheral_disable(prh);
    // Release.
    while (!_transaction_lock.give()) {
        /* NOP */
    }
}

}  // namespace stm32

template class stm32::I2C<I2C1>;
template class stm32::I2C<I2C2>;
template class stm32::I2C<I2C3>;
