#ifndef MMCC_RTOSQUEUE_HPP
#define MMCC_RTOSQUEUE_HPP

#include <interface/rtos/RTOS.hpp>
#include <utils/Class.hpp>

#include <cstdint>
#include <optional>
#include <utility>

#include <FreeRTOS.h>
#include <queue.h>
#include <utils/macros.h>

namespace rtos {

template<typename T> class Queue : public meta::INonCopyable {
   protected:
    QueueHandle_t _handle = nullptr;

   public:
    constexpr Queue() = default;
    Queue(uint32_t length) {
        init(length);
    }
    Queue(uint32_t length, const char* name) {
        init(length, name);
    }
    cold_func_attr ~Queue() {
        if (_handle != nullptr) {
            vQueueDelete(_handle);
            unregister_queue();
            _handle = nullptr;
        }
    }
    Queue(Queue&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    Queue& operator=(Queue&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    cold_func_attr bool init(uint32_t length) {
        _handle = xQueueCreate(length, sizeof(T));
        return _handle != nullptr;
    }
    cold_func_attr bool init(uint32_t length, const char* name) {
        if (init(length)) {
            add_to_registry(name);
            return true;
        }
        return false;
    }
    operator bool() const {
        return _handle != nullptr;
    }

    bool send_to_back_ticks(T& v, TickType_t timeout_ticks = 0) const {
        return xQueueSendToBack(_handle, &v, timeout_ticks) == pdTRUE;
    }
    bool send_to_back_from_isr(T& v) const {
        BaseType_t task_switch = 0;
        auto r = xQueueSendToBackFromISR(_handle, &v, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
        return r == pdTRUE;
    }
    bool send_to_front_ticks(T& v, TickType_t timeout_ticks = 0) const {
        return xQueueSendToFront(_handle, &v, timeout_ticks) == pdTRUE;
    }
    bool send_to_front_from_isr(T& v) const {
        BaseType_t task_switch = 0;
        auto r = xQueueSendToFrontFromISR(_handle, &v, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
        return r == pdTRUE;
    }

    std::optional<T> receive_ticks(TickType_t timeout_ticks = portMAX_DELAY) const {
        T v;
        bool ok = xQueueReceive(_handle, &v, timeout_ticks) == pdTRUE;
        if (ok) {
            return v;
        }
        else {
            return {};
        }
    }
    std::optional<T> receive_from_isr() const {
        BaseType_t task_switch = 0;
        T v;
        bool ok = xQueueReceiveFromISR(_handle, &v, &task_switch) == pdTRUE;
        if (task_switch) {
            taskYIELD();
        }
        if (ok) {
            return v;
        }
        else {
            return {};
        }
    }

    uint32_t messages_waiting() const {
        return uxQueueMessagesWaiting(_handle);
    }
    uint32_t messages_waiting_from_isr() const {
        return uxQueueMessagesWaitingFromISR(_handle);
    }

    uint32_t spaces_available() const {
        return uxQueueSpacesAvailable(_handle);
    }
    void reset() const {
        xQueueReset(_handle);
    }

    void overwrite(T& v) const {
        xQueueOverwrite(_handle, &v);
    }
    void overwrite_from_isr(T& v) const {
        BaseType_t task_switch = 0;
        xQueueOverwriteFromISR(_handle, &v, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
    }

    std::optional<T> peek_ticks(TickType_t timeout_ticks = 0) const {
        T v;
        bool ok = xQueuePeek(_handle, &v, timeout_ticks) == pdTRUE;
        if (ok) {
            return v;
        }
        else {
            return {};
        }
    }
    std::optional<T> peek_from_isr() const {
        T v;
        bool ok = xQueuePeekFromISR(_handle, &v) == pdTRUE;
        if (ok) {
            return v;
        }
        else {
            return {};
        }
    }

    void add_to_registry([[maybe_unused]] const char* name) const {
        vQueueAddToRegistry(_handle, name);
    }
    void unregister_queue() const {
        vQueueUnregisterQueue(_handle);
    }
    const char* get_name() const {
        return pcQueueGetName(_handle);
    }

    bool is_queue_full_from_isr() const {
        return xQueueIsQueueFullFromISR(_handle) != pdFALSE;
    }
    bool is_queue_empty_from_isr() const {
        return xQueueIsQueueEmptyFromISR(_handle) != pdFALSE;
    }
};

template<typename T, size_t N> class StaticQueue : public Queue<T> {
   protected:
    StaticQueue_t _handle_storage;
    T _element_storage[N];

   public:
    constexpr StaticQueue(std::nullopt_t) : Queue<T>() {}

    StaticQueue() {
        init();
    }

    StaticQueue(const char* name) {
        init(name);
    }

    cold_func_attr bool init() {
        Queue<T>::_handle =
             xQueueCreateStatic(N, sizeof(T), (uint8_t*) _element_storage, &_handle_storage);
        return Queue<T>::_handle != nullptr;
    }
    cold_func_attr bool init(const char* name) {
        if (init()) {
            Queue<T>::add_to_registry(name);
            return true;
        }
        return false;
    }
};

}  // namespace rtos

#endif /*MMCC_RTOSQUEUE_HPP*/
