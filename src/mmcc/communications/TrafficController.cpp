#include "TrafficController.hpp"

#include <interface/rtos/RTOS.hpp>
#include <interface/tcode/TCode.hpp>
#include <mmcc/Objects.hpp>
#include <mmcc/communications/TCodeConfig.hpp>

#include <algorithm>
#include <cassert>
#include <cstdlib>

#define ASSERT_FILE_ID (0xA841)

namespace mmcc {

void TrafficController::init() {}
bool TrafficController::start() {
    return rtos::StaticTask<768>::start<TrafficController, &TrafficController::run>("com_ctl",
                                                                                    3);
}

void TrafficController::run() {
    while (true) {
        switch (_connection_state) {
            case Connection::Disconnected: {
                wait_for_connection();
            } break;
            case Connection::ConnectedUSB:
            case Connection::ConnectedNet: {
                handle_communication();
            } break;
        }
    }
}

void TrafficController::wait_for_connection() {
    // Do not receive any data here, but we still need to provide a valid buffer.
    size_t staging_buffer_len =
         usb_task.receive_ticks(*this, _staging_buffer, 0, 1, rtos::ms2ticks(3000));
    assert(staging_buffer_len == 0, 0);

    bool usb_user_connected =
         usb_task.get_connection_state() == USBTask::ConnectionState::UserConnected;
    if (usb_user_connected) {
        reset();
        _connection_state = Connection::ConnectedUSB;
        return;
    }

    // TODO: Test for Net.
}

void TrafficController::handle_communication() {
    // Handle timeouts.
    TickType_t timeout_tick = handle_timeouts();
    TickType_t current_tick = rtos::Kernel::get_tickcount();
    TickType_t timeout;
    if (current_tick >= timeout_tick) {
        // Passed timeout.
        timeout = 0;
    }
    else /* timeout_tick > current_tick */ [[likely]] {
        timeout = std::min(timeout_tick - current_tick, rtos::ms2ticks(333));
    }
    // Wait for data or event from active connection.
    size_t staging_buffer_len = receive(timeout);
    if (_connection_state != Connection::Disconnected) {
        // Pass data to ParserDispatcher.
        bool ok = handle_input(_staging_buffer, staging_buffer_len);
        if (!ok) {
            /**
             * Reset connection on parser or callback error.
             *
             * This is not required by all types of errors,
             * but it is difficult to always ensure correct state.
             */
            disconnect();
        }
    }
}

size_t TrafficController::receive(TickType_t timeout_ticks) {
    if (_connection_state == Connection::ConnectedUSB) {
        size_t staging_buffer_len = usb_task.receive_ticks(
             *this, _staging_buffer, sizeof(_staging_buffer), 1, timeout_ticks);
        // Check if we are still connected.
        bool usb_user_connected =
             usb_task.get_connection_state() == USBTask::ConnectionState::UserConnected;
        if (!usb_user_connected) [[unlikely]] {
            _connection_state = Connection::Disconnected;
            on_disconnect();
            return 0;
        }
        return staging_buffer_len;
    }
    else {
        std::abort();
    }
}
size_t TrafficController::send(const void* data, size_t n) {
    if (_connection_state == Connection::ConnectedUSB) {
        size_t sent = 0;
        while (sent < n) {
            sent += usb_task.send_ticks(((uint8_t*) data) + sent, n - sent, 10);
        }
        return sent;
    }
    else {
        mmcc::dbg.print("TrafficController::send: not connected");
        std::abort();
    }
}

void TrafficController::disconnect() {
    if (_connection_state == Connection::ConnectedUSB) {
        usb_task.disconnect(true);
        on_disconnect();
        do {
            delay_ticks(rtos::ms2ticks(100));
        } while (usb_task.get_connection_state() != USBTask::ConnectionState::PhyDisconnected);
        _connection_state = Connection::Disconnected;
        usb_task.disconnect(false);
    }
    else {
        mmcc::dbg.print("TrafficController::disconnect: not connected");
        std::abort();
    }
}

void TrafficController::on_disconnect() {
    tcode::callbacks::stop_all();
    if (sonar_task.is_scanning()) {
        sonar_task.stop_scanning();
    }
    // Parser reset happens on new connection.
}

}  // namespace mmcc
