#ifndef MMCC_TCODE_MESSAGES_HPP
#define MMCC_TCODE_MESSAGES_HPP

#include <interface/tcode/Utils.hpp>
#include <mapbox/eternal.hpp>
#include <utils/Allocators.hpp>

#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <limits>

#include <etl/string_view.h>

namespace tcode {

namespace common {

/** Supported types of command prefixes - axis and others combined. */
enum class CommandType : uint8_t {
    /* Meta */
    Unknown,
    /* Axis */
    Linear,
    Rotate,
    Vibrate,
    Auxiliary,
    /* Others */
    Device
};

inline constexpr CommandType char2cmdtyp(char c) {
    switch (c) {
        /* Axis */
        case 'L':
        case 'l': return CommandType::Linear;
        case 'R':
        case 'r': return CommandType::Rotate;
        case 'V':
        case 'v': return CommandType::Vibrate;
        case 'A':
        case 'a': return CommandType::Auxiliary;
        case 'D':
        case 'd': return CommandType::Device;
        default: return CommandType::Unknown;
    }
}

inline constexpr char cmdtyp2char(CommandType cmd) {
    switch (cmd) {
        case CommandType::Linear: return 'L';
        case CommandType::Rotate: return 'R';
        case CommandType::Vibrate: return 'V';
        case CommandType::Auxiliary: return 'A';
        case CommandType::Device: return 'D';
        case CommandType::Unknown:
        default: return '\0';
    }
}

struct CommandIndex {
    CommandType cmd = CommandType::Unknown;
    int8_t idx = -1;

    constexpr CommandIndex() = default;
    constexpr CommandIndex(CommandType cmd_, int8_t idx_) : cmd(cmd_), idx(idx_) {}

    constexpr operator uint16_t() const {
        return (static_cast<uint16_t>(cmd) << 8) | static_cast<uint16_t>(idx);
    }

    constexpr bool operator<(const CommandIndex& rhs) const {
        return static_cast<uint16_t>(*this) < static_cast<uint16_t>(rhs);
    }
    constexpr bool operator==(const CommandIndex& rhs) const {
        return static_cast<uint16_t>(*this) == static_cast<uint16_t>(rhs);
    }
    constexpr size_t hash() const {
        // TODO: Compare different methods, regarding performance and collisions.
        char cmd_char = cmdtyp2char(cmd);
        char idx_char = '0' + idx;
        auto value = mapbox::eternal::impl::hash_offset;
        constexpr auto prime = mapbox::eternal::impl::hash_prime;
        value = (value ^ static_cast<size_t>(cmd_char)) * prime;
        value = (value ^ static_cast<size_t>(idx_char)) * prime;
        return value;
    }

    etl::array<char, 2> to_string() const {
        return {cmdtyp2char(cmd), static_cast<char>(idx + '0')};
    }
};

struct PropertyData {
    const char* name_begin = nullptr;
    const char* name_end = nullptr;

    constexpr PropertyData() = default;
    constexpr PropertyData(const char* begin, const char* end) :
        name_begin(begin), name_end(end) {}
    template<std::size_t N> constexpr PropertyData(const char (&str)[N]) {
        name_begin = &str[0];
        name_end = &str[N - 1];
    }
    constexpr PropertyData(etl::string_view str) {
        name_begin = str.cbegin();
        name_end = str.cend();
    }
};

}  // namespace common

namespace request {

using common::CommandType;

struct CommandIndex : public common::CommandIndex {
    constexpr CommandIndex() = default;

    bool parse(const char* begin, const char* const sequence_start);
};
struct AxisUpdateData {
    CommandIndex cmd = {};
    fractional<uint32_t> value = {};

    constexpr AxisUpdateData() = default;
    bool parse(const char* cmd_begin, const char* value_begin, const char* end,
               const char* const sequence_start);
};
struct IntervalData {
    uint32_t interval = 0;

    constexpr IntervalData() = default;
    bool parse(const char* begin, const char* end, const char* const sequence_start);
};
struct SpeedData {
    uint32_t speed = 0;

    constexpr SpeedData() = default;
    bool parse(const char* begin, const char* end, const char* const sequence_start);
};
using common::PropertyData;
struct Z85Data {
    /** Decoded - binary data. */
    uint8_t* data = nullptr;
    /** Size in bytes. */
    size_t n = 0;

    constexpr Z85Data() = default;
    bool parse(const char* begin, const char* end, utils::BufferPoolAllocator& tmp_alloc,
               const char* const sequence_start);
};

union TokenData {
    CommandIndex cmd_idx;
    AxisUpdateData axis_updt;
    IntervalData interval;
    SpeedData speed;
    PropertyData prop;
    Z85Data z85;

    constexpr TokenData() : cmd_idx() {}
    constexpr TokenData(CommandIndex cmd_idx_) noexcept : cmd_idx(cmd_idx_) {}
    constexpr TokenData(AxisUpdateData axis_updt_) noexcept : axis_updt(axis_updt_) {}
    constexpr TokenData(IntervalData interval_) noexcept : interval(interval_) {}
    constexpr TokenData(SpeedData speed_) noexcept : speed(speed_) {}
    constexpr TokenData(PropertyData prop_) noexcept : prop(prop_) {}
    constexpr TokenData(Z85Data z85_) noexcept : z85(z85_) {}
};

}  // namespace request

namespace response {

using common::CommandIndex;
using common::CommandType;
using common::PropertyData;
struct Z85Data : public request::Z85Data {
    constexpr Z85Data() = default;
    constexpr Z85Data(const uint8_t* data_, size_t n_) {
        data = const_cast<uint8_t*>(data_);
        n = n_;
    }
    constexpr Z85Data(const char* str_begin, const char* str_end) {
        data = (uint8_t*) str_begin;
        n = static_cast<uintptr_t>(str_end - str_begin);
    }
    template<std::size_t N> constexpr Z85Data(const char (&str)[N]) {
        data = (uint8_t*) str;
        n = static_cast<uintptr_t>(N - 1);
    }
    constexpr Z85Data(etl::string_view str) {
        data = (uint8_t*) str.data();
        n = str.size();
    }
};

enum class ErrorCode : uint8_t {
    Success = 0,
    Tokenization,
    Parsing,
    Allocation,
    InvalidCommandIndex,
    UnknownProperty,
    InvalidOperation,
    Generic = 9
};

struct Error {
    ErrorCode code = ErrorCode::Success;
    uint8_t extra_data = 0;
    uint16_t stream_idx = std::numeric_limits<uint16_t>::max();
    /** NOTE: any form of dynamic string is disallowed */
    const char* extra_msg = nullptr;

    /** 0 byte payload */
    constexpr Error() = default;
    constexpr Error(ErrorCode code_) : code(code_) {}
    /** 4 byte payload */
    constexpr Error(ErrorCode code_, uint16_t stream_idx_, uint8_t extra_data_ = 0) :
        code(code_), extra_data(extra_data_), stream_idx(stream_idx_) {}
    /** 4+n byte payload */
    constexpr Error(ErrorCode code_, uint16_t stream_idx_, const char* extra_msg_) :
        code(code_), stream_idx(stream_idx_), extra_msg(extra_msg_) {}
    constexpr Error(ErrorCode code_, uint16_t stream_idx_, uint8_t extra_data_,
                    const char* extra_msg_) :
        code(code_),
        extra_data(extra_data_), stream_idx(stream_idx_), extra_msg(extra_msg_) {}
    constexpr Error(ErrorCode code_, const char* extra_msg_) :
        code(code_), stream_idx(0xffff), extra_msg(extra_msg_) {}

    /** Return true if an error is set. */
    constexpr bool has_error() const {
        return code != ErrorCode::Success;
    };
    constexpr operator bool() const {
        return has_error();
    }

    constexpr bool has_payload() const {
        return extra_data != 0 || stream_idx != std::numeric_limits<uint16_t>::max() ||
               extra_msg != nullptr;
    }
};

}  // namespace response

}  // namespace tcode

namespace std {

template<> struct hash<::tcode::common::CommandIndex> {
    constexpr size_t operator()(const ::tcode::common::CommandIndex cmd_idx) const {
        return static_cast<uint16_t>(cmd_idx);
    }
};

}  // namespace std

#endif /*MMCC_TCODE_MESSAGES_HPP*/