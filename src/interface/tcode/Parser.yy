%name parser
%token_type { request::TokenData }
/* For each token parsed, also pass index to character sequence for error reporting. */
%extra_argument { response::Error& error }
/* Maximum exact stack usage for this grammar. */
%stack_size 6

%include {
#include <interface/tcode/ParserDispatcher.hpp>

#include <cstddef>
#include <cstdint>
#include <cstdlib>
}

%parse_accept {}

%parse_failure {
    #error Disable this function by defining YYNOERRORRECOVERY.
}

%syntax_error {
    error.extra_data = yymajor;
    error.extra_msg = "syntax error";
}

%stack_overflow {
    error.extra_msg = "stack overflow";
}

%start_symbol sequence

sequence ::= Exec.
sequence ::= codes Exec.

codes ::= codes Pad code.
codes ::= code.

code ::= AxisUpdate(ua). {
    error = ParserDispatcher::_on_request(ua.axis_updt);
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}
code ::= AxisUpdate(ua) Interval(ub). {
    error = ParserDispatcher::_on_request(ua.axis_updt, ub.interval);
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}
code ::= AxisUpdate(ua) Speed(ub). {
    error = ParserDispatcher::_on_request(ua.axis_updt, ub.speed);
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}

code ::= CmdIdx(ua). {
    error = ParserDispatcher::_on_request(ua.cmd_idx);
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}
code ::= CmdIdx(ua) Property(ub). {
    error = ParserDispatcher::_on_request(ua.cmd_idx, ub.prop);
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}
code ::= CmdIdx(ua) Property(ub) Z85Data(uc). {
    error = ParserDispatcher::_on_request(ua.cmd_idx, ub.prop, uc.z85);
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}
code ::= CmdIdx(ua) Property(ub) Interval(uc). {
    error = ParserDispatcher::_on_request(ua.cmd_idx, ub.prop, uc.interval);
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}

code ::= CmdIdx(ua) AxisStop. {
    error = ParserDispatcher::_on_request_stop(ua.cmd_idx);
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}
code ::= DeviceStop. {
    error = ParserDispatcher::_on_request_stop();
    if (error) [[unlikely]] {
        return REDUCE_USER_ERROR;
    }
}
