#include "ParserDispatcher.hpp"
#include "TCode.hpp"

#include <TCodeInstance.hpp>
#include <interface/rtos/RTOS.hpp>
#include <interface/tcode/Messages.hpp>
#include <utils/Z85.hpp>

#include <cassert>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>

#include <boot.h>
#include <etl/algorithm.h>
#include <etl/array.h>
#include <stdint.h>

#define ASSERT_FILE_ID (0x0E9F)

namespace tcode {

void ParserDispatcher::reset() {
    _state = State::Inactive;
    _previous_state = State::Inactive;
    _response_code_count = 0;

    // RX buffer
    std::memset(_input_buffer, 0, sizeof(_input_buffer));
    _input_buffer_usage = 0;

    // TX buffer
    std::memset(_output_buffer, 0, sizeof(_output_buffer));
    _output_buffer_usage = 0;

    // Parser
    _decode_buffer.reset();
    _decode_buffer.clear();
    _parser_state.reset();

    // Timeout state/queue.
    std::memset(
         _timeouts_state.data(), 0,
         decltype(_timeouts_state)::SIZE * sizeof(decltype(_timeouts_state)::value_type));
    std::memset(
         _timeouts_queue.data(), 0,
         decltype(_timeouts_queue)::SIZE * sizeof(decltype(_timeouts_queue)::value_type));
    _timeouts_queue_usage = 0;

    if (!_event_queue) [[unlikely]] {
        // Perform one-time init.
        _event_queue.init();
    }
    while (!_event_queue.reset()) [[unlikely]] {
        mmcc::dbg.print("ParserDispatcher::reset: _event_queue");
    }
}

void ParserDispatcher::send_raw_data(const void* data, size_t n) {
    // Bypass staging buffer on large inputs.
    if (n >= sizeof(_output_buffer)) [[unlikely]] {
        if (_output_buffer_usage != 0) {
            flush_output();
        }
        TCODE_INSTANCE.send(data, n);
    }
    // Append to buffer until it fills up.
    const size_t output_buffer_space = get_output_buffer_space();
    const size_t pass_1_n = std::min(output_buffer_space, n);
    std::memcpy(&_output_buffer[_output_buffer_usage], data, pass_1_n);
    size_t pass_2_n = n - pass_1_n;
    _output_buffer_usage += pass_1_n;
    if (_output_buffer_usage >= sizeof(_output_buffer)) {
        flush_output();
    }
    // Append any remaining data.
    if (pass_2_n > 0) {
        const size_t output_buffer_space = get_output_buffer_space();
        assert(pass_2_n <= output_buffer_space, __COUNTER__);
        std::memcpy(&_output_buffer[_output_buffer_usage], ((uint8_t*) data) + pass_1_n,
                    pass_2_n);
        _output_buffer_usage += pass_2_n;
        if (_output_buffer_usage >= sizeof(_output_buffer)) {
            flush_output();
        }
    }
}
void ParserDispatcher::send_raw_byte(char ch) {
    _output_buffer[_output_buffer_usage++] = ch;
    if (_output_buffer_usage >= sizeof(_output_buffer)) {
        flush_output();
    }
}
void ParserDispatcher::send_z85_data(const void* data_raw, size_t n_raw, uint8_t null_symbol) {
    const uint32_t* data = (const uint32_t*) data_raw;
    while (n_raw >= 4) {
        // Ensure there is output buffer enough space.
        size_t output_buffer_space = get_output_buffer_space();
        if (output_buffer_space < 5) {
            flush_output();
            output_buffer_space = sizeof(_output_buffer);
        }
        // Encode data.
        size_t batch_tokens = std::min(output_buffer_space / 5, n_raw / 4);
        ::codec::encode((::codec::z85_pack_t*) &_output_buffer[_output_buffer_usage],
                        batch_tokens, data, batch_tokens);
        _output_buffer_usage += batch_tokens * 5;
        n_raw -= batch_tokens * 4;
        data += batch_tokens;
    }
    // Append partial, padded with null symbols.
    if (n_raw > 0) {
        // Ensure there is output buffer enough space.
        size_t output_buffer_space = get_output_buffer_space();
        if (output_buffer_space < 5) {
            flush_output();
            output_buffer_space = sizeof(_output_buffer);
        }
        uint32_t end_data;
        // TODO: Improve perf with fallthrough switch/case construct.
        std::memset(&end_data, null_symbol, 4);
        std::memcpy(&end_data, data, n_raw);
        ::codec::encode((::codec::z85_pack_t*) &_output_buffer[_output_buffer_usage], 1,
                        &end_data, 1);
        _output_buffer_usage += 5;
    }
    if (_output_buffer_usage >= sizeof(_output_buffer)) {
        flush_output();
    }
}

void ParserDispatcher::flush_output() {
    assert(_output_buffer_usage <= sizeof(_output_buffer), __COUNTER__);
    TCODE_INSTANCE.send(_output_buffer, _output_buffer_usage);
    _output_buffer_usage = 0;
}

bool ParserDispatcher::start_response() {
    if (_state != State::Inactive && _state != State::Timeouts) {
        assert(0, __COUNTER__);
        return false;
    }
    _state = State::Parsing;
    return true;
}
bool ParserDispatcher::finalize_response() {
    if (_state != State::Parsing) {
        assert(0, __COUNTER__);
        return false;
    }
    _state = State::Inactive;
    _response_code_count = 0;
    return true;
}
void ParserDispatcher::_finalize_response(response::Error status) {
    TCODE_INSTANCE.send_response(status);
    assert(TCODE_INSTANCE._output_buffer_usage < sizeof(_output_buffer), __COUNTER__);
    TCODE_INSTANCE._output_buffer[TCODE_INSTANCE._output_buffer_usage++] = '\n';
    TCODE_INSTANCE.flush_output();
    // No need to check return value, error will be caught at the start of the next request.
    TCODE_INSTANCE.finalize_response();
}

bool ParserDispatcher::start_timeouts() {
    if (_state == State::Timeouts) {
        return true;
    }
    else if (_state == State::Inactive) {
        _state = State::Timeouts;
        return true;
    }
    else {
        assert(0, __COUNTER__);
        return false;
    }
}
bool ParserDispatcher::finalize_timeouts() {
    // Send end token periodically even on host inactivity.
    assert(_state == State::Timeouts, __COUNTER__);
    assert(_output_buffer_usage < sizeof(_output_buffer), __COUNTER__);
    _output_buffer[_output_buffer_usage++] = '\n';
    flush_output();
    _state = State::Inactive;
    _response_code_count = 0;
    return true;
}

void ParserDispatcher::start_ubjson() {
    assert(_state == State::Timeouts || _state == State::Parsing, __COUNTER__);
    _previous_state = _state;
    _state = State::UBJBuilder;
}
void ParserDispatcher::finalize_ubjson() {
    assert(_state == State::UBJBuilder, __COUNTER__);
    _state = _previous_state;
}

uint32_t ParserDispatcher::get_timeout(
     const decltype(config::property_registry)::element_type& prop_ref) {
    constexpr auto* base = config::property_registry.data();
    const auto* const elem = &prop_ref;
    size_t idx = static_cast<uintptr_t>(elem - base);
    if (idx >= config::property_registry.size()) {
        assert(0, __COUNTER__);
        return std::numeric_limits<uint32_t>::max();
    }
    return _timeouts_state[idx];
}
response::Error ParserDispatcher::register_timeout(
     const decltype(config::property_registry)::element_type& prop_ref, uint32_t interval) {
    constexpr auto* base = config::property_registry.data();
    const auto* const elem = &prop_ref;
    size_t idx = static_cast<uintptr_t>(elem - base);
    if (idx >= config::property_registry.size()) {
        assert(0, __COUNTER__);
        return response::Error(response::ErrorCode::Generic,
                               "register_timeout: property_registry overflow");
    }

    if (interval > 0) {
        if (_timeouts_state[idx] == 0) {
            _timeouts_state[idx] = interval;
            // Append to queue.
            assert(_timeouts_queue_usage < config::property_registry.size(), __COUNTER__);
            _timeouts_queue[_timeouts_queue_usage] = {rtos::Kernel::get_tickcount() + interval,
                                                      prop_ref};
            _timeouts_queue_usage++;
            etl::push_heap(&_timeouts_queue[0], &_timeouts_queue[_timeouts_queue_usage],
                           TimeoutEntry::comparator);
        }
        else {
            // Update timeout interval, effective from the next timeout.
            _timeouts_state[idx] = interval;
        }
    }
    else { /*interval == 0*/
        if (_timeouts_state[idx] != 0) {
            assert(_timeouts_queue_usage >= 1, __COUNTER__);
            // De-register.
            _timeouts_state[idx] = 0;
            _timeouts_queue_usage--;
            _timeouts_queue[idx] = _timeouts_queue[_timeouts_queue_usage];
            _timeouts_queue[_timeouts_queue_usage] = {};
            etl::make_heap(&_timeouts_queue[0], &_timeouts_queue[_timeouts_queue_usage],
                           TimeoutEntry::comparator);
        }
    }

    return {};
}

bool ParserDispatcher::pend_event(const EventQueueEntry* e, size_t n) {
    if (!_event_queue) [[unlikely]] {
        return false;
    }
    if (!e->meta->has_getter() ||
        !(e->meta->get_flags() & callbacks::Property::FlagBits::Event)) {
        return false;
    }
    size_t expected_count = sizeof(uintptr_t) + n;
    if (expected_count > 256) {
        return false;
    }
    size_t actual_count = 0;
    if (rtos::is_inside_interrupt()) {
        auto tk = rtos::Kernel::enter_critical_from_isr();
        actual_count = _event_queue.send_from_isr(e, expected_count);
        rtos::Kernel::exit_critical_from_isr(tk);
    }
    else {
        rtos::Kernel::enter_critical();
        actual_count = _event_queue.send_ticks(e, expected_count, 0);
        rtos::Kernel::exit_critical();
    }
    return actual_count == expected_count;
}

TickType_t ParserDispatcher::handle_timeouts() {
    if (_state != State::Inactive && _state != State::Timeouts) {
        assert(0, __COUNTER__);
        return portMAX_DELAY;
    }
    // Send any pending events first.
    while (!_event_queue.is_empty()) {
        if (!start_timeouts()) [[unlikely]] {
            assert(0, __COUNTER__);
            return rtos::Kernel::get_tickcount();
        }
        static uint8_t buffer[256];
        size_t msg_l = _event_queue.receive_ticks(&buffer, sizeof(buffer), 0);
        if (msg_l == 0) {
            break;
        }
        assert(msg_l >= sizeof(uintptr_t), __COUNTER__);
        EventQueueEntry* msg = reinterpret_cast<EventQueueEntry*>(buffer);
        assert(msg->meta->has_getter() &&
                    msg->meta->get_flags() & callbacks::Property::FlagBits::Event,
               __COUNTER__);
        // Ignore errors.
        msg->meta->event(&msg->data[0], msg_l - sizeof(uintptr_t));
    }
    // Move onto timeout handling.
    if (_timeouts_queue_usage == 0) {
        return portMAX_DELAY;
    }
    // Handle all expired timeouts.
    TickType_t current_tick = rtos::Kernel::get_tickcount();
    while (current_tick >= _timeouts_queue[0].next_timeout) {
        if (!start_timeouts()) {
            assert(0, __COUNTER__);
            return current_tick;
        }
        etl::pop_heap(&_timeouts_queue[0], &_timeouts_queue[_timeouts_queue_usage],
                      TimeoutEntry::comparator);
        auto& queue_cur_elem = _timeouts_queue[_timeouts_queue_usage - 1];
        assert(queue_cur_elem.prop_reg_ref != nullptr, __COUNTER__);
        auto& queue_cur_elem_cbs = (*queue_cur_elem.prop_reg_ref)->second;
        if (queue_cur_elem_cbs.has_getter()) {
            // Ignore errors.
            queue_cur_elem_cbs.get();
        }
        constexpr auto* base = config::property_registry.data();
        const auto* const elem = queue_cur_elem.prop_reg_ref;
        size_t idx = static_cast<uintptr_t>(elem - base);
        assert(idx < config::property_registry.size(), __COUNTER__);
        // Adjust next timeout tick value.
        do {
            queue_cur_elem.next_timeout += _timeouts_state[idx];
        } while (queue_cur_elem.next_timeout <= current_tick);
        // Re-insert to heap.
        etl::push_heap(&_timeouts_queue[0], &_timeouts_queue[_timeouts_queue_usage],
                       TimeoutEntry::comparator);
    }
    return _timeouts_queue[0].next_timeout;
}

bool ParserDispatcher::handle_input(const char* input_staging_buffer,
                                    size_t input_staging_buffer_len) {
    for (size_t i = 0; i < input_staging_buffer_len; i++) {
        /* Check for buffer overflow.
         * Also ensure that there is enough space for the null terminator. */
        if ((_input_buffer_usage + 1) > (sizeof(_input_buffer) - 1)) [[unlikely]] {
            if (start_response()) {
                _finalize_response({response::ErrorCode::Allocation,
                                    (uint16_t) _input_buffer_usage, "request buffer overflow"});
            }
            _input_buffer_usage = 0;
            return false;
        }
        char ch = input_staging_buffer[i];
        _input_buffer[_input_buffer_usage++] = ch;
        // Completed a command sequence.
        if (ch == '\n') {
            // Add sentinel.
            _input_buffer[_input_buffer_usage] = '\0';
            // Call parser.
            bool ok = false;
            if (start_response()) [[likely]] {
                ok = parser::tokenize(_parser_state, _input_buffer, _input_buffer_usage,
                                      _decode_buffer);
            }
            // Partial state reset.
            _decode_buffer.reset();
            _input_buffer_usage = 0;
            // Stop on error.
            if (!ok) [[unlikely]] {
                return false;
            }
        }
    }
    if (_state == State::Timeouts) {
        return finalize_timeouts();
    }

    return true;
}

void ParserDispatcher::send_response(response::CommandIndex cmd_idx, response::Z85Data data) {
    send_partial_response(cmd_idx);
    send_raw_byte('Z');
    send_z85_data(data.data, data.n);
}
void ParserDispatcher::send_response(response::CommandIndex cmd_idx,
                                     response::PropertyData prop, response::Z85Data data) {
    send_partial_response(cmd_idx, prop);
    send_raw_byte('Z');
    send_z85_data(data.data, data.n);
}

codec::UBJsonInstance ParserDispatcher::send_ubjson_response(response::CommandIndex cmd_idx) {
    send_partial_response(cmd_idx);
    send_raw_byte('Z');
    start_ubjson();
    return codec::UBJsonInstance(*this);
}
codec::UBJsonInstance ParserDispatcher::send_ubjson_response(response::CommandIndex cmd_idx,
                                                             response::PropertyData prop) {
    send_partial_response(cmd_idx, prop);
    send_raw_byte('Z');
    start_ubjson();
    return codec::UBJsonInstance(*this);
}

void ParserDispatcher::on_new_response() {
    if (_response_code_count != 0) {
        send_raw_byte(' ');
    }
    _response_code_count++;
}

void ParserDispatcher::send_partial_response(response::CommandIndex cmd_idx) {
    on_new_response();
    if (get_output_buffer_space() < 2) {
        flush_output();
    }
    _output_buffer[_output_buffer_usage] = common::cmdtyp2char(cmd_idx.cmd);
    _output_buffer[_output_buffer_usage + 1] = '0' + cmd_idx.idx;
    _output_buffer_usage += 2;
    if (_output_buffer_usage >= sizeof(_output_buffer)) {
        flush_output();
    }
}
void ParserDispatcher::send_partial_response(response::CommandIndex cmd_idx,
                                             response::PropertyData prop) {
    on_new_response();
    if (get_output_buffer_space() < 3) {
        flush_output();
    }
    _output_buffer[_output_buffer_usage] = common::cmdtyp2char(cmd_idx.cmd);
    _output_buffer[_output_buffer_usage + 1] = '0' + cmd_idx.idx;
    _output_buffer[_output_buffer_usage + 2] = 'P';
    _output_buffer_usage += 3;
    if (_output_buffer_usage >= sizeof(_output_buffer)) {
        flush_output();
    }
    send_raw_data(prop.name_begin, static_cast<uintptr_t>(prop.name_end - prop.name_begin));
}

void ParserDispatcher::send_response(response::Error status) {
    on_new_response();
    {
        // Write header.
        if (get_output_buffer_space() < 2) {
            flush_output();
        }
        _output_buffer[_output_buffer_usage] = 'E';
        _output_buffer[_output_buffer_usage + 1] = '0' + static_cast<uint8_t>(status.code);
        _output_buffer_usage += 2;
        if (_output_buffer_usage >= sizeof(_output_buffer)) {
            flush_output();
        }
    }
    if (status.has_payload()) {
        // 4 byte payload.
        uint32_t info_bytes = static_cast<uint32_t>(status.stream_idx) |
                              (static_cast<uint32_t>(status.extra_data) << 16);
        send_z85_data(&info_bytes, 4);
        if (status.extra_msg != nullptr) {
            // String payload (with null terminator).
            send_z85_data(status.extra_msg, strlen(status.extra_msg) + 1, '\0');
        }
    }
}

}  // namespace tcode
