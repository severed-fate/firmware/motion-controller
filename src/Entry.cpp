#include <interface/rtos/RTOS.hpp>
#include <mmcc/Objects.hpp>

#include <cstdint>
#include <cstdlib>

void rtos_init() {
    // Load freeRTOS interrupts into vector table.
    runtime_vector_table.sv_call = &vPortSVCHandler;
    runtime_vector_table.pend_sv = &xPortPendSVHandler;
    runtime_vector_table.systick = &xPortSysTickHandler;
    // Switch to FreeRTOS.
    rtos::Kernel::start();
}

int main() {
    // Initialize & register tasks.
    mmcc::init();
    // Start rtos scheduler.
    rtos_init();
    return 0;
}
