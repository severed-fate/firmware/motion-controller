# Severed Fate motion controller with TCode control

## FreeRTOS

Resource allocation:

### Direct to task notification indexes

- 0: Stream Buffers
- 1: USB
- 2: Private

## eTCode

TCode extended.
Most of the command set is compatible with TCode v0.4.
There are 2 main differences:

1. Device commands have different output formatting.
2. Introduction of properties and removal of specialized save commands.

For properties, there are two types - read-only and read-write.
Also, a special character is prepended, '?' for reading or '$' for writing.

For reading a property, the format is as follows:
\<Command Index\>P\<Property name\>
, while writing also requires
Z\<Z85 encoded data quintuples\>
at the end.
Property names can be a combination of lower-case alphanumeric and underscores.

Properties can from a selection of multiple types:

1. 32bit signed integer.
2. 64bit signed integer.
3. 32bit unsigned integer.
4. 64bit unsigned integer.
5. Single precision floating point.
6. Double precision floating point.
7. Character string (null-padded).
8. Ubjson binary data.

Additional metadata can also be appended, such as enumeration, min/max,
{suggested/min/max/current}update_rate etc.
These are used for providing usage hints and building a more coherent user interface.

Also properties can be subscribed to, so that the device sends automatic updates.

### Axis commands

Axis identification is compatible with TCode.
Specifically each axis is specified with '[LRVA][0-9]', where:

- L: linear
- R: rotation
- V: vibration
- A: auxiliary

### Device commands

- DSTOP: Stop all axis motion.
- D0: Identify device and firmware.
- D1: Identify protocol version.
- D2: List available axes, device commands and properties.
- D3: Generic device configuration interface.
- D4: OS facility interface.
- D5: Common configuration/status for all actuators.

### Property monitoring

### Return format

## Long TODOs

Section for better interrupt code locality.
