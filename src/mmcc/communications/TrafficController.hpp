#ifndef MMCC_CONTROLLER_HPP
#define MMCC_CONTROLLER_HPP
/**
 * @file
 * @brief Manages commands received from the various communication interfaces and synchronizes
 * everything with the actuators.
 */

#include <interface/rtos/RTOSTask.hpp>
#include <interface/tcode/ParserDispatcher.hpp>

namespace mmcc {

class TrafficController : public rtos::StaticTask<768>, public tcode::ParserDispatcher {
   public:
    enum class Connection {
        /** State for no active connection. */
        Disconnected,
        /** Direct usb connection. */
        ConnectedUSB,
        /** Wireless network(telnet) connection through esp8266. */
        ConnectedNet
    };

   protected:
    Connection _connection_state = Connection::Disconnected;
    char _staging_buffer[96];

   public:
    constexpr TrafficController() : rtos::StaticTask<768>(), tcode::ParserDispatcher() {}
    cold_func_attr ~TrafficController() = default;

    void init();
    bool start();
    void run();

    bool is_connected() const {
        return _connection_state != Connection::Disconnected;
    }

   private:
    void wait_for_connection();
    void handle_communication();

    size_t receive(TickType_t timeout_ticks);
    size_t send(const void* dat, size_t n);
    friend class tcode::ParserDispatcher;
    /** Force a disconnection with the currently active interface. */
    void disconnect();

    /**
     * On connection loss:
     * 1. activate safeguards (stop actuator),
     * 2. reset parser state.
     */
    void on_disconnect();
};

}  // namespace mmcc

#endif /*MMCC_CONTROLLER_HPP*/
