#include <boot.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/rcc.h>

void clock_setup(void) {
    // Setup clocks.
    const struct rcc_clock_scale rcc_hse_25mhz_pll_96mhz_3v3 = {
         25,
         192,
         2,
         4,
         0,
         RCC_CFGR_PLLSRC_HSE_CLK,
         FLASH_ACR_DCEN | FLASH_ACR_ICEN | FLASH_ACR_LATENCY_3WS,
         RCC_CFGR_HPRE_NODIV,
         RCC_CFGR_PPRE_DIV2,
         RCC_CFGR_PPRE_NODIV,
         PWR_SCALE1,
         AHB_CLOCK_HZ,
         APB1_CLOCK_HZ,
         APB2_CLOCK_HZ,
    };

    rcc_periph_clock_enable(RCC_PWR);
    rcc_periph_clock_enable(RCC_GPIOH);

    // Configure PVD and wait until 2.7V is reached.
    pwr_enable_power_voltage_detect(PWR_CR_PLS_2V7);
    while (!pwr_voltage_high()) {
        nop;
    }

    // Enable HSE, configure PLL and clockup.
    rcc_clock_setup_pll(&rcc_hse_25mhz_pll_96mhz_3v3);
    // Enable instruction prefetch.
    flash_prefetch_enable();
    // Enable 'co-core' clocks.
    rcc_periph_clock_enable(RCC_DMA1);
    rcc_periph_clock_enable(RCC_DMA2);
    // Start LSI, as it will be used later for clocking the watchdog.
    if (!(RCC_CSR & RCC_CSR_LSION)) {
        rcc_osc_on(RCC_LSI);
    }
}
