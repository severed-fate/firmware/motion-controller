#ifndef MMCC_BOOTLOADER_HPP
#define MMCC_BOOTLOADER_HPP

#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/vector.h>

// Special types.
typedef struct {
    unsigned int* initial_sp_value; /**< Initial stack pointer value. */
    vector_table_entry_t reset;
    vector_table_entry_t nmi;
    vector_table_entry_t hard_fault;
    vector_table_entry_t memory_manage_fault; /* not in CM0 */
    vector_table_entry_t bus_fault;           /* not in CM0 */
    vector_table_entry_t usage_fault;         /* not in CM0 */
    vector_table_entry_t reserved_x001c[4];
    vector_table_entry_t sv_call;
    vector_table_entry_t debug_monitor; /* not in CM0 */
    vector_table_entry_t reserved_x0034;
    vector_table_entry_t pend_sv;
    vector_table_entry_t systick;
} small_vector_table_t;

typedef struct {
    vector_table_entry_t reset;
} mmcc_bin_header;

// Vector table.
__attribute__((section(".vectors_static"))) extern small_vector_table_t static_vector_table;

// Forward declarations.
void bootloader_reset_handler(void);
void null_handler(void);

#endif /*MMCC_BOOTLOADER_HPP*/
