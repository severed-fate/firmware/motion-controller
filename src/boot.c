#include "boot.h"

#include <interface/io/core.h>
#include <stdlib.h>
#include <string.h>

cold_func_attr void mmcc_reset_handler(void) {
    // Ensure proper Cortex-M core state.
    SCB_CCR |= SCB_CCR_STKALIGN;
    #if CORE_HAS_FPU
        // Enable access to Floating-Point coprocessor.
        SCB_CPACR |= SCB_CPACR_FULL * (SCB_CPACR_CP10 | SCB_CPACR_CP11);
    #endif
    // CRT Startup.
    memset(&__bss_start, 0, __bss_size);
    memcpy(&__data_start, &__data_loadaddr, __data_size);
    memcpy(&__sramtext_start, &__sramtext_loadaddr, __sramtext_size);
    // Vector table initialization.
    runtime_vector_table.initial_sp_value = &_stack;
    runtime_vector_table.reset = mmcc_reset_handler;
    runtime_vector_table.nmi = null_handler;
    runtime_vector_table.hard_fault = null_handler;
    runtime_vector_table.memory_manage_fault = null_handler;
    runtime_vector_table.bus_fault = null_handler;
    runtime_vector_table.usage_fault = null_handler;
    runtime_vector_table.sv_call = null_handler;
    runtime_vector_table.debug_monitor = null_handler;
    runtime_vector_table.pend_sv = null_handler;
    runtime_vector_table.systick = null_handler;
    for (int i = 0; i < NVIC_IRQ_COUNT; i++) {
        runtime_vector_table.irq[i] = null_handler;
    }
    SCB_VTOR = (uintptr_t) &runtime_vector_table;
    // HW initialization.
    clock_setup();
    gpio_setup();
    // C/C++ Constructors.
    funcp_t* fp;
    for (fp = &__preinit_array_start; fp < &__preinit_array_end; fp++) {
        (*fp)();
    }
    for (fp = &__init_array_start; fp < &__init_array_end; fp++) {
        (*fp)();
    }
    // TODO: Initialize static rtos tasks.
    // Interrupts will be enabled from prvPortStartFirstTask.
    mmcc_set_priority_grouping(SCB_AIRCR_PRIGROUP_GROUP16_NOSUB);
    // Call main.
    main();
    // Destructors.
    for (fp = &__fini_array_start; fp < &__fini_array_end; fp++) {
        (*fp)();
    }
    // Reset microcontroller.
    mmcc_reset_system();
}

void null_handler(void) {
    nop;
}
