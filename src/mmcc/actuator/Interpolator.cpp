#include "Interpolator.hpp"

#include <algorithm>

namespace mmcc {

void AxisInterpolator::update(float start_state, float end_state, size_t duration_ticks) {
    _current_state = start_state;
    _step_size = (end_state - start_state) / static_cast<float>(duration_ticks);
    _remaining_ticks = duration_ticks;
}
void AxisInterpolator::update(float target_state, size_t duration_ticks) {
    _step_size = (target_state - _current_state) / static_cast<float>(duration_ticks);
    _remaining_ticks = duration_ticks;
}
void AxisInterpolator::update(float target_state) {
    _current_state = target_state;
    _step_size = 0;
    _remaining_ticks = 0;
}

float AxisInterpolator::step() {
    if (_remaining_ticks > 0) {
        _current_state += _step_size;
        _remaining_ticks--;
    }
    return _current_state;
}

void SpeedLimiter::setup(float current_pos) {
    _last_target = current_pos;
}
float SpeedLimiter::step() {
    float new_target = AxisInterpolator::step();
    /**
     * Every tick move a bit closer to the target.
     * This is a form of smoothing and
     * doesn't provide a hard speed limit.
     */
    if (new_target > _last_target) {
        _last_target = std::min(_last_target + _speed_limit, new_target);
    }
    else if (new_target < _last_target) {
        _last_target = std::max(_last_target - _speed_limit, new_target);
    }
    return _last_target;
}

}  // namespace mmcc
