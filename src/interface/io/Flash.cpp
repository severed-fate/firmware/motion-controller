#include "Flash.hpp"

#include <board/Config.hpp>
#include <interface/io/Core.hpp>
#include <mmcc/Objects.hpp>

#include <algorithm>

namespace stm32::flash {

PrgError erase_sector(sector_index_t sector_index) {
    mmcc::disable_interrupts();
    mmcc::disable_faults();
    auto status = _flash_prg_erase(sector_index);
    mmcc::enable_faults();
    mmcc::enable_interrupts();
    return static_cast<PrgError>(status);
}

PrgError erase_sector(const void* flash_addr) {
    uintptr_t flash_addr_value = (uintptr_t) flash_addr;
    for (sector_index_t sector_index = 0; sector_index < (int) mmcc::FLASH_MODULE_MAP.size();
         sector_index++) {
        auto [base_address, length] = mmcc::FLASH_MODULE_MAP.at(sector_index);
        if (flash_addr_value >= base_address && flash_addr_value < (base_address + length)) {
            return erase_sector(sector_index);
        }
    }
    // Invalid value for flash_addr argument.
    return PrgError::RuntimeError;
}

PrgError write(void* dst, const void* src, size_t n) {
    if ((((uintptr_t) dst) % 4) != 0) {
        size_t c = std::min(((uintptr_t) dst) % 4, n);
        mmcc::disable_interrupts();
        mmcc::disable_faults();
        auto err = _flash_prg_write_bytes((uint8_t*) dst, (const uint8_t*) src, c);
        mmcc::enable_faults();
        mmcc::enable_interrupts();
        if (err != FLASH_PRG_ERROR_SUCCESS) {
            return static_cast<PrgError>(err);
        }
        dst = ((uint8_t*) dst) + c;
        src = ((const uint8_t*) src) + c;
        n -= c;
    }

    size_t c = n / 4;
    while (c > 0) {
        size_t b = std::min<size_t>(c, 8);
        mmcc::disable_interrupts();
        mmcc::disable_faults();
        auto err = _flash_prg_write_words((uint32_t*) dst, (const uint32_t*) src, b);
        mmcc::enable_faults();
        mmcc::enable_interrupts();
        if (err != FLASH_PRG_ERROR_SUCCESS) {
            return static_cast<PrgError>(err);
        }
        c -= b;
        dst = ((uint8_t*) dst) + b * 4;
        src = ((const uint8_t*) src) + b * 4;
        n -= b * 4;
    }

    if (n > 0) {
        mmcc::disable_interrupts();
        mmcc::disable_faults();
        auto err = _flash_prg_write_bytes((uint8_t*) dst, (const uint8_t*) src, n);
        mmcc::enable_faults();
        mmcc::enable_interrupts();
        if (err != FLASH_PRG_ERROR_SUCCESS) {
            return static_cast<PrgError>(err);
        }
    }

    return PrgError::Success;
}

}  // namespace stm32::flash
