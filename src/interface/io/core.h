#ifndef MMCC_CORE
#define MMCC_CORE

#include <stdbool.h>
#include <stdint.h>

void mmcc_enable_interrupts(void);
void mmcc_disable_interrupts(void);
void mmcc_enable_faults(void);
void mmcc_disable_faults(void);
bool mmcc_is_masked_interrupts(void);
bool mmcc_is_masked_faults(void);
uint32_t mmcc_mask_interrupts(uint32_t mask);
uint32_t mmcc_mask_faults(uint32_t mask);

void mmcc_reset_system(void);
void mmcc_reset_core(void);
void mmcc_set_priority_grouping(uint32_t prigroup);

#endif /*MMCC_CORE*/
