#ifndef MMCC_TCODEIMPLSENSOR_HPP
#define MMCC_TCODEIMPLSENSOR_HPP

#include <TCodeConfig.hpp>

#include <cstddef>

namespace tcode::callbacks {

void meta_sonar_config_fullscale(codec::UBJsonObject& obj);
void meta_sonar_config_normalized(codec::UBJsonObject& obj);

response::Error sonar_get_is_scanning(const void*, size_t);
response::Error sonar_set_is_scanning(void* dat, size_t n);
response::Error sonar_get_raw_measurement(const void*, size_t);
response::Error sonar_get_measurement_errors(const void*, size_t);
response::Error sonar_get_scaled_measurement(const void*, size_t);
response::Error sonar_get_filtered_measurement(const void*, size_t);
response::Error sonar_get_normalized_measurement(const void*, size_t);

response::Error sonar_get_norm_near(const void*, size_t);
response::Error sonar_set_norm_near(void* dat, size_t n);
response::Error sonar_get_norm_far(const void*, size_t);
response::Error sonar_set_norm_far(void* dat, size_t n);
response::Error sonar_get_norm_invert(const void*, size_t);
response::Error sonar_set_norm_invert(void* dat, size_t n);

response::Error sonar_get_threshold1(const void*, size_t);
response::Error sonar_set_threshold1(void* dat, size_t n);
response::Error sonar_get_threshold2(const void*, size_t);
response::Error sonar_set_threshold2(void* dat, size_t n);
response::Error sonar_get_threshold_selector(const void*, size_t);
response::Error sonar_set_threshold_selector(void* dat, size_t n);
response::Error sonar_get_alpha1(const void*, size_t);
response::Error sonar_set_alpha1(void* dat, size_t n);
response::Error sonar_get_alpha2(const void*, size_t);
response::Error sonar_set_alpha2(void* dat, size_t n);

response::Error sonar_reset_filter(void* dat, size_t n);

struct SonarPlotData {
    float time;
    float scaled;
    float filtered;
    float normalized;
};

void meta_sonar_plot(codec::UBJsonObject&);
response::Error sonar_get_plot(const void*, size_t);

}  // namespace tcode::callbacks

#endif /*MMCC_TCODEIMPLSENSOR_HPP*/
