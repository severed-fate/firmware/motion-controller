#ifndef MMCC_TCODE_NUMERIC_HPP
#define MMCC_TCODE_NUMERIC_HPP

#include <cstdint>
#include <limits>
#include <type_traits>

namespace tcode {

template<typename T>
requires(std::numeric_limits<T>::is_integer && !std::numeric_limits<T>::is_signed)
class fractional {
   protected:
    T _num = 0;
    T _denom = std::numeric_limits<T>::max();

   public:
    constexpr fractional() = default;
    /**
     * Construct by converting from string to integer.
     */
    fractional(const char* const begin, const char* const end) noexcept {
        if (!parse(begin, end)) {
            _num = 0;
            _denom = std::numeric_limits<T>::max();
        }
    }

    bool parse(const char* const begin, const char* const end) noexcept {
        return parse(begin, static_cast<uintptr_t>(end - begin));
    }
    bool parse(const char* const str, const size_t n) noexcept {
        if (n == 0 || n > std::numeric_limits<uint32_t>::digits10) [[unlikely]] {
            return false;
        }
        _num = 0;
        _denom = 0;
        for (size_t i = 0; i < n; i++) {
            uint8_t digit = str[i] - '0';
            if (digit > 9) [[unlikely]] {
                return false;
            }
            _num *= 10;
            _num += digit;
            _denom *= 10;
            _denom += 9;
        }
        return true;
    }

    T numerator() const {
        return _num;
    }
    T denominator() const {
        return _denom;
    }

    template<typename R = float> requires(std::is_floating_point_v<R>)
    R quotient() const {
        return static_cast<R>(_num) / static_cast<R>(_denom);
    }

    // TODO: void to_ratio() const;

    operator float() const {
        return quotient<float>();
    }
    operator double() const {
        return quotient<double>();
    }
};

}  // namespace tcode

#endif /*MMCC_TCODE_NUMERIC_HPP*/