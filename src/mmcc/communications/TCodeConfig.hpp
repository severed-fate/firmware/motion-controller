#ifndef MMCC_TCODECONFIG_HPP
#define MMCC_TCODECONFIG_HPP
/**
 * @file
 * @brief TCode callback configuration for TrafficController.
 */
#include <interface/tcode/Messages.hpp>
#include <interface/tcode/TCode.hpp>
#include <mapbox/eternal.hpp>

namespace tcode::callbacks {

void update_interval_meta_500(codec::UBJsonObject&);
void update_interval_meta_500_norm(codec::UBJsonObject&);

response::Error identify_device();
response::Error get_device_name(const void*, size_t);
response::Error get_device_version(const void*, size_t);
response::Error get_device_uuid(const void*, size_t);

response::Error get_config_save_count(const void*, size_t);
response::Error save_config(void* dat, size_t n);

response::Error get_rtos_stats();
response::Error get_rtos_time(const void*, size_t);
response::Error set_rtos_time(void* dat, size_t n);
response::Error get_rtos_date(const void*, size_t);
response::Error get_log(const void*, size_t);

}  // namespace tcode::callbacks

namespace tcode::config {

using namespace tcode::callbacks;
using tcode::common::CommandIndex;
using tcode::common::CommandType;

using mapbox::eternal::impl::element;
using mapbox::eternal::impl::element_hash;
using mapbox::eternal::impl::map;

const extern map<element_hash<CommandIndex, Command>, 12U> command_registry;
const extern map<element_hash<CommandIndex, AxisUpdate>, 6U> axis_registry;
const extern map<element_hash<PropertyKey, Property>, 79U> property_registry;

const extern Property& event_meta_pid_loop_status;
const extern Property& event_meta_act0_engaged;
const extern Property& event_meta_act1_engaged;
const extern Property& event_meta_act2_engaged;
const extern Property& event_meta_aux_engaged;
const extern Property& event_meta_act0_plot;
const extern Property& event_meta_act1_plot;
const extern Property& event_meta_act2_plot;
const extern Property& event_meta_sonar_plot;

}  // namespace tcode::config

#endif /*MMCC_TCODECONFIG_HPP*/