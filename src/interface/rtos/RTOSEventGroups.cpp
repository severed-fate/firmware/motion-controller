#include "RTOSEventGroups.hpp"

namespace rtos {

EventGroup::EventGroup() : EventGroup(xEventGroupCreate()) {}

EventGroup::~EventGroup() {
    if (_handle != nullptr) {
        vEventGroupDelete(_handle);
        _handle = nullptr;
    }
}

EventBits_t EventGroup::wait_ticks(EventBits_t mask, bool clear, bool mask_and, TickType_t timeout_ticks) {
    return xEventGroupWaitBits(_handle, mask, clear ? pdTRUE : pdFALSE,
                               mask_and ? pdTRUE : pdFALSE, timeout_ticks);
}

EventBits_t EventGroup::set(EventBits_t mask) {
    if (is_inside_interrupt()) {
        // BaseType_t task_switch = 0;
        // EventBits_t value = xEventGroupSetBitsFromISR(_handle, mask, &task_switch);
        // if (task_switch) {
        //     taskYIELD();
        // }
        // return value;
        return -1;
    }
    else {
        return xEventGroupSetBits(_handle, mask);
    }
}

EventBits_t EventGroup::clear(EventBits_t mask) {
    if (is_inside_interrupt()) {
        // BaseType_t task_switch = 0;
        // EventBits_t value = xEventGroupClearBitsFromISR(_handle, mask, &task_switch);
        // if (task_switch) {
        //     taskYIELD();
        // }
        // return value;
        return -1;
    }
    else {
        return xEventGroupClearBits(_handle, mask);
    }
}

EventBits_t EventGroup::get() {
    if (is_inside_interrupt()) {
        return xEventGroupGetBitsFromISR(_handle);
    }
    else {
        return xEventGroupGetBits(_handle);
    }
}

EventBits_t EventGroup::sync_ticks(EventBits_t set_mask, EventBits_t wait_mask, TickType_t timeout_ticks) {
    return xEventGroupSync(_handle, set_mask, wait_mask, timeout_ticks);
}

}  // namespace rtos
