#include "ConfigBlock.hpp"

#include <Objects.hpp>
#include <interface/io/Core.hpp>
#include <interface/io/Flash.hpp>

#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <limits>

#include <etl/bit.h>
#include <etl/crc16.h>

#define ASSERT_FILE_ID (0x3F1F)

namespace mmcc {

static inline ConfigBlock* get_block(sector_index_t si, size_t bi) {
    uintptr_t p = FLASH_MODULE_MAP[si].first + bi * CONFIG_BLOCK_SIZE;
    return (ConfigBlock*) p;
}

uint16_t ConfigBlockData::calc_crc() const {
    etl::crc16_t<16> hasher;
    const uint8_t* view_begin = reinterpret_cast<const uint8_t*>(this);
    const uint8_t* view_end = view_begin + sizeof(ConfigBlockData);
    hasher.add(view_begin, view_end);
    return hasher.value();
}

bool ConfigBlock::is_valid() const {
    return header.verify() && header.crc == data.calc_crc();
}
bool ConfigBlock::is_free() const {
    static_assert((sizeof(ConfigBlock) % sizeof(uint64_t)) == 0);
    const uint64_t* view = reinterpret_cast<const uint64_t*>(this);
    static constexpr size_t view_size = sizeof(ConfigBlock) / sizeof(uint64_t);
    static constexpr uint64_t flash_default_value = std::numeric_limits<uint64_t>::max();
    for (size_t c = 0; c < view_size; c++) {
        if (view[c] != flash_default_value) {
            return false;
        }
    }
    return true;
}

cold_func_attr void Config::init() {
    // Misc initialization.
    _state_mutex.init();

    // Verify blocks for all sectors.
    for (size_t avail_sect_idx = 0; avail_sect_idx < AVAILABLE_FLASH_SECTORS.size();
         avail_sect_idx++) {
        auto si = AVAILABLE_FLASH_SECTORS[avail_sect_idx];
        for (size_t bi = 0; bi < SECTOR_BLOCK_COUNT; bi++) {
            auto bp = get_block(si, bi);
            if (bp->is_valid()) {
                _free_blocks[avail_sect_idx][bi] = false;
                _valid_blocks[avail_sect_idx][bi] = true;
            }
            else if (bp->is_free()) {
                _free_blocks[avail_sect_idx][bi] = true;
                _valid_blocks[avail_sect_idx][bi] = false;
            }
            else {
                _free_blocks[avail_sect_idx][bi] = false;
                _valid_blocks[avail_sect_idx][bi] = false;
            }
        }
    }
    // Erase sectors with all invalid blocks.
    for (size_t avail_sect_idx = 0; avail_sect_idx < AVAILABLE_FLASH_SECTORS.size();
         avail_sect_idx++) {
        auto si = AVAILABLE_FLASH_SECTORS[avail_sect_idx];
        if (_free_blocks[avail_sect_idx].none() && _valid_blocks[avail_sect_idx].none())
             [[unlikely]] {
            // No need to check for errors or update state, as we immediately reset.
            dbg.print("Erasing config sector...\n");
            stm32::flash::erase_sector(si);
            // We have limited time, unless we kick the watchdog.
            mmcc::reset_system();
        }
    }
    // Load first valid block to sram.
    bool block_found = false;
    for (size_t avail_sect_idx = 0; avail_sect_idx < AVAILABLE_FLASH_SECTORS.size();
         avail_sect_idx++) {
        auto c = _valid_blocks[avail_sect_idx].count();
        if ((block_found && c > 0) || c > 1) [[unlikely]] {
            // TODO: Auto recovery. Currently the flash needs to be flashed externally.
            dbg.print("Multiple valid config blocks have been found...\n");
        }
        if (!block_found && c > 0) {
            uint16_t v = _valid_blocks[avail_sect_idx].value<uint16_t>();
            size_t bi = etl::countr_zero(v);
            assert(bi < SECTOR_BLOCK_COUNT, __COUNTER__);
            if (bi < SECTOR_BLOCK_COUNT) {
                auto si = AVAILABLE_FLASH_SECTORS[avail_sect_idx];
                auto bp = get_block(si, bi);
                assert(bp->is_valid(), __COUNTER__);
                _state = bp->data;
                _state_available_flash_sector_index = avail_sect_idx;
                _state_block_index = bi;
                block_found = true;
            }
        }
    }
    // Save default state if no valid block exists.
    if (!block_found) {
        dbg.print("Loading default config/state...\n");
        write_state();
    }
}

Config::ConfigBlockRef::ConfigBlockRef(ConfigBlockData& dat,
                                       rtos::StaticMutex<false>& mtx) noexcept :
    _dat(&dat),
    _mtx(&mtx) {
    while (!_mtx->take_ticks()) {
    }
}
Config::ConfigBlockRef::~ConfigBlockRef() {
    if (_mtx != nullptr) {
        assert(_mtx->give(), __COUNTER__);
        _dat = nullptr;
        _mtx = nullptr;
    }
}

Config::ConfigBlockRef Config::get_current_state() {
    return Config::ConfigBlockRef(_state, _state_mutex);
}
bool Config::commit_current_state() {
    while (!_state_mutex.take_ticks()) {
    }
    _state.counter++;
    bool ok = write_state();
    assert(_state_mutex.give(), __COUNTER__);
    return ok;
}

bool Config::is_block_free(size_t avail_sect_idx, size_t bi) const {
    return _free_blocks[avail_sect_idx][bi];
}
bool Config::is_block_valid(size_t avail_sect_idx, size_t bi) const {
    return _valid_blocks[avail_sect_idx][bi];
}
bool Config::is_block_invalid(size_t avail_sect_idx, size_t bi) const {
    return !is_block_free(avail_sect_idx, bi) && !is_block_valid(avail_sect_idx, bi);
}

bool Config::write_state() {
    // Select first free block.
    size_t free_avail_sect_idx = SIZE_MAX;
    size_t free_bi = SIZE_MAX;
    bool last_free_block = true;
    for (size_t avail_sect_idx = 0; avail_sect_idx < AVAILABLE_FLASH_SECTORS.size();
         avail_sect_idx++) {
        for (size_t bi = 0; bi < SECTOR_BLOCK_COUNT; bi++) {
            if (is_block_free(avail_sect_idx, bi)) {
                if (free_avail_sect_idx == SIZE_MAX) {
                    free_avail_sect_idx = avail_sect_idx;
                    free_bi = bi;
                }
                else {
                    last_free_block = false;
                }
            }
        }
    }

    assert(free_avail_sect_idx < AVAILABLE_FLASH_SECTORS.size(), __COUNTER__);
    assert(free_bi < SECTOR_BLOCK_COUNT, __COUNTER__);
    sector_index_t free_si = AVAILABLE_FLASH_SECTORS[free_avail_sect_idx];

    // Write data.
    auto free_bp = get_block(free_si, free_bi);
    auto status = stm32::flash::write(&free_bp->data, &_state, sizeof(ConfigBlockData));
    if (status != stm32::flash::PrgError::Success) {
        dbg.print("Config block data write failure: ");
        const char* err_name = FLASH_PRG_ERROR_NAME[static_cast<int>(status)];
        dbg.print(err_name, std::strlen(err_name));
        dbg.print("!\n");
        return false;
    }

    // Calculate crc and write header.
    ConfigBlockHeader header(_state.calc_crc());
    status = stm32::flash::write(&free_bp->header, &header, sizeof(ConfigBlockHeader));
    if (status != stm32::flash::PrgError::Success) {
        dbg.print("Config block header write failure!\n");
        return false;
    }

    // Invalidate previous block, if present.
    if (_state_available_flash_sector_index < AVAILABLE_FLASH_SECTORS.size() &&
        _state_block_index < SECTOR_BLOCK_COUNT) [[likely]] {

        auto state_sector_index = AVAILABLE_FLASH_SECTORS[_state_available_flash_sector_index];
        auto prev_bp = get_block(state_sector_index, _state_block_index);

        ConfigBlockHeader zero_header;
        zero_header.crc = 0;
        zero_header.ncrc = 0;
        status = stm32::flash::write(&prev_bp->header, &zero_header, sizeof(ConfigBlockHeader));
        if (status != stm32::flash::PrgError::Success) [[unlikely]] {
            dbg.print("Couldn't invalidate previous config block!\n");
        }
        else {
            _valid_blocks[_state_available_flash_sector_index][_state_block_index] = false;
        }
    }

    // Update state to point to the new valid block.
    _valid_blocks[free_avail_sect_idx][free_bi] = true;
    _free_blocks[free_avail_sect_idx][free_bi] = false;
    _state_available_flash_sector_index = free_avail_sect_idx;
    _state_block_index = free_bi;

    /**
     * Reboot if no more free blocks available
     * Sector erasing happens at init(),
     * as to not interrupt any realtime processes.
     */
    if (last_free_block) [[unlikely]] {
        dbg.print("No more free config blocks...\n");
        mmcc::reset_system();
    }

    return true;
}

}  // namespace mmcc
