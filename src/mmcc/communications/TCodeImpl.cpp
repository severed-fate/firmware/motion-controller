#include "Actuator.hpp"
#include "Config.hpp"
#include "TCodeConfig.hpp"
#include "date/date.h"

#include <TCodeInstance.hpp>
#include <interface/rtos/RTOS.hpp>
#include <interface/tcode/Messages.hpp>
#include <interface/tcode/UBJsonBuilder.hpp>
#include <mmcc/Objects.hpp>

#include <time.h>

#define TCODE_DEVICE_NAME "Severed Fate - Motion Controller"
#define TCODE_DEVICE_VERSION "0.1.3"

namespace tcode::callbacks {

void update_interval_meta_500(codec::UBJsonObject& obj) {
    obj["suggested_update_interval"].write(static_cast<int32_t>(500));
}
void update_interval_meta_500_norm(codec::UBJsonObject& obj) {
    obj["suggested_update_interval"].write(static_cast<int32_t>(500));
    obj["min"].write(static_cast<float>(0.0f));
    obj["max"].write(static_cast<float>(1.0f));
}

response::Error identify_device() {
    auto ubjson = TCODE_INSTANCE.send_ubjson_response({response::CommandType::Device, 0});
    {
        auto root = ubjson.begin_object(3);
        root["name"].write(TCODE_DEVICE_NAME);
        root["version"].write(TCODE_DEVICE_VERSION);
        {
            auto uuid_arr = root["uuid"].begin_array(codec::UBJsonType::UInt8, 12);
            uuid_arr.write_bytes((const uint8_t*) (uintptr_t) DESIG_UNIQUE_ID_BASE, 12);
        }
    }
    return {};
}
response::Error get_device_name(const void*, size_t) {
    TCODE_INSTANCE.send_response({response::CommandType::Device, 0},
                                 response::PropertyData("name"),
                                 response::Z85Data(TCODE_DEVICE_NAME));
    return {};
}
response::Error get_device_version(const void*, size_t) {
    TCODE_INSTANCE.send_response({response::CommandType::Device, 0},
                                 response::PropertyData("version"),
                                 response::Z85Data(TCODE_DEVICE_VERSION));
    return {};
}
response::Error get_device_uuid(const void*, size_t) {
    auto ubjson = TCODE_INSTANCE.send_ubjson_response({response::CommandType::Device, 0},
                                                      response::PropertyData("uuid"));
    {
        auto root = ubjson.begin_array(codec::UBJsonType::UInt8, 12);
        root.write_bytes((const uint8_t*) (uintptr_t) DESIG_UNIQUE_ID_BASE, 12);
    }
    return {};
}

response::Error get_config_save_count(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    TCODE_INSTANCE.send_response(
         {response::CommandType::Device, 3}, "save_count",
         response::Z85Data((const uint8_t*) &cfg->counter, sizeof(uint32_t)));
    return {};
}
response::Error save_config(void*, size_t) {
    mmcc::dbg.print("Saving config...\n");
    mmcc::config.commit_current_state();
    /* Ignore errors, or report errors to user with an event property? */
    return {};
}

response::Error get_rtos_stats() {
    auto ubjson = TCODE_INSTANCE.send_ubjson_response({response::CommandType::Device, 4});
    {
        auto root = ubjson.begin_object();
        root["tickcount"].write_smart(rtos::Kernel::get_tickcount());
        root["taskcount"].write_smart(rtos::Kernel::get_number_of_tasks());
    }
    return {};
}

response::Error get_rtos_time(const void*, size_t) {
    int64_t timestamp = mmcc::lftask.get_unix_time();
    TCODE_INSTANCE.send_response(
         {response::CommandType::Device, 4}, "time",
         response::Z85Data((const uint8_t*) &timestamp, sizeof(timestamp)));
    return {};
}
response::Error set_rtos_time(void* dat, size_t n) {
    if (n != 8) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    int64_t timestamp;
    memcpy(&timestamp, dat, 8);
    bool ok = mmcc::lftask.set_time(timestamp);
    if (!ok) {
        return response::Error(response::ErrorCode::Generic, "Cannot set time!");
    }
    else {
        return {};
    }
}
response::Error get_rtos_date(const void*, size_t) {
    auto ubjson =
         TCODE_INSTANCE.send_ubjson_response({response::CommandType::Device, 4}, "date");
    {
        auto root = ubjson.begin_object();
        auto [year_month_day, weekday, daytime] = mmcc::lftask.get_date_time();
        root["year"].write_smart(static_cast<int32_t>(year_month_day.year()));
        root["month"].write_smart(static_cast<uint32_t>(year_month_day.month()));
        root["day"].write_smart(static_cast<uint32_t>(year_month_day.day()));
        if (weekday == date::Monday) {
            root["wday"].write("Monday");
        }
        else if (weekday == date::Tuesday) {
            root["wday"].write("Tuesday");
        }
        else if (weekday == date::Wednesday) {
            root["wday"].write("Wednesday");
        }
        else if (weekday == date::Thursday) {
            root["wday"].write("Thursday");
        }
        else if (weekday == date::Friday) {
            root["wday"].write("Friday");
        }
        else if (weekday == date::Saturday) {
            root["wday"].write("Saturday");
        }
        else if (weekday == date::Sunday) {
            root["wday"].write("Sunday");
        }
        root["hour"].write_smart(daytime.hours().count());
        root["minutes"].write_smart(daytime.minutes().count());
        root["seconds"].write_smart(daytime.seconds().count());
    }
    return {};
}
response::Error get_log(const void*, size_t) {
    auto& log_buffer = mmcc::dbg.get_log_buffer();
    size_t s;
    {
        rtos::Kernel::enter_critical();
        s = log_buffer.size();
        rtos::Kernel::exit_critical();
    }
    char* tmp = new char[s];
    if (tmp == nullptr) {
        return response::Error(response::ErrorCode::Allocation,
                               "get_log: Failure to allocate temp buffer.");
    }
    size_t c = 0;
    {
        rtos::Kernel::enter_critical();
        const auto end = log_buffer.end();
        for (auto it = log_buffer.begin(); it != end; ++it) {
            if (c >= s) {
                break;
            }
            tmp[c] = *it;
            c++;
        }
        rtos::Kernel::exit_critical();
    }

    TCODE_INSTANCE.send_response({response::CommandType::Device, 4}, "log",
                                 response::Z85Data((uint8_t*) tmp, c));
    delete[] tmp;
    return {};
}

}  // namespace tcode::callbacks
