#include "flash_prg.h"

#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/pwr.h>
#include <stdbool.h>

#ifndef nop
    #define nop __asm__("nop")
    #define nop2                                                                               \
        nop;                                                                                   \
        nop
    #define nop3                                                                               \
        nop2;                                                                                  \
        nop
    #define nop4                                                                               \
        nop3;                                                                                  \
        nop
#endif

/**
 * The following implementation is specific to the STM32F4 line, and tested on STM32F411.
 *
 * NOTE: No other code must be allowed run while inside the following functions.
 * As such, interrupts and faults must be disabled before calling them.
 */
sram_func_attr static inline bool flash_prg_voltage_ok(void) {
    return !(PWR_CSR & PWR_CSR_PVDO);
}

sram_func_attr static inline void flash_prg_dcache_enable(void) {
    FLASH_ACR |= FLASH_ACR_DCEN;
}
sram_func_attr static inline void flash_prg_dcache_disable(void) {
    FLASH_ACR &= ~FLASH_ACR_DCEN;
}
sram_func_attr static inline void flash_prg_icache_enable(void) {
    FLASH_ACR |= FLASH_ACR_ICEN;
}
sram_func_attr static inline void flash_prg_icache_disable(void) {
    FLASH_ACR &= ~FLASH_ACR_ICEN;
}
sram_func_attr static inline void flash_prg_dcache_reset(void) {
    FLASH_ACR |= FLASH_ACR_DCRST;
}
sram_func_attr static inline void flash_prg_icache_reset(void) {
    FLASH_ACR |= FLASH_ACR_ICRST;
}

sram_func_attr static inline void flash_prg_set_program_size(uint32_t psize) {
    FLASH_CR &= ~(FLASH_CR_PROGRAM_MASK << FLASH_CR_PROGRAM_SHIFT);
    FLASH_CR |= psize << FLASH_CR_PROGRAM_SHIFT;
}
sram_func_attr static inline void flash_prg_unlock(void) {
    /* Authorize the FPEC access. */
    FLASH_KEYR = FLASH_KEYR_KEY1;
    FLASH_KEYR = FLASH_KEYR_KEY2;
}
sram_func_attr static inline void flash_prg_lock(void) {
    FLASH_CR |= FLASH_CR_LOCK;
}
sram_func_attr static inline void flash_prg_pipeline_stall(void) {
    __asm__ volatile("dsb" ::: "memory");
}

sram_func_attr FlashPrgError _flash_prg_erase(sector_index_t sector_index) {
    // Validate parameters.
    if (sector_index < 0 || sector_index > FLASH_CR_SNB_MASK) {
        return FLASH_PRG_ERROR_RUNTIME;
    }
    // Sanity check: Check if flash is busy.
    if (FLASH_SR & FLASH_SR_BSY) {
        return FLASH_PRG_ERROR_BUSY;
    }
    /* Sector numbering is not contiguous internally! */
    if (sector_index >= 12) {
        sector_index += 4;
    }
    // Pre-check voltage.
    if (!flash_prg_voltage_ok()) {
        return FLASH_PRG_ERROR_LOW_VOLTAGE;
    }

    bool voltage_dropped = false;
    { /* No returns allowed section. */
        // Disable caches, to be invalidated later.
        bool icache_enabled = false;
        bool dcache_enabled = false;
        {
            uint32_t acr = FLASH_ACR;
            if (acr & FLASH_ACR_ICEN) {
                icache_enabled = true;
                flash_prg_icache_disable();
            }
            if (acr & FLASH_ACR_DCEN) {
                dcache_enabled = true;
                flash_prg_dcache_disable();
            }
        }
        // Prepare flash operation.
        flash_prg_unlock();
        flash_prg_set_program_size(FLASH_CR_PROGRAM_X32);
        FLASH_CR &= ~(FLASH_CR_SER | (FLASH_CR_SNB_MASK << FLASH_CR_SNB_SHIFT));
        // Start the erase.
        FLASH_CR |= FLASH_CR_SER | ((sector_index & FLASH_CR_SNB_MASK) << FLASH_CR_SNB_SHIFT);
        FLASH_CR |= FLASH_CR_STRT;
        // Wait for erase operation to complete, while continuing to monitor the voltage.
        do {
            // nop;
            if (!flash_prg_voltage_ok()) {
                voltage_dropped = true;
            }
        } while (FLASH_SR & FLASH_SR_BSY);
        // Flash operation clean-up.
        FLASH_CR &= ~(FLASH_CR_SER | (FLASH_CR_SNB_MASK << FLASH_CR_SNB_SHIFT));
        flash_prg_lock();
        // Invalidate and re-enable caches.
        if (icache_enabled) {
            flash_prg_icache_reset();
            flash_prg_icache_enable();
        }
        if (dcache_enabled) {
            flash_prg_dcache_reset();
            flash_prg_dcache_enable();
        }
    }

    // Check for errors.
    if (FLASH_SR & FLASH_SR_WRPERR) {
        FLASH_SR = FLASH_SR_WRPERR;
        return FLASH_PRG_ERROR_WPROT;
    }
    if (voltage_dropped) {
        return FLASH_PRG_ERROR_LOW_VOLTAGE;
    }

    return FLASH_PRG_ERROR_SUCCESS;
}

static const uint32_t FLASH_SR_WRITE_ERRORS_MASK =
     FLASH_SR_WRPERR | FLASH_SR_PGAERR | FLASH_SR_PGPERR | FLASH_SR_PGSERR;

sram_func_attr FlashPrgError _flash_prg_write_bytes(uint8_t* dst, const uint8_t* src,
                                                    const size_t n) {
    // Verify that destination is in flash.
    if (((uintptr_t) dst) < 0x08000000UL || ((uintptr_t) dst) >= 0x10000000UL) {
        return FLASH_PRG_ERROR_RUNTIME;
    }
    if (n == 0) {
        return FLASH_PRG_ERROR_SUCCESS;
    }
    // Sanity check: Check if flash is busy.
    if (FLASH_SR & FLASH_SR_BSY) {
        return FLASH_PRG_ERROR_BUSY;
    }

    size_t i;
    { /* No returns allowed section. */
        // Clear previous errors.
        FLASH_SR = FLASH_SR_WRITE_ERRORS_MASK;
        // Prepare flash operation.
        flash_prg_unlock();
        flash_prg_set_program_size(FLASH_CR_PROGRAM_X8);
        FLASH_CR |= FLASH_CR_PG;
        // Programming loop.
        for (i = 0; i < n; i++) {
            MMIO8(dst + i) = src[i];
            // Wait for completion.
            flash_prg_pipeline_stall();
            do {
                nop;
            } while (FLASH_SR & FLASH_SR_BSY);
            // Check for errors.
            if (FLASH_SR & FLASH_SR_WRITE_ERRORS_MASK) {
                // Partial write, the specific error is checked at epilogue.
                break;
            }
        }
        // Flash operation clean-up.
        FLASH_CR &= ~FLASH_CR_PG;
        flash_prg_lock();
    }

    // Check for errors.
    uint32_t fsr = FLASH_SR;
    FLASH_SR = FLASH_SR_WRITE_ERRORS_MASK;
    if (fsr & FLASH_SR_WRPERR) {
        return FLASH_PRG_ERROR_WPROT;
    }
    if (fsr & FLASH_SR_PGAERR) {
        return FLASH_PRG_ERROR_ALIGNMENT;
    }
    if (fsr & FLASH_SR_PGPERR) {
        return FLASH_PRG_ERROR_PARALLELISM;
    }
    if (fsr & FLASH_SR_PGSERR) {
        return FLASH_PRG_ERROR_SEQUENCE;
    }

    if (i != n) {
        return FLASH_PRG_ERROR_PARTIAL;
    }
    return FLASH_PRG_ERROR_SUCCESS;
}

sram_func_attr FlashPrgError _flash_prg_write_words(uint32_t* dst, const uint32_t* src,
                                                    const size_t n) {
    // Verify that destination is in flash.
    if (((uintptr_t) dst) < 0x08000000UL || ((uintptr_t) dst) >= 0x10000000UL) {
        return FLASH_PRG_ERROR_RUNTIME;
    }
    // Verify destination alignment.
    if ((((uintptr_t) dst) % 4) != 0) {
        return FLASH_PRG_ERROR_RUNTIME;
    }
    // Skip if no data.
    if (n == 0) {
        return FLASH_PRG_ERROR_SUCCESS;
    }
    // Sanity check: Check if flash is busy.
    if (FLASH_SR & FLASH_SR_BSY) {
        return FLASH_PRG_ERROR_BUSY;
    }
    // Pre-check voltage.
    if (!flash_prg_voltage_ok()) {
        return FLASH_PRG_ERROR_LOW_VOLTAGE;
    }

    size_t i;
    bool voltage_dropped = false;
    { /* No returns allowed section. */
        // Clear previous errors.
        FLASH_SR = FLASH_SR_WRITE_ERRORS_MASK;
        // Prepare flash operation.
        flash_prg_unlock();
        flash_prg_set_program_size(FLASH_CR_PROGRAM_X32);
        FLASH_CR |= FLASH_CR_PG;
        // Programming loop.
        for (i = 0; i < n; i++) {
            MMIO32(dst + i) = src[i];
            // Wait for completion.
            flash_prg_pipeline_stall();
            do {
                // nop;
                if (!flash_prg_voltage_ok()) {
                    voltage_dropped = true;
                }
            } while (FLASH_SR & FLASH_SR_BSY);
            // Check for errors.
            if (voltage_dropped || (FLASH_SR & FLASH_SR_WRITE_ERRORS_MASK)) {
                // Partial write, the specific error is checked at epilogue.
                break;
            }
        }
        // Flash operation clean-up.
        FLASH_CR &= ~FLASH_CR_PG;
        flash_prg_lock();
    }

    // Check for errors.
    uint32_t fsr = FLASH_SR;
    FLASH_SR = FLASH_SR_WRITE_ERRORS_MASK;
    if (fsr & FLASH_SR_WRPERR) {
        return FLASH_PRG_ERROR_WPROT;
    }
    if (fsr & FLASH_SR_PGAERR) {
        return FLASH_PRG_ERROR_ALIGNMENT;
    }
    if (fsr & FLASH_SR_PGPERR) {
        return FLASH_PRG_ERROR_PARALLELISM;
    }
    if (fsr & FLASH_SR_PGSERR) {
        return FLASH_PRG_ERROR_SEQUENCE;
    }

    if (voltage_dropped) {
        return FLASH_PRG_ERROR_LOW_VOLTAGE;
    }

    if (i != n) {
        return FLASH_PRG_ERROR_PARTIAL;
    }
    return FLASH_PRG_ERROR_SUCCESS;
}
