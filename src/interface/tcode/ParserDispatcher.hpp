#ifndef MMCC_TCODE_PARSERDISPATCHER_HPP
#define MMCC_TCODE_PARSERDISPATCHER_HPP

#include <TCodeConfig.hpp>
#include <interface/rtos/RTOS.hpp>
#include <interface/tcode/Messages.hpp>
#include <interface/tcode/Parser.hpp>
#include <interface/tcode/TCode.hpp>
#include <interface/tcode/UBJsonBuilder.hpp>
#include <rtos/RTOSBuffers.hpp>
#include <utils/Class.hpp>

#include <cstddef>
#include <cstdint>
#include <functional>
#include <tuple>

#include <etl/array.h>

namespace tcode {

namespace parser {

bool tokenize(yyParser& parser, const char* const data, const size_t len,
              utils::BufferPoolAllocator& tmp_alloc);

}  // namespace parser

class ParserDispatcher : public meta::INonCopyable {
   public:
    /** State based on what/how we are responding. */
    enum class State : uint8_t { Inactive, Timeouts, Parsing, UBJBuilder };

    struct TimeoutEntry {
        TickType_t next_timeout;
        const decltype(config::property_registry)::element_type* prop_reg_ref;

        constexpr TimeoutEntry() : next_timeout(0), prop_reg_ref(nullptr) {}
        constexpr TimeoutEntry(
             TickType_t next_timeout_,
             const decltype(config::property_registry)::element_type& prop_reg_ref_) :
            next_timeout(next_timeout_),
            prop_reg_ref(&prop_reg_ref_) {}

        constexpr operator bool() const {
            return prop_reg_ref != nullptr;
        }

        static constexpr bool comparator(const TimeoutEntry& a, const TimeoutEntry& b) {
            // Invert for min heap.
            return a.next_timeout > b.next_timeout;
        }
    };

    struct EventQueueEntry {
        const callbacks::Property* meta;
        char data[];
    };
    template<typename T> struct EventQueueEntryT {
        const callbacks::Property* meta;
        T data;
    };
    template<> struct EventQueueEntryT<void> {
        const callbacks::Property* meta;
    };

   protected:
    State _state = State::Inactive;
    /** Holds previous state when in ubjson building mode. */
    State _previous_state = State::Inactive;
    size_t _response_code_count = 0;

    size_t _input_buffer_usage = 0;
    char _input_buffer[2048];

    size_t _output_buffer_usage = 0;
    char _output_buffer[96];

    utils::BufferStaticPoolAllocator<(sizeof(_input_buffer) / 5) * 4> _decode_buffer;
    parser::yyParser _parser_state;

    etl::array<uint32_t, config::property_registry.size()> _timeouts_state;
    size_t _timeouts_queue_usage = 0;
    etl::array<TimeoutEntry, config::property_registry.size()> _timeouts_queue;

    rtos::StaticMessageBuffer<8 * 1024> _event_queue;

   public:
    constexpr ParserDispatcher() = default;
    ~ParserDispatcher() = default;

    size_t get_output_buffer_space() const {
        return sizeof(_output_buffer) - _output_buffer_usage;
    }

   public:
    /** Public interface. */
    void send_response(response::CommandIndex cmd_idx, response::Z85Data data);
    void send_response(response::CommandIndex cmd_idx, response::PropertyData prop,
                       response::Z85Data data);
    codec::UBJsonInstance send_ubjson_response(response::CommandIndex cmd_idx);
    codec::UBJsonInstance send_ubjson_response(response::CommandIndex cmd_idx,
                                               response::PropertyData prop);
    void send_response(response::Error status = {});

    void send_raw_data(const void* data, size_t n);
    void send_raw_byte(char ch);
    /** Send data encoded in z85 */
    void send_z85_data(const void* data, size_t n, uint8_t null_symbol = '\0');

    uint32_t get_timeout(const decltype(config::property_registry)::element_type& prop_index);

    template<typename T> bool pend_event(const EventQueueEntryT<T>& entry) {
        return pend_event(reinterpret_cast<const EventQueueEntry*>(&entry), sizeof(T));
    }
    template<> bool pend_event<void>(const EventQueueEntryT<void>& entry) {
        return pend_event(reinterpret_cast<const EventQueueEntry*>(&entry), 0);
    }

   private:
    /** Private interface. */
    void flush_output();

    bool start_response();
    bool finalize_response();
    bool start_timeouts();
    bool finalize_timeouts();

    void start_ubjson();
    void finalize_ubjson();
    friend class codec::UBJsonInstance;

    void on_new_response();
    void send_partial_response(response::CommandIndex cmd_idx);
    void send_partial_response(response::CommandIndex cmd_idx, response::PropertyData prop);

    response::Error register_timeout(
         const decltype(config::property_registry)::element_type& prop_index,
         uint32_t interval);

    bool pend_event(const EventQueueEntry* e, size_t n);

   protected:
    /** Interface for inheritors. */
    /**
     * Reset internal state and buffer.
     * Call once per new connection.
     */
    void reset();
    /**
     * Handles any timeout events based on the current rtos tick timer.
     * @returns tick count of the next timeout.
     */
    TickType_t handle_timeouts();
    /**
     * @returns false when input buffer overflow or any other error occurred.
     */
    bool handle_input(const char* input_staging_buffer, size_t input_staging_buffer_len);

   public:
    /** Tokenizer/Parser interface. */
    static response::Error _on_request(request::AxisUpdateData);
    static response::Error _on_request(request::AxisUpdateData, request::IntervalData);
    static response::Error _on_request(request::AxisUpdateData, request::SpeedData);
    static response::Error _on_request(request::CommandIndex);
    static response::Error _on_request(request::CommandIndex, request::PropertyData);
    static response::Error _on_request(request::CommandIndex, request::PropertyData,
                                       request::Z85Data);
    static response::Error _on_request(request::CommandIndex, request::PropertyData,
                                       request::IntervalData);
    static response::Error _on_request_stop(request::CommandIndex);
    static response::Error _on_request_stop();

    static void _finalize_response(response::Error status = {});
};

}  // namespace tcode

#endif /*MMCC_TCODE_PARSERDISPATCHER_HPP*/