#ifndef MMCC_OBJECTS_HPP
#define MMCC_OBJECTS_HPP

#include <Actuator.hpp>
#include <COM.hpp>
#include <ConfigBlock.hpp>
#include <DebugPrinter.hpp>
#include <LFT.hpp>
#include <Sonar.hpp>
#include <TrafficController.hpp>

namespace mmcc {

extern DebugPrinter dbg;
extern Config config;

// Static/Core tasks.
extern LFTask lftask;
extern USBTask usb_task;
extern TrafficController trfctl_task;
extern Actuator actuator_task;
extern SonarResolverTask sonar_task;

// Static task initializer.
cold_func_attr void init();

}  // namespace mmcc

// Unified callbacks.
extern "C" abort_func_attr void on_abort(void);

#endif /*MMCC_OBJECTS_HPP*/