#include "Sonar.hpp"

#include <Objects.hpp>
#include <interface/io/Core.hpp>
#include <interface/io/GPIO.hpp>
#include <interface/io/Timer.hpp>
#include <interface/rtos/RTOS.hpp>
#include <interface/rtos/RTOSTask.hpp>
#include <mmcc/board/Config.hpp>
#include <mmcc/communications/TCodeConfig.hpp>
#include <mmcc/communications/TCodeImplSensor.hpp>

#include <boot.h>
#include <etl/absolute.h>
#include <etl/algorithm.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

namespace mmcc {

bool SonarResolverTask::_measurement_mode = false;
std::pair<bool, bool> SonarResolverTask::_measurement_noise = {false, false};
std::pair<uint16_t, uint16_t> SonarResolverTask::_measurement = {
     std::numeric_limits<uint16_t>::max(), 0};

void SonarResolverTask::init() {
    SONAR_TIMER::activate();
    // Configure timer for max resolution at ~10Hz.
    SONAR_TIMER::set_prescaler(145);
    SONAR_TIMER::set_period(65535);
    SONAR_TIMER::set_counter(0);
    // Trigger active timer - for 0.1 ms.
    SONAR_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC1,
                             stm32::timer::OutputChannelMode::Frozen);
    // Timer operation mode and hooking.
    SONAR_TIMER::set_mode(stm32::timer::RunMode::OneShotMode);
    SONAR_TIMER::set_interrupt(TIM_DIER_CC1IE, true);
    runtime_vector_table.irq[NVIC_TIM1_UP_TIM10_IRQ] = __timer_handler;
    nvic_set_priority(NVIC_TIM1_UP_TIM10_IRQ, 0x30);
    nvic_enable_irq(NVIC_TIM1_UP_TIM10_IRQ);
    // Prepare echo signal edge capturing.
    constexpr auto sonar_echo_gpio_port =
         SONAR_ECHO_PIN.get_port_addresses<SONAR_ECHO_PIN.get_port_count()>()[0];
    exti_set_trigger(EXTI10, EXTI_TRIGGER_BOTH);
    exti_select_source(EXTI10, sonar_echo_gpio_port);
    exti_enable_request(EXTI10);
    runtime_vector_table.irq[NVIC_EXTI15_10_IRQ] = __echo_signal_handler;
    nvic_set_priority(NVIC_EXTI15_10_IRQ, 0x20);
    nvic_enable_irq(NVIC_EXTI15_10_IRQ);
}
bool SonarResolverTask::start() {
    return rtos::StaticTask<384>::start<SonarResolverTask, &SonarResolverTask::run>("sonar", 7);
}
SonarResolverTask::~SonarResolverTask() {
    nvic_disable_irq(NVIC_EXTI15_10_IRQ);
    runtime_vector_table.irq[NVIC_EXTI15_10_IRQ] = NULL;
    exti_disable_request(EXTI10);
    nvic_disable_irq(NVIC_TIM1_UP_TIM10_IRQ);
    runtime_vector_table.irq[NVIC_TIM1_UP_TIM10_IRQ] = NULL;
    SONAR_TIMER::deactivate();
}

void SonarResolverTask::run() {
    while (true) {
        if (_scanning) {
            // Start new scan.
            TickType_t relative_timestamp = rtos::Kernel::get_tickcount();
            _measurement_mode = false;
            _measurement_noise = {false, false};
            _measurement.first = std::numeric_limits<uint16_t>::max();
            _measurement.second = 0;
            SONAR_TIMER::set_oc_value(stm32::timer::OutputChannel::OC1, TRIGGER_INTERVAL);
            {
                InterruptGuard<> _g;
                PINSET_SET_ALL_CTX(SONAR_TRIG_PIN, _g);
                SONAR_TIMER::enable();
            }
            // Wait for scan completion (with dynamic update rate control).
            delay_ticks_until(relative_timestamp, rtos::ms2ticks(50));
            if (_measurement_mode) {
                // Not finished - wait a bit more!
                delay_ticks_until(relative_timestamp, rtos::ms2ticks(50));
            }
            else {
                // Already finished measurement cycle - use faster update rate.
            }
            SONAR_TIMER::generate_event(stm32::timer::Event::UG);

            // Ensure correct timer state (disabled).
            _measurement_mode = false;
            while (SONAR_TIMER::is_enabled()) {
                // Watchdog should catch this if it deadlocks.
                nop;
            }

            // Verify and copy current distance measurement.
            if (_is_measurement_valid()) {
                _update_measurement();
                /*
                dbg.printf("%7u\t%hu\t%u\t%u\r\n", tick, _last_valid_measurement,
                           (uint32_t) _last_valid_measurement_scaled,
                           _invalid_measurement_counter);
                */
            }
            else {
                _invalid_measurement_counter++;
                /*
                auto tick = rtos::Kernel::get_tickcount();
                dbg.printf("%7u\t%hu\t%hu\t%c\t%c\r\n", tick, _measurement.first,
                           _measurement.second, _measurement_noise.first ? 'T' : 'F',
                           _measurement_noise.second ? 'T' : 'F');
                */
            }
        }
        else { /* !_scanning */
            notify_wait_ticks(0, 0, 0, rtos::ms2ticks(3000));
        }
    }
}

bool SonarResolverTask::_is_measurement_valid() {
    return _measurement.first < _measurement.second && !_measurement_noise.first &&
           !_measurement_noise.second &&
           (_measurement.second - _measurement.first) <= MAX_VALID_RAW_MEASUREMENT;
}

namespace {
constexpr float __interp_linear(float x, float x0, float y0, float x1, float y1) {
    float a = (y1 - y0) / (x1 - x0);
    float y = y0 + (x - x0) * a;
    return y;
}
}  // namespace
void SonarResolverTask::_update_measurement() {
    _latest_valid_measurement = _measurement.second - _measurement.first;
    float previous_scaled = _latest_valid_measurement_scaled;
    float current_scaled = _latest_valid_measurement * MILLIMETER_SCALE_FACTOR;
    _latest_valid_measurement_scaled = current_scaled;

    // Load user settings.
    bool plot_enabled;
    bool norm_invert;
    float norm_near;
    float norm_far;
    float norm_diff;
    float norm_mag;
    float flt_t1;
    float flt_t2;
    float flt_tsel;
    float flt_a1;
    float flt_a2;
    TickType_t tick;
    {
        auto cfg = mmcc::config.get_current_state();
        plot_enabled = cfg->actuator.plot_enable;
        norm_invert = cfg->sonar.invert;
        norm_near = cfg->sonar.near;
        norm_far = cfg->sonar.far;
        norm_diff = norm_far - norm_near;
        norm_mag = etl::absolute(norm_diff);
        flt_t1 = cfg->sonar.threshold1 * norm_mag;
        flt_t2 = cfg->sonar.threshold2 * norm_mag;
        flt_tsel = cfg->sonar.threshold_selector;
        flt_a1 = cfg->sonar.alpha1;
        flt_a2 = cfg->sonar.alpha2;
        tick = rtos::Kernel::get_tickcount();
    }

    if (!_pending_filter_reset) {
        // Apply filtering to scaled measurement.
        float previous_filtered = _latest_valid_measurement_filtered;
        float t_sca = etl::absolute(current_scaled - previous_scaled);
        float t_flt = etl::absolute(current_scaled - previous_filtered);
        float t = flt_tsel * t_sca + (1.f - flt_tsel) * t_flt;
        float a = __interp_linear(t, flt_t1, flt_a1, flt_t2, flt_a2);
        float flt_amin = etl::min(flt_a1, flt_a2);
        float flt_amax = etl::max(flt_a1, flt_a2);
        a = etl::clamp(a, flt_amin, flt_amax);
        float current_filtered = a * current_scaled + (1.f - a) * previous_filtered;
        _latest_valid_measurement_filtered = current_filtered;
    }
    else /* _pending_filter_reset == true */ [[unlikely]] {
        _latest_valid_measurement_filtered = current_scaled;
        _pending_filter_reset = false;
    }

    {  // Apply normalization to filtered output.
        float current_normalized =
             (_latest_valid_measurement_filtered - norm_near) / (norm_diff);
        current_normalized = etl::clamp(current_normalized, 0.f, 1.f);
        if (norm_invert) {
            current_normalized = 1.f - current_normalized;
        }
        _latest_valid_measurement_normalized = current_normalized;
    }

    // Send plot data if enabled.
    if (trfctl_task.is_connected() && plot_enabled) {
        tcode::callbacks::SonarPlotData plot_data = {
             rtos::ticks2sec(tick), _latest_valid_measurement_scaled * 1e-03f,
             _latest_valid_measurement_filtered * 1e-03f, _latest_valid_measurement_normalized};
        tcode::ParserDispatcher::EventQueueEntryT<tcode::callbacks::SonarPlotData> ev = {
             &tcode::config::event_meta_sonar_plot, plot_data};
        trfctl_task.pend_event(ev);
    }
}

void SonarResolverTask::__timer_handler() {
    if (SONAR_TIMER::get_interrupt_flags(TIM_SR_CC1IF)) {
        if (!_measurement_mode) {
            // Release trigger, which starts the measurement cycle.
            _measurement_mode = true;
            PINSET_RESET_ALL_CTX(SONAR_TRIG_PIN, InterruptGuard<true>());
        }
        else { /* _measurement_mode==true */
            // Delayed measurement completion.
            _measurement_mode = false;
        }
    }
}
void SonarResolverTask::__echo_signal_handler() {
    if (exti_get_flag_status(EXTI10)) {
        exti_reset_request(EXTI10);
        if (_measurement_mode) {
            bool level = PINSET_TEST_ALL_CTX(SONAR_ECHO_PIN, InterruptGuard<true>());
            if (level) {
                if (_measurement.first == std::numeric_limits<uint16_t>::max()) {
                    // Low to high.
                    _measurement.first = SONAR_TIMER::get_counter();
                }
                else {
                    _measurement_noise.first = true;
                }
            }
            else {
                if (_measurement.second == 0) {
                    // High to low.
                    _measurement.second = SONAR_TIMER::get_counter();
                    // Delay measurement completion.
                    SONAR_TIMER::set_oc_value(
                         stm32::timer::OutputChannel::OC1,
                         SONAR_TIMER::get_counter() + MEASUREMENT_COMPLETE_INTERVAL);
                }
                else {
                    _measurement_noise.second = true;
                }
            }
        }
    }
}

void SonarResolverTask::start_scanning() {
    _scanning = true;
    notify(0, NotificationAction::eNoAction, 0);
}
void SonarResolverTask::stop_scanning() {
    _scanning = false;
    notify(0, NotificationAction::eNoAction, 0);
}

}  // namespace mmcc
