#include "LFT.hpp"

#include <Objects.hpp>

namespace mmcc {

void LFTask::ResetReason::print() {
    dbg.print("Reset Source: ");
    bool first = true;
    if (brownout) {
        dbg.print("BOR");
        first = false;
    }
    if (pin) {
        if (!first) {
            dbg.print(", ");
        }
        else {
            first = false;
        }
        dbg.print("PIN");
    }
    if (power) {
        if (!first) {
            dbg.print(", ");
        }
        else {
            first = false;
        }
        dbg.print("PWR");
    }
    if (software) {
        if (!first) {
            dbg.print(", ");
        }
        else {
            first = false;
        }
        dbg.print("SFT");
    }
    if (independent_watchdog) {
        if (!first) {
            dbg.print(", ");
        }
        else {
            first = false;
        }
        dbg.print("IWD");
    }
    if (window_watchdog) {
        if (!first) {
            dbg.print(", ");
        }
        else {
            first = false;
        }
        dbg.print("WWD");
    }
    if (low_power) {
        if (!first) {
            dbg.print(", ");
        }
        else {
            first = false;
        }
        dbg.print("LPR");
    }
    dbg.print("\n");
}

void LFTask::stacktrace_minimal_t::print() const {
    dbg.printf("-@0x%08X\n", FaultData::decode_address(return_addr));
}
void LFTask::stacktrace_small_t::print() const {
    dbg.printf("0x%08X@0x%08X\n", FaultData::decode_address(frame_addr),
               FaultData::decode_address(return_addr));
}

void LFTask::CoreHardFaultData::print() const {
    dbg.print("Flags: ");
    bool first_flag = true;
    if (psp_frame) {
        first_flag = false;
        /* MSP is implicitly default */
        dbg.print("PSP");
    }
    if (debugevt) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("DEBUGEVT");
    }
    if (forced) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("FORCED");
    }
    if (has_usage) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("USAGE");
    }
    if (has_bus) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("BUS");
    }
    if (has_mem_manage) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("MEM_MANAGE");
    }
    if (vecttbl) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("VECTTBL");
    }
    if (first_flag) {
        dbg.print("-\n");
    }
    else {
        dbg.print("\n");
    }
    dbg.printf("pc = 0x%08X\n", FaultData::decode_address(faulty_pc));
    dbg.printf("lr = 0x%08X\n", FaultData::decode_address(faulty_lr));
}
void LFTask::CoreUsageFaultData::print() const {
    dbg.print("Flags: ");
    bool first_flag = true;
    if (psp_frame) {
        first_flag = false;
        /* MSP is implicitly default */
        dbg.print("PSP");
    }
    if (divbyzero) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("DIVBYZERO");
    }
    if (unaligned) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("UNALIGNED");
    }
    if (nocp) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("NOCP");
    }
    if (invpc) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("INVPC");
    }
    if (invstate) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("INVSTATE");
    }
    if (undefinstr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("UNDEFINSTR");
    }
    if (first_flag) {
        dbg.print("-\n");
    }
    else {
        dbg.print("\n");
    }
    dbg.printf("pc = 0x%08X\n", FaultData::decode_address(faulty_pc));
    dbg.printf("lr = 0x%08X\n", FaultData::decode_address(faulty_lr));
}
void LFTask::CoreBusFaultData::print() const {
    dbg.print("Flags: ");
    bool first_flag = true;
    if (psp_frame) {
        first_flag = false;
        /* MSP is implicitly default */
        dbg.print("PSP");
    }
    if (bfarvalid) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("BFARVALID");
    }
    if (lsperr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("LSPERR");
    }
    if (stkerr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("STKERR");
    }
    if (unstkerr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("UNSTKERR");
    }
    if (impreciserr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("IMPRECISERR");
    }
    if (preciserr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("PRECISERR");
    }
    if (ibuserr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("IBUSERR");
    }
    if (first_flag) {
        dbg.print("-\n");
    }
    else {
        dbg.print("\n");
    }
    dbg.printf("pc = 0x%08X\n", FaultData::decode_address(faulty_pc));
    dbg.printf("lr = 0x%08X\n", FaultData::decode_address(faulty_lr));
    if (bfarvalid) {
        dbg.printf("bfar = 0x%08X\n", bfar);
    }
}
void LFTask::CoreMemManageFaultData::print() const {
    dbg.print("Flags: ");
    bool first_flag = true;
    if (psp_frame) {
        first_flag = false;
        /* MSP is implicitly default */
        dbg.print("PSP");
    }
    if (mmarvalid) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("MMARVALID");
    }
    if (mlsperr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("MLSPERR");
    }
    if (mstkerr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("MSTKERR");
    }
    if (munstkerr) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("MUNSTKERR");
    }
    if (daccviol) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("DACCVIOL");
    }
    if (iaccviol) {
        if (!first_flag) {
            dbg.print(", ");
        }
        else {
            first_flag = false;
        }
        dbg.print("IACCVIOL");
    }
    if (first_flag) {
        dbg.print("-\n");
    }
    else {
        dbg.print("\n");
    }
    dbg.printf("pc = 0x%08X\n", FaultData::decode_address(faulty_pc));
    dbg.printf("lr = 0x%08X\n", FaultData::decode_address(faulty_lr));
    if (mmarvalid) {
        dbg.printf("bfar = 0x%08X\n", mmfar);
    }
}

void LFTask::EabiAssertFaultData::print() const {
    switch (subtype) {
        case EabiAssertType::PureVirtual: {
            dbg.print("Subtype: PureVirtual\n");
        } break;
        case EabiAssertType::PureDeleted: {
            dbg.print("Subtype: PureDeleted\n");
        } break;
        case EabiAssertType::Terminate: {
            dbg.print("Subtype: Terminate\n");
        } break;
        case EabiAssertType::Unexpected: {
            dbg.print("Subtype: Unexpected\n");
        } break;
        case EabiAssertType::Invalid:
        default: {
        } break;
    }
    dbg.print("Traceback:\n");
    dbg.print(" 0. ");
    trace0.print();
    dbg.print(" 1. ");
    trace1.print();
    dbg.print(" 2. ");
    trace2.print();
}
void LFTask::EabiStackOverflowFaultData::print() const {
    dbg.print("Traceback:\n");
    dbg.print(" 0. ");
    trace0.print();
    dbg.print(" 1. ");
    trace1.print();
    dbg.print(" 2. ");
    trace2.print();
}

void LFTask::LibAssertFaultData::print() const {
    dbg.printf("library_id = %u\n", (uint32_t) library_id);
    dbg.printf("file_id = 0x%04X\n", (uint32_t) file_id);
    dbg.printf("instance_id = %u\n", (uint32_t) instance_id);
    dbg.print("Traceback:\n");
    dbg.print(" 0. ");
    trace0.print();
    dbg.print(" 1. ");
    trace1.print();
}
void LFTask::LibExitFaultData::print() const {
    dbg.printf("exit_code = %d\n", exit_code);
    dbg.print("Traceback:\n");
    dbg.print(" 0. ");
    trace0.print();
    dbg.print(" 1. ");
    trace1.print();
}
void LFTask::LibAbortFaultData::print() const {
    dbg.print("Traceback:\n");
    dbg.print(" 0. ");
    trace0.print();
    dbg.print(" 1. ");
    trace1.print();
    dbg.print(" 2. ");
    trace2.print();
}

void LFTask::RtosAssertFaultData::print() const {
    switch (file_id) {
        case RtosSourceFileIdentifierHeap: {
            dbg.print("file_id = heap\n");
        } break;
        case RtosSourceFileIdentifierPort: {
            dbg.print("file_id = port\n");
        } break;
        case RtosSourceFileIdentifierTasks: {
            dbg.print("file_id = tasks\n");
        } break;
        case RtosSourceFileIdentifierList: {
            dbg.print("file_id = list\n");
        } break;
        case RtosSourceFileIdentifierQueue: {
            dbg.print("file_id = queue\n");
        } break;
        case RtosSourceFileIdentifierEventGroups: {
            dbg.print("file_id = event_groups\n");
        } break;
        case RtosSourceFileIdentifierStreamBuffer: {
            dbg.print("file_id = stream_buffer\n");
        } break;
        case RtosSourceFileIdentifierTimers: {
            dbg.print("file_id = timers\n");
        } break;
        case RtosSourceFileIdentifierCoRoutine: {
            dbg.print("file_id = co_routine\n");
        } break;
        case RtosSourceFileIdentifierUnknown:
        default: {
        } break;
    }
    dbg.printf("line = %i\n", (int32_t) line);
    dbg.print("Traceback:\n");
    dbg.print(" 0. ");
    trace0.print();
    dbg.print(" 1. ");
    trace1.print();
}
void LFTask::RtosStackOverflowFaultData::print() const {
    dbg.printf("task_name = %.*s\n", configMAX_TASK_NAME_LEN, task_name);
}

void LFTask::FaultData::print() const {
    dbg.print("Previous fault status: ");
    switch (common.source) {
        case FaultSource::Unknown: {
            dbg.print("Unknown\n");
        } break;
        case FaultSource::NoFault: {
            dbg.print("NoFault\n");
        } break;
        case FaultSource::CoreHard: {
            dbg.print("HardFault\n");
            core_hard.print();
        } break;
        case FaultSource::CoreUsage: {
            dbg.print("UsageFault\n");
            core_usage.print();
        } break;
        case FaultSource::CoreBus: {
            dbg.print("BusFault\n");
            core_bus.print();
        } break;
        case FaultSource::CoreMemManage: {
            dbg.print("MemManageFault\n");
            core_mem_manage.print();
        } break;
        case FaultSource::EabiStackOverflow: {
            dbg.print("EabiStackOverflow\n");
            eabi_stack_overflow.print();
        } break;
        case FaultSource::EabiAssert: {
            dbg.print("EabiAssert\n");
            eabi_assert.print();
        } break;
        case FaultSource::LibAssert: {
            dbg.print("LibAssert\n");
            lib_assert.print();
        } break;
        case FaultSource::LibAbort: {
            dbg.print("LibAbort\n");
            lib_abort.print();
        } break;
        case FaultSource::LibExit: {
            dbg.print("LibExit\n");
            lib_exit.print();
        } break;
        case FaultSource::RtosAssert: {
            dbg.print("RtosAssert\n");
            rtos_assert.print();
        } break;
        case FaultSource::RtosStackOverflow: {
            dbg.print("RtosStackOverflow\n");
            rtos_stack_overflow.print();
        } break;
    }
}

}  // namespace mmcc
