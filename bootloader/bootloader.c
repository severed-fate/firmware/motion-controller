#include "bootloader.h"

#include <stdint.h>
#include <stdlib.h>

#define nop __asm__("nop")

small_vector_table_t static_vector_table = {.initial_sp_value = &_stack,
                                            .reset = bootloader_reset_handler,
                                            .nmi = null_handler,
                                            .hard_fault = null_handler,
                                            .memory_manage_fault = null_handler,
                                            .bus_fault = null_handler,
                                            .usage_fault = null_handler,
                                            .sv_call = null_handler,
                                            .debug_monitor = null_handler,
                                            .pend_sv = null_handler,
                                            .systick = null_handler};

static const uintptr_t mmcc_bin_header_ref = 0x08020000;

void bootloader_reset_handler(void) {
    mmcc_bin_header* hd = (mmcc_bin_header*) mmcc_bin_header_ref;
    hd->reset();
}

void null_handler(void) {
    nop;
}
