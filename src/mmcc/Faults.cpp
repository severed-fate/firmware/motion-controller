#include "LFT.hpp"

#include <Objects.hpp>
#include <interface/io/Core.hpp>
#include <interface/rtos/RTOS.hpp>

#include <cstdlib>

#include <boot.h>
#include <cxxabi.h>
#include <libopencm3/cm3/scb.h>

#define GET_LR()                                                                               \
    ({                                                                                         \
        uint32_t lr;                                                                           \
        __asm__ volatile("mov %0, lr\n" : "=r"(lr));                                           \
        lr;                                                                                    \
    })

#define GET_MSP()                                                                              \
    ({                                                                                         \
        uint32_t msp;                                                                          \
        __asm__ volatile("mrs %0, msp\n" : "=r"(msp));                                         \
        msp;                                                                                   \
    })

#define GET_PSP()                                                                              \
    ({                                                                                         \
        uint32_t psp;                                                                          \
        __asm__ volatile("mrs %0, psp\n" : "=r"(psp));                                         \
        psp;                                                                                   \
    })

#define fault_prologue()                                                                       \
    uint32_t lr = GET_LR();                                                                    \
    bool psp_frame = !!(lr & 0b100);                                                           \
    scb_exception_stack_frame* stack_frame;                                                    \
    if (psp_frame) {                                                                           \
        stack_frame = (scb_exception_stack_frame*) GET_PSP();                                  \
    }                                                                                          \
    else {                                                                                     \
        stack_frame = (scb_exception_stack_frame*) GET_MSP();                                  \
    }

#define SCB_CFSR_MLSPERR (1 << 5)
#define SCB_CFSR_LSPERR (1 << 5)
#define SCB_CFSR_USAGE_MASK (0xffff0000)
#define SCB_CFSR_BUS_MASK (0x0000ff00)
#define SCB_CFSR_MEM_MANAGE_MASK (0x000000ff)

extern "C" {

abort_func_attr void _fault_hard(void) {
    fault_prologue();
    uint32_t hfsr = SCB_HFSR;
    uint32_t cfsr = SCB_CFSR;
    mmcc::LFTask::CoreHardFaultData core_hard = {
         .source = mmcc::LFTask::FaultSource::CoreHard,
         .psp_frame = psp_frame,
         .debugevt = !!(hfsr & SCB_HFSR_DEBUG_VT),
         .forced = !!(hfsr & SCB_HFSR_FORCED),
         .has_usage = !!(cfsr & SCB_CFSR_USAGE_MASK),
         .has_bus = !!(cfsr & SCB_CFSR_BUS_MASK),
         .has_mem_manage = !!(cfsr & SCB_CFSR_MEM_MANAGE_MASK),
         .vecttbl = !!(hfsr & SCB_HFSR_VECTTBL),
         .faulty_pc = mmcc::LFTask::FaultData::encode_code_address(stack_frame->pc),
         .faulty_lr = mmcc::LFTask::FaultData::encode_code_address(stack_frame->lr),
    };
    mmcc::set_backup_fault_data(core_hard);
    on_abort();
}
abort_func_attr void _fault_usage(void) {
    fault_prologue();
    uint32_t cfsr = SCB_CFSR;
    mmcc::LFTask::CoreUsageFaultData core_usage = {
         .source = mmcc::LFTask::FaultSource::CoreUsage,
         .psp_frame = psp_frame,
         .divbyzero = !!(cfsr & SCB_CFSR_DIVBYZERO),
         .unaligned = !!(cfsr & SCB_CFSR_UNALIGNED),
         .nocp = !!(cfsr & SCB_CFSR_NOCP),
         .invpc = !!(cfsr & SCB_CFSR_INVPC),
         .invstate = !!(cfsr & SCB_CFSR_INVSTATE),
         .undefinstr = !!(cfsr & SCB_CFSR_UNDEFINSTR),
         .faulty_pc = mmcc::LFTask::FaultData::encode_code_address(stack_frame->pc),
         .faulty_lr = mmcc::LFTask::FaultData::encode_code_address(stack_frame->lr),
    };
    mmcc::set_backup_fault_data(core_usage);
    on_abort();
}
abort_func_attr void _fault_bus(void) {
    fault_prologue();
    uint32_t cfsr = SCB_CFSR;
    mmcc::LFTask::CoreBusFaultData core_bus = {
         .source = mmcc::LFTask::FaultSource::CoreBus,
         .psp_frame = psp_frame,
         .bfarvalid = !!(cfsr & SCB_CFSR_BFARVALID),
         .lsperr = !!(cfsr & SCB_CFSR_LSPERR),
         .stkerr = !!(cfsr & SCB_CFSR_STKERR),
         .unstkerr = !!(cfsr & SCB_CFSR_UNSTKERR),
         .impreciserr = !!(cfsr & SCB_CFSR_IMPRECISERR),
         .preciserr = !!(cfsr & SCB_CFSR_PRECISERR),
         .ibuserr = !!(cfsr & SCB_CFSR_IBUSERR),
         .faulty_pc = mmcc::LFTask::FaultData::encode_code_address(stack_frame->pc),
         .faulty_lr = mmcc::LFTask::FaultData::encode_code_address(stack_frame->lr),
         .bfar = SCB_BFAR,
    };
    mmcc::set_backup_fault_data(core_bus);
    on_abort();
}
abort_func_attr void _fault_mem_manage(void) {
    fault_prologue();
    uint32_t cfsr = SCB_CFSR;
    mmcc::LFTask::CoreMemManageFaultData core_mem_manage = {
         .source = mmcc::LFTask::FaultSource::CoreMemManage,
         .psp_frame = psp_frame,
         .mmarvalid = !!(cfsr & SCB_CFSR_MMARVALID),
         .mlsperr = !!(cfsr & SCB_CFSR_MLSPERR),
         .mstkerr = !!(cfsr & SCB_CFSR_MSTKERR),
         .munstkerr = !!(cfsr & SCB_CFSR_MUNSTKERR),
         .daccviol = !!(cfsr & SCB_CFSR_DACCVIOL),
         .iaccviol = !!(cfsr & SCB_CFSR_IACCVIOL),
         .faulty_pc = mmcc::LFTask::FaultData::encode_code_address(stack_frame->pc),
         .faulty_lr = mmcc::LFTask::FaultData::encode_code_address(stack_frame->lr),
         .mmfar = SCB_MMFAR,
    };
    mmcc::set_backup_fault_data(core_mem_manage);
    on_abort();
}

}  // extern "C"

/**
 * TODO: Implement proper stack unwinding:
 *   - cortex_m4_ehabi32.pdf
 *   - https://alexkalmuk.medium.com/how-stack-trace-on-arm-works-5634b35ddca1
 *   - https://github.com/red-rocket-computing/backtrace/
 */

#define GEN_STACKTRACE3()                                                                      \
    void* current_return = __builtin_return_address(0);                                        \
    void* lvl0_frame = __builtin_frame_address(1);                                             \
    void* lvl0_return = nullptr;                                                               \
    void* lvl1_frame = nullptr;                                                                \
    void* lvl1_return = nullptr;                                                               \
    if (lvl0_frame != nullptr) {                                                               \
        lvl0_return = __builtin_return_address(1);                                             \
        lvl1_frame = __builtin_frame_address(2);                                               \
        if (lvl1_frame != nullptr) {                                                           \
            lvl1_return = __builtin_return_address(2);                                         \
        }                                                                                      \
    }

#define GEN_STACKTRACE2()                                                                      \
    void* current_return = __builtin_return_address(0);                                        \
    void* lvl0_frame = __builtin_frame_address(1);                                             \
    void* lvl0_return = nullptr;                                                               \
    if (lvl0_frame != nullptr) {                                                               \
        lvl0_return = __builtin_return_address(1);                                             \
    }

namespace __cxxabiv1 {
extern "C" {

abort_func_attr void __cxa_pure_virtual(void) {
    mmcc_disable_interrupts();
    GEN_STACKTRACE3();
    mmcc::LFTask::EabiAssertFaultData eabi_assert = {
         .source = mmcc::LFTask::FaultSource::EabiAssert,
         .subtype = mmcc::LFTask::EabiAssertType::PureVirtual,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
         .trace2 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl1_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl1_return)},
    };
    mmcc::set_backup_fault_data(eabi_assert);
    on_abort();
}

abort_func_attr void __cxa_deleted_virtual(void) {
    mmcc_disable_interrupts();
    GEN_STACKTRACE3();
    mmcc::LFTask::EabiAssertFaultData eabi_assert = {
         .source = mmcc::LFTask::FaultSource::EabiAssert,
         .subtype = mmcc::LFTask::EabiAssertType::PureDeleted,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
         .trace2 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl1_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl1_return)},
    };
    mmcc::set_backup_fault_data(eabi_assert);
    on_abort();
}

}  // extern "C"
}  // namespace __cxxabiv1

namespace std {

abort_func_attr void unexpected() {
    mmcc_disable_interrupts();
    GEN_STACKTRACE3();
    mmcc::LFTask::EabiAssertFaultData eabi_assert = {
         .source = mmcc::LFTask::FaultSource::EabiAssert,
         .subtype = mmcc::LFTask::EabiAssertType::Unexpected,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
         .trace2 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl1_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl1_return)},
    };
    mmcc::set_backup_fault_data(eabi_assert);
    on_abort();
}

abort_func_attr void terminate() _NOEXCEPT {
    mmcc_disable_interrupts();
    GEN_STACKTRACE3();
    mmcc::LFTask::EabiAssertFaultData eabi_assert = {
         .source = mmcc::LFTask::FaultSource::EabiAssert,
         .subtype = mmcc::LFTask::EabiAssertType::Terminate,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
         .trace2 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl1_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl1_return)},
    };
    mmcc::set_backup_fault_data(eabi_assert);
    on_abort();
}

}  // namespace std

extern "C" {

abort_func_attr void __stack_chk_fail(void) {
    mmcc_disable_interrupts();
    GEN_STACKTRACE3();
    mmcc::LFTask::EabiStackOverflowFaultData eabi_stack_overflow = {
         .source = mmcc::LFTask::FaultSource::EabiStackOverflow,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
         .trace2 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl1_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl1_return)},
    };
    mmcc::set_backup_fault_data(eabi_stack_overflow);
    on_abort();
}

abort_func_attr void __assert_fail(uint8_t lib_id, uint16_t file_id,
                                             uint16_t instance_id) {
    mmcc_disable_interrupts();
    GEN_STACKTRACE2();
    mmcc::LFTask::LibAssertFaultData lib_assert = {
         .source = mmcc::LFTask::FaultSource::LibAssert,
         .library_id = lib_id,
         .file_id = file_id,
         .instance_id = instance_id,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
    };
    mmcc::set_backup_fault_data(lib_assert);
    on_abort();
}

abort_func_attr void _Exit(int exit_code) {
    mmcc_disable_interrupts();
    GEN_STACKTRACE2();
    mmcc::LFTask::LibExitFaultData lib_exit = {
         .source = mmcc::LFTask::FaultSource::LibExit,
         .exit_code = exit_code,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
    };
    mmcc::set_backup_fault_data(lib_exit);
    on_abort();
}

abort_func_attr void abort() {
    mmcc_disable_interrupts();
    GEN_STACKTRACE3();
    mmcc::LFTask::LibAbortFaultData lib_abort = {
         .source = mmcc::LFTask::FaultSource::LibAbort,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
         .trace2 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl1_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl1_return)},
    };
    mmcc::set_backup_fault_data(lib_abort);
    on_abort();
}

abort_func_attr void _rtos_bindings_on_assert(RtosSourceFileIdentifier file_id, int line) {
    mmcc_disable_interrupts();
    GEN_STACKTRACE2();
    mmcc::LFTask::RtosAssertFaultData rtos_assert = {
         .source = mmcc::LFTask::FaultSource::RtosAssert,
         .file_id = file_id,
         .line = (uint16_t) line,
         .trace0 = {.return_addr = mmcc::LFTask::FaultData::encode_code_address(
                         (uintptr_t) current_return)},
         .trace1 = {.frame_addr =
                         mmcc::LFTask::FaultData::encode_address((uintptr_t) lvl0_frame),
                    .return_addr =
                         mmcc::LFTask::FaultData::encode_code_address((uintptr_t) lvl0_return)},
    };

    mmcc::set_backup_fault_data(rtos_assert);
    on_abort();
}

abort_func_attr void _rtos_bindings_on_stack_overflow([[maybe_unused]] TaskHandle_t tsk_hdl, char* name) {
    mmcc_disable_interrupts();
    mmcc::LFTask::RtosStackOverflowFaultData rtos_stack_overflow = {
         .source = mmcc::LFTask::FaultSource::RtosStackOverflow,
    };
    __builtin_memcpy(rtos_stack_overflow.task_name, name, configMAX_TASK_NAME_LEN);
    mmcc::set_backup_fault_data(rtos_stack_overflow);
    on_abort();
}

}  // extern "C"
