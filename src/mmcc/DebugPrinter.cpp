#include "DebugPrinter.hpp"
#include "rtos/RTOS.hpp"

#include <mmcc/board/Config.hpp>

#include <cstdarg>
#include <cstdio>

#include <utils/macros.h>

namespace mmcc {

cold_func_attr void DebugPrinter::init() {
    B2B_USART::activate();
    B2B_USART::set_mode(true, false);
    B2B_USART::set_baudrate(115200);
    B2B_USART::set_databits(8);
    B2B_USART::set_stopbits(B2B_USART::StopBits::_1);
    B2B_USART::set_parity(B2B_USART::Parity::None);
    B2B_USART::set_flow_control(false, false);
    B2B_USART::enable();
}

void DebugPrinter::printf(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);
    int written = std::vsnprintf(_format_buffer, sizeof(_format_buffer), fmt, args);
    print(_format_buffer, written);
}

cold_func_attr void DebugPrinter::on_data_send(const char* dat, size_t len) {
    uint32_t interrupt_state;
    if (rtos::is_inside_interrupt()) {
        interrupt_state = rtos::Kernel::enter_critical_from_isr();
    }
    else {
        rtos::Kernel::enter_critical();
    }
    for (size_t i = 0; i < len; i++) {
        // Implicit CR on LF.
        if (dat[i] == '\n') {
            B2B_USART::tx('\r');
        }
        if (dat[i] != '\0') {
            B2B_USART::tx(dat[i]);
            _log_buffer.push(dat[i]);
        }
    }
    if (rtos::is_inside_interrupt()) {
        rtos::Kernel::exit_critical_from_isr(interrupt_state);
    }
    else {
        rtos::Kernel::exit_critical();
    }
}

}  // namespace mmcc

#include <printf.h>
void putchar_(char) {}
