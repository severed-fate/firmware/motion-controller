#ifndef MMCC_RTOSMUTEX_HPP
#define MMCC_RTOSMUTEX_HPP

#include <interface/rtos/RTOS.hpp>
#include <utils/Class.hpp>

#include <utility>

#include <FreeRTOS.h>
#include <semphr.h>

namespace rtos {

template<bool recursive = false> class Mutex : public meta::INonCopyable {
   protected:
    SemaphoreHandle_t _handle = nullptr;

   public:
    constexpr Mutex() noexcept = default;
    constexpr Mutex(SemaphoreHandle_t handle) noexcept : _handle(handle) {}

    constexpr Mutex(Mutex&& o) noexcept : _handle(std::exchange(o._handle, nullptr)) {}
    constexpr Mutex& operator=(Mutex&& o) noexcept {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    ~Mutex() {
        if (_handle != nullptr) {
            vSemaphoreDelete(_handle);
            _handle = nullptr;
        }
    }

    void init() {
        _handle = recursive ? xSemaphoreCreateRecursiveMutex() : xSemaphoreCreateMutex();
    }

    size_t get_count() {
        return uxSemaphoreGetCount(_handle);
    }
    bool take_ticks(TickType_t timeout_ticks = portMAX_DELAY) {
        bool taken;
        if constexpr (recursive) {
            taken = xSemaphoreTakeRecursive(_handle, timeout_ticks) == pdTRUE;
        }
        else {
            taken = xSemaphoreTake(_handle, timeout_ticks) == pdTRUE;
        }
        return taken;
    }
    bool give() {
        bool ok;
        if constexpr (recursive) {
            ok = xSemaphoreGiveRecursive(_handle) == pdTRUE;
        }
        else {
            ok = xSemaphoreGive(_handle) == pdTRUE;
        }
        return ok;
    }
};

template<bool recursive = false> class StaticMutex : public Mutex<recursive> {
   protected:
    StaticSemaphore_t _handle_storage;

   public:
    constexpr StaticMutex() noexcept = default;

    void init() {
        Mutex<recursive>::_handle =
             recursive ? xSemaphoreCreateRecursiveMutexStatic(&_handle_storage) :
                         xSemaphoreCreateMutexStatic(&_handle_storage);
    }
};

}  // namespace rtos

#endif /*MMCC_RTOSMUTEX_HPP*/