#include "UBJsonBuilder.hpp"

#include <interface/tcode/ParserDispatcher.hpp>

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <limits>

#include <etl/bit.h>
#include <etl/endianness.h>

#define ASSERT_FILE_ID (0x34D0)

namespace tcode::codec {

static constexpr auto Z85_UBJSON_PADDING_SYMBOL = static_cast<char>(UBJsonType::NoOp);

UBJsonInstance::UBJsonInstance(ParserDispatcher& owner) : _owner(&owner), _buffer_usage(0) {
    std::memset(_buffer, 0, sizeof(_buffer));
}
UBJsonInstance::~UBJsonInstance() {
    if (_owner != nullptr) {
        finalize();
    }
}

size_t UBJsonInstance::get_buffer_space() const {
    return sizeof(_buffer) - _buffer_usage;
}
void UBJsonInstance::append(uint8_t ch) {
    assert(_owner != nullptr, 0);
    assert(_buffer_usage < sizeof(_buffer), 1);
    // Buffer always has one space.
    _buffer[_buffer_usage++] = ch;
    // Flush buffer if full.
    if (_buffer_usage >= sizeof(_buffer)) {
        _owner->send_z85_data(_buffer, _buffer_usage, Z85_UBJSON_PADDING_SYMBOL);
        _buffer_usage = 0;
    }
}
void UBJsonInstance::append(const void* dat_raw, size_t n) {
    assert(_owner != nullptr, 2);
    const uint8_t* dat = (const uint8_t*) dat_raw;
    if (_buffer_usage % 4 != 0) {
        // Fill buffer up until the next full z85 token.
        const size_t c = std::min(4 - _buffer_usage % 4, n);
        std::memcpy(&_buffer[_buffer_usage], dat, c);
        _buffer_usage += c;
        dat += c;
        n -= c;
    }
    if (n > 0) {
        // Flush all buffer data.
        _owner->send_z85_data(_buffer, _buffer_usage, Z85_UBJSON_PADDING_SYMBOL);
        _buffer_usage = 0;
        // Direct send any remaining data that make up full z85 tokens.
        if (n >= 4) {
            const size_t c = (n / 4) * 4;
            _owner->send_z85_data(dat, c, Z85_UBJSON_PADDING_SYMBOL);
            dat += c;
            n -= c;
        }
        if (n > 0) {
            std::memcpy(_buffer, dat, n);
            _buffer_usage = n;
        }
    }
    // Flush buffer if full.
    if (_buffer_usage >= sizeof(_buffer)) {
        _owner->send_z85_data(_buffer, _buffer_usage, Z85_UBJSON_PADDING_SYMBOL);
        _buffer_usage = 0;
    }
}
size_t UBJsonInstance::flush() {
    assert(_owner != nullptr, 3);
    assert(_buffer_usage <= sizeof(_buffer), 4);
    // Bytes that cannot be encoded, the will remain in the buffer.
    const size_t remaining = _buffer_usage % 4;
    // Bytes that make up full z85 tokens, and as such, will be encoded.
    const size_t to_encode = _buffer_usage - remaining;
    if (to_encode > 0) {
        // Send data.
        _owner->send_z85_data(_buffer, to_encode);
        // Move remaing data.
        std::memmove(&_buffer[0], &_buffer[to_encode], remaining);
        _buffer_usage = remaining;
    }
    return get_buffer_space();
}
void UBJsonInstance::finalize() {
    assert(_owner != nullptr, 5);
    _owner->send_z85_data(_buffer, _buffer_usage, Z85_UBJSON_PADDING_SYMBOL);
    _buffer_usage = 0;
    _owner->finalize_ubjson();
    _owner = nullptr;
}

UBJsonContainer::UBJsonContainer(UBJsonInstance& instance, char start_marker) :
    _instance(&instance), _end_marker(start_marker + 2) {
    _instance->append(start_marker);
}
UBJsonContainer::UBJsonContainer(UBJsonInstance& instance, char start_marker, size_t size) :
    _instance(&instance), _end_marker(start_marker + 2), _size(size) {
    _instance->append(start_marker);
    _instance->append('#');
    _instance->write_size_smart(size);
}
UBJsonContainer::UBJsonContainer(UBJsonInstance& instance, char start_marker, UBJsonType type,
                                 size_t size) :
    _instance(&instance),
    _end_marker(start_marker + 2), _type(type), _size(size) {
    _instance->append(start_marker);
    _instance->append('$');
    _instance->append(static_cast<char>(type));
    _instance->append('#');
    _instance->write_size_smart(size);
}
UBJsonContainer::~UBJsonContainer() {
    if (_instance != nullptr) {
        if (is_sized()) {
            if (_count != _size) {
                std::abort();
            }
        }
        else {
            _instance->append(_end_marker);
        }
        _instance = nullptr;
    }
}

bool UBJsonContainer::is_sized() const {
    return _size != SIZE_MAX;
}
size_t UBJsonContainer::get_max_size() const {
    return _size;
}
size_t UBJsonContainer::get_size() const {
    return _count;
}

bool UBJsonContainer::is_typed() const {
    return _type != UBJsonType::NoOp;
}
UBJsonType UBJsonContainer::get_type() const {
    return _type;
}

UBJsonObject::UBJsonObject(UBJsonInstance& instance) : UBJsonContainer(instance, '{') {}
UBJsonObject::UBJsonObject(UBJsonInstance& instance, size_t size) :
    UBJsonContainer(instance, '{', size) {}
UBJsonObject::UBJsonObject(UBJsonInstance& instance, UBJsonType type, size_t size) :
    UBJsonContainer(instance, '{', type, size) {}

UBJsonArray::UBJsonArray(UBJsonInstance& instance) : UBJsonContainer(instance, '[') {}
UBJsonArray::UBJsonArray(UBJsonInstance& instance, size_t size) :
    UBJsonContainer(instance, '[', size) {}
UBJsonArray::UBJsonArray(UBJsonInstance& instance, UBJsonType type, size_t size) :
    UBJsonContainer(instance, '[', type, size) {}

UBJsonObject UBJsonInstance::begin_object() {
    return UBJsonObject(*this);
}
UBJsonObject UBJsonInstance::begin_object(size_t size) {
    return UBJsonObject(*this, size);
}
UBJsonObject UBJsonInstance::begin_object(UBJsonType type, size_t size) {
    return UBJsonObject(*this, type, size);
}
UBJsonArray UBJsonInstance::begin_array() {
    return UBJsonArray(*this);
}
UBJsonArray UBJsonInstance::begin_array(size_t size) {
    return UBJsonArray(*this, size);
}
UBJsonArray UBJsonInstance::begin_array(UBJsonType type, size_t size) {
    return UBJsonArray(*this, type, size);
}

void UBJsonInstance::write_size_smart(size_t size) {
    if (size <= std::numeric_limits<int8_t>::max()) {
        write(static_cast<int8_t>(size));
    }
    else if (size <= std::numeric_limits<uint8_t>::max()) {
        write(static_cast<uint8_t>(size));
    }
    else if (size <= std::numeric_limits<int16_t>::max()) {
        write(static_cast<int16_t>(size));
    }
    else if (size <= std::numeric_limits<int32_t>::max()) {
        write(static_cast<int32_t>(size));
    }
    else {
        write(static_cast<int64_t>(size));
    }
}

void UBJsonInstance::write_noop() {
    append(Z85_UBJSON_PADDING_SYMBOL);
}
void UBJsonInstance::write_null() {
    append(static_cast<char>(UBJsonType::Null));
}
void UBJsonInstance::write(bool v) {
    if (v) {
        append(static_cast<char>(UBJsonType::BoolTrue));
    }
    else {
        append(static_cast<char>(UBJsonType::BoolFalse));
    }
}
void UBJsonInstance::write(char v, bool skip_header) {
    if (!skip_header) {
        append(static_cast<char>(UBJsonType::Char));
    }
    append(v);
}
void UBJsonInstance::write(int8_t v, bool skip_header) {
    if (!skip_header) {
        append(static_cast<char>(UBJsonType::Int8));
    }
    append(v);
}
void UBJsonInstance::write(uint8_t v, bool skip_header) {
    if (!skip_header) {
        append(static_cast<char>(UBJsonType::UInt8));
    }
    append(v);
}
void UBJsonInstance::write(int16_t v, bool skip_header) {
    if (!skip_header) {
        append(static_cast<char>(UBJsonType::Int16));
    }
    constexpr size_t v_s = sizeof(v);
    size_t space = get_buffer_space();
    if (space < v_s) {
        space = flush();
        assert(space >= v_s, 6);
    }
    v = etl::ntoh(v);
    std::memcpy(&_buffer[_buffer_usage], &v, v_s);
    _buffer_usage += v_s;
    // Flush buffer if full.
    if (_buffer_usage >= sizeof(_buffer)) {
        _owner->send_z85_data(_buffer, _buffer_usage, Z85_UBJSON_PADDING_SYMBOL);
        _buffer_usage = 0;
    }
}
void UBJsonInstance::write(int32_t v, bool skip_header) {
    if (!skip_header) {
        append(static_cast<char>(UBJsonType::Int32));
    }
    constexpr size_t v_s = sizeof(v);
    size_t space = get_buffer_space();
    if (space < v_s) {
        space = flush();
        assert(space >= v_s, 7);
    }
    v = etl::ntoh(v);
    std::memcpy(&_buffer[_buffer_usage], &v, v_s);
    _buffer_usage += v_s;
    // Flush buffer if full.
    if (_buffer_usage >= sizeof(_buffer)) {
        _owner->send_z85_data(_buffer, _buffer_usage, Z85_UBJSON_PADDING_SYMBOL);
        _buffer_usage = 0;
    }
}
void UBJsonInstance::write(int64_t v, bool skip_header) {
    if (!skip_header) {
        append(static_cast<char>(UBJsonType::Int64));
    }
    constexpr size_t v_s = sizeof(v);
    size_t space = get_buffer_space();
    if (space < v_s) {
        space = flush();
        assert(space >= v_s, 8);
    }
    v = etl::ntoh(v);
    std::memcpy(&_buffer[_buffer_usage], &v, v_s);
    _buffer_usage += v_s;
    // Flush buffer if full.
    if (_buffer_usage >= sizeof(_buffer)) {
        _owner->send_z85_data(_buffer, _buffer_usage, Z85_UBJSON_PADDING_SYMBOL);
        _buffer_usage = 0;
    }
}
void UBJsonInstance::write(float v, bool skip_header) {
    if (!skip_header) {
        append(static_cast<char>(UBJsonType::Float));
    }
    write(etl::bit_cast<int32_t>(v), true);
}
void UBJsonInstance::write(double v, bool skip_header) {
    if (!skip_header) {
        append(static_cast<char>(UBJsonType::Double));
    }
    write(etl::bit_cast<int64_t>(v), true);
}
void UBJsonInstance::write(const char* str, size_t n, bool skip_header) {
    if (str == nullptr && !skip_header) [[unlikely]] {
        write_null();
        return;
    }

    if (!skip_header) {
        append(static_cast<char>(UBJsonType::String));
    }
    write_size_smart(n);
    append(str, n);
}

void UBJsonContainer::on_new_element(UBJsonType type) {
    assert(_instance != nullptr, 9);
    _count++;
    assert(_count <= _size, 10);
    assert(!is_typed() || _type == type, 11);
}

/** Reference: https://github.com/ubjson/universal-binary-json/issues/57 */
UBJsonObject UBJsonContainer::begin_object() {
    on_new_element(UBJsonType::Object);
    return _instance->begin_object();
}
UBJsonObject UBJsonContainer::begin_object(size_t size) {
    on_new_element(UBJsonType::Object);
    return _instance->begin_object(size);
}
UBJsonObject UBJsonContainer::begin_object(UBJsonType type, size_t size) {
    on_new_element(UBJsonType::Object);
    return _instance->begin_object(type, size);
}

UBJsonArray UBJsonContainer::begin_array() {
    on_new_element(UBJsonType::Array);
    return _instance->begin_array();
}
UBJsonArray UBJsonContainer::begin_array(size_t size) {
    on_new_element(UBJsonType::Array);
    return _instance->begin_array(size);
}
UBJsonArray UBJsonContainer::begin_array(UBJsonType type, size_t size) {
    on_new_element(UBJsonType::Array);
    return _instance->begin_array(type, size);
}

void UBJsonContainer::write_null() {
    on_new_element(UBJsonType::Null);
    if (!is_typed()) {
        _instance->write_null();
    }
}
void UBJsonContainer::write(bool v) {
    on_new_element(v ? UBJsonType::BoolTrue : UBJsonType::BoolFalse);
    if (!is_typed()) {
        _instance->write(v);
    }
}
void UBJsonContainer::write(char v) {
    on_new_element(UBJsonType::Char);
    _instance->write(v, is_typed());
}
void UBJsonContainer::write(int8_t v) {
    on_new_element(UBJsonType::Int8);
    _instance->write(v, is_typed());
}
void UBJsonContainer::write(uint8_t v) {
    on_new_element(UBJsonType::UInt8);
    _instance->write(v, is_typed());
}
void UBJsonContainer::write(int16_t v) {
    on_new_element(UBJsonType::Int16);
    _instance->write(v, is_typed());
}
void UBJsonContainer::write(int32_t v) {
    on_new_element(UBJsonType::Int32);
    _instance->write(v, is_typed());
}
void UBJsonContainer::write(int64_t v) {
    on_new_element(UBJsonType::Int64);
    _instance->write(v, is_typed());
}
void UBJsonContainer::write(float v) {
    on_new_element(UBJsonType::Float);
    _instance->write(v, is_typed());
}
void UBJsonContainer::write(double v) {
    on_new_element(UBJsonType::Double);
    _instance->write(v, is_typed());
}
void UBJsonContainer::write(const char* str, size_t n) {
    on_new_element(UBJsonType::String);
    _instance->write(str, n, is_typed());
}

void UBJsonArray::write_bytes(const uint8_t* data, size_t n) {
    assert(_instance != nullptr, 12);
    _count += n;
    assert(_count <= _size, 13);
    assert(is_typed() && _type == UBJsonType::UInt8, 14);
    _instance->append(data, n);
}

void UBJsonObject::write_key(const char* str, size_t n) {
    assert(_instance != nullptr, 15);
    _instance->write(str, n, true);
}

}  // namespace tcode::codec
