#ifndef MMCC_ALLOCATORS_HPP
#define MMCC_ALLOCATORS_HPP
/**
 * @file
 * @brief Special allocators/pools.
 */

#include <utils/Class.hpp>

#include <cstddef>
#include <cstdint>

namespace utils {

/**
 * Implements an upward growing stack with individual-sized allocations.
 * Does not support freeing individual allocations.
 */
class BufferPoolAllocator : public meta::INonCopyable {
   protected:
    uint8_t* _buffer;
    size_t _len;
    uint8_t* _end;

   public:
    constexpr BufferPoolAllocator(void* buffer, size_t length) :
        _buffer((uint8_t*) buffer), _len(length), _end(_buffer) {}

    /**
     * Make a new allocation.
     * Returns nullptr if there is not enough space.
     * Optionally accepts an alignment argument.
     */
    __attribute__((malloc)) void* allocate(size_t size, size_t alignment = 1);
    /**
     * Reset allocations.
     */
    void reset();
    /**
     * Clear internal storage.
     */
    void clear();
};

template<size_t N> class BufferStaticPoolAllocator : public BufferPoolAllocator {
   protected:
    uint8_t _data[N];

   public:
    constexpr BufferStaticPoolAllocator() : BufferPoolAllocator(_data, N) {}
};

}  // namespace utils

#endif /*MMCC_ALLOCATORS_HPP*/