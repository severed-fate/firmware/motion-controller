#include "Actuator.hpp"

#include <interface/io/Core.hpp>
#include <interface/io/GPIO.hpp>
#include <mmcc/Objects.hpp>
#include <mmcc/board/Config.hpp>

#include <boot.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/dma.h>

namespace mmcc {

void Actuator::_configure_act0_encoder() {
    ACT0_POS_ENC_TIMER::set_preload(false);
    ACT0_POS_ENC_TIMER::set_prescaler(0);
    ACT0_POS_ENC_TIMER::set_period(0x4000); /* Set to 14 bit resolution */
    ACT0_POS_ENC_TIMER::set_counter(0x4000 / 2);
    ACT0_POS_ENC_TIMER::set_slave_mode(stm32::timer::SlaveMode::EncoderMode3);
    ACT0_POS_ENC_TIMER::set_ic_polarity(stm32::timer::InputChannel::IC1,
                                        stm32::timer::InputChannelPolarity::Rising);
    ACT0_POS_ENC_TIMER::set_ic_input(stm32::timer::InputChannel::IC1,
                                     stm32::timer::InputChannelConf::IN_TI1);
    ACT0_POS_ENC_TIMER::set_ic_filter(stm32::timer::InputChannel::IC1,
                                      stm32::timer::InputChannelFilter::DTF_DIV_2_N_6);
    ACT0_POS_ENC_TIMER::set_ic_polarity(stm32::timer::InputChannel::IC2,
                                        stm32::timer::InputChannelPolarity::Rising);
    ACT0_POS_ENC_TIMER::set_ic_input(stm32::timer::InputChannel::IC2,
                                     stm32::timer::InputChannelConf::IN_TI2);
    ACT0_POS_ENC_TIMER::set_ic_filter(stm32::timer::InputChannel::IC2,
                                      stm32::timer::InputChannelFilter::DTF_DIV_2_N_6);
    ACT0_POS_ENC_TIMER::enable();
}

// static Actuator::AngularPositionCaptureState __act1_pos;
// static void __act1_capture() {
//     // WARNING: Latency-bound!!
//     if (timer_get_flag(ACT1_POS_TIMER, TIM_SR_CC2IF)) {
//         timer_clear_flag(ACT1_POS_TIMER, TIM_SR_CC2IF);
//         // TODO: Handle overcapture.
//         uint16_t ch1_v = TIM_CCR1(ACT1_POS_TIMER);
//         uint16_t ch2_v = TIM_CCR2(ACT1_POS_TIMER);
//         __act1_pos.last = __act1_pos.end;
//         __act1_pos.start = ch1_v;
//         __act1_pos.end = ch2_v;
//     }
// }
void Actuator::_configure_act1_position() {
    ACT1_POS_TIMER::set_mode(stm32::timer::CkIntDiv::DIV1, stm32::timer::Alignment::Center1,
                             stm32::timer::Direction::Up);
    ACT1_POS_TIMER::set_preload(false);
    ACT1_POS_TIMER::set_prescaler(3);
    ACT1_POS_TIMER::set_period(0xffff);

    // Input on CH2.
    ACT1_POS_TIMER::set_ic_input(stm32::timer::InputChannel::IC1,
                                 stm32::timer::InputChannelConf::IN_TI2);
    ACT1_POS_TIMER::set_ic_input(stm32::timer::InputChannel::IC2,
                                 stm32::timer::InputChannelConf::IN_TI2);
    // Select edge direction and filtering.
    ACT1_POS_TIMER::set_ic_filter(stm32::timer::InputChannel::IC1,
                                  stm32::timer::InputChannelFilter::DTF_DIV_16_N_5);
    ACT1_POS_TIMER::set_ic_filter(stm32::timer::InputChannel::IC2,
                                  stm32::timer::InputChannelFilter::DTF_DIV_16_N_5);
    ACT1_POS_TIMER::set_ic_polarity(stm32::timer::InputChannel::IC1,
                                    stm32::timer::InputChannelPolarity::Falling);
    ACT1_POS_TIMER::set_ic_polarity(stm32::timer::InputChannel::IC2,
                                    stm32::timer::InputChannelPolarity::Rising);
    // Configure in reset mode.
    ACT1_POS_TIMER::set_slave_trigger(stm32::timer::SlaveTrigger::FilteredTI2);
    ACT1_POS_TIMER::set_slave_mode(stm32::timer::SlaveMode::ResetMode);
    // Configure capture interrupt.
    // ACT1_POS_TIMER::set_interrupt(TIM_DIER_CC2IE, true);
    // runtime_vector_table.irq[NVIC_TIM3_IRQ] = __act1_capture;
    // nvic_set_priority(NVIC_TIM3_IRQ, 0x20);
    // nvic_enable_irq(NVIC_TIM3_IRQ);
    // Enable the captures.
    ACT1_POS_TIMER::set_ic(stm32::timer::InputChannel::IC1, true);
    ACT1_POS_TIMER::set_ic(stm32::timer::InputChannel::IC2, true);
    ACT1_POS_TIMER::enable();
}

// static Actuator::AngularPositionCaptureState __act2_pos;
// static void __act2_capture() {
//     // WARNING: Latency-bound!!
//     if (timer_get_flag(ACT2_POS_TIMER, TIM_SR_CC2IF)) {
//         timer_clear_flag(ACT2_POS_TIMER, TIM_SR_CC2IF);
//         // TODO: Handle overcapture.
//         uint16_t ch1_v = TIM_CCR1(ACT2_POS_TIMER);
//         uint16_t ch2_v = TIM_CCR2(ACT2_POS_TIMER);
//         __act2_pos.last = __act2_pos.end;
//         __act2_pos.start = ch1_v;
//         __act2_pos.end = ch2_v;
//     }
// }
void Actuator::_configure_act2_position() {
    ACT2_POS_TIMER::set_mode(stm32::timer::CkIntDiv::DIV1, stm32::timer::Alignment::Center1,
                             stm32::timer::Direction::Up);
    ACT2_POS_TIMER::set_preload(false);
    ACT2_POS_TIMER::set_prescaler(3);
    ACT2_POS_TIMER::set_period(0xffff);

    // Input on CH2.
    ACT2_POS_TIMER::set_ic_input(stm32::timer::InputChannel::IC1,
                                 stm32::timer::InputChannelConf::IN_TI2);
    ACT2_POS_TIMER::set_ic_input(stm32::timer::InputChannel::IC2,
                                 stm32::timer::InputChannelConf::IN_TI2);
    // Select edge direction and filtering.
    ACT2_POS_TIMER::set_ic_filter(stm32::timer::InputChannel::IC1,
                                  stm32::timer::InputChannelFilter::DTF_DIV_16_N_5);
    ACT2_POS_TIMER::set_ic_filter(stm32::timer::InputChannel::IC2,
                                  stm32::timer::InputChannelFilter::DTF_DIV_16_N_5);
    ACT2_POS_TIMER::set_ic_polarity(stm32::timer::InputChannel::IC1,
                                    stm32::timer::InputChannelPolarity::Falling);
    ACT2_POS_TIMER::set_ic_polarity(stm32::timer::InputChannel::IC2,
                                    stm32::timer::InputChannelPolarity::Rising);
    // Configure in reset mode.
    ACT2_POS_TIMER::set_slave_trigger(stm32::timer::SlaveTrigger::FilteredTI2);
    ACT2_POS_TIMER::set_slave_mode(stm32::timer::SlaveMode::ResetMode);
    // Configure capture interrupt.
    // ACT2_POS_TIMER::set_interrupt(TIM_DIER_CC2IE, true);
    // runtime_vector_table.irq[NVIC_TIM4_IRQ] = __act2_capture;
    // nvic_set_priority(NVIC_TIM4_IRQ, 0x20);
    // nvic_enable_irq(NVIC_TIM4_IRQ);
    // Enable the captures.
    ACT2_POS_TIMER::set_ic(stm32::timer::InputChannel::IC1, true);
    ACT2_POS_TIMER::set_ic(stm32::timer::InputChannel::IC2, true);
    ACT2_POS_TIMER::enable();
}

void Actuator::_configure_pwm() {
    ACT_PWM_TIMER::set_mode(stm32::timer::CkIntDiv::DIV1, stm32::timer::Alignment::Center1,
                            stm32::timer::Direction::Up);
    {
        auto cfg = mmcc::config.get_current_state();
        ACT_PWM_TIMER::set_prescaler(cfg->actuator.pwm_prescaler);
    }
    ACT_PWM_TIMER::set_period(PWM_PERIOD_RESOLUTION - 1);
    ACT_PWM_TIMER::set_counter(0x0);

    /* Configure break/inactive state */
    ACT_PWM_TIMER::set_oc_idle_state(stm32::timer::OutputChannel::OC1, false);
    ACT_PWM_TIMER::set_oc_idle_state(stm32::timer::OutputChannel::OC2, false);
    ACT_PWM_TIMER::set_oc_idle_state(stm32::timer::OutputChannel::OC3, false);
    ACT_PWM_TIMER::set_idle_state_in_idle_mode(true);
    ACT_PWM_TIMER::set_break_main_output(false);

    ACT_PWM_TIMER::set_oc_polarity(stm32::timer::OutputChannel::OC1,
                                   stm32::timer::OutputChannelPolarity::Buffer);
    ACT_PWM_TIMER::set_oc_polarity(stm32::timer::OutputChannel::OC2,
                                   stm32::timer::OutputChannelPolarity::Buffer);
    ACT_PWM_TIMER::set_oc_polarity(stm32::timer::OutputChannel::OC3,
                                   stm32::timer::OutputChannelPolarity::Buffer);

    ACT_PWM_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC1,
                               stm32::timer::OutputChannelMode::Pwm1);
    ACT_PWM_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC2,
                               stm32::timer::OutputChannelMode::Pwm1);
    ACT_PWM_TIMER::set_oc_mode(stm32::timer::OutputChannel::OC3,
                               stm32::timer::OutputChannelMode::Pwm1);

    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC1, 0x0);
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC2, 0x0);
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC3, 0x0);
    ACT_PWM_TIMER::set_oc_output(stm32::timer::OutputChannel::OC1, true);
    ACT_PWM_TIMER::set_oc_output(stm32::timer::OutputChannel::OC2, true);
    ACT_PWM_TIMER::set_oc_output(stm32::timer::OutputChannel::OC3, true);

    ACT_PWM_TIMER::set_preload(true);
    ACT_PWM_TIMER::generate_event(stm32::timer::Event::COMG);
    ACT_PWM_TIMER::generate_event(stm32::timer::Event::UG);
    ACT_PWM_TIMER::enable();
}

uint32_t Actuator::get_actuator0_position_raw() {
    return ACT0_POS_ENC_TIMER::get_counter();
}
void Actuator::set_actuator0_position_limits(uint32_t start, uint32_t end) {
    _act0_pos_start = start;
    _act0_pos_end = end;
    _act0_pos_fmag = static_cast<float>(_act0_pos_end - _act0_pos_start);
}
float Actuator::get_actuator0_position_scaled() {
    uint32_t pos = get_actuator0_position_raw();
    float start_zeroed = static_cast<int32_t>(pos) - static_cast<int32_t>(_act0_pos_start);
    float s = start_zeroed / _act0_pos_fmag;
    return s;
}

std::pair<uint16_t, uint16_t> Actuator::get_actuator1_position_raw() {
    return {ACT1_POS_TIMER::get_ic_value(stm32::timer::InputChannel::IC1),
            ACT1_POS_TIMER::get_ic_value(stm32::timer::InputChannel::IC2)};
}
std::pair<uint16_t, uint16_t> Actuator::get_actuator2_position_raw() {
    return {ACT2_POS_TIMER::get_ic_value(stm32::timer::InputChannel::IC1),
            ACT2_POS_TIMER::get_ic_value(stm32::timer::InputChannel::IC2)};
}

float Actuator::get_actuator1_position_scaled() {
    auto [a, b] = get_actuator1_position_raw();
    float v = static_cast<float>(a) / static_cast<float>(b);
    auto [db_min, db_max] = ACT1_POS_DEADBAND;
    // Scale.
    float s = (v - db_min) / (db_max - db_min);
    // Clamp.
    if (s < 0.0f) {
        return 0.0f;
    }
    if (s > 1.0f) {
        return 1.0f;
    }
    if (s != s) { /* isnan */
        return 0.5f;
    }
    return s;
}
float Actuator::get_actuator2_position_scaled() {
    auto [a, b] = get_actuator2_position_raw();
    float v = static_cast<float>(a) / static_cast<float>(b);
    auto [db_min, db_max] = ACT2_POS_DEADBAND;
    // Scale.
    float s = (v - db_min) / (db_max - db_min);
    // Clamp.
    if (s < 0.0f) {
        return 0.0f;
    }
    if (s > 1.0f) {
        return 1.0f;
    }
    if (s != s) { /* isnan */
        return 0.5f;
    }
    return s;
}

// Actuator::AngularPositionCaptureState Actuator::get_actuator1_position() {
//     return __act1_pos;
// }
// Actuator::AngularPositionCaptureState Actuator::get_actuator2_position() {
//     return __act2_pos;
// }

void Actuator::_apply_emergency_stop() {
    _emergency_stopped = true;
    _set_actuator_pwm_brake();
    _set_aux_pwm_brake();
    _set_full_bridge_enable(false);
}

void Actuator::_set_actuator_pwm_brake() {
    ACT_PWM_TIMER::set_break_main_output(false);
    // ACT_PWM_TIMER::generate_event(stm32::timer::Event::BG);
}

void Actuator::_reset_enable_actuators_pwm() {
    _emergency_stopped = false;
    _set_actuators_pwm(false, 0, false, 0, false, 0);
    // TODO: seperate.
    _set_aux1_pwm(false, 0);
    _set_aux2_pwm(false, 0);
    ACT_PWM_TIMER::set_break_main_output(true);
}

void Actuator::_set_full_bridge_enable(bool act_en) {
    InterruptGuard<true> sup;
    if (act_en) {
        PINSET_SET_ALL_CTX(ACT_CTL_EN_PINS, sup);
    }
    else {
        PINSET_RESET_ALL_CTX(ACT_CTL_EN_PINS, sup);
    }
}

void Actuator::_set_full_bridge_enable(bool act0_en, bool act1_en, bool act2_en) {
    decltype(ACT_CTL_EN_PINS)::packed_pin_mask_t pinmask;
    pinmask[0] = act0_en;
    pinmask[1] = act1_en;
    pinmask[2] = act2_en;
    PINSET_WRITE_DYN(ACT_CTL_EN_PINS, pinmask); /* atomic R-M-W */
}

void Actuator::set_actuators_pwm_prescaler(uint16_t psc) {
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.pwm_prescaler = psc;
    ACT_PWM_TIMER::set_prescaler(psc);
}

void Actuator::_set_actuator0_pwm(bool dir0, uint16_t pwm0) {
    if (dir0) {
        InterruptGuard<true> sup;
        PINSET_SET_ALL_CTX(ACT0_CTL_DIR_PINS, sup);
    }
    else {
        InterruptGuard<true> sup;
        PINSET_RESET_ALL_CTX(ACT0_CTL_DIR_PINS, sup);
    }
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC1, pwm0);
}
void Actuator::_set_actuator1_pwm(bool dir1, uint16_t pwm1) {
    if (dir1) {
        InterruptGuard<true> sup;
        PINSET_SET_ALL_CTX(ACT1_CTL_DIR_PINS, sup);
    }
    else {
        InterruptGuard<true> sup;
        PINSET_RESET_ALL_CTX(ACT1_CTL_DIR_PINS, sup);
    }
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC2, pwm1);
}
void Actuator::_set_actuator2_pwm(bool dir2, uint16_t pwm2) {
    if (dir2) {
        InterruptGuard<true> sup;
        PINSET_SET_ALL_CTX(ACT2_CTL_DIR_PINS, sup);
    }
    else {
        InterruptGuard<true> sup;
        PINSET_RESET_ALL_CTX(ACT2_CTL_DIR_PINS, sup);
    }
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC3, pwm2);
}
void Actuator::_set_actuator12_pwm(bool dir1, uint16_t pwm1, bool dir2, uint16_t pwm2) {
    // Update directions.
    decltype(ACT12_CTL_DIR_PINS)::packed_pin_mask_t pinmask;
    pinmask[0] = dir1;
    pinmask[1] = dir2;
    PINSET_WRITE_DYN(ACT12_CTL_DIR_PINS, pinmask); /* atomic R-M-W */
    // Mask shadow register updates.
    ACT_PWM_TIMER::set_update_enable(false);
    // Apply new compare counter values.
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC2, pwm1);
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC3, pwm2);
    // Unmask shadow register updates.
    ACT_PWM_TIMER::set_update_enable(true);
}
void Actuator::_set_actuators_pwm(bool dir0, uint16_t pwm0, bool dir1, uint16_t pwm1, bool dir2,
                                  uint16_t pwm2) {
    // Update directions.
    decltype(ACT_CTL_DIR_PINS)::packed_pin_mask_t pinmask;
    pinmask[0] = dir0;
    pinmask[1] = dir1;
    pinmask[2] = dir2;
    PINSET_WRITE_DYN(ACT_CTL_DIR_PINS, pinmask); /* atomic R-M-W */
    // Mask shadow register updates.
    ACT_PWM_TIMER::set_update_enable(false);
    // Apply new compare counter values.
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC1, pwm0);
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC2, pwm1);
    ACT_PWM_TIMER::set_oc_value(stm32::timer::OutputChannel::OC3, pwm2);
    // Unmask shadow register updates.
    ACT_PWM_TIMER::set_update_enable(true);
}

}  // namespace mmcc
