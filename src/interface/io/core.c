#include "core.h"

#include <libopencm3/cm3/cortex.h>
#include <libopencm3/cm3/scb.h>

void mmcc_enable_interrupts(void) {
    cm_enable_interrupts();
}
void mmcc_disable_interrupts(void) {
    cm_disable_interrupts();
}
void mmcc_enable_faults(void) {
    cm_enable_faults();
}
void mmcc_disable_faults(void) {
    cm_disable_faults();
}
bool mmcc_is_masked_interrupts(void) {
    return cm_is_masked_interrupts();
}
bool mmcc_is_masked_faults(void) {
    return cm_is_masked_faults();
}
uint32_t mmcc_mask_interrupts(uint32_t mask) {
    return cm_mask_interrupts(mask);
}
uint32_t mmcc_mask_faults(uint32_t mask) {
    return cm_mask_faults(mask);
}

void mmcc_reset_system(void) {
    scb_reset_system();
}
void mmcc_reset_core(void) {
    scb_reset_core();
}
void mmcc_set_priority_grouping(uint32_t prigroup) {
    scb_set_priority_grouping(prigroup);
}
