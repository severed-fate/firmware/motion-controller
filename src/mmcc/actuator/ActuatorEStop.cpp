#include "Actuator.hpp"

#include <Objects.hpp>
#include <mmcc/board/Config.hpp>

#include <cstdlib>

#include <boot.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

namespace mmcc {

void Actuator::on_abort() {
    _apply_emergency_stop();
}

void Actuator::_configure_home_estop_button() { /* Home/Estop button filtering & events. */
    constexpr auto home_estop_button_gpio_port =
         HOME_ESTOP_BUTTON.get_port_addresses<HOME_ESTOP_BUTTON.get_port_count()>()[0];

    HOME_ESTOP_TIMER::set_mode(stm32::timer::CkIntDiv::DIV1,
                                       stm32::timer::Alignment::Edge,
                                       stm32::timer::Direction::Up);
    HOME_ESTOP_TIMER::set_prescaler(9600 - 1);
    HOME_ESTOP_TIMER::set_period(10 - 1);
    HOME_ESTOP_TIMER::set_counter(0x0);
    HOME_ESTOP_TIMER::set_interrupt(TIM_DIER_UIE, true);

    exti_set_trigger(EXTI0, EXTI_TRIGGER_FALLING);
    _actuator_home_estop_button_state = HomeEstopButtonState::WaitingFall;
    _actuator_home_estop_button_counter = 0;
    exti_select_source(EXTI0, home_estop_button_gpio_port);
    exti_enable_request(EXTI0);

    runtime_vector_table.irq[NVIC_TIM1_TRG_COM_TIM11_IRQ] = __on_home_estop_button_trigger;
    nvic_set_priority(NVIC_TIM1_TRG_COM_TIM11_IRQ, 0x50);
    nvic_enable_irq(NVIC_TIM1_TRG_COM_TIM11_IRQ);

    runtime_vector_table.irq[NVIC_EXTI0_IRQ] = __on_home_estop_button_event;
    nvic_set_priority(NVIC_EXTI0_IRQ, 0x20);
    nvic_enable_irq(NVIC_EXTI0_IRQ);
}

void Actuator::on_home_estop_button_event() {
    exti_reset_request(EXTI0);
    exti_disable_request(EXTI0);
    if (_actuator_home_estop_button_state == HomeEstopButtonState::WaitingRise) {
        _actuator_home_estop_button_state = HomeEstopButtonState::Rise2Steady;
        _actuator_home_estop_button_counter = HOME_ESTOP_BUTTON_DEBOUNCE_TIMEOUT;
        HOME_ESTOP_TIMER::set_counter(0x0);
        HOME_ESTOP_TIMER::enable();
    }
    else if (_actuator_home_estop_button_state == HomeEstopButtonState::WaitingFall) {
        _actuator_home_estop_button_state = HomeEstopButtonState::Fall2Steady;
        _actuator_home_estop_button_counter = HOME_ESTOP_BUTTON_DEBOUNCE_TIMEOUT;
        HOME_ESTOP_TIMER::set_counter(0x0);
        HOME_ESTOP_TIMER::enable();
    }
    else {
        std::abort();
    }
}

void Actuator::on_home_estop_button_trigger() {
    /* Called every millisecond while waiting for input to reach steady state. */
    if (HOME_ESTOP_TIMER::get_interrupt_flags(TIM_SR_UIF)) {
        if (_actuator_home_estop_button_state == HomeEstopButtonState::Rise2Steady) {
            InterruptGuard ctx;
            if (PINSET_TEST_ALL_CTX(HOME_ESTOP_BUTTON, ctx)) {
                _actuator_home_estop_button_counter--;
                if (_actuator_home_estop_button_counter == 0) {
                    HOME_ESTOP_TIMER::disable();
                    // Reached steady high state - button has been released.
                    TickType_t home_estop_button_last_release = rtos::Kernel::get_tickcount();
                    TickType_t home_estop_button_press_duration =
                         home_estop_button_last_release - _home_estop_button_last_press;

                    // Select event based on state, and potentially do some critical work.
                    Events ev = Events::Invalid;
                    switch (_state) {
                        case States::Pending: /* Could appear on very short presses */
                        case States::Homing: {
                            _apply_emergency_stop();
                            ev = Events::HomeStop;
                        } break;
                        case States::EmergencyStopped: {
                            if (home_estop_button_press_duration >=
                                HOME_ESTOP_BUTTON_LONG_PRESS) {
                                // on long press
                                ev = Events::EmergencyStopReset;
                            }
                            else {
                                // on normal press
                                ev = Events::EmergencyStopRelease;
                            }
                        } break;
                        case States::Idle:
                        case States::Running: {
                            /* Break applied on press. */
                            if (_emergency_stopped) {
                                ev = Events::EmergencyStopped;
                            }
                        } break;
                        /* Invalid states */
                        case States::Invalid:
                        default: {
                            /* NOP */
                        } break;
                    }
                    // Send event to be handled by the task thread.
                    if (ev != Events::Invalid) {
                        if (!_evque.send_to_back_from_isr(ev)) {
                            static const char MSG[] =
                                 "Actuator::on_home_estop_button_trigger(on button release) -> "
                                 "send_to_back_from_isr failed!\n";
                            mmcc::dbg.print(MSG, sizeof(MSG));
                            std::abort();
                        }
                    }

                    exti_set_trigger(EXTI0, EXTI_TRIGGER_FALLING);
                    exti_enable_request(EXTI0);
                    _actuator_home_estop_button_state = HomeEstopButtonState::WaitingFall;
                }
            }
            else {
                // Just a glitch!
                HOME_ESTOP_TIMER::disable();
                exti_set_trigger(EXTI0, EXTI_TRIGGER_RISING);
                exti_enable_request(EXTI0);
                _actuator_home_estop_button_state = HomeEstopButtonState::WaitingRise;
            }
        }
        else if (_actuator_home_estop_button_state == HomeEstopButtonState::Fall2Steady) {
            InterruptGuard ctx;
            if (!PINSET_TEST_ALL_CTX(HOME_ESTOP_BUTTON, ctx)) {
                _actuator_home_estop_button_counter--;
                if (_actuator_home_estop_button_counter == 0) {
                    HOME_ESTOP_TIMER::disable();
                    // Reached steady low state - button has been pressed.
                    _home_estop_button_last_press = rtos::Kernel::get_tickcount();

                    // Select event based on state, and potentially do some critical work.
                    Events ev = Events::Invalid;
                    switch (_state) {
                        case States::Pending: {
                            ev = Events::HomeStart;
                        } break;
                        case States::Idle:
                        case States::Running: {
                            _apply_emergency_stop();
                            /* Wait until button release for state transition. */
                        } break;
                        /* States handled on button release */
                        case States::Homing:
                        case States::EmergencyStopped:
                        /* Invalid states */
                        case States::Invalid:
                        default: {
                            /* NOP */
                        } break;
                    }
                    // Send event to be handled by the task thread.
                    if (ev != Events::Invalid) {
                        if (!_evque.send_to_back_from_isr(ev)) {
                            static const char MSG[] =
                                 "Actuator::on_home_estop_button_trigger(on button press) -> "
                                 "send_to_back_from_isr failed!\n";
                            mmcc::dbg.print(MSG, sizeof(MSG));
                            std::abort();
                        }
                    }

                    exti_set_trigger(EXTI0, EXTI_TRIGGER_RISING);
                    exti_enable_request(EXTI0);
                    _actuator_home_estop_button_state = HomeEstopButtonState::WaitingRise;
                }
            }
            else {
                HOME_ESTOP_TIMER::disable();
                exti_set_trigger(EXTI0, EXTI_TRIGGER_FALLING);
                exti_enable_request(EXTI0);
                _actuator_home_estop_button_state = HomeEstopButtonState::WaitingFall;
            }
        }
        else {
            std::abort();
        }
    }
}

void Actuator::__on_home_estop_button_event() {
    actuator_task.on_home_estop_button_event();
}
void Actuator::__on_home_estop_button_trigger() {
    actuator_task.on_home_estop_button_trigger();
}

}  // namespace mmcc
