#ifndef MMCC_DEBUGPRINTER_HPP
#define MMCC_DEBUGPRINTER_HPP

#include <interface/io/USART.hpp>

#include <etl/circular_buffer.h>
#include <utils/macros.h>

namespace mmcc {

class DebugPrinter {
   protected:
    etl::circular_buffer<char, 1772> _log_buffer{};
    char _format_buffer[256]{};

   public:
    constexpr DebugPrinter() noexcept = default;

    void init();

    template<std::size_t N> __attribute__((always_inline)) void print(const char (&MSG)[N]) {
        print(&MSG[0], N - 1);
    }
    __attribute__((always_inline)) void print(const char* msg, size_t len) {
        on_data_send(msg, len);
    }
    __attribute__((__format__(printf, 2, 3))) void printf(const char* fmt, ...);

    const auto& get_log_buffer() const {
        return _log_buffer;
    }

   private:
    cold_func_attr void on_data_send(const char* dat, size_t len);
};

}  // namespace mmcc

#endif /*MMCC_DEBUGPRINTER_HPP*/