#ifndef MMCC_RTOSEVENTGROUPS_HPP
#define MMCC_RTOSEVENTGROUPS_HPP

#include <utils/Class.hpp>
#include <interface/rtos/RTOS.hpp>

#include <utility>

#include <FreeRTOS.h>
#include <event_groups.h>

namespace rtos {

class EventGroup : public meta::INonCopyable {
   protected:
    EventGroupHandle_t _handle = nullptr;

   public:
   protected:
    EventGroup(EventGroupHandle_t handle) : _handle(handle) {}

   public:
    EventGroup();
    ~EventGroup();
    EventGroup(EventGroup&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    EventGroup& operator=(EventGroup&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    EventBits_t wait_ticks(EventBits_t wait_mask, bool clear_on_exit, bool wait_for_all,
                     TickType_t timeout_ticks = 0);
    EventBits_t sync_ticks(EventBits_t set_mask, EventBits_t wait_mask, TickType_t timeout_ticks = 0);

    // TODO: ISR version requires timer task.
    EventBits_t set(EventBits_t mask);
    // TODO: ISR version requires timer task.
    EventBits_t clear(EventBits_t mask);
    EventBits_t get();
};

class StaticEventGroup : public EventGroup {
   protected:
    StaticEventGroup_t _handle_storage;

   public:
    StaticEventGroup() : EventGroup(xEventGroupCreateStatic(&_handle_storage)) {}
};

}  // namespace rtos

#endif /*MMCC_RTOSEVENTGROUPS_HPP*/