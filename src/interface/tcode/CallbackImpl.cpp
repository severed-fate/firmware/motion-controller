#include <TCodeConfig.hpp>
#include <TCodeInstance.hpp>
#include <interface/rtos/RTOS.hpp>
#include <interface/tcode/Messages.hpp>
#include <interface/tcode/TCode.hpp>
#include <interface/tcode/UBJsonBuilder.hpp>

#include <utility>

#include <etl/vector.h>

#define TCODE_PROTOCOL_NAME "eTCode"
#define TCODE_PROTOCOL_VERSION "0.1"

namespace tcode::callbacks {

response::Error noop() {
    return {};
}

response::Error identify_protocol() {
    auto ubjson = TCODE_INSTANCE.send_ubjson_response({response::CommandType::Device, 1});
    {
        auto root = ubjson.begin_object(codec::UBJsonType::String, 2);
        root["name"].write(TCODE_PROTOCOL_NAME);
        root["version"].write(TCODE_PROTOCOL_VERSION);
    }
    return {};
}
response::Error get_protocol_name(const void*, size_t) {
    TCODE_INSTANCE.send_response({response::CommandType::Device, 1},
                                 response::PropertyData("name"),
                                 response::Z85Data(TCODE_PROTOCOL_NAME));
    return {};
}
response::Error get_protocol_version(const void*, size_t) {
    TCODE_INSTANCE.send_response({response::CommandType::Device, 1},
                                 response::PropertyData("version"),
                                 response::Z85Data(TCODE_PROTOCOL_VERSION));
    return {};
}

static void build_enumeration(codec::UBJsonObject& root, const common::CommandIndex cmd_idx,
                              const callbacks::Command cmd_entry,
                              const std::optional<callbacks::AxisUpdate> axis_entry) {
    root.write_key(cmd_idx.to_string());
    codec::UBJsonObject obj = root.begin_object();

    obj["support_callback"].write(cmd_entry.has_callback());
    if (cmd_entry.has_description()) {
        obj["description"].write(cmd_entry.get_description());
    }

    // Axis info.
    if (axis_entry.has_value()) {
        obj["support_update_callback"].write(axis_entry->has_update_callback());
        obj["support_update_interval_callback"].write(
             axis_entry->has_update_interval_callback());
        obj["support_update_speed_callback"].write(axis_entry->has_update_speed_callback());
        obj["support_stop_callback"].write(axis_entry->has_stop_callback());
        // TODO: axis hint
    }

    // Properties.
    {
        codec::UBJsonObject props_obj = obj["props"].begin_object();
        for (size_t i = 0; i < config::property_registry.size(); i++) {
            auto& prop_ref = config::property_registry.at_index(i);
            auto& prop_entry = *prop_ref;
            // Filter all properties for current CommandIndex.
            if (prop_entry.first.cmd_idx == cmd_idx) {
                props_obj.write_key(prop_entry.first.name);
                codec::UBJsonObject prop_obj = props_obj.begin_object();
                const auto& prop = prop_entry.second;
                prop_obj["type"].write(static_cast<char>(prop.get_type()));
                {
                    etl::vector<char, 10> flags_seq;
                    if (prop.has_getter()) {
                        flags_seq.push_back('r');
                    }
                    if (prop.has_setter()) {
                        flags_seq.push_back('w');
                    }
                    if (prop.get_flags() & Property::FlagBits::Event) {
                        flags_seq.push_back('e');
                    }
                    if (prop.get_flags() & Property::FlagBits::Action) {
                        flags_seq.push_back('a');
                    }
                    if (prop.get_flags() & Property::FlagBits::Enum) {
                        flags_seq.push_back('n');
                    }
                    if (prop.get_flags() & Property::FlagBits::Boolean) {
                        flags_seq.push_back('l');
                    }
                    if (prop.get_flags() & Property::FlagBits::Bitfield) {
                        flags_seq.push_back('f');
                    }
                    if (prop.get_flags() & Property::FlagBits::Observations) {
                        flags_seq.push_back('o');
                    }
                    prop_obj["flags"].write(flags_seq.data(), flags_seq.size());
                }
                if (prop.get_display_type() != Property::DisplayType::Default) {
                    prop_obj["display_type"].write(
                         static_cast<uint8_t>(prop.get_display_type()));
                }
                prop_obj["current_update_interval"].write(
                     static_cast<int32_t>(TCODE_INSTANCE.get_timeout(prop_ref)));
                prop.append_metadata(prop_obj);
            }
        }
    }
}

response::Error build_enumeration() {
    auto ubjson = TCODE_INSTANCE.send_ubjson_response({response::CommandType::Device, 2});
    {
        auto root = ubjson.begin_object();
        // Ensure enough interval between updates to allow rtos task progression.
        root["min_update_interval"].write(static_cast<int32_t>(rtos::ticks2ms(5)));
        root["max_update_interval"].write(3000); /* 3 seconds max */
        // For each command:
        for (auto& cmd_entry : config::command_registry) {
            auto axis_entry = config::axis_registry.find(cmd_entry.first);
            bool has_axis_entry = axis_entry != config::axis_registry.end();
            build_enumeration(root, cmd_entry.first, cmd_entry.second,
                              has_axis_entry ?
                                   std::optional<callbacks::AxisUpdate>(axis_entry->second) :
                                   std::optional<callbacks::AxisUpdate>());
        }
        // Also append available axis not in command list.
        for (auto& axis_entry : config::axis_registry) {
            if (!config::command_registry.contains(axis_entry.first)) {
                build_enumeration(root, axis_entry.first, callbacks::Command({}, nullptr),
                                  axis_entry.second);
            }
        }
    }
    return {};
}

}  // namespace tcode::callbacks
