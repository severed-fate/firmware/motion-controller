#include "USART.hpp"

#include <interface/io/Core.hpp>

#include <libopencm3/stm32/rcc.h>

namespace stm32 {

template<uintptr_t prh> constexpr rcc_periph_clken prh2rcc();
template<> constexpr rcc_periph_clken prh2rcc<USART1>() {
    return RCC_USART1;
}
template<> constexpr rcc_periph_clken prh2rcc<USART2>() {
    return RCC_USART2;
}
template<> constexpr rcc_periph_clken prh2rcc<USART3>() {
    return RCC_USART3;
}
template<> constexpr rcc_periph_clken prh2rcc<UART4>() {
    return RCC_UART4;
}
template<> constexpr rcc_periph_clken prh2rcc<UART5>() {
    return RCC_UART5;
}
template<> constexpr rcc_periph_clken prh2rcc<USART6>() {
    return RCC_USART6;
}
template<> constexpr rcc_periph_clken prh2rcc<UART7>() {
    return RCC_UART7;
}
template<> constexpr rcc_periph_clken prh2rcc<UART8>() {
    return RCC_UART8;
}

template<uintptr_t prh> constexpr rcc_periph_rst prh2rst();
template<> constexpr rcc_periph_rst prh2rst<USART1>() {
    return RST_USART1;
}
template<> constexpr rcc_periph_rst prh2rst<USART2>() {
    return RST_USART2;
}
template<> constexpr rcc_periph_rst prh2rst<USART3>() {
    return RST_USART3;
}
template<> constexpr rcc_periph_rst prh2rst<UART4>() {
    return RST_UART4;
}
template<> constexpr rcc_periph_rst prh2rst<UART5>() {
    return RST_UART5;
}
template<> constexpr rcc_periph_rst prh2rst<USART6>() {
    return RST_USART6;
}
template<> constexpr rcc_periph_rst prh2rst<UART7>() {
    return RST_UART7;
}
template<> constexpr rcc_periph_rst prh2rst<UART8>() {
    return RST_UART8;
}

template<uintptr_t prh> void USART<prh>::activate() {
    rcc_periph_reset_release(prh2rst<prh>());
    rcc_periph_clock_enable(prh2rcc<prh>());
}

template<uintptr_t prh> void USART<prh>::deactivate() {
    rcc_periph_reset_hold(prh2rst<prh>());
    rcc_periph_clock_disable(prh2rcc<prh>());
}

template<uintptr_t prh> void USART<prh>::set_baudrate(uint32_t baudrate) {
    usart_set_baudrate(prh, baudrate);
}
template<uintptr_t prh> void USART<prh>::set_databits(uint8_t bits) {
    usart_set_databits(prh, bits);
}
template<uintptr_t prh> void USART<prh>::set_stopbits(StopBits bits) {
    usart_set_stopbits(prh, bits);
}
template<uintptr_t prh> void USART<prh>::set_parity(Parity mode) {
    usart_set_parity(prh, mode);
}
template<uintptr_t prh> void USART<prh>::set_mode(bool tx, bool rx) {
    uint32_t mode = 0x0;
    if (tx) {
        mode |= USART_MODE_TX;
    }
    if (rx) {
        mode |= USART_MODE_RX;
    }
    usart_set_mode(prh, mode);
}
template<uintptr_t prh> void USART<prh>::set_flow_control(bool rts, bool cts) {
    uint32_t mode = 0x0;
    if (rts) {
        mode |= USART_FLOWCONTROL_RTS;
    }
    if (cts) {
        mode |= USART_FLOWCONTROL_CTS;
    }
    usart_set_flow_control(prh, mode);
}
template<uintptr_t prh> void USART<prh>::set_rx_dma(bool en) {
    if (en) {
        usart_enable_rx_dma(prh);
    }
    else {
        usart_disable_rx_dma(prh);
    }
}
template<uintptr_t prh> void USART<prh>::set_tx_dma(bool en) {
    if (en) {
        usart_enable_tx_dma(prh);
    }
    else {
        usart_disable_tx_dma(prh);
    }
}
template<uintptr_t prh> void USART<prh>::set_rx_interrupt(bool en) {
    if (en) {
        usart_enable_rx_interrupt(prh);
    }
    else {
        usart_disable_rx_interrupt(prh);
    }
}
template<uintptr_t prh> void USART<prh>::set_tx_interrupt(bool en) {
    if (en) {
        usart_enable_tx_interrupt(prh);
    }
    else {
        usart_disable_tx_interrupt(prh);
    }
}
template<uintptr_t prh> void USART<prh>::set_tx_complete_interrupt(bool en) {
    if (en) {
        usart_enable_tx_complete_interrupt(prh);
    }
    else {
        usart_disable_tx_complete_interrupt(prh);
    }
}
template<uintptr_t prh> void USART<prh>::set_idle_interrupt(bool en) {
    if (en) {
        usart_enable_idle_interrupt(prh);
    }
    else {
        usart_disable_idle_interrupt(prh);
    }
}
template<uintptr_t prh> void USART<prh>::set_error_interrupt(bool en) {
    if (en) {
        usart_enable_error_interrupt(prh);
    }
    else {
        usart_disable_error_interrupt(prh);
    }
}

template<uintptr_t prh> uint32_t USART<prh>::get_baudrate() {
    uint32_t clock = rcc_get_usart_clk_freq(prh);
    return clock / USART_BRR(prh);
}
template<uintptr_t prh> uint8_t USART<prh>::get_databits() {
    bool d9 = USART_CR1(prh) & USART_CR1_M;
    return 9 ? d9 : 8;
}
template<uintptr_t prh> typename USART<prh>::StopBits USART<prh>::get_stopbits() {
    return USART<prh>::StopBits(USART_CR2(prh) & USART_CR2_STOPBITS_MASK);
}
template<uintptr_t prh> typename USART<prh>::Parity USART<prh>::get_parity() {
    return USART<prh>::Parity(USART_CR1(prh) & USART_PARITY_MASK);
}
template<uintptr_t prh> bool USART<prh>::get_mode_tx() {
    return !!(USART_CR1(prh) & USART_MODE_TX);
}
template<uintptr_t prh> bool USART<prh>::get_mode_rx() {
    return !!(USART_CR1(prh) & USART_MODE_RX);
}
template<uintptr_t prh> bool USART<prh>::get_flow_control_rts() {
    return !!(USART_CR3(prh) & USART_FLOWCONTROL_RTS);
}
template<uintptr_t prh> bool USART<prh>::get_flow_control_cts() {
    return !!(USART_CR3(prh) & USART_FLOWCONTROL_CTS);
}
template<uintptr_t prh> bool USART<prh>::get_rx_dma() {
    return !!(USART_CR3(prh) & USART_CR3_DMAR);
}
template<uintptr_t prh> bool USART<prh>::get_tx_dma() {
    return !!(USART_CR3(prh) & USART_CR3_DMAT);
}
template<uintptr_t prh> bool USART<prh>::get_rx_interrupt() {
    return !!(USART_CR1(prh) & USART_CR1_RXNEIE);
}
template<uintptr_t prh> bool USART<prh>::get_tx_interrupt() {
    return !!(USART_CR1(prh) & USART_CR1_TXEIE);
}
template<uintptr_t prh> bool USART<prh>::get_tx_complete_interrupt() {
    return !!(USART_CR1(prh) & USART_CR1_TCIE);
}
template<uintptr_t prh> bool USART<prh>::get_idle_interrupt() {
    return !!(USART_CR1(prh) & USART_CR1_IDLEIE);
}
template<uintptr_t prh> bool USART<prh>::get_error_interrupt() {
    return !!(USART_CR3(prh) & USART_CR3_EIE);
}

template<uintptr_t prh> void USART<prh>::enable() {
    usart_enable(prh);
}
template<uintptr_t prh> void USART<prh>::disable() {
    usart_disable(prh);
}
template<uintptr_t prh> bool USART<prh>::enabled() {
    return !!(USART_CR1(prh) & USART_CR1_UE);
}

template<uintptr_t prh> void USART<prh>::wait_tx() {
    if (enabled()) {
        usart_wait_send_ready(prh);
    }
}
template<uintptr_t prh> bool USART<prh>::tx_ready() {
    if (enabled()) {
        return !!(USART_SR(prh) & USART_SR_TXE);
    }
    return false;
}
template<uintptr_t prh> void USART<prh>::wait_rx() {
    if (enabled()) {
        usart_wait_recv_ready(prh);
    }
}
template<uintptr_t prh> bool USART<prh>::rx_ready() {
    if (enabled()) {
        return !!(USART_SR(prh) & USART_SR_RXNE);
    }
    return false;
}

template<uintptr_t prh> void USART<prh>::tx(uint16_t data, bool blocking) {
    if (enabled()) {
        if (blocking) {
            usart_wait_send_ready(prh);
        }
        usart_send(prh, data);
    }
}
template<uintptr_t prh> uint16_t USART<prh>::rx(bool blocking) {
    if (enabled()) {
        if (blocking) {
            usart_wait_recv_ready(prh);
        }
        return usart_recv(prh);
    }
    return 0;
}

}  // namespace stm32

template class stm32::USART<USART1>;
template class stm32::USART<USART2>;
template class stm32::USART<USART3>;
template class stm32::USART<UART4>;
template class stm32::USART<UART5>;
template class stm32::USART<USART6>;
template class stm32::USART<UART7>;
template class stm32::USART<UART8>;
