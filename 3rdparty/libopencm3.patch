From 263ee714c38c5c573b7414f8e46e61e10a4cbba3 Mon Sep 17 00:00:00 2001
From: Anastasios Simeonidis <anastasios2014@gmail.com>
Date: Sun, 1 Aug 2021 16:39:42 +0300
Subject: [PATCH] USB fix for STM32 devices with hardware VBUS detection.
 Changes for fixing compiler warnings.

---
 include/libopencm3/cm3/common.h         |  2 ++
 include/libopencm3/usb/dwc/otg_common.h |  2 ++
 lib/stm32/common/gpio_common_all.c      | 23 ++++++++++-------------
 lib/usb/usb_f107.c                      | 13 ++++++++-----
 4 files changed, 22 insertions(+), 18 deletions(-)

diff --git a/include/libopencm3/cm3/common.h b/include/libopencm3/cm3/common.h
index 04714e0a..30058d7b 100644
--- a/include/libopencm3/cm3/common.h
+++ b/include/libopencm3/cm3/common.h
@@ -53,6 +53,8 @@
 #define MMIO32(addr)	(addr)
 #define MMIO64(addr)	(addr)
 
+#error "Should not use non-volatile MMIOs!"
+
 #define BBIO_SRAM(addr, bit) \
 	(((addr) & 0x0FFFFF) * 32 + 0x22000000 + (bit) * 4)
 
diff --git a/include/libopencm3/usb/dwc/otg_common.h b/include/libopencm3/usb/dwc/otg_common.h
index 56483f6e..95b69b45 100644
--- a/include/libopencm3/usb/dwc/otg_common.h
+++ b/include/libopencm3/usb/dwc/otg_common.h
@@ -99,6 +99,8 @@
 #define OTG_GOTGCTL_HSHNPEN		(1 << 10)
 #define OTG_GOTGCTL_HNPRQ		(1 << 9)
 #define OTG_GOTGCTL_HNGSCS		(1 << 8)
+#define OTG_GOTGCTL_BVALOVAL    (1 << 7)
+#define OTG_GOTGCTL_BVALOEN     (1 << 6)
 #define OTG_GOTGCTL_SRQ			(1 << 1)
 #define OTG_GOTGCTL_SRQSCS		(1 << 0)
 
diff --git a/lib/stm32/common/gpio_common_all.c b/lib/stm32/common/gpio_common_all.c
index 9602a999..1d8c6d66 100644
--- a/lib/stm32/common/gpio_common_all.c
+++ b/lib/stm32/common/gpio_common_all.c
@@ -133,19 +133,16 @@ void gpio_port_config_lock(uint32_t gpioport, uint16_t gpios)
 {
 	uint32_t reg32;
 
-	/* Special "Lock Key Writing Sequence", see datasheet. */
-	GPIO_LCKR(gpioport) = GPIO_LCKK | gpios;	/* Set LCKK. */
-	GPIO_LCKR(gpioport) = ~GPIO_LCKK & gpios;	/* Clear LCKK. */
-	GPIO_LCKR(gpioport) = GPIO_LCKK | gpios;	/* Set LCKK. */
-	reg32 = GPIO_LCKR(gpioport);			/* Read LCKK. */
-	reg32 = GPIO_LCKR(gpioport);			/* Read LCKK again. */
-
-	/* Tell the compiler the variable is actually used. It will get
-	 * optimized out anyways.
-	 */
-	reg32 = reg32;
-
-	/* If (reg32 & GPIO_LCKK) is true, the lock is now active. */
+	do
+	{
+		/* Special "Lock Key Writing Sequence", see datasheet. */
+		GPIO_LCKR(gpioport) = GPIO_LCKK | gpios;	/* Set LCKK. */
+		GPIO_LCKR(gpioport) = ~GPIO_LCKK & gpios;	/* Clear LCKK. */
+		GPIO_LCKR(gpioport) = GPIO_LCKK | gpios;	/* Set LCKK. */
+		reg32 = GPIO_LCKR(gpioport);			/* Read LCKK. */
+		reg32 = GPIO_LCKR(gpioport);			/* Read LCKK again. */
+		/* If (reg32 & GPIO_LCKK) is true, the lock is now active. */
+	} while (!(reg32 & GPIO_LCKK));
 }
 
 /**@}*/
diff --git a/lib/usb/usb_f107.c b/lib/usb/usb_f107.c
index 52df7172..007e350d 100644
--- a/lib/usb/usb_f107.c
+++ b/lib/usb/usb_f107.c
@@ -63,11 +63,14 @@ static usbd_device *stm32f107_usbd_init(void)
 	while (OTG_FS_GRSTCTL & OTG_GRSTCTL_CSRST);
 
 	if (OTG_FS_CID >= OTG_CID_HAS_VBDEN) {
-		/* Enable VBUS detection in device mode and power up the PHY. */
-		OTG_FS_GCCFG |= OTG_GCCFG_VBDEN | OTG_GCCFG_PWRDWN;
-	} else {
-		/* Enable VBUS sensing in device mode and power up the PHY. */
-		OTG_FS_GCCFG |= OTG_GCCFG_VBUSBSEN | OTG_GCCFG_PWRDWN;
+        /* Disable VBUS sensing and power up the PHY. */
+        OTG_FS_GCCFG |= OTG_GCCFG_PWRDWN;
+        OTG_FS_GCCFG &= ~OTG_GCCFG_VBDEN;
+        OTG_FS_GOTGCTL |= OTG_GOTGCTL_BVALOEN | OTG_GOTGCTL_BVALOVAL;
+    } else {
+        /* Disable VBUS sensing and power up the PHY. */
+        OTG_FS_GCCFG |= OTG_GCCFG_NOVBUSSENS | OTG_GCCFG_PWRDWN;
+        OTG_FS_GCCFG &= ~(OTG_GCCFG_VBUSBSEN | OTG_GCCFG_VBUSASEN);
 	}
 	/* Explicitly enable DP pullup (not all cores do this by default) */
 	OTG_FS_DCTL &= ~OTG_DCTL_SDIS;
-- 
2.32.0

