#include "COM.hpp"

#include <Objects.hpp>
#include <interface/io/Core.hpp>
#include <interface/io/GPIO.hpp>
#include <interface/rtos/RTOS.hpp>
#include <mmcc/board/Config.hpp>

#include <algorithm>
#include <cstddef>
#include <cstdlib>
#include <optional>

#include <libopencm3/cm3/nvic.h>

namespace mmcc {

void USBTask::init() {
    _receive_stream.init(1);
    _send_stream.init(1);
}
bool USBTask::start() {
    return rtos::StaticTask<384>::start<USBTask, &USBTask::run>("usb", 6);
}

void USBTask::configure() {
    stm32::USB& usb = stm32::USB::get_instance();
    mmcc::InterruptGuard ctx;
    usb.init(ctx, _dev, _config, std::make_pair(_strings, 3),
             std::make_pair(usb_control_buffer, sizeof(usb_control_buffer)));
    usb.register_config_callback(on_set_config);
    usb.register_sof_callback(on_sof);
    mmcc::dbg.print("USB is ready!\n");
}

void USBTask::run() {
    configure();
    _sof_watchdog_last_tick = rtos::Kernel::get_tickcount();
    while (true) {
        // Wait for an event.
        const uint32_t NOTIFICATION_MASK =
             NOTIFICATION_CONNECT_STATE_CHANGE_BIT | NOTIFICATION_DATA_SEND_PENDING_BIT;
        auto wait_timeout_ticks = (_connection_state != ConnectionState::PhyDisconnected) ?
                                       SOF_WATCHDOG_PERIOD / 2 :
                                       rtos::ms2ticks(1000);
        auto state = notify_wait_ticks(1, 0x0, NOTIFICATION_MASK, wait_timeout_ticks);

        run_watchdog();

        if (state.first) {
            if (state.second & NOTIFICATION_CONNECT_STATE_CHANGE_BIT) {
                // Notify task on receive.
                if (_receive_task != nullptr) {
                    /**
                     * NOTE: As long as we priority over(>) the task
                     * calling receive() method, then the following
                     * won't have race conditions.
                     */
                    _receive_task->notify(0, NotificationAction::eNoAction, 0);
                }
            }
        }
        run_send();
    }
}

void USBTask::run_watchdog() {
    TickType_t current_tick = rtos::Kernel::get_tickcount();
    // Update sof watchdog every at least 100ms.
    if ((current_tick - _sof_watchdog_last_tick) >= SOF_WATCHDOG_PERIOD) {
        mmcc::InterruptGuard<> guard;
        if (_connection_state != ConnectionState::PhyDisconnected) {
            if (_sof_watchdog_counter > 0) {
                _sof_watchdog_counter = _sof_watchdog_counter - 1;
            }
            else { /* _sof_watchdog_counter == 0 */
                // Watchdog triggered - consider usb disconnected!
                _connection_state = ConnectionState::PhyDisconnected;
                // TODO: Test if the suspend interrupt is usable for this purpose.
                notify(1, NotificationAction::eSetBits, NOTIFICATION_CONNECT_STATE_CHANGE_BIT);
            }
            // `Until ticks`-style delay.
            _sof_watchdog_last_tick += SOF_WATCHDOG_PERIOD;
        }
        else {
            _sof_watchdog_last_tick = current_tick;
        }
    }
}

// TODO: Will stall system watchdog updates when transmitting at full bandwidth.
void USBTask::run_send() {
    // Wait for flag clear.
    if (_usb_transmitting) {
        return;
    }

    // This is the only location that receives data from _send_stream.
    static uint8_t send_buffer[BULK_OUT_INTERMEDIATE_BUFFER_SIZE];
    size_t packet_size = std::min(_send_stream.bytes_available(), sizeof(send_buffer));

    // Nothing to send.
    if (packet_size == 0) {
        return;
    }

    // Avoid send while send error.
    _usb_transmitting = true;

    // Skip if endpoint is busy (edge case - mostly fixed with other methods).
    while (_data_endp[1].enabled()) {
        dbg.print("USBTask send busy.\n");
        delay_ticks(10);
        // notify(1, NotificationAction::eSetBits, NOTIFICATION_DATA_SEND_PENDING_BIT);
    }

    if (_send_stream.receive_ticks(send_buffer, packet_size) != packet_size) {
        // Test for incomplete data-stream read.
        dbg.print("USBTask failure to get packet data for sending.\n");
        std::abort();
    }

    {
        // InterruptGuard<> guard;
        nvic_disable_irq(NVIC_OTG_FS_IRQ);
        bool sent = _data_endp[1].write(send_buffer, packet_size);
        nvic_enable_irq(NVIC_OTG_FS_IRQ);

        // Fail when last transfer hasn't completed.
        if (!sent) {
            // Report error.
            dbg.print("USBTask failure to send packet.\n");
            std::abort();
        }
    }
}

USBTask::ConnectionState USBTask::get_connection_state() const {
    /* byte read is by nature atomic */
    return _connection_state;
}
size_t USBTask::receive_bytes_available() {
    return _receive_stream.bytes_available();
}
void USBTask::disconnect(bool dc) {
    stm32::USB& usb = stm32::USB::get_instance();
    if (dc) {
        // Flush output buffer before disconnecting.
        while (!_send_stream.is_empty()) {
            rtos::Kernel::yield();
        }
    }
    usb.disconnect(dc);
}

size_t USBTask::receive_ticks(rtos::iTask& task, void* data, size_t size, size_t trigger_level,
                              TickType_t timeout_ticks) {
    if (!_receive_stream.set_trigger_level(trigger_level)) {
        return 0;
    }

    _receive_task = &task;
    size_t received = _receive_stream.receive_ticks(data, size, timeout_ticks);
    _receive_task = nullptr;
    if (_not_receiving) {
        // Un-block usb if enough space is available.
        if (_receive_stream.spaces_available() >= 96) {
            InterruptGuard<> guard;
            _data_endp[0].nak_set(false);
            _not_receiving = false;
            // nvic_enable_irq(NVIC_OTG_FS_IRQ);
        }
    }
    return received;
}

size_t USBTask::send_ticks(const void* data, size_t len, TickType_t timeout_ticks) {
    size_t queued = _send_stream.send_ticks(data, len, timeout_ticks);
    if (_send_stream.bytes_available() == queued) {
        /**
         * Queue first packet.
         *
         * This is required only in the case
         * that no transfer was pending before.
         * The rest packets are queued by usb interrupt
         * as long as the buffer is non-empty.
         */
        notify(1, NotificationAction::eSetBits, NOTIFICATION_DATA_SEND_PENDING_BIT);
    }
    return queued;
}

size_t USBTask::send_from_isr(const void* data, size_t len) {
    size_t queued = _send_stream.send_from_isr(data, len);
    // Just send it.
    notify(1, NotificationAction::eSetBits, NOTIFICATION_DATA_SEND_PENDING_BIT);
    return queued;
}

/* Handle usb interrupts! */
void USBTask::on_set_config(stm32::USB& usb, uint16_t) {
    _not_receiving = false;
    _usb_transmitting = false;
    _data_endp[0].setup(usb, on_data_received);
    _data_endp[1].setup(usb, on_data_sent);
    _comm_endp[0].setup(usb);

    usb.register_control_callback(cdcacm_control_request,
                                  USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
                                  USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT);
}

usbd_request_return_codes USBTask::cdcacm_control_request(stm32::USB&, usb_setup_data* req,
                                                          uint8_t**, uint16_t* len,
                                                          usbd_control_complete_callback*) {
    switch (req->bRequest) {
        case USB_CDC_REQ_SET_CONTROL_LINE_STATE: {
            /*
             * Detect serial port connect/disconnect events by
             * monitoring configuration requests.
             *
             * Probably won't work with all clients.
             */
            if (req->wValue) {
                _connection_state = ConnectionState::UserConnected;
            }
            else {
                _connection_state = ConnectionState::PhyConnected;
            }
            notify(1, NotificationAction::eSetBits, NOTIFICATION_CONNECT_STATE_CHANGE_BIT);
            return USBD_REQ_HANDLED;
        }
        case USB_CDC_REQ_SET_LINE_CODING:
            if (*len < sizeof(struct usb_cdc_line_coding)) {
                return USBD_REQ_NOTSUPP;
            }

            return USBD_REQ_HANDLED;
    }
    return USBD_REQ_NEXT_CALLBACK;
}

void USBTask::on_sof(stm32::USB&) {
    InterruptGuard<true> sup;
    if (_connection_state == ConnectionState::PhyDisconnected) {
        _connection_state = ConnectionState::PhyConnected;
        notify(1, NotificationAction::eSetBits, NOTIFICATION_CONNECT_STATE_CHANGE_BIT);
    }
    _sof_watchdog_counter = SOF_WATCHDOG_RELOAD;
}

void USBTask::on_data_received(stm32::USB& usb, uint8_t) {
    /**
     * TODO: We might read duplicate data when
     * we read some data, then read more data,
     * but that data doesn't fit to send in the stream.
     */
    static char receive_buffer[BULK_IN_INTERMEDIATE_BUFFER_SIZE];
    size_t received_cnt;
    do {
        // We must read all the received data residing in the FIFO.
        received_cnt = _data_endp[0].read(usb, receive_buffer, sizeof(receive_buffer));
        if (received_cnt > 0) {
            // Test and avoid buffer overflow.
            if (_receive_stream.spaces_available() >= received_cnt) {
                if (_receive_stream.send_from_isr(receive_buffer, received_cnt) !=
                    received_cnt) {
                    dbg.print("USBTask::on_data_received couldn't send.\n");
                    std::abort();
                }
            }
            else {
                dbg.print("USBTask::on_data_received not enough space.\n");
                std::abort();
            }
        }
        else { /*received_cnt == 0*/
            // Test if we will have enough space for the next potential packet.
            if (_receive_stream.spaces_available() < 64) {
                _data_endp[0].nak_set(usb, true);
                _not_receiving = true;
                // nvic_disable_irq(NVIC_OTG_FS_IRQ);
            }
        }
    } while (received_cnt != 0);
}

void USBTask::on_data_sent(stm32::USB&, uint8_t) {
    // Unlock flag.
    _usb_transmitting = false;
    // Notify to send next packet.
    if (!_send_stream.is_empty()) {
        notify(1, NotificationAction::eSetBits, NOTIFICATION_DATA_SEND_PENDING_BIT);
    } /* else idle */
}

/* Static callback dispatchers */
void USBTask::on_set_config(usbd_device*, uint16_t v) {
    stm32::USB& usb = stm32::USB::get_instance();
    usb_task.on_set_config(usb, v);
}

usbd_request_return_codes USBTask::cdcacm_control_request(
     usbd_device*, usb_setup_data* req, uint8_t** buf, uint16_t* len,
     usbd_control_complete_callback* complete) {
    stm32::USB& usb = stm32::USB::get_instance();
    return usb_task.cdcacm_control_request(usb, req, buf, len, complete);
}

void USBTask::on_sof() {
    stm32::USB& usb = stm32::USB::get_instance();
    usb_task.on_sof(usb);
}

void USBTask::on_data_received(usbd_device*, uint8_t ep) {
    stm32::USB& usb = stm32::USB::get_instance();
    usb_task.on_data_received(usb, ep);
}

void USBTask::on_data_sent(usbd_device*, uint8_t ep) {
    stm32::USB& usb = stm32::USB::get_instance();
    usb_task.on_data_sent(usb, ep);
}

}  // namespace mmcc
