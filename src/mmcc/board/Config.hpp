#ifndef MMCC_CONFIG_HPP
#define MMCC_CONFIG_HPP

#include <interface/io/GPIO.hpp>
#include <interface/io/Timer.hpp>
#include <interface/io/USART.hpp>

#include <ratio>

namespace mmcc {

static constexpr auto FLASH_MODULE_MAP =
     std::array<std::pair<uintptr_t /*base_address*/, size_t /*length*/>, 8>({
          {0x08000000, 16 * 1024},
          {0x08004000, 16 * 1024},
          {0x08008000, 16 * 1024},
          {0x0800C000, 16 * 1024},
          {0x08010000, 64 * 1024},
          {0x08020000, 128 * 1024},
          {0x08040000, 128 * 1024},
          {0x08060000, 128 * 1024},
     });

/* Oscillator pins */
constexpr auto HSE_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'H', 0>(), stm32::Pin::make<'H', 1>()>();
constexpr auto LSE_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'C', 14>(), stm32::Pin::make<'C', 15>()>();

/* Debug pins */
constexpr auto JTAG_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'A', 15>(), stm32::Pin::make<'B', 3>(),
                           stm32::Pin::make<'B', 4>()>();
;
constexpr auto SWD_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'A', 13>(), stm32::Pin::make<'A', 14>()>();

/* State pins */
constexpr auto BOOT1 = stm32::PinSet<>::make<stm32::Pin::make<'B', 2>()>();
constexpr auto STATUS_LED = stm32::PinSet<>::make<stm32::Pin::make<'C', 13>()>();

constexpr auto HOME_ESTOP_BUTTON = stm32::PinSet<>::make<stm32::Pin::make<'A', 0>()>();
constexpr auto HOME_ESTOP_LED = stm32::PinSet<>::make<stm32::Pin::make<'A', 1>()>();
/* Debounce timeout in milliseconds */
constexpr auto HOME_ESTOP_BUTTON_DEBOUNCE_TIMEOUT = 20;

/* Communication interfaces */
constexpr auto B2B_TX = stm32::PinSet<>::make<stm32::Pin::make<'A', 2>()>();
constexpr auto B2B_RX = stm32::PinSet<>::make<stm32::Pin::make<'A', 3>()>();
constexpr auto B2B_PINS = PINSET_CONCAT(B2B_TX, B2B_RX);
using B2B_USART = stm32::USART<USART2>;

constexpr auto USB_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'A', 11>(), stm32::Pin::make<'A', 12>()>();

/* Main actuator */
constexpr auto ACT_PWM_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'A', 8>(), stm32::Pin::make<'A', 9>(),
                           stm32::Pin::make<'A', 10>()>();
using ACT_PWM_TIMER = stm32::timer::AdvancedTimer<TIM1>;
using ACT_PID_TIMER = stm32::timer::Timer<TIM9>;
using HOME_ESTOP_TIMER = stm32::timer::Timer<TIM11>;

// 1st pin is ACT?_EN, second is ACT?_DIR.
constexpr auto ACT0_CTL_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'B', 4>(), stm32::Pin::make<'B', 13>()>();
constexpr auto ACT1_CTL_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'B', 5>(), stm32::Pin::make<'B', 14>()>();
constexpr auto ACT2_CTL_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'B', 6>(), stm32::Pin::make<'B', 15>()>();
constexpr auto ACT_CTL_PINS =
     PINSET_CONCAT(ACT0_CTL_PINS, PINSET_CONCAT(ACT1_CTL_PINS, ACT2_CTL_PINS));

/* Extra combination of the control pins. */
constexpr auto ACT_CTL_EN_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'B', 4>(), stm32::Pin::make<'B', 5>(),
                           stm32::Pin::make<'B', 6>()>();
constexpr auto ACT_CTL_DIR_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'B', 13>(), stm32::Pin::make<'B', 14>(),
                           stm32::Pin::make<'B', 15>()>();
constexpr auto ACT0_CTL_DIR_PINS = stm32::PinSet<>::make<ACT_CTL_DIR_PINS[0]>();
constexpr auto ACT0_CTL_EN_PINS = stm32::PinSet<>::make<ACT_CTL_EN_PINS[0]>();
constexpr auto ACT1_CTL_DIR_PINS = stm32::PinSet<>::make<ACT_CTL_DIR_PINS[1]>();
constexpr auto ACT1_CTL_EN_PINS = stm32::PinSet<>::make<ACT_CTL_EN_PINS[1]>();
constexpr auto ACT2_CTL_DIR_PINS = stm32::PinSet<>::make<ACT_CTL_DIR_PINS[2]>();
constexpr auto ACT2_CTL_EN_PINS = stm32::PinSet<>::make<ACT_CTL_EN_PINS[2]>();
constexpr auto ACT12_CTL_DIR_PINS = PINSET_CONCAT(ACT1_CTL_DIR_PINS, ACT2_CTL_DIR_PINS);

constexpr auto ACT0_POS_ENC_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'A', 15>(), stm32::Pin::make<'B', 3>()>();
using ACT0_POS_ENC_TIMER = stm32::timer::Timer<TIM2>;

constexpr auto ACT1_POS_PIN = stm32::PinSet<>::make<stm32::Pin::make<'A', 7>()>();
using ACT1_POS_TIMER = stm32::timer::Timer<TIM3>; /* CH2 */
constexpr auto ACT2_POS_PIN = stm32::PinSet<>::make<stm32::Pin::make<'B', 7>()>();
using ACT2_POS_TIMER = stm32::timer::Timer<TIM4>; /* CH2 */
constexpr auto ACT12_POS_PINS = PINSET_CONCAT(ACT1_POS_PIN, ACT2_POS_PIN);

/* Auxiliary */
constexpr auto AUX1_MOTOR_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'B', 0>(), stm32::Pin::make<'B', 1>()>();
using AUX1_MOTOR_TIMER = stm32::timer::Timer<TIM3>;
constexpr auto AUX2_MOTOR_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'B', 8>(), stm32::Pin::make<'B', 9>()>();
using AUX2_MOTOR_TIMER = stm32::timer::Timer<TIM4>;
constexpr auto AUX_MOTOR_PINS = PINSET_CONCAT(AUX1_MOTOR_PINS, AUX2_MOTOR_PINS);

constexpr auto SONAR_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'B', 10>(), stm32::Pin::make<'B', 12>()>();
constexpr auto SONAR_ECHO_PIN = stm32::PinSet<>::make<stm32::Pin::make<'B', 10>()>();
constexpr auto SONAR_TRIG_PIN = stm32::PinSet<>::make<stm32::Pin::make<'B', 12>()>();
using SONAR_TIMER = stm32::timer::Timer<TIM10>;

constexpr auto UNUSED_PINS =
     stm32::PinSet<>::make<stm32::Pin::make<'A', 4>(), stm32::Pin::make<'A', 5>(),
                           stm32::Pin::make<'A', 6>()>();

}  // namespace mmcc

#endif /*MMCC_CONFIG_HPP*/
