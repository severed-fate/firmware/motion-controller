#include "TCodeImplSensor.hpp"

#include <TCodeInstance.hpp>
#include <mmcc/Objects.hpp>

namespace tcode::callbacks {

void meta_sonar_config_fullscale(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(15.0f));
    mt["max"].write(static_cast<float>(3200.0f));
}
void meta_sonar_config_normalized(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(1.0f));
}

response::Error sonar_get_is_scanning(const void*, size_t) {
    uint32_t status = mmcc::sonar_task.is_scanning();
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "is_scanning",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error sonar_set_is_scanning(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    if (status) {
        mmcc::sonar_task.start_scanning();
    }
    else {
        mmcc::sonar_task.stop_scanning();
    }
    return {};
}

response::Error sonar_get_raw_measurement(const void*, size_t) {
    uint32_t value = mmcc::sonar_task.get_measurement_raw();
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "raw_measurement",
                                 response::Z85Data((const uint8_t*) &value, sizeof(value)));
    return {};
}
response::Error sonar_get_measurement_errors(const void*, size_t) {
    uint32_t count = mmcc::sonar_task.get_invalid_measurement_count();
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "measurement_errors",
                                 response::Z85Data((const uint8_t*) &count, sizeof(count)));
    return {};
}
response::Error sonar_get_scaled_measurement(const void*, size_t) {
    float value = mmcc::sonar_task.get_measurement_scaled();
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "scaled_measurement",
                                 response::Z85Data((const uint8_t*) &value, sizeof(value)));
    return {};
}
response::Error sonar_get_filtered_measurement(const void*, size_t) {
    float value = mmcc::sonar_task.get_measurement_filtered();
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "filtered_measurement",
                                 response::Z85Data((const uint8_t*) &value, sizeof(value)));
    return {};
}
response::Error sonar_get_normalized_measurement(const void*, size_t) {
    float value = mmcc::sonar_task.get_measurement_normalized();
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0},
                                 "normalized_measurement",
                                 response::Z85Data((const uint8_t*) &value, sizeof(value)));
    return {};
}

response::Error sonar_get_norm_near(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->sonar.near;
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "norm_near",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error sonar_set_norm_near(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->sonar.near = v;
    return {};
}
response::Error sonar_get_norm_far(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->sonar.far;
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "norm_far",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error sonar_set_norm_far(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->sonar.far = v;
    return {};
}
response::Error sonar_get_norm_invert(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    uint32_t status = cfg->sonar.invert;
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "norm_invert",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error sonar_set_norm_invert(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->sonar.invert = !!status;
    return {};
}

response::Error sonar_get_threshold1(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->sonar.threshold1;
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "threshold1",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error sonar_set_threshold1(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->sonar.threshold1 = v;
    return {};
}
response::Error sonar_get_threshold2(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->sonar.threshold2;
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "threshold2",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error sonar_set_threshold2(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->sonar.threshold2 = v;
    return {};
}
response::Error sonar_get_threshold_selector(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->sonar.threshold_selector;
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "threshold_selector",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error sonar_set_threshold_selector(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->sonar.threshold_selector = v;
    return {};
}
response::Error sonar_get_alpha1(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->sonar.alpha1;
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "alpha1",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error sonar_set_alpha1(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->sonar.alpha1 = v;
    return {};
}
response::Error sonar_get_alpha2(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->sonar.alpha2;
    TCODE_INSTANCE.send_response({response::CommandType::Auxiliary, 0}, "alpha2",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error sonar_set_alpha2(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->sonar.alpha2 = v;
    return {};
}

response::Error sonar_reset_filter(void*, size_t) {
    mmcc::sonar_task.pend_filter_reset();
    return {};
}

void meta_sonar_plot(codec::UBJsonObject& root) {
    {
        auto obj = root["axis_mapping"].begin_object();
        {
            auto labels = obj["labels"].begin_array();
            labels.write("time");
            labels.write("scaled");
            labels.write("filtered");
            labels.write("normalized");
        }
    }
}
response::Error sonar_get_plot(const void* ev_dat, size_t ev_n) {
    if (ev_dat != nullptr && ev_n == sizeof(SonarPlotData)) {
        const SonarPlotData* ev = reinterpret_cast<const SonarPlotData*>(ev_dat);
        auto root =
             TCODE_INSTANCE.send_ubjson_response({response::CommandType::Auxiliary, 0}, "plot");
        {
            auto dat = root.begin_array(codec::UBJsonType::Float, 4);
            dat.write(ev->time);
            dat.write(ev->scaled);
            dat.write(ev->filtered);
            dat.write(ev->normalized);
        }
    }
    return {};
}

}  // namespace tcode::callbacks
