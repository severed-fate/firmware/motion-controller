#include "Actuator.hpp"
#include "ConfigBlock.hpp"

#include <Objects.hpp>
#include <interface/rtos/RTOS.hpp>
#include <sensors/Sonar.hpp>

namespace mmcc {

void ActuatorHomingTask::init() {}
bool ActuatorHomingTask::start() {
    return rtos::StaticTask<256>::start<ActuatorHomingTask, &ActuatorHomingTask::run>("homer",
                                                                                      6);
}

void ActuatorHomingTask::run() {
    while (true) {
        while (actuator_task.get_state() != Actuator::States::Homing) {
            notify_wait_ticks(0, 0, 0, rtos::ms2ticks(3000));
        }
        {
            InterruptGuard<true> sup;
            PINSET_SET_ALL_CTX(HOME_ESTOP_LED, sup);
        }
        _home();
        {
            InterruptGuard<true> sup;
            PINSET_RESET_ALL_CTX(HOME_ESTOP_LED, sup);
        }
        while (actuator_task.get_state() == Actuator::States::Homing) {
            if (actuator_task.push_event_ticks(Actuator::Events::HomeEnd,
                                               rtos::ms2ticks(100))) {
                break;
            }
        }
    }
}
bool ActuatorHomingTask::_home() {
    float act0_spd;
    float act1_spd;
    float act2_spd;
    {
        auto cfg = mmcc::config.get_current_state();
        if (!cfg->actuator.home_enable) {
            // Bypass homing.
            return true;
        }
        act0_spd = cfg->actuator.act0.homing_speed;
        act1_spd = cfg->actuator.act1.homing_speed;
        act2_spd = cfg->actuator.act2.homing_speed;
    }

    // Center Act 1.
    if (actuator_task.get_actuator1_position_scaled() >= (0.5f + HOMING_ACT12_DEADBAND)) {
        // Move 'down'.
        actuator_task.set_actuator1_pwm(-act1_spd);
        while (actuator_task.get_actuator1_position_scaled() > 0.5f) {
            if (actuator_task.get_state() != Actuator::States::Homing) {
                actuator_task.set_actuator1_pwm(0);
                return false;
            }
            delay_ticks(3);
        }
        actuator_task.set_actuator1_pwm(0);
    }
    else if (actuator_task.get_actuator1_position_scaled() <= (0.5f - HOMING_ACT12_DEADBAND)) {
        // pos <= 0.5 => Move 'up'.
        actuator_task.set_actuator1_pwm(act1_spd);
        while (actuator_task.get_actuator1_position_scaled() < 0.5f) {
            if (actuator_task.get_state() != Actuator::States::Homing) {
                actuator_task.set_actuator1_pwm(0);
                return false;
            }
            delay_ticks(3);
        }
        actuator_task.set_actuator1_pwm(0);
    }
    // Break motors to limit runaway due to inertia.
    actuator_task.set_actuators_enabled(true, false, true);
    // Delay between steps.
    for (size_t i = 0; i < 15; i++) {
        delay_ticks(rtos::ms2ticks(20));
        if (actuator_task.get_state() != Actuator::States::Homing) {
            return false;
        }
    }

    // Center Act 2.
    if (actuator_task.get_actuator2_position_scaled() >= (0.5f + HOMING_ACT12_DEADBAND)) {
        // Move 'down'.
        actuator_task.set_actuator2_pwm(-act2_spd);
        while (actuator_task.get_actuator2_position_scaled() > 0.5f) {
            if (actuator_task.get_state() != Actuator::States::Homing) {
                actuator_task.set_actuator2_pwm(0);
                return false;
            }
            delay_ticks(3);
        }
        actuator_task.set_actuator2_pwm(0);
    }
    else if (actuator_task.get_actuator2_position_scaled() <= (0.5f - HOMING_ACT12_DEADBAND)) {
        // pos <= 0.5 => Move 'up'.
        actuator_task.set_actuator2_pwm(act2_spd);
        while (actuator_task.get_actuator2_position_scaled() < 0.5f) {
            if (actuator_task.get_state() != Actuator::States::Homing) {
                actuator_task.set_actuator2_pwm(0);
                return false;
            }
            delay_ticks(3);
        }
        actuator_task.set_actuator2_pwm(0);
    }
    // Break motors to limit runaway due to inertia.
    actuator_task.set_actuators_enabled(true, false, false);
    // Delay between steps.
    for (size_t i = 0; i < 15; i++) {
        delay_ticks(rtos::ms2ticks(20));
        if (actuator_task.get_state() != Actuator::States::Homing) {
            return false;
        }
    }

    // Calibrate Act 0.
    uint32_t act0_pos_start;
    uint32_t act0_pos_end;
    {  // Go all the way to the bottom.
        uint32_t steady_stall_counter = 0;
        uint32_t last_pos = actuator_task.get_actuator0_position_raw();
        actuator_task.set_actuator0_pwm(act0_spd);
        while (steady_stall_counter <= 20) {
            uint32_t current_pos = actuator_task.get_actuator0_position_raw();
            uint32_t speed =
                 (last_pos > current_pos) ? (last_pos - current_pos) : (current_pos - last_pos);
            if (speed == 0) {
                steady_stall_counter++;
            }
            else {
                steady_stall_counter = 0;
            }
            last_pos = current_pos;
            if (actuator_task.get_state() != Actuator::States::Homing) {
                actuator_task.set_actuator0_pwm(0);
                return false;
            }
            delay_ticks(3);
        }
        act0_pos_end = actuator_task.get_actuator0_position_raw();
        actuator_task.set_actuator0_pwm(0);
    }
    // Wait a bit for the system to settle.
    for (size_t i = 0; i < 15; i++) {
        delay_ticks(rtos::ms2ticks(20));
        if (actuator_task.get_state() != Actuator::States::Homing) {
            return false;
        }
    }
    {  // Go all the way to up.
        uint32_t steady_stall_counter = 0;
        uint32_t last_pos = actuator_task.get_actuator0_position_raw();
        actuator_task.set_actuator0_pwm(-act0_spd);
        while (steady_stall_counter <= 20) {
            uint32_t current_pos = actuator_task.get_actuator0_position_raw();
            uint32_t speed =
                 (last_pos > current_pos) ? (last_pos - current_pos) : (current_pos - last_pos);
            if (speed == 0) {
                steady_stall_counter++;
            }
            else {
                steady_stall_counter = 0;
            }
            last_pos = current_pos;
            if (actuator_task.get_state() != Actuator::States::Homing) {
                actuator_task.set_actuator0_pwm(0);
                return false;
            }
            delay_ticks(3);
        }
        act0_pos_start = actuator_task.get_actuator0_position_raw();
        actuator_task.set_actuator0_pwm(0);
    }
    // Update parameters.
    actuator_task.set_actuator0_position_limits(act0_pos_start, act0_pos_end);
    // Wait a bit for the system to settle.
    for (size_t i = 0; i < 15; i++) {
        delay_ticks(rtos::ms2ticks(20));
        if (actuator_task.get_state() != Actuator::States::Homing) {
            return false;
        }
    }
    {  // Go a default position/setpoint.
        const float target = 0.66f;
        // We are at 0, so we only need to go down.
        actuator_task.set_actuator0_pwm(act0_spd);
        while (actuator_task.get_actuator0_position_scaled() < target) {
            if (actuator_task.get_state() != Actuator::States::Homing) {
                actuator_task.set_actuator0_pwm(0);
                return false;
            }
            delay_ticks(3);
        }
        actuator_task.set_actuator0_pwm(0);
    }

    return true;
}

void ActuatorHomingTask::start_homing() const {
    notify(0, NotificationAction::eNoAction, 0);
}

}  // namespace mmcc