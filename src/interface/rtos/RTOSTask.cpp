#include "RTOSTask.hpp"

namespace rtos {

void iTask::delay_ticks(TickType_t ticks) {
    vTaskDelay(ticks);
}
void iTask::delay_ticks_until(TickType_t& previous_wake_time, TickType_t time_increment) {
    xTaskDelayUntil(&previous_wake_time, time_increment);
}
void iTask::abort_delay() const {
    xTaskAbortDelay(_handle);
}

void iTask::set_priority(uint prio) const {
    vTaskPrioritySet(_handle, prio);
}
uint iTask::get_priority() const {
    return uxTaskPriorityGet(_handle);
}
void iTask::suspend() const {
    vTaskSuspend(_handle);
}
void iTask::resume() const {
    if (is_inside_interrupt()) {
        xTaskResumeFromISR(_handle);
    }
    else {
        vTaskResume(_handle);
    }
}
void iTask::stop() {
    vTaskDelete(_handle);
    // Invalidate.
    _handle = nullptr;
}
iTask::State iTask::get_state() {
    return (iTask::State) eTaskGetState(_handle);
}
const char* iTask::task_get_name() {
    return pcTaskGetName(_handle);
}

bool iTask::notify(uint i, NotificationAction a, uint32_t v) const {
    if (is_inside_interrupt()) {
        BaseType_t task_switch = 0;
        auto r = xTaskNotifyIndexedFromISR(_handle, i, v, a, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
        return r == pdPASS;
    }
    else {
        return xTaskNotifyIndexed(_handle, i, v, a) == pdPASS;
    }
}
uint32_t iTask::notify_take_ticks(uint i, bool c, TickType_t timeout_ticks) {
    return ulTaskNotifyTakeIndexed(i, c, timeout_ticks);
}
std::pair<bool, uint32_t> iTask::notify_and_query(uint i, NotificationAction a,
                                                  uint32_t v) const {
    if (is_inside_interrupt()) {
        BaseType_t task_switch = 0;
        uint32_t previous_value;
        auto r =
             xTaskNotifyAndQueryIndexedFromISR(_handle, i, v, a, &previous_value, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
        return {r == pdPASS, previous_value};
    }
    else {
        uint32_t previous_value;
        auto r = xTaskNotifyAndQueryIndexed(_handle, i, v, a, &previous_value);
        return {r == pdPASS, previous_value};
    }
}
std::pair<bool, uint32_t> iTask::notify_wait_ticks(uint i, uint32_t cm_entry,
                                                   uint32_t cm_exit, TickType_t timeout) const {
    uint32_t value = 0;
    auto r = xTaskNotifyWaitIndexed(i, cm_entry, cm_exit, &value, timeout);
    return {r == pdPASS, value};
}
bool iTask::notify_state_clear(uint i) const {
    return xTaskNotifyStateClearIndexed(_handle, i) == pdTRUE;
}
uint32_t iTask::notify_value_clear(uint i, uint32_t cm) const {
    return ulTaskNotifyValueClearIndexed(_handle, i, cm);
}

iTask::~iTask() {
    // If object has not been invalid,
    if (_handle != nullptr) {
        // then mark rtos task for deletion.
        stop();
    }
}

}  // namespace rtos
