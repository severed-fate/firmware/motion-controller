import matplotlib.pyplot as plt
import numpy as np


def __interp_linear(x, x0, y0, x1, y1):
    a = (y1 - y0) / (x1 - x0)
    y = y0 + (x - x0) * a
    return y


if __name__ == "__main__":
    x = np.linspace(20, 3200, 4096)
    y = __interp_linear(x, 0.66*(250-35), 0.05, 1.0*(250-35), 0.95)
    y = np.clip(y, 0.05, 0.95)
    plt.plot(x, y)
    plt.show()
