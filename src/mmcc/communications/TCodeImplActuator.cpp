#include "TCodeImplActuator.hpp"

#include <TCodeInstance.hpp>
#include <interface/tcode/UBJsonBuilder.hpp>
#include <mmcc/Objects.hpp>

namespace tcode::callbacks {

static void _activate_pid_loop_if_idle() {
    if (mmcc::actuator_task.get_state() == mmcc::Actuator::States::Idle) {
        mmcc::actuator_task.push_event_ticks(mmcc::Actuator::Events::LoopStart, 3);
    }
}
static void _deactivate_pid_loop_if_running() {
    if (mmcc::actuator_task.get_state() == mmcc::Actuator::States::Running) {
        mmcc::actuator_task.push_event_ticks(mmcc::Actuator::Events::LoopStop, 3);
    }
}

response::Error get_pid_loop_status(const void*, size_t) {
    uint32_t status = mmcc::actuator_task.is_pid_loop_enabled();
    TCODE_INSTANCE.send_response({response::CommandType::Device, 5}, "pid_loop_status",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error set_pid_loop_status(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    if (status) {
        _activate_pid_loop_if_idle();
    }
    else {
        _deactivate_pid_loop_if_running();
    }
    return {};
}
response::Error get_homing_enable(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    uint32_t status = cfg->actuator.home_enable;
    TCODE_INSTANCE.send_response({response::CommandType::Device, 5}, "homing_enable",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error set_homing_enable(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.home_enable = !!status;
    return {};
}
response::Error get_pid_tick_count(const void*, size_t) {
    size_t counter = mmcc::actuator_task.get_pid_loop_tick_count();
    TCODE_INSTANCE.send_response({response::CommandType::Device, 5}, "pid_tick_count",
                                 response::Z85Data((const uint8_t*) &counter, sizeof(counter)));
    return {};
}
response::Error get_pwm_prescaler(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    int32_t prescaler = cfg->actuator.pwm_prescaler;
    TCODE_INSTANCE.send_response(
         {response::CommandType::Device, 5}, "pwm_prescaler",
         response::Z85Data((const uint8_t*) &prescaler, sizeof(prescaler)));
    return {};
}
response::Error set_pwm_prescaler(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint16_t prescaler = *reinterpret_cast<int32_t*>(dat);
    mmcc::actuator_task.set_actuators_pwm_prescaler(prescaler);
    return {};
}
void meta_pwm_prescaler(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<int32_t>(0));
    mt["max"].write(static_cast<int32_t>(1024));
}

void meta_homing_speed(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(1.0f));
}
void meta_tune_pid_kp(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(30.0f));
}
void meta_tune_pid_ki(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(30.0f));
}
void meta_tune_pid_kd(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(3.0f));
}
void meta_tune_pid_bias(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(1.0f));
}
void meta_axis_limit_min(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(1.0f));
}
void meta_axis_limit_max(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(1.0f));
}
void meta_tune_pwm_bias(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(0.5f));
}
void meta_tune_pwm_deadband(codec::UBJsonObject& mt) {
    mt["min"].write(static_cast<float>(0.0f));
    mt["max"].write(static_cast<float>(0.5f));
}

response::Error act0_normal_update(fractional<uint32_t> value) {
    if (mmcc::actuator_task.is_pid_loop_enabled() && mmcc::actuator_task.is_act0_engaged()) {
        mmcc::actuator_task.actuate0(value.quotient());
    }
    return {};
}
response::Error act0_interval_update(fractional<uint32_t> value,
                                     request::IntervalData interval) {
    if (mmcc::actuator_task.is_pid_loop_enabled() && mmcc::actuator_task.is_act0_engaged()) {
        mmcc::actuator_task.actuate0(value.quotient(), interval.interval);
    }
    return {};
}
response::Error act0_stop() {
    mmcc::actuator_task.set_act0_engage(false);
    return {};
}
response::Error act0_get_engaged(const void*, size_t) {
    uint32_t status = mmcc::actuator_task.is_act0_engaged();
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "engaged",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error act0_set_engaged(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    mmcc::actuator_task.set_act0_engage(!!status);
    return {};
}
response::Error act0_get_engaged_break(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    uint32_t status = cfg->actuator.act0_disengage_break;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "engaged_break",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error act0_set_engaged_break(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0_disengage_break = !!status;
    return {};
}
response::Error act0_get_raw_pos(const void*, size_t) {
    uint32_t raw = mmcc::actuator_task.get_actuator0_position_raw();
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "raw_pos",
                                 response::Z85Data((const uint8_t*) &raw, sizeof(raw)));
    return {};
}
response::Error act0_get_scaled_pos(const void*, size_t) {
    float pos = mmcc::actuator_task.get_actuator0_position_scaled();
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "scaled_pos",
                                 response::Z85Data((const uint8_t*) &pos, sizeof(pos)));
    return {};
}
response::Error act0_get_homing_speed(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act0.homing_speed;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "homing_speed",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act0_set_homing_speed(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0.homing_speed = v;
    return {};
}
response::Error act0_get_tune_pid_kp(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act0.pid_kp;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "tune_pid_kp",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act0_set_tune_pid_kp(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0.pid_kp = v;
    return {};
}
response::Error act0_get_tune_pid_ki(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act0.pid_ki;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "tune_pid_ki",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act0_set_tune_pid_ki(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0.pid_ki = v;
    return {};
}
response::Error act0_get_tune_pid_kd(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act0.pid_kd;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "tune_pid_kd",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act0_set_tune_pid_kd(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0.pid_kd = v;
    return {};
}
response::Error act0_get_tune_pid_bias(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act0.pid_bias;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "tune_pid_bias",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act0_set_tune_pid_bias(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0.pid_bias = v;
    return {};
}
response::Error act0_get_tune_pwm_deadband(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act0.pwm_deadband;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "tune_pwm_deadband",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act0_set_tune_pwm_deadband(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0.pwm_deadband = v;
    return {};
}
response::Error act0_get_axis_limit_min(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act0.axis_limits_min;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "axis_limit_min",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act0_set_axis_limit_min(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0.axis_limits_min = v;
    return {};
}
response::Error act0_get_axis_limit_max(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act0.axis_limits_max;
    TCODE_INSTANCE.send_response({response::CommandType::Linear, 0}, "axis_limit_max",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act0_set_axis_limit_max(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act0.axis_limits_max = v;
    return {};
}

response::Error act1_normal_update(fractional<uint32_t> value) {
    if (mmcc::actuator_task.is_pid_loop_enabled() && mmcc::actuator_task.is_act1_engaged()) {
        mmcc::actuator_task.actuate1(value.quotient());
    }
    return {};
}
response::Error act1_interval_update(fractional<uint32_t> value,
                                     request::IntervalData interval) {
    if (mmcc::actuator_task.is_pid_loop_enabled() && mmcc::actuator_task.is_act1_engaged()) {
        mmcc::actuator_task.actuate1(value.quotient(), interval.interval);
    }
    return {};
}
response::Error act1_stop() {
    mmcc::actuator_task.set_act1_engage(false);
    return {};
}
response::Error act1_get_engaged(const void*, size_t) {
    uint32_t status = mmcc::actuator_task.is_act1_engaged();
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "engaged",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error act1_set_engaged(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    mmcc::actuator_task.set_act1_engage(!!status);
    return {};
}
response::Error act1_get_engaged_break(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    uint32_t status = cfg->actuator.act1_disengage_break;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "engaged_break",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error act1_set_engaged_break(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1_disengage_break = !!status;
    return {};
}
response::Error act1_get_raw_pos(const void*, size_t) {
    auto raw = mmcc::actuator_task.get_actuator1_position_raw();
    auto ubjson =
         TCODE_INSTANCE.send_ubjson_response({response::CommandType::Rotate, 0}, "raw_pos");
    {
        auto root = ubjson.begin_array(codec::UBJsonType::Int16, 2);
        root.write((int16_t) raw.first);
        root.write((int16_t) raw.second);
    }
    return {};
}
response::Error act1_get_scaled_pos(const void*, size_t) {
    float pos = mmcc::actuator_task.get_actuator1_position_scaled();
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "scaled_pos",
                                 response::Z85Data((const uint8_t*) &pos, sizeof(pos)));
    return {};
}
response::Error act1_get_homing_speed(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act1.homing_speed;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "homing_speed",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act1_set_homing_speed(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1.homing_speed = v;
    return {};
}
response::Error act1_get_tune_pid_kp(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act1.pid_kp;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "tune_pid_kp",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act1_set_tune_pid_kp(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1.pid_kp = v;
    return {};
}
response::Error act1_get_tune_pid_ki(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act1.pid_ki;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "tune_pid_ki",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act1_set_tune_pid_ki(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1.pid_ki = v;
    return {};
}
response::Error act1_get_tune_pid_kd(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act1.pid_kd;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "tune_pid_kd",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act1_set_tune_pid_kd(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1.pid_kd = v;
    return {};
}
response::Error act1_get_tune_pid_bias(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act1.pid_bias;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "tune_pid_bias",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act1_set_tune_pid_bias(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1.pid_bias = v;
    return {};
}
response::Error act1_get_tune_pwm_deadband(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act1.pwm_deadband;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "tune_pwm_deadband",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act1_set_tune_pwm_deadband(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1.pwm_deadband = v;
    return {};
}
response::Error act1_get_axis_limit_min(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act1.axis_limits_min;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "axis_limit_min",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act1_set_axis_limit_min(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1.axis_limits_min = v;
    return {};
}
response::Error act1_get_axis_limit_max(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act1.axis_limits_max;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 0}, "axis_limit_max",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act1_set_axis_limit_max(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act1.axis_limits_max = v;
    return {};
}

response::Error act2_normal_update(fractional<uint32_t> value) {
    if (mmcc::actuator_task.is_pid_loop_enabled() && mmcc::actuator_task.is_act2_engaged()) {
        mmcc::actuator_task.actuate2(value.quotient());
    }
    return {};
}
response::Error act2_interval_update(fractional<uint32_t> value,
                                     request::IntervalData interval) {
    if (mmcc::actuator_task.is_pid_loop_enabled() && mmcc::actuator_task.is_act2_engaged()) {
        mmcc::actuator_task.actuate2(value.quotient(), interval.interval);
    }
    return {};
}
response::Error act2_stop() {
    mmcc::actuator_task.set_act2_engage(false);
    return {};
}
response::Error act2_get_engaged(const void*, size_t) {
    uint32_t status = mmcc::actuator_task.is_act2_engaged();
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "engaged",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error act2_set_engaged(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    mmcc::actuator_task.set_act2_engage(!!status);
    return {};
}
response::Error act2_get_engaged_break(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    uint32_t status = cfg->actuator.act2_disengage_break;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "engaged_break",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error act2_set_engaged_break(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2_disengage_break = !!status;
    return {};
}
response::Error act2_get_raw_pos(const void*, size_t) {
    auto raw = mmcc::actuator_task.get_actuator2_position_raw();
    auto ubjson =
         TCODE_INSTANCE.send_ubjson_response({response::CommandType::Rotate, 1}, "raw_pos");
    {
        auto root = ubjson.begin_array(codec::UBJsonType::Int16, 2);
        root.write((int16_t) raw.first);
        root.write((int16_t) raw.second);
    }
    return {};
}
response::Error act2_get_scaled_pos(const void*, size_t) {
    float pos = mmcc::actuator_task.get_actuator2_position_scaled();
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "scaled_pos",
                                 response::Z85Data((const uint8_t*) &pos, sizeof(pos)));
    return {};
}
response::Error act2_get_homing_speed(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act2.homing_speed;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "homing_speed",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act2_set_homing_speed(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2.homing_speed = v;
    return {};
}
response::Error act2_get_tune_pid_kp(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act2.pid_kp;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "tune_pid_kp",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act2_set_tune_pid_kp(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2.pid_kp = v;
    return {};
}
response::Error act2_get_tune_pid_ki(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act2.pid_ki;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "tune_pid_ki",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act2_set_tune_pid_ki(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2.pid_ki = v;
    return {};
}
response::Error act2_get_tune_pid_kd(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act2.pid_kd;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "tune_pid_kd",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act2_set_tune_pid_kd(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2.pid_kd = v;
    return {};
}
response::Error act2_get_tune_pid_bias(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act2.pid_bias;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "tune_pid_bias",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act2_set_tune_pid_bias(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2.pid_bias = v;
    return {};
}
response::Error act2_get_tune_pwm_deadband(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act2.pwm_deadband;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "tune_pwm_deadband",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act2_set_tune_pwm_deadband(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2.pwm_deadband = v;
    return {};
}
response::Error act2_get_axis_limit_min(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act2.axis_limits_min;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "axis_limit_min",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act2_set_axis_limit_min(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2.axis_limits_min = v;
    return {};
}
response::Error act2_get_axis_limit_max(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.act2.axis_limits_max;
    TCODE_INSTANCE.send_response({response::CommandType::Rotate, 1}, "axis_limit_max",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error act2_set_axis_limit_max(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.act2.axis_limits_max = v;
    return {};
}

response::Error aux_get_engaged(const void*, size_t) {
    uint32_t status = mmcc::actuator_task.is_aux_engaged();
    TCODE_INSTANCE.send_response({response::CommandType::Device, 5}, "aux_engaged",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error aux_set_engaged(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    mmcc::actuator_task.set_aux_engage(!!status);
    return {};
}

response::Error aux1_normal_update(fractional<uint32_t> value) {
    if (mmcc::actuator_task.is_aux_engaged()) {
        mmcc::actuator_task.set_aux1_pwm((value.quotient() - 0.5f) * 2.0f);
    }
    return {};
}
response::Error aux1_stop() {
    mmcc::actuator_task.set_aux1_pwm(0);
    return {};
}
response::Error aux1_get_tune_pwm_deadband(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.aux1.pwm_deadband;
    TCODE_INSTANCE.send_response({response::CommandType::Vibrate, 0}, "tune_pwm_deadband",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error aux1_set_tune_pwm_deadband(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.aux1.pwm_deadband = v;
    return {};
}
response::Error aux1_get_tune_pwm_bias(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.aux1.pwm_bias;
    TCODE_INSTANCE.send_response({response::CommandType::Vibrate, 0}, "tune_pwm_bias",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error aux1_set_tune_pwm_bias(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.aux1.pwm_bias = v;
    return {};
}
response::Error aux1_get_axis_limit_min(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.aux1.axis_limits_min;
    TCODE_INSTANCE.send_response({response::CommandType::Vibrate, 0}, "axis_limit_min",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error aux1_set_axis_limit_min(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.aux1.axis_limits_min = v;
    return {};
}
response::Error aux1_get_axis_limit_max(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.aux1.axis_limits_max;
    TCODE_INSTANCE.send_response({response::CommandType::Vibrate, 0}, "axis_limit_max",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error aux1_set_axis_limit_max(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.aux1.axis_limits_max = v;
    return {};
}

response::Error aux2_normal_update(fractional<uint32_t> value) {
    if (mmcc::actuator_task.is_aux_engaged()) {
        mmcc::actuator_task.set_aux2_pwm((value.quotient() - 0.5f) * 2.0f);
    }
    return {};
}
response::Error aux2_stop() {
    mmcc::actuator_task.set_aux2_pwm(0);
    return {};
}
response::Error aux2_get_tune_pwm_deadband(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.aux2.pwm_deadband;
    TCODE_INSTANCE.send_response({response::CommandType::Vibrate, 1}, "tune_pwm_deadband",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error aux2_set_tune_pwm_deadband(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.aux2.pwm_deadband = v;
    return {};
}
response::Error aux2_get_tune_pwm_bias(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.aux2.pwm_bias;
    TCODE_INSTANCE.send_response({response::CommandType::Vibrate, 1}, "tune_pwm_bias",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error aux2_set_tune_pwm_bias(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.aux2.pwm_bias = v;
    return {};
}
response::Error aux2_get_axis_limit_min(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.aux2.axis_limits_min;
    TCODE_INSTANCE.send_response({response::CommandType::Vibrate, 1}, "axis_limit_min",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error aux2_set_axis_limit_min(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.aux2.axis_limits_min = v;
    return {};
}
response::Error aux2_get_axis_limit_max(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    float v = cfg->actuator.aux2.axis_limits_max;
    TCODE_INSTANCE.send_response({response::CommandType::Vibrate, 1}, "axis_limit_max",
                                 response::Z85Data((const uint8_t*) &v, sizeof(v)));
    return {};
}
response::Error aux2_set_axis_limit_max(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    float v = *reinterpret_cast<float*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.aux2.axis_limits_max = v;
    return {};
}

response::Error stop_all() {
    mmcc::actuator_task.apply_emergency_stop();
    return {};
}

response::Error get_plot_enable(const void*, size_t) {
    auto cfg = mmcc::config.get_current_state();
    uint32_t status = !!cfg->actuator.plot_enable;
    TCODE_INSTANCE.send_response({response::CommandType::Device, 5}, "plot_enable",
                                 response::Z85Data((const uint8_t*) &status, sizeof(status)));
    return {};
}
response::Error set_plot_enable(void* dat, size_t n) {
    if (n != 4) {
        return response::Error(response::ErrorCode::Generic,
                               "Invalid data size for property set!");
    }
    uint32_t status = *reinterpret_cast<uint32_t*>(dat);
    auto cfg = mmcc::config.get_current_state();
    cfg->actuator.plot_enable = !!status;
    return {};
}
response::Error get_act0_plot(const void* ev_dat, size_t ev_n) {
    if (ev_dat != nullptr && ev_n == sizeof(ActPlotData)) {
        const ActPlotData* ev = reinterpret_cast<const ActPlotData*>(ev_dat);
        auto root =
             TCODE_INSTANCE.send_ubjson_response({response::CommandType::Linear, 0}, "plot");
        {
            auto dat = root.begin_array(codec::UBJsonType::Float, 6);
            dat.write(ev->time);
            dat.write(ev->pos);
            dat.write(ev->target);
            dat.write(ev->power);
            dat.write(ev->error);
            dat.write(ev->integral_term);
        }
    }
    return {};
}
response::Error get_act1_plot(const void* ev_dat, size_t ev_n) {
    if (ev_dat != nullptr && ev_n == sizeof(ActPlotData)) {
        const ActPlotData* ev = reinterpret_cast<const ActPlotData*>(ev_dat);
        auto root =
             TCODE_INSTANCE.send_ubjson_response({response::CommandType::Rotate, 0}, "plot");
        {
            auto dat = root.begin_array(codec::UBJsonType::Float, 6);
            dat.write(ev->time);
            dat.write(ev->pos);
            dat.write(ev->target);
            dat.write(ev->power);
            dat.write(ev->error);
            dat.write(ev->integral_term);
        }
    }
    return {};
}
response::Error get_act2_plot(const void* ev_dat, size_t ev_n) {
    if (ev_dat != nullptr && ev_n == sizeof(ActPlotData)) {
        const ActPlotData* ev = reinterpret_cast<const ActPlotData*>(ev_dat);
        auto root =
             TCODE_INSTANCE.send_ubjson_response({response::CommandType::Rotate, 1}, "plot");
        {
            auto dat = root.begin_array(codec::UBJsonType::Float, 6);
            dat.write(ev->time);
            dat.write(ev->pos);
            dat.write(ev->target);
            dat.write(ev->power);
            dat.write(ev->error);
            dat.write(ev->integral_term);
        }
    }
    return {};
}
void meta_act_plot(codec::UBJsonObject& root) {
    {
        auto obj = root["axis_mapping"].begin_object();
        {
            auto labels = obj["labels"].begin_array();
            labels.write("time");
            labels.write("pos");
            labels.write("target");
            labels.write("power");
            labels.write("error");
            labels.write("integral_term");
        }
    }
}

}  // namespace tcode::callbacks
