#ifndef MMCC_LFT_HPP
#define MMCC_LFT_HPP
/**
 * @file
 * @brief Stands for Low Frequency Task.
 * Manages RTC, watchdog and fault tracking.
 */

#include <interface/rtos/RTOSTask.hpp>
#include <utils/Class.hpp>

#include <chrono>
#include <cstddef>
#include <cstdint>

#include <boot.h>
#include <date/date.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/rtc.h>

extern "C" {
void _fault_hard(void);
void _fault_usage(void);
void _fault_bus(void);
void _fault_mem_manage(void);
}

namespace mmcc {

class LFTask : public rtos::StaticTask<256> {
   public:
    // TODO: enum class MonitoredTaskState : uint8_t { Unknown, Alive, Asleep };

    struct ResetReason {
        bool brownout : 1;
        bool pin : 1;
        bool power : 1;
        bool software : 1;
        bool independent_watchdog : 1;
        bool window_watchdog : 1;
        bool low_power : 1;

        constexpr ResetReason() :
            brownout(false), pin(false), power(false), software(false),
            independent_watchdog(false), window_watchdog(false), low_power(false) {}

        void update();
        void update(uint32_t rst_flags);

        void print();
    };

    enum class FaultSource : uint8_t {
        Unknown = 0,
        /* Arm faults */
        CoreHard,
        CoreUsage,
        CoreBus,
        CoreMemManage,
        /* Eabi */
        EabiAssert,
        EabiStackOverflow,
        /* libc/libc++ */
        LibAssert,
        LibExit,
        LibAbort,
        /* FreeRTOS */
        RtosAssert,
        RtosStackOverflow,
        NoFault = 15,
    };

    struct [[gnu::packed]] stacktrace_small_t {
        uint32_t frame_addr : 24;
        uint32_t return_addr : 24;

        void print() const;
    };

    struct [[gnu::packed]] stacktrace_minimal_t {
        uint32_t return_addr : 24;

        void print() const;
    };

    struct [[gnu::packed]] CommonFaultData {
        FaultSource source : 4;
    };

    /*
     * Register reference:
     * https://developer.arm.com/documentation/dui0552/a/cortex-m3-peripherals/system-control-block/configurable-fault-status-register?lang=en
     */
    struct [[gnu::packed]] CoreHardFaultData {
        FaultSource source : 4;
        /* Stack frame was in psp. */
        bool psp_frame : 1;
        bool debugevt : 1;
        bool forced : 1;
        bool has_usage : 1;
        bool has_bus : 1;
        bool has_mem_manage : 1;
        bool vecttbl : 1;
        bool __padding0 : 5;
        uint32_t faulty_pc : 24;
        uint32_t faulty_lr : 24;

        void print() const;
    };

    struct [[gnu::packed]] CoreUsageFaultData {
        FaultSource source : 4;
        /* Stack frame was in psp. */
        bool psp_frame : 1;
        bool divbyzero : 1;
        bool unaligned : 1;
        bool nocp : 1;
        bool invpc : 1;
        bool invstate : 1;
        bool undefinstr : 1;
        bool __padding0 : 5;
        uint32_t faulty_pc : 24;
        uint32_t faulty_lr : 24;

        void print() const;
    };

    struct [[gnu::packed]] CoreBusFaultData {
        FaultSource source : 4;
        /* Stack frame was in psp. */
        bool psp_frame : 1;
        bool bfarvalid : 1;
        bool lsperr : 1;
        bool stkerr : 1;
        bool unstkerr : 1;
        bool impreciserr : 1;
        bool preciserr : 1;
        bool ibuserr : 1;
        bool __padding0 : 4;
        uint32_t faulty_pc : 24;
        uint32_t faulty_lr : 24;
        uint32_t bfar;

        void print() const;
    };

    struct [[gnu::packed]] CoreMemManageFaultData {
        FaultSource source : 4;
        /* Stack frame was in psp. */
        bool psp_frame : 1;
        bool mmarvalid : 1;
        bool mlsperr : 1;
        bool mstkerr : 1;
        bool munstkerr : 1;
        bool daccviol : 1;
        bool iaccviol : 1;
        bool __padding0 : 5;
        uint32_t faulty_pc : 24;
        uint32_t faulty_lr : 24;
        uint32_t mmfar;

        void print() const;
    };

    enum class EabiAssertType : uint8_t {
        Invalid,
        PureVirtual,
        PureDeleted,
        Terminate,
        Unexpected
    };

    struct [[gnu::packed]] EabiAssertFaultData {
        FaultSource source : 4;
        EabiAssertType subtype : 4;
        stacktrace_minimal_t trace0;
        stacktrace_small_t trace1;
        stacktrace_small_t trace2;

        void print() const;
    };
    struct [[gnu::packed]] EabiStackOverflowFaultData {
        FaultSource source : 4;
        uint32_t __padding0 : 4;
        stacktrace_minimal_t trace0;
        stacktrace_small_t trace1;
        stacktrace_small_t trace2;

        void print() const;
    };

    struct [[gnu::packed]] LibAssertFaultData {
        FaultSource source : 4;
        /* Per-library definition */
        uint8_t library_id : 4;
        /* CRC-16/USB of relative file path */
        uint16_t file_id;
        /* Incrementing counter */
        uint16_t instance_id;
        stacktrace_minimal_t trace0;
        stacktrace_small_t trace1;

        void print() const;
    };
    struct [[gnu::packed]] LibExitFaultData {
        FaultSource source : 4;
        uint32_t __padding0 : 4;
        uint32_t __padding1 : 8;
        int32_t exit_code;
        stacktrace_minimal_t trace0;
        stacktrace_small_t trace1;

        void print() const;
    };
    struct [[gnu::packed]] LibAbortFaultData {
        FaultSource source : 4;
        uint32_t __padding0 : 4;
        stacktrace_minimal_t trace0;
        stacktrace_small_t trace1;
        stacktrace_small_t trace2;

        void print() const;
    };

    struct [[gnu::packed]] RtosAssertFaultData {
        FaultSource source : 4;
        RtosSourceFileIdentifier file_id : 4;
        uint16_t line;
        stacktrace_minimal_t trace0;
        stacktrace_small_t trace1;

        void print() const;
    };
    struct [[gnu::packed]] RtosStackOverflowFaultData {
        FaultSource source : 4;
        uint32_t __padding0 : 4;
        char task_name[configMAX_TASK_NAME_LEN];

        void print() const;
    };

    union [[gnu::packed]] FaultData {
        CommonFaultData common;

        CoreHardFaultData core_hard;
        CoreUsageFaultData core_usage;
        CoreBusFaultData core_bus;
        CoreMemManageFaultData core_mem_manage;

        EabiAssertFaultData eabi_assert;
        EabiStackOverflowFaultData eabi_stack_overflow;

        LibAssertFaultData lib_assert;
        LibExitFaultData lib_exit;
        LibAbortFaultData lib_abort;

        RtosAssertFaultData rtos_assert;
        RtosStackOverflowFaultData rtos_stack_overflow;

        constexpr FaultData() : common({.source = FaultSource::Unknown}) {}
        constexpr FaultData(FaultSource source) : common({.source = source}) {}

        constexpr FaultData(CoreHardFaultData core_hard_) : core_hard(core_hard_) {}
        constexpr FaultData(CoreUsageFaultData core_usage_) : core_usage(core_usage_) {}
        constexpr FaultData(CoreBusFaultData core_bus_) : core_bus(core_bus_) {}
        constexpr FaultData(CoreMemManageFaultData core_mem_manage_) :
            core_mem_manage(core_mem_manage_) {}

        constexpr FaultData(EabiAssertFaultData eabi_assert_) : eabi_assert(eabi_assert_) {}
        constexpr FaultData(EabiStackOverflowFaultData eabi_stack_overflow_) :
            eabi_stack_overflow(eabi_stack_overflow_) {}

        constexpr FaultData(LibAssertFaultData lib_assert_) : lib_assert(lib_assert_) {}
        constexpr FaultData(LibExitFaultData lib_exit_) : lib_exit(lib_exit_) {}
        constexpr FaultData(LibAbortFaultData lib_abort_) : lib_abort(lib_abort_) {}

        constexpr FaultData(RtosAssertFaultData rtos_assert_) : rtos_assert(rtos_assert_) {}
        constexpr FaultData(RtosStackOverflowFaultData rtos_stack_overflow_) :
            rtos_stack_overflow(rtos_stack_overflow_) {}

        static __attribute__((always_inline)) uint32_t encode_code_address(uint32_t reg_value) {
            return encode_address(reg_value ^ 0b1); /* Remove thumb bit first */
        }
        static __attribute__((always_inline)) uint32_t encode_address(uint32_t reg_value);
        static __attribute__((always_inline)) uint32_t decode_address(uint32_t compact);

        void print() const;

       private:
        static constexpr uint32_t SRAM_START = 0x20000000;
        static constexpr uint32_t FLASH_START = 0x08000000;
        static constexpr uint32_t ENC_ADDR_MASK = 0x003fffff;
        static constexpr uint32_t ENC_SRAM_BASE_FLAG = 0x00400000;
        static constexpr uint32_t ENC_OUT_OF_BOUNDS_FLAG = 0x00800000;
    };

    static_assert(sizeof(FaultData) <= 16, "FaultData structure has gotten too big!");

   protected:
    FaultData _previous_fault;
    ResetReason _reset_reason;

   public:
    constexpr LFTask() = default;
    cold_func_attr ~LFTask() = default;

    cold_func_attr void init();
    cold_func_attr bool start();
    void run();

    /**
     * TODO: RTC getters/setters
     * https://patchwork.kernel.org/project/linux-arm-kernel/patch/1484142403-11556-1-git-send-email-amelie.delaunay@st.com/
     */
    bool has_time();
    bool set_time(int64_t ms_since_unix_epoch);

    /**
     * Returns the milliseconds elapsed since the Unix epoch.
     */
    int64_t get_unix_time();
    date::hh_mm_ss<std::chrono::milliseconds> get_time();
    std::pair<date::year_month_day, date::weekday> get_date();
    std::tuple<date::year_month_day, date::weekday, date::hh_mm_ss<std::chrono::milliseconds>>
    get_date_time();

   private:
    void _init_faults();
    void _init_prg_opts();
    void _init_rtc();
    void _init_wd();
};

static LFTask::FaultData* const BACKUP_FAULT_DATA = (LFTask::FaultData*) RTC_BKP_BASE;
static uint32_t* const BACKUP_USER_DATA = (uint32_t*) (RTC_BKP_BASE + 16);
static constexpr size_t BACKUP_USER_DATA_SIZE = 64;

inline __attribute__((always_inline)) void set_backup_fault_data(LFTask::FaultData dat) {
    pwr_disable_backup_domain_write_protect();
    *BACKUP_FAULT_DATA = dat;
    pwr_enable_backup_domain_write_protect();
}

inline uint32_t LFTask::FaultData::encode_address(uint32_t reg_value) {
    if (reg_value >= SRAM_START) {
        uint32_t offset = reg_value - SRAM_START;
        bool out_of_bounds = offset > ENC_ADDR_MASK;
        return (out_of_bounds ? ENC_OUT_OF_BOUNDS_FLAG : 0) | ENC_SRAM_BASE_FLAG |
               (offset & ENC_ADDR_MASK);
    }
    else if (reg_value >= FLASH_START) {
        uint32_t offset = reg_value - FLASH_START;
        bool out_of_bounds = offset > ENC_ADDR_MASK;
        return (out_of_bounds ? ENC_OUT_OF_BOUNDS_FLAG : 0) | (offset & ENC_ADDR_MASK);
    }
    else {
        return ENC_OUT_OF_BOUNDS_FLAG | 0b1;
    }
}

inline uint32_t LFTask::FaultData::decode_address(uint32_t compact) {
    // Querry flags.
    bool out_of_bounds = !!(compact & ENC_OUT_OF_BOUNDS_FLAG);
    bool sram_base = !!(compact & ENC_SRAM_BASE_FLAG);
    // Clear flags.
    compact &= ~(ENC_OUT_OF_BOUNDS_FLAG | ENC_SRAM_BASE_FLAG);
    if (out_of_bounds) {
        return 0x0;
    }
    else if (sram_base) {
        return SRAM_START + compact;
    }
    else {
        return FLASH_START + compact;
    }
}

}  // namespace mmcc

#endif /*MMCC_LFT_HPP*/