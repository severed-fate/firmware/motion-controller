#ifndef MMCC_TCODE_HPP
#define MMCC_TCODE_HPP

#include "utils/Flags.hpp"

#include <interface/tcode/Messages.hpp>
#include <interface/tcode/UBJsonBuilder.hpp>
#include <interface/tcode/Utils.hpp>

#include <cstddef>
#include <cstdint>

#include <etl/delegate.h>
#include <etl/string_view.h>

namespace tcode {

namespace config {

struct PropertyKey {
    common::CommandIndex cmd_idx;
    etl::string_view name;

    constexpr PropertyKey() = default;
    constexpr PropertyKey(common::CommandIndex cmd_idx_, etl::string_view name_) :
        cmd_idx(cmd_idx_), name(name_) {}
    constexpr PropertyKey(common::CommandType cmd_, int8_t idx_, etl::string_view name_) :
        cmd_idx(cmd_, idx_), name(name_) {}
    constexpr PropertyKey(common::CommandIndex cmd_idx_, request::PropertyData name_) :
        cmd_idx(cmd_idx_), name(name_.name_begin, name_.name_end) {}

    constexpr bool operator<(const PropertyKey& rhs) const {
        return cmd_idx < rhs.cmd_idx || (!(rhs.cmd_idx < cmd_idx) && name < rhs.name);
    }
    constexpr bool operator==(const PropertyKey& rhs) const {
        return cmd_idx == rhs.cmd_idx && name == rhs.name;
    }

    constexpr size_t hash() const {
        constexpr auto prime = mapbox::eternal::impl::hash_prime;
        size_t value = cmd_idx.hash();
        for (char c : name) {
            value = (value ^ static_cast<size_t>(c)) * prime;
        }
        return value;
    }
};

}  // namespace config

namespace callbacks {

class AxisUpdate {
   public:
    using update_delegate = etl::delegate<response::Error(fractional<uint32_t> value)>;
    using update_interval_delegate = etl::delegate<response::Error(
         fractional<uint32_t> value, request::IntervalData interval)>;
    using update_speed_delegate =
         etl::delegate<response::Error(fractional<uint32_t> value, request::SpeedData speed)>;
    using stop_delegate = etl::delegate<response::Error()>;

   protected:
    update_delegate _update_cb;
    update_interval_delegate _update_interval_cb;
    update_speed_delegate _update_speed_cb;
    stop_delegate _stop_cb;

   public:
    constexpr AxisUpdate(update_delegate update_cb, update_interval_delegate update_interval_cb,
                         update_speed_delegate update_speed_cb, stop_delegate stop_cb) :
        _update_cb(update_cb),
        _update_interval_cb(update_interval_cb), _update_speed_cb(update_speed_cb),
        _stop_cb(stop_cb) {}

    constexpr bool has_update_callback() const {
        return _update_cb.is_valid();
    }
    constexpr bool has_update_interval_callback() const {
        return _update_interval_cb.is_valid();
    }
    constexpr bool has_update_speed_callback() const {
        return _update_speed_cb.is_valid();
    }
    constexpr bool has_stop_callback() const {
        return _stop_cb.is_valid();
    }

    response::Error operator()(fractional<uint32_t> value) const;
    response::Error operator()(fractional<uint32_t> value,
                               request::IntervalData interval) const;
    response::Error operator()(fractional<uint32_t> value, request::SpeedData speed) const;
    response::Error stop() const;
};

class Command {
   public:
    using delegate = etl::delegate<response::Error()>;

   protected:
    delegate _cb;
    etl::string_view _description;

   public:
    template<std::size_t N>
    constexpr Command(delegate cb, const char (&description)[N]) :
        _cb(cb), _description(description, N - 1) {}
    constexpr Command(delegate cb, std::nullptr_t) : _cb(cb), _description() {}

    constexpr bool has_callback() const {
        return _cb.is_valid();
    }
    constexpr bool has_description() const {
        return !_description.empty();
    }
    constexpr etl::string_view get_description() const {
        return _description;
    }
    response::Error operator()() const;
};

class Property {
   public:
    using getter_delegate =
         etl::delegate<response::Error(const void* event_data /* = nullptr */, size_t n)>;
    using setter_delegate = etl::delegate<response::Error(void* dat, size_t n)>;
    using append_metadata_delegate = etl::delegate<void(codec::UBJsonObject&)>;

    enum class Type : char {
        UInt32 = 'u',
        Int32 = 'i',
        UInt64 = 'U',
        Int64 = 'I',
        FP32 = 'F',
        FP64 = 'D',
        String = 'S',
        UBJson = 'O'
    };

    enum class FlagBits : uint8_t {
        /**
         * Device informs host of something.
         * Flag sequence string character: e.
         */
        Event = 0x1 << 0,
        /**
         * Host triggers something on the device.
         * Flag sequence string character: a.
         */
        Action = 0x1 << 1,
        /**
         * Data interpretation hint:
         *  - data represents an integer acting as an enum value,
         *  - and interpreted based on the specified `enum_mapping` metadata.
         *
         *  The `enum_mapping` metadata is always required, and is formatted as:
         *  {
         *      keys: [<index_to_enum_values>],
         *      labels: [<index_to_enum_names>]
         *  }
         *
         * Flag sequence string character: n.
         */
        Enum = 0x1 << 3,
        /**
         * Data interpretation hint for specialized enums,
         * where the possible values/states are false(0) and true(1).
         * The `enum_mapping` metadata is ignored.
         *
         * Flag sequence string character: l.
         */
        Boolean = 0x1 << 4,
        /**
         * Data interpretation hint similar to Enum,
         * but supports selecting multiple options.
         * Enum values are treated as bit indexes.
         *
         * Flag sequence string character: f.
         */
        Bitfield = 0x1 << 5,
        /**
         * Data interpretation hint:
         *  - ubjson array where each index maps to an axis,
         *  - required metadata `axis_mapping`: mapping between axis indexes and labels.
         *  - Axis are indexed sequentially, with index 0 required and always being the x-axis.
         *
         * Flag sequence string character: o.
         */
        Observations = 0x1 << 6,
    };

    enum class DisplayType : uint8_t {
        /**
         * Allow the host to automatically decide how to present the data.
         */
        Default = 0,

        /* common */
        TextBox = 1,
        DragBox = 2,

        /* button */
        PressButton = 11,
        ToggleButton = 12,
        CheckboxButton = 13,

        /* enum and derivatives */
        RadioButton = 21,
        ComboBox = 22,
        SliderBox = 23,
        ListBox = 24,

        /* special purpose */
        Plot = 91
    };

    enum class DisplayFlagBits : uint8_t {
        /* reserved */
    };

    using Flags = meta::Flags<FlagBits>;
    using DisplayFlags = meta::Flags<DisplayFlagBits>;

   protected:
    /* Metadata */
    Type _type;
    Flags _flags = {};
    DisplayType _disp_type = DisplayType::Default;
    DisplayFlags _disp_flags = {};
    const char* _description = nullptr;
    /* Callbacks */
    getter_delegate _getter_cb;
    setter_delegate _setter_cb;
    append_metadata_delegate _append_metadata_cb;

   public:
    constexpr Property(Type type, getter_delegate getter_cb, setter_delegate setter_cb = {},
                       append_metadata_delegate append_metadata_cb = {}) :
        _type(type),
        _description(nullptr), _getter_cb(getter_cb), _setter_cb(setter_cb),
        _append_metadata_cb(append_metadata_cb) {}
    constexpr Property(Type type, const char* description, getter_delegate getter_cb,
                       setter_delegate setter_cb = {},
                       append_metadata_delegate append_metadata_cb = {}) :
        _type(type),
        _description(description), _getter_cb(getter_cb), _setter_cb(setter_cb),
        _append_metadata_cb(append_metadata_cb) {}
    constexpr Property(Type type, Flags flags, getter_delegate getter_cb,
                       setter_delegate setter_cb = {},
                       append_metadata_delegate append_metadata_cb = {}) :
        _type(type),
        _flags(flags), _description(nullptr), _getter_cb(getter_cb), _setter_cb(setter_cb),
        _append_metadata_cb(append_metadata_cb) {}
    constexpr Property(Type type, Flags flags, const char* description,
                       getter_delegate getter_cb, setter_delegate setter_cb = {},
                       append_metadata_delegate append_metadata_cb = {}) :
        _type(type),
        _flags(flags), _description(description), _getter_cb(getter_cb), _setter_cb(setter_cb),
        _append_metadata_cb(append_metadata_cb) {}
    constexpr Property(Type type, DisplayType disp_type, getter_delegate getter_cb,
                       setter_delegate setter_cb = {},
                       append_metadata_delegate append_metadata_cb = {}) :
        _type(type),
        _disp_type(disp_type), _description(nullptr), _getter_cb(getter_cb),
        _setter_cb(setter_cb), _append_metadata_cb(append_metadata_cb) {}
    constexpr Property(Type type, DisplayType disp_type, const char* description,
                       getter_delegate getter_cb, setter_delegate setter_cb = {},
                       append_metadata_delegate append_metadata_cb = {}) :
        _type(type),
        _disp_type(disp_type), _description(description), _getter_cb(getter_cb),
        _setter_cb(setter_cb), _append_metadata_cb(append_metadata_cb) {}
    constexpr Property(Type type, Flags flags, DisplayType disp_type, getter_delegate getter_cb,
                       setter_delegate setter_cb = {},
                       append_metadata_delegate append_metadata_cb = {}) :
        _type(type),
        _flags(flags), _disp_type(disp_type), _description(nullptr), _getter_cb(getter_cb),
        _setter_cb(setter_cb), _append_metadata_cb(append_metadata_cb) {}
    constexpr Property(Type type, Flags flags, DisplayType disp_type, const char* description,
                       getter_delegate getter_cb, setter_delegate setter_cb = {},
                       append_metadata_delegate append_metadata_cb = {}) :
        _type(type),
        _flags(flags), _disp_type(disp_type), _description(description), _getter_cb(getter_cb),
        _setter_cb(setter_cb), _append_metadata_cb(append_metadata_cb) {}

    constexpr Type get_type() const {
        return _type;
    }
    constexpr Flags get_flags() const {
        return _flags;
    }
    constexpr DisplayType get_display_type() const {
        return _disp_type;
    }
    constexpr bool has_getter() const {
        return _getter_cb.is_valid();
    }
    constexpr bool has_setter() const {
        return _setter_cb.is_valid();
    }

    response::Error get() const;
    response::Error event(const void* event_data, size_t n) const;
    response::Error set(void* dat, size_t n) const;
    void append_metadata(codec::UBJsonObject& obj) const;
};

response::Error noop();

response::Error identify_protocol();
response::Error get_protocol_name(const void*, size_t);
response::Error get_protocol_version(const void*, size_t);

response::Error build_enumeration();

response::Error stop_all();

}  // namespace callbacks

}  // namespace tcode

namespace meta {

template<> struct FlagTraits<tcode::callbacks::Property::FlagBits> {
    static constexpr bool isBitmask = true;
    static constexpr tcode::callbacks::Property::Flags allFlags =
         tcode::callbacks::Property::FlagBits::Event |
         tcode::callbacks::Property::FlagBits::Action |
         tcode::callbacks::Property::FlagBits::Enum |
         tcode::callbacks::Property::FlagBits::Boolean |
         tcode::callbacks::Property::FlagBits::Bitfield |
         tcode::callbacks::Property::FlagBits::Observations;
};

template<> struct FlagTraits<tcode::callbacks::Property::DisplayFlagBits> {
    static constexpr bool isBitmask = true;
    static constexpr tcode::callbacks::Property::DisplayFlags allFlags = {};
};

}  // namespace meta

namespace std {

template<> struct hash<::tcode::config::PropertyKey> {
    constexpr size_t operator()(const ::tcode::config::PropertyKey& prop_key) const {
        return prop_key.hash();
    }
};

}  // namespace std

#endif /*MMCC_TCODE_HPP*/
