#include "LFT.hpp"

#include <Objects.hpp>
#include <interface/io/Core.hpp>
#include <interface/rtos/RTOS.hpp>

#include <chrono>
#include <cstdlib>
#include <valarray>

#include <boot.h>
#include <date/date.h>
#include <etl/absolute.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/iwdg.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/rtc.h>
#include <utils/macros.h>

namespace mmcc {

void LFTask::init() {
    _init_wd();
    _init_faults();
    _init_prg_opts();
    _init_rtc();
}

bool LFTask::start() {
    return rtos::StaticTask<256>::start<LFTask, &LFTask::run>("wdg", 1);
}

void LFTask::run() {
    while (true) {
        iwdg_reset();
        delay_ticks(rtos::ms2ticks(333));
    }
}

cold_func_attr void LFTask::_init_faults() {
    // Record previous reset/error/fault information.
    _reset_reason.update();
    _reset_reason.print();
    _previous_fault = *BACKUP_FAULT_DATA;
    constexpr FaultData DEFAULT_FAULT_DATA(FaultSource::NoFault);
    set_backup_fault_data(DEFAULT_FAULT_DATA);
    _previous_fault.print();
    // Configure fault handlers.
    mmcc::disable_faults();
    SCB_CCR |= SCB_CCR_DIV_0_TRP;
    SCB_SHCSR |= (SCB_SHCSR_USGFAULTENA | SCB_SHCSR_BUSFAULTENA | SCB_SHCSR_MEMFAULTENA);
    runtime_vector_table.hard_fault = _fault_hard;
    runtime_vector_table.usage_fault = _fault_usage;
    runtime_vector_table.bus_fault = _fault_bus;
    runtime_vector_table.memory_manage_fault = _fault_mem_manage;
    mmcc::enable_faults();
}

cold_func_attr void LFTask::_init_prg_opts() {
    // Update flash option bytes.
    InterruptGuard<> _ig;
    constexpr uint32_t flash_opt_mask = FLASH_OPTCR_NRST_STDBY | FLASH_OPTCR_NRST_STOP |
                                        FLASH_OPTCR_WDG_SW | FLASH_OPTCR_BOR_OFF;
    constexpr uint32_t flash_opt_value =
         FLASH_OPTCR_NRST_STDBY | FLASH_OPTCR_NRST_STOP | FLASH_OPTCR_WDG_SW |
         FLASH_OPTCR_BOR_LEVEL_3 /* Ensures minimum 2.8V for x32 flash programming */
         ;
    static_assert((flash_opt_value & (~flash_opt_mask)) == 0,
                  "flash_opt_value sets bits which are not defined in flash_opt_mask!");
    if ((FLASH_OPTCR & flash_opt_mask) != flash_opt_value) {
        dbg.print("Programming option bytes...\n");
        uint32_t flash_opt_retain_value = FLASH_OPTCR & (~flash_opt_mask);
        flash_program_option_bytes(flash_opt_retain_value | flash_opt_value);
    }
}
// Ensure that the RTC get clocked.
cold_func_attr void LFTask::_init_rtc() {
    pwr_disable_backup_domain_write_protect();
    if (!(RCC_BDCR & RCC_BDCR_RTCEN)) {
        dbg.print("Initializing RTC...\n");
        rcc_osc_on(RCC_LSE);
        while (!rcc_is_osc_ready(RCC_LSE))
            nop;
        // Enable RTC clocking.
        RCC_BDCR &= ~(RCC_BDCR_RTCSEL_MASK << RCC_BDCR_RTCSEL_SHIFT);
        RCC_BDCR |= RCC_BDCR_RTCSEL_LSE << RCC_BDCR_RTCSEL_SHIFT;
        rcc_periph_clock_enable(RCC_RTC);
    }
    else { /* Synchronize with clock */
    }
    pwr_enable_backup_domain_write_protect();
}
cold_func_attr void LFTask::_init_wd() {
    // Configure the watchdog.
    while (!rcc_is_osc_ready(RCC_LSI))
        nop;
    iwdg_start();
    while (iwdg_prescaler_busy())
        ;
    IWDG_KR = IWDG_KR_UNLOCK;
    IWDG_PR = IWDG_PR_DIV64;
    while (iwdg_reload_busy())
        ;
    IWDG_KR = IWDG_KR_UNLOCK;
    IWDG_RLR = 0x5db;
    iwdg_reset();
}

static constexpr uint16_t _rtc_sync_div = 0x3ff;
static constexpr uint16_t _rtc_async_div = (32768 / (_rtc_sync_div + 1)) - 1;
static constexpr uint32_t _rtc_dec_to_bcd(uint8_t dec) {
    return ((dec / 10) << 4) | (dec % 10);
}
static constexpr uint8_t _rtc_bcd_to_dec(uint8_t bcd) {
    return (((bcd & 0xf0) >> 4) * 10) + (bcd & 0x0f);
}
[[maybe_unused]] static constexpr int16_t _rtc_ms_to_ss(int16_t ms) {
    return _rtc_sync_div - ((ms * (_rtc_sync_div + 1)) / 1000);
}
static constexpr int16_t _rtc_ss_to_ms(int16_t ss) {
    return ((_rtc_sync_div - ss) * 1000) / (_rtc_sync_div + 1);
}

static void _rtc_sync() {
    RTC_ISR &= ~(RTC_ISR_RSF);

    while (!(RTC_ISR & RTC_ISR_RSF))
        nop3;
}

static constexpr uint32_t _rtc_bcd_year_mask =
     RTC_DR_YT_MASK << RTC_DR_YT_SHIFT | RTC_DR_YU_MASK << RTC_DR_YU_SHIFT;
static constexpr uint32_t _rtc_bcd_wday_mask = RTC_DR_WDU_MASK << RTC_DR_WDU_SHIFT;
static constexpr uint32_t _rtc_bcd_month_mask =
     RTC_DR_MT_MASK << RTC_DR_MT_SHIFT | RTC_DR_MU_MASK << RTC_DR_MU_SHIFT;
static constexpr uint32_t _rtc_bcd_day_mask =
     RTC_DR_DT_MASK << RTC_DR_DT_SHIFT | RTC_DR_DU_MASK << RTC_DR_DU_SHIFT;
static constexpr uint32_t _rtc_bcd_hour_mask =
     RTC_TR_HT_MASK << RTC_TR_HT_SHIFT | RTC_TR_HU_MASK << RTC_TR_HU_SHIFT;
static constexpr uint32_t _rtc_bcd_minutes_mask =
     RTC_TR_MNT_MASK << RTC_TR_MNT_SHIFT | RTC_TR_MNU_MASK << RTC_TR_MNU_SHIFT;
static constexpr uint32_t _rtc_bcd_seconds_mask =
     RTC_TR_ST_MASK << RTC_TR_ST_SHIFT | RTC_TR_SU_MASK << RTC_TR_SU_SHIFT;

bool LFTask::has_time() {
    return RTC_ISR & RTC_ISR_INITS;
}
bool LFTask::set_time(int64_t ms_since_unix_epoch) {
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;

    milliseconds unix_time(ms_since_unix_epoch);

    // Split into days count and remainder time.
    date::sys_days unix_days(duration_cast<date::days>(unix_time));
    auto unix_days_ms = duration_cast<milliseconds>(unix_days.time_since_epoch());
    milliseconds daytime(unix_time - unix_days_ms);

    // Split epoch into calendar values.
    date::year_month_day date(unix_days);
    date::weekday wdate(unix_days);
    date::hh_mm_ss<milliseconds> time(daytime);

    if (!date.ok() || !wdate.ok() ||
        !(time.hours().count() >= 0 && time.hours().count() < 24) ||
        !(time.minutes().count() >= 0 && time.minutes().count() < 60) ||
        !(time.seconds().count() >= 0 && time.seconds().count() < 60) ||
        !(time.subseconds().count() >= 0 && time.subseconds().count() < 1000)) {
        return false;
    }

    uint8_t rtc_year = static_cast<int>(date.year()) % 100;
    uint8_t rtc_month = static_cast<unsigned>(date.month());
    uint8_t rtc_day = static_cast<unsigned>(date.day());
    rtc_weekday rtc_wdate;
    switch (wdate.c_encoding()) {
        case 0: {
            rtc_wdate = RTC_DR_WDU_SUN;
        } break;
        case 1: {
            rtc_wdate = RTC_DR_WDU_MON;
        } break;
        case 2: {
            rtc_wdate = RTC_DR_WDU_TUE;
        } break;
        case 3: {
            rtc_wdate = RTC_DR_WDU_WED;
        } break;
        case 4: {
            rtc_wdate = RTC_DR_WDU_THU;
        } break;
        case 5: {
            rtc_wdate = RTC_DR_WDU_FRI;
        } break;
        case 6: {
            rtc_wdate = RTC_DR_WDU_SAT;
        } break;
        default: {
            // Invalid date::weekdate index.
            std::abort();
        } break;
    }

    uint8_t rtc_hour = time.hours().count();
    uint8_t rtc_minutes = time.minutes().count();
    uint8_t rtc_seconds = time.seconds().count();
    uint16_t rtc_milliseconds = time.subseconds().count();

    uint32_t bcd_year = _rtc_dec_to_bcd(rtc_year) << RTC_DR_YU_SHIFT;
    uint32_t bcd_wday = _rtc_dec_to_bcd(rtc_wdate) << RTC_DR_WDU_SHIFT;
    uint32_t bcd_month = _rtc_dec_to_bcd(rtc_month) << RTC_DR_MU_SHIFT;
    uint32_t bcd_day = _rtc_dec_to_bcd(rtc_day) << RTC_DR_DU_SHIFT;

    uint32_t bcd_hour = _rtc_dec_to_bcd(rtc_hour) << RTC_TR_HU_SHIFT;
    uint32_t bcd_minutes = _rtc_dec_to_bcd(rtc_minutes) << RTC_TR_MNU_SHIFT;
    uint32_t bcd_seconds = _rtc_dec_to_bcd(rtc_seconds) << RTC_TR_SU_SHIFT;

    rtos::Kernel::enter_critical();
    pwr_disable_backup_domain_write_protect();
    rtc_unlock();
    if (!has_time()) {
        // Initialize calendar.
        rtc_set_init_flag();
        while (!rtc_init_flag_is_ready())
            nop3;
        rtc_set_prescaler(_rtc_sync_div, _rtc_async_div);
        // Apply crystal calibration. TODO: Make it per-board configurable.
        // Reference is a ahead by 99348 ms.
        // Measured interval of 606696924 ms.
        // Aka the real world interval is 606597576 ms.
        constexpr uint16_t RTC_CALR_CALM = 172;
        RTC_CALR = (RTC_CALR_CALM & RTC_CALR_CALM_MASK) << RTC_CALR_CALM_SHIFT;
        // AM format == 24-hour clock.
        rtc_set_am_format();
        RTC_TR = bcd_seconds | bcd_minutes | bcd_hour;
        RTC_DR = bcd_day | bcd_month | bcd_wday | bcd_year;
        rtc_clear_init_flag();
    }
    else {
        // Update calendar.
        bool do_finetune = true;
        // Synchronize shadow registers.
        _rtc_sync();
        // Acquire current calendar values.
        uint32_t ssr = RTC_SSR;
        uint32_t tr = RTC_TR;
        uint32_t dr = RTC_DR;
        auto rtc_cur_sec = _rtc_bcd_to_dec((tr & _rtc_bcd_seconds_mask) >> RTC_TR_SU_SHIFT);
        int rtc_cur_ms = rtc_cur_sec * 1000 + _rtc_ss_to_ms(ssr);
        int rtc_tgt_ms = rtc_seconds * 1000 + rtc_milliseconds;
        // Apply time to calendar values.
        if ((dr & _rtc_bcd_year_mask) != bcd_year) {
            do_finetune = false;
            dr &= ~_rtc_bcd_year_mask;
            dr |= bcd_year;
        }
        if ((dr & _rtc_bcd_wday_mask) != bcd_wday) {
            do_finetune = false;
            dr &= ~_rtc_bcd_wday_mask;
            dr |= bcd_wday;
        }
        if ((dr & _rtc_bcd_month_mask) != bcd_month) {
            do_finetune = false;
            dr &= ~_rtc_bcd_month_mask;
            dr |= bcd_month;
        }
        if ((dr & _rtc_bcd_day_mask) != bcd_day) {
            do_finetune = false;
            dr &= ~_rtc_bcd_day_mask;
            dr |= bcd_day;
        }
        if ((tr & _rtc_bcd_hour_mask) != bcd_hour) {
            do_finetune = false;
            tr &= ~_rtc_bcd_hour_mask;
            tr |= bcd_hour;
        }
        if ((tr & _rtc_bcd_minutes_mask) != bcd_minutes) {
            do_finetune = false;
            tr &= ~_rtc_bcd_minutes_mask;
            tr |= bcd_minutes;
        }
        if ((tr & _rtc_bcd_seconds_mask) != bcd_seconds) {
            if (!do_finetune || etl::absolute(rtc_tgt_ms - rtc_cur_ms) >= 995) {
                do_finetune = false;
                tr &= ~_rtc_bcd_seconds_mask;
                tr |= bcd_seconds;
            }
        }
        // Modify RTC state.
        if (!do_finetune) {
            // Clock is offset by more than one second - reinit calendar.
            rtc_set_init_flag();
            while (!rtc_init_flag_is_ready())
                nop3;
            RTC_TR = tr;
            RTC_DR = dr;
            rtc_clear_init_flag();
        }
        else if (!(RTC_ISR & RTC_ISR_SHPF)) {
            // Apply subsecond correction.
            int rtc_err_ms = rtc_tgt_ms - rtc_cur_ms;
            if (rtc_err_ms >= 3) {
                // RTC is subseconds behind - advance.
                uint16_t ssub = ((1000 - rtc_err_ms) * (_rtc_sync_div + 1)) / 1000;
                RTC_SHIFTR = RTC_SHIFTR_ADD1S | (ssub & RTC_SHIFTR_SUBFS_MASK);
            }
            else if (rtc_err_ms <= -3) {
                // RTC is subseconds ahead - delay.
                uint16_t ssub = ((-rtc_err_ms) * (_rtc_sync_div + 1)) / 1000;
                RTC_SHIFTR = ssub & RTC_SHIFTR_SUBFS_MASK;
            }
        }
    }
    rtc_lock();
    pwr_enable_backup_domain_write_protect();
    rtos::Kernel::exit_critical();

    return true;
}

std::tuple<date::year_month_day, date::weekday, date::hh_mm_ss<std::chrono::milliseconds>>
LFTask::get_date_time() {
    if (!has_time()) {
        return {};
    }

    rtos::Kernel::enter_critical();
    pwr_disable_backup_domain_write_protect();
    rtc_unlock();
    // Wait for updated calendar shadow registers.
    _rtc_sync();
    // Read the calendar registers. The order is important.
    uint32_t ssr = RTC_SSR;
    uint32_t tr = RTC_TR;
    uint32_t dr = RTC_DR;
    rtc_lock();
    pwr_enable_backup_domain_write_protect();
    rtos::Kernel::exit_critical();

    using std::chrono::duration_cast;
    using std::chrono::milliseconds;

    // Convert
    date::year date_year(2000 + _rtc_bcd_to_dec((dr & _rtc_bcd_year_mask) >> RTC_DR_YU_SHIFT));
    date::month date_month(_rtc_bcd_to_dec((dr & _rtc_bcd_month_mask) >> RTC_DR_MU_SHIFT));
    date::day date_day(_rtc_bcd_to_dec((dr & _rtc_bcd_day_mask) >> RTC_DR_DU_SHIFT));
    date::year_month_day date(date_year, date_month, date_day);

    rtc_weekday rtc_wdate =
         (rtc_weekday) _rtc_bcd_to_dec((dr & _rtc_bcd_wday_mask) >> RTC_DR_WDU_SHIFT);
    date::weekday wday;
    switch (rtc_wdate) {
        case RTC_DR_WDU_SUN: {
            wday = date::weekday(0);
        } break;
        case RTC_DR_WDU_MON: {
            wday = date::weekday(1);
        } break;
        case RTC_DR_WDU_TUE: {
            wday = date::weekday(2);
        } break;
        case RTC_DR_WDU_WED: {
            wday = date::weekday(3);
        } break;
        case RTC_DR_WDU_THU: {
            wday = date::weekday(4);
        } break;
        case RTC_DR_WDU_FRI: {
            wday = date::weekday(5);
        } break;
        case RTC_DR_WDU_SAT: {
            wday = date::weekday(6);
        } break;
        default: {
            // Invalid rtc_weekday enum.
            std::abort();
        } break;
    }

    std::chrono::hours hours(_rtc_bcd_to_dec((tr & _rtc_bcd_hour_mask) >> RTC_TR_HU_SHIFT));
    std::chrono::minutes minutes(
         _rtc_bcd_to_dec((tr & _rtc_bcd_minutes_mask) >> RTC_TR_MNU_SHIFT));
    std::chrono::seconds seconds(
         _rtc_bcd_to_dec((tr & _rtc_bcd_seconds_mask) >> RTC_TR_SU_SHIFT));
    milliseconds subseconds(_rtc_ss_to_ms(ssr));

    date::hh_mm_ss<milliseconds> time(duration_cast<milliseconds>(hours) +
                                      duration_cast<milliseconds>(minutes) +
                                      duration_cast<milliseconds>(seconds) + subseconds);

    return {date, wday, time};
}

int64_t LFTask::get_unix_time() {
    auto [date, wday, time] = get_date_time();
    auto days = static_cast<date::sys_days>(date).time_since_epoch();
    std::chrono::milliseconds days_ms =
         std::chrono::duration_cast<std::chrono::milliseconds>(days);
    std::chrono::milliseconds day_time_ms = time.to_duration();
    return days_ms.count() + day_time_ms.count();
}
date::hh_mm_ss<std::chrono::milliseconds> LFTask::get_time() {
    auto [date, wday, time] = get_date_time();
    return time;
}
std::pair<date::year_month_day, date::weekday> LFTask::get_date() {
    auto [date, wday, time] = get_date_time();
    return {date, wday};
}

void LFTask::ResetReason::update() {
    uint32_t csr = RCC_CSR;
    update(csr);
    RCC_CSR = csr | RCC_CSR_RMVF;
}
void LFTask::ResetReason::update(uint32_t rst_flags) {
    brownout = rst_flags & RCC_CSR_BORRSTF;
    pin = rst_flags & RCC_CSR_PINRSTF;
    power = rst_flags & RCC_CSR_PORRSTF;
    software = rst_flags & RCC_CSR_SFTRSTF;
    independent_watchdog = rst_flags & RCC_CSR_IWDGRSTF;
    window_watchdog = rst_flags & RCC_CSR_WWDGRSTF;
    low_power = rst_flags & RCC_CSR_LPWRRSTF;
}

}  // namespace mmcc
