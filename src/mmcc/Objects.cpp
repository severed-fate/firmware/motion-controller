#include "Objects.hpp"
#include "Sonar.hpp"

#include <mmcc/board/Config.hpp>

#include <boot.h>

namespace mmcc {

/**
 * TODO:
 * Compile this file with -Os.
 * Move usb metadata to static const storage.
 */

DebugPrinter dbg;
Config config;
LFTask lftask;
USBTask usb_task;
TrafficController trfctl_task;
Actuator actuator_task;
SonarResolverTask sonar_task;

void init() {
    // Debug logger.
    dbg.init();
    // Watchdog and fault reporting
    lftask.init();
    // System configuration.
    config.init();

    actuator_task.init();
    sonar_task.init();
    usb_task.init();
    trfctl_task.init();

    lftask.start();
    actuator_task.start();
    sonar_task.start();
    usb_task.start();
    trfctl_task.start();
}

}  // namespace mmcc

void on_abort(void) {
    /* Disable actuators */
    mmcc::actuator_task.on_abort();
    /* Full reboot (except backup domain) */
    mmcc::reset_system();
    do {
        nop3;
    } while (true);
    /* Unreachable */
}

// class CH0 final : public tcode::Axis {
//    protected:
//    public:
//     CH0(char type, uint8_t index) : Axis(type, index, "Ch0") {}
//     ~CH0() = default;
//
//     virtual void set_state(uint16_t v) override {
//         float fv = tcode::to_float(v);
//         // actuator_task.set_actuation(false, fv, 0);
//         actuator_task.set_pwm((fv * 2.f) - 1.f);
//         // char buf[32];
//         // int written = std::snprintf(buf, sizeof(buf), "ch0::%c%u::%u::%0.4f\n", _type,
//         // _index, v, fv); cli_task.print(buf, written);
//     };
//     void set_limits(uint16_t, uint16_t) override {}
// };

// class CH1 final : public tcode::Axis {
//    protected:
//    public:
//     CH1(char type, uint8_t index) : Axis(type, index, "Ch1") {}
//     ~CH1() = default;
//
//     virtual void set_state(uint16_t v) override {
//         float fv = tcode::to_float(v);
//         actuator_task.set_aux_pwm(0, (fv * 2.f) - 1.f);
//     };
//     void set_limits(uint16_t, uint16_t) override {}
// };

// class CH2 final : public tcode::Axis {
//    protected:
//    public:
//     CH2(char type, uint8_t index) : Axis(type, index, "Ch2") {}
//     ~CH2() = default;
//
//     virtual void set_state(uint16_t v) override {
//         float fv = tcode::to_float(v);
//         actuator_task.set_aux_pwm(1, (fv * 2.f) - 1.f);
//     };
//     void set_limits(uint16_t, uint16_t) override {}
// };

// static CH0 ch0('V', 0);
// static CH1 ch1('V', 1);
// static CH2 ch2('V', 2);
