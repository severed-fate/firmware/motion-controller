#include <TCodeConfig.hpp>
#include <TCodeInstance.hpp>
#include <interface/tcode/Messages.hpp>
#include <interface/tcode/ParserDispatcher.hpp>
#include <interface/tcode/TCode.hpp>

#define ASSERT_FILE_ID (0xBFCA)

namespace tcode {

namespace callbacks {

response::Error AxisUpdate::operator()(fractional<uint32_t> value) const {
    if (_update_cb) {
        return _update_cb(value);
    }
    else {
        return response::Error(response::ErrorCode::InvalidOperation, "unsupported operation");
    }
}
response::Error AxisUpdate::operator()(fractional<uint32_t> value,
                                       request::IntervalData interval) const {
    if (_update_interval_cb) {
        return _update_interval_cb(value, interval);
    }
    else {
        return response::Error(response::ErrorCode::InvalidOperation, "unsupported operation");
    }
}
response::Error AxisUpdate::operator()(fractional<uint32_t> value,
                                       request::SpeedData speed) const {
    if (_update_speed_cb) {
        return _update_speed_cb(value, speed);
    }
    else {
        return response::Error(response::ErrorCode::InvalidOperation, "unsupported operation");
    }
}
response::Error AxisUpdate::stop() const {
    if (_stop_cb) {
        return _stop_cb();
    }
    else {
        return response::Error(response::ErrorCode::InvalidOperation, "unsupported operation");
    }
}

response::Error Command::operator()() const {
    if (_cb) {
        return _cb();
    }
    else {
        return response::Error(response::ErrorCode::InvalidOperation, "unsupported operation");
    }
}

response::Error Property::get() const {
    if (_getter_cb) {
        return _getter_cb(nullptr, 0);
    }
    else {
        return response::Error(response::ErrorCode::InvalidOperation, "unsupported operation");
    }
}
response::Error Property::event(const void* event_data, size_t n) const {
    if (_getter_cb && (_flags & FlagBits::Event)) {
        return _getter_cb(event_data, n);
    }
    else {
        return response::Error(response::ErrorCode::InvalidOperation, "unsupported operation");
    }
}
response::Error Property::set(void* dat, size_t n) const {
    if (_setter_cb) {
        return _setter_cb(dat, n);
    }
    else {
        return response::Error(response::ErrorCode::InvalidOperation, "unsupported operation");
    }
}
void Property::append_metadata(codec::UBJsonObject& obj) const {
    if (_append_metadata_cb) {
        _append_metadata_cb(obj);
    }
}

}  // namespace callbacks

/*** Static callbacks used by tokenizer/parser. ***/
response::Error ParserDispatcher::_on_request(request::AxisUpdateData axis_updt) {
    auto cb_obj_it = config::axis_registry.find(axis_updt.cmd);
    if (cb_obj_it != config::axis_registry.end()) {
        const callbacks::AxisUpdate& cb_obj = cb_obj_it->second;
        response::Error err = cb_obj(axis_updt.value);
        return err;
    }
    else {
        return response::Error(response::ErrorCode::InvalidCommandIndex,
                               "unsupported axis index");
    }
}
response::Error ParserDispatcher::_on_request(request::AxisUpdateData axis_updt,
                                              request::IntervalData interval) {
    auto cb_obj_it = config::axis_registry.find(axis_updt.cmd);
    if (cb_obj_it != config::axis_registry.end()) {
        const callbacks::AxisUpdate& cb_obj = cb_obj_it->second;
        response::Error err = cb_obj(axis_updt.value, interval);
        return err;
    }
    else {
        return response::Error(response::ErrorCode::InvalidCommandIndex,
                               "unsupported axis index");
    }
}
response::Error ParserDispatcher::_on_request(request::AxisUpdateData axis_updt,
                                              request::SpeedData speed) {
    auto cb_obj_it = config::axis_registry.find(axis_updt.cmd);
    if (cb_obj_it != config::axis_registry.end()) {
        const callbacks::AxisUpdate& cb_obj = cb_obj_it->second;
        response::Error err = cb_obj(axis_updt.value, speed);
        return err;
    }
    else {
        return response::Error(response::ErrorCode::InvalidCommandIndex,
                               "unsupported axis index");
    }
}
response::Error ParserDispatcher::_on_request(request::CommandIndex cmd_idx) {
    auto cb_obj_it = config::command_registry.find(cmd_idx);
    if (cb_obj_it != config::command_registry.end()) {
        const callbacks::Command& cb_obj = cb_obj_it->second;
        response::Error err = cb_obj();
        return err;
    }
    else {
        return response::Error(response::ErrorCode::InvalidCommandIndex,
                               "unsupported command index");
    }
}
response::Error ParserDispatcher::_on_request(request::CommandIndex cmd_idx,
                                              request::PropertyData prop) {
    auto cb_obj_it = config::property_registry.find(config::PropertyKey{cmd_idx, prop});
    if (cb_obj_it != config::property_registry.end()) {
        const callbacks::Property& cb_obj = cb_obj_it->second;
        response::Error err = cb_obj.get();
        return err;
    }
    else {
        return response::Error(response::ErrorCode::UnknownProperty, "unknown property");
    }
}
response::Error ParserDispatcher::_on_request(request::CommandIndex cmd_idx,
                                              request::PropertyData prop,
                                              request::Z85Data dat) {
    auto cb_obj_it = config::property_registry.find(config::PropertyKey{cmd_idx, prop});
    if (cb_obj_it != config::property_registry.end()) {
        const callbacks::Property& cb_obj = cb_obj_it->second;
        response::Error err = cb_obj.set(dat.data, dat.n);
        return err;
    }
    else {
        return response::Error(response::ErrorCode::UnknownProperty, "unknown property");
    }
}
response::Error ParserDispatcher::_on_request(request::CommandIndex cmd_idx,
                                              request::PropertyData prop,
                                              request::IntervalData interval) {
    auto registry_entry = config::property_registry.find(config::PropertyKey{cmd_idx, prop});
    if (registry_entry != config::property_registry.end()) {
        return TCODE_INSTANCE.register_timeout(*registry_entry.get_raw_pos(),
                                               interval.interval);
    }
    else {
        return response::Error(response::ErrorCode::UnknownProperty, "unknown property");
    }
}
response::Error ParserDispatcher::_on_request_stop(request::CommandIndex cmd_idx) {
    auto cb_obj_it = config::axis_registry.find(cmd_idx);
    if (cb_obj_it != config::axis_registry.end()) {
        const callbacks::AxisUpdate& cb_obj = cb_obj_it->second;
        response::Error err = cb_obj.stop();
        return err;
    }
    else {
        return response::Error(response::ErrorCode::InvalidCommandIndex,
                               "unsupported axis index");
    }
}
response::Error ParserDispatcher::_on_request_stop() {
    return callbacks::stop_all();
}

}  // namespace tcode
