#ifndef MMCC_USART_HPP
#define MMCC_USART_HPP

#include <cstdint>
#include <utility>

#include <libopencm3/stm32/usart.h>

namespace stm32 {

/**
 * Hardware level control of USART peripherals.
 */
template<uint32_t prh> class USART {
   public:
    enum StopBits {
        _0_5 = USART_STOPBITS_0_5,
        _1 = USART_STOPBITS_1,
        _1_5 = USART_STOPBITS_1_5,
        _2 = USART_STOPBITS_2,
    };

    enum Parity { None = USART_PARITY_NONE, Even = USART_PARITY_EVEN, Odd = USART_PARITY_ODD };

   public:
    USART() = delete;

    static void activate();
    static void deactivate();

    static void enable();
    static void disable();
    static bool enabled();

    static void set_baudrate(uint32_t baudrate);
    static void set_databits(uint8_t bits);
    static void set_stopbits(StopBits bits);
    static void set_parity(Parity mode);
    static void set_mode(bool tx, bool rx);
    static void set_flow_control(bool rts, bool cts);
    static void set_rx_dma(bool en);
    static void set_tx_dma(bool en);
    static void set_rx_interrupt(bool en);
    static void set_tx_interrupt(bool en);
    static void set_tx_complete_interrupt(bool en);
    static void set_idle_interrupt(bool en);
    static void set_error_interrupt(bool en);

    static uint32_t get_baudrate();
    static uint8_t get_databits();
    static StopBits get_stopbits();
    static Parity get_parity();
    static bool get_mode_tx();
    static bool get_mode_rx();
    static bool get_flow_control_rts();
    static bool get_flow_control_cts();
    static bool get_rx_dma();
    static bool get_tx_dma();
    static bool get_rx_interrupt();
    static bool get_tx_interrupt();
    static bool get_tx_complete_interrupt();
    static bool get_idle_interrupt();
    static bool get_error_interrupt();

    /**
     * Wait until the send buffer is empty.
     */
    static void wait_tx();
    static bool tx_ready();
    /**
     * Wait until some data has been received.
     */
    static void wait_rx();
    static bool rx_ready();

    static void tx(uint16_t data, bool blocking = true);
    static uint16_t rx(bool blocking = true);
};

}  // namespace stm32

#endif /*MMCC_USART_HPP*/