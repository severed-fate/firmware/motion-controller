#include "USB.hpp"
#include "libopencm3/usb/usbd.h"

#include <mmcc/board/Config.hpp>

#include <cstdlib>

#include <boot.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/usb/dwc/otg_common.h>

namespace stm32 {

void USB::EndpointDescriptor::setup() const {
    stm32::USB& usb = stm32::USB::get_instance();
    setup(usb);
}
void USB::EndpointDescriptor::setup(usbd_endpoint_callback cb) const {
    stm32::USB& usb = stm32::USB::get_instance();
    setup(usb, cb);
}
uint16_t USB::EndpointDescriptor::read(void* buf, uint16_t buf_len) const {
    stm32::USB& usb = stm32::USB::get_instance();
    return read(usb, buf, buf_len);
}
bool USB::EndpointDescriptor::write(const void* buf, uint16_t len) const {
    stm32::USB& usb = stm32::USB::get_instance();
    int status = usbd_ep_write_packet(usb._dev, bEndpointAddress, buf, len);
    return status != 0;
}
void USB::EndpointDescriptor::stall_set(bool stall) const {
    stm32::USB& usb = stm32::USB::get_instance();
    stall_set(usb, stall);
}
bool USB::EndpointDescriptor::stall_get() const {
    stm32::USB& usb = stm32::USB::get_instance();
    return stall_get(usb);
}
void USB::EndpointDescriptor::nak_set(bool nak) const {
    stm32::USB& usb = stm32::USB::get_instance();
    nak_set(usb, nak);
}

void USB::EndpointDescriptor::setup(stm32::USB& usb) const {
    usbd_ep_setup(usb._dev, bEndpointAddress, bmAttributes & 0b11, wMaxPacketSize, nullptr);
}
void USB::EndpointDescriptor::setup(stm32::USB& usb, usbd_endpoint_callback cb) const {
    usbd_ep_setup(usb._dev, bEndpointAddress, bmAttributes & 0b11, wMaxPacketSize, cb);
}
uint16_t USB::EndpointDescriptor::read(stm32::USB& usb, void* buf, uint16_t buf_len) const {
    return usbd_ep_read_packet(usb._dev, bEndpointAddress, buf, buf_len);
}
bool USB::EndpointDescriptor::write(stm32::USB& usb, const void* buf, uint16_t len) const {
    int status = usbd_ep_write_packet(usb._dev, bEndpointAddress, buf, len);
    return status != 0;
}
void USB::EndpointDescriptor::stall_set(stm32::USB& usb, bool stall) const {
    usbd_ep_stall_set(usb._dev, bEndpointAddress, stall);
}
bool USB::EndpointDescriptor::stall_get(stm32::USB& usb) const {
    return usbd_ep_stall_get(usb._dev, bEndpointAddress);
}
void USB::EndpointDescriptor::nak_set(stm32::USB& usb, bool nak) const {
    usbd_ep_nak_set(usb._dev, bEndpointAddress, nak);
}

#define REBASE(x) MMIO32((x) + (USB_OTG_FS_BASE))
bool USB::EndpointDescriptor::enabled() const {
    uint8_t addr = bEndpointAddress & 0x7F;
    bool dir = !!(bEndpointAddress & 0x80);

    if (dir) {
        return !!(REBASE(OTG_DIEPTSIZ(addr)) & OTG_DIEPSIZ0_PKTCNT);
    }
    else {
        return true;
    }
}

USB& USB::get_instance() {
    static USB instance;
    return instance;
}

void USB::init(mmcc::InterruptGuard<>& ctx, const DeviceDescriptor& dev,
               const ConfigDescriptor& conf, std::pair<const char* const*, uint16_t> strings,
               std::pair<uint8_t*, uint16_t> ctl_buf) {
    // Start clocking usb controller.
    rcc_periph_clock_enable(RCC_OTGFS);
    // Prepare interrupts.
    runtime_vector_table.irq[NVIC_OTG_FS_IRQ] = on_usb_interrupt;
    nvic_set_priority(NVIC_OTG_FS_IRQ, 0x90);
    /**
     * Prepare pins.
     * NOTE: If the pins are enabled early and the usb is connected
     * during boot, then the controller gets confused.
     */
    PINSET_CONFIGURE_CTX(mmcc::USB_PINS, ctx, stm32::PinSet<>::Direction::AlternateFunction);
    PINSET_CONFIGURE_PUD_CTX(mmcc::USB_PINS, ctx, false, false);
    PINSET_CONFIGURE_ALTERNATE_FUNCTION_INDEX_CTX(mmcc::USB_PINS, ctx, 10);
    // Initialize hidden state.
    _dev = usbd_init(&otgfs_usb_driver, &dev, &conf, strings.first, strings.second,
                     ctl_buf.first, ctl_buf.second);
    // Finalize.
    nvic_enable_irq(NVIC_OTG_FS_IRQ);
}

void USB::poll() {
    usbd_poll(_dev);
}

void USB::disconnect(bool dc) {
    usbd_disconnect(_dev, dc);
}

void USB::register_reset_callback(usb_event_callback cb) {
    usbd_register_reset_callback(_dev, cb);
}
void USB::register_suspend_callback(usb_event_callback cb) {
    usbd_register_suspend_callback(_dev, cb);
}
void USB::register_resume_callback(usb_event_callback cb) {
    usbd_register_resume_callback(_dev, cb);
}
void USB::register_sof_callback(usb_event_callback cb) {
    usbd_register_sof_callback(_dev, cb);
}

void USB::register_config_callback(usbd_set_config_callback cb) {
    usbd_register_set_config_callback(_dev, cb);
}

void USB::register_control_callback(usbd_control_callback cb, uint8_t type, uint8_t type_mask) {
    usbd_register_control_callback(_dev, type, type_mask, cb);
}

void USB::register_set_altsetting_callback(usbd_set_altsetting_callback cb) {
    usbd_register_set_altsetting_callback(_dev, cb);
}

void USB::on_usb_interrupt() {
    stm32::USB& usb = stm32::USB::get_instance();
    usb.poll();
}

}  // namespace stm32

/**
 * In the stm32 reference manual, when configured in device mode,
 * `OUT` endpoints refer to endpoints which receive data.
 */
