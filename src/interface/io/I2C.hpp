#ifndef MMCC_I2C_HPP
#define MMCC_I2C_HPP

#include <interface/rtos/RTOS.hpp>
#include <interface/rtos/RTOSMutex.hpp>
#include <utils/Class.hpp>

#include <cstddef>
#include <cstdint>
#include <utility>

#include <libopencm3/stm32/i2c.h>

namespace stm32 {

template<uint32_t prh> class I2C : public meta::INonCopyable {
   public:
    using Mode = i2c_speeds;

    class Transaction : public meta::INonCopyable {
       protected:
        std::reference_wrapper<I2C<prh>> _unit;
        bool _receiving;
        bool _valid;

       protected:
        Transaction(I2C<prh>& unit);

       public:
        Transaction(Transaction&& o) :
            _unit(o._unit), _receiving(o._receiving), _valid(std::exchange(o._valid, false)) {}
        Transaction& operator=(Transaction&& o) {
            _unit = o._unit;
            _receiving = o._receiving;
            _valid = std::exchange(o._valid, false);
            return *this;
        }
        ~Transaction();

        bool start();
        bool address(uint8_t addr, bool write, float timeout_ms = 16);
        size_t send(const uint8_t* data, const size_t n);
        size_t receive(uint8_t* data, const size_t n);
        bool stop();

        void end();

        friend class I2C;
    };

   protected:
    rtos::StaticMutex<true> _transaction_lock;

   protected:
    I2C();

   public:
    ~I2C();

    static I2C& get_instance();

    void set_speed(Mode speed, uint32_t apb_mhz);

    /**
     * Enable the interface and mark the start of a transaction/transimition.
     */
    Transaction start_transaction();
    /*
     * End a transaction/transimition and reset the interface.
     */
    void end_transaction(Transaction&);
};

}  // namespace stm32

#endif /*MMCC_I2C_HPP*/