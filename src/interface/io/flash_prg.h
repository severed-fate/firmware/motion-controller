#ifndef MMCC_FLASH_PRG
#define MMCC_FLASH_PRG
/**
 * @file
 * @brief Low-level flash programming functions.
 * Not recommended for use by code outside 'stm32::flash::*'.
 */

#include <stddef.h>
#include <stdint.h>
#include <utils/macros.h>

C_DECLS_START

typedef enum {
    FLASH_PRG_ERROR_SUCCESS = 0,
    /** Category for programming errors, such as invalid arguments. */
    FLASH_PRG_ERROR_RUNTIME,
    FLASH_PRG_ERROR_BUSY,
    /** Generic error, a partial write was performed. */
    FLASH_PRG_ERROR_PARTIAL,
    /** Operation failed because of write protection. */
    FLASH_PRG_ERROR_WPROT,
    FLASH_PRG_ERROR_ALIGNMENT,
    FLASH_PRG_ERROR_PARALLELISM,
    FLASH_PRG_ERROR_SEQUENCE,
    /** Voltage dropped below the threshold. */
    FLASH_PRG_ERROR_LOW_VOLTAGE,
} FlashPrgError;

static const char* FLASH_PRG_ERROR_NAME[] = {
     "Success",          "RuntimeError",        "BusyError",
     "PartialError",     "WireProtectionError", "AlignmentError",
     "ParallelismError", "SequenceError",       "LowVoltage",
};

typedef int sector_index_t;

FlashPrgError _flash_prg_erase(sector_index_t sector_index);
FlashPrgError _flash_prg_write_bytes(uint8_t* dst, const uint8_t* src, const size_t n);
FlashPrgError _flash_prg_write_words(uint32_t* dst, const uint32_t* src, const size_t n);

C_DECLS_END

#endif /*MMCC_FLASH_PRG*/
