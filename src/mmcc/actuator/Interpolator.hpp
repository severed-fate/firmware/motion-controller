#ifndef MMCC_INTERPOLATOR_HPP
#define MMCC_INTERPOLATOR_HPP

#include <cstddef>

namespace mmcc {

class AxisInterpolator {
   protected:
    float _current_state = 0;
    float _step_size = 0;
    size_t _remaining_ticks = 0;

   public:
    constexpr AxisInterpolator() noexcept = default;

    void update(float start_state, float end_state, size_t duration_ticks);
    void update(float target_state, size_t duration_ticks);
    void update(float target_state);

    float step();
};

class SpeedLimiter : protected AxisInterpolator {
   protected:
    AxisInterpolator _interp;
    /**
     * Speed limit is stored in normalized units per tick period.
     */
    float _speed_limit;
    float _last_target;

   public:
    constexpr SpeedLimiter(float speed_limit) noexcept : _speed_limit(speed_limit) {}

    using AxisInterpolator::update;

    void setup(float current_pos);
    float step();
};

}  // namespace mmcc

#endif /*MMCC_INTERPOLATOR_HPP*/